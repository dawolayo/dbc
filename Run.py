# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 18:48:55 2021

@author: adedapo.awolayo
"""
import os, sys, numpy as np
## or relative path ...
sys.path.append(os.path.abspath('.'))

from supcrt_utility import *

#%% specify the direct access thermodynamic database
dbaccess ='./speq20.dat'
dbacessdic, dbname = readAqspecdb(dbaccess)

#%% 

#%% CON file from SUPCRT if not EQ3/6 P-T grid
fname = 'fCON.con' #'500barExample.con'
P, T = importconfile(fname)
#T=25
#P=200
#Tvect=np.arange(25,205, 5)
#Pvect=200*np.ones([len(Tvect),1]).ravel()
T = np.array([  0.010,   25.0000 ,  60.0000,  100.0000, 150.0000,  200.0000,  250.0000,  300.0000])
# P = np.array([  1.0132,    1.0132,    1.0132,    1.0132, 4.7572,   15.5365,   39.7365,   85.8378])
P = P*1.25
#%% 

#%% source EQ3/6 or GWB thermodynamic database
sourcedb ='./thermo_clay.com.dat'
sourcedic, specielist, chargedic, MWdic, Mineraltype = readSourcedb(sourcedb)

# objective EQ3/6 or GWB thermodynamic database
objdb ='./thermo.new'
nCa = 1
write_db(T, P, dbaccess, sourcedb, objdb, nCa, solid_solution = True, 
         co2actmodel = 'Drummond')

filename = './logK.txt'
fid = open(filename, 'w')

# nCa = 1
# for XMg in np.round(np.linspace(1,0.0,11),2):
#     logK, Rxn = calclogKDiHedEnFe(nCa, XMg, T, P, dbacessdic )
#     outputtxt(fid, logK, Rxn)
#     print('XMg = {}'.format(XMg))
# print(logK)

# for XAn in np.round(np.linspace(1,0,11),1):
#     logK, Rxn = calclogKAnAb(XAn, T, P, dbacessdic )
#     outputtxt(fid, logK, Rxn)
#     print('XAn = {}'.format(XAn))
# print(logK)
logK, Rxn = calclogKDiHedEnFe(1, 0.85, T, P, dbacessdic)
outputtxt(fid, logK, Rxn)

logK, Rxn = calclogKDiHedEnFe(0.845, 0.649, T, P, dbacessdic)
outputtxt(fid, logK, Rxn)

# logK, Rxn = calclogKAnAb(0.4,Tvect,Pvect, dbacessdic )
# #print(logK)

# logK, Rxn = calclogKEnFe(0.5, 20, 250, dbacessdic)
# outputtxt(fid, logK, Rxn)
# # logK, Rxn = calclogKAnAb(0.75,20,250, dbacessdic )
logK, Rxn = calclogKFoFa(0.85, T, P, dbacessdic )
outputtxt(fid, logK, Rxn)

fid.close()
#fout.close()
