# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 16:02:22 2021

@author: adedapo.awolayo
"""

import re, os
import numpy as np
import functools
import time
from scipy.optimize import root_scalar, newton, fsolve, curve_fit #, fmin, root, fminbound
from scipy.linalg import lu_factor, lu_solve
import inspect
import math
import textwrap
# import matplotlib.pyplot as plt
# import warnings
# import time
# import pandas as pd
# from pandas import DataFrame
# from string import digits
# from collections import OrderedDict
# from random import random
np.random.seed(4321)
global IAPWS95_COEFFS, eps, MW, J_to_cal

eps = 2.220446049250313e-16
J_to_cal=4.184

def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = end_time - start_time    # 3
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer

def readIAPWS95data():
    """
    returns all constants and coefficients needed for the 
     IAPWS95 formulation, packed into a dictionary
    """    
    #fid = open('./iapws95.dat', 'r')
    fid = open(os.path.join(os.path.dirname(os.path.abspath(__file__)),'iapws95.dat'), 'r')
    df=fid.readlines()
    
    R       = float(df[1].split()[0])
    Tc      = float(df[2].split()[0])
    rhoc    = float(df[3].split()[0])
    Pc      = float(df[4].split()[0])
    n0      = []
    for k in range(2):
        n0  += [float(i) for i in df[7+k].rstrip('\n').split(',') if len(i)!=0]
    gamma0      = []
    for k in range(2):
        gamma0  += [float(i) for i in df[10+k].rstrip('\n').split(', ') if len(i)!=0]
    n       = []
    for k in range(19):
        n   += [float(i) for i in df[13+k].rstrip('\n').split(',') if len(i)!=0]
    c       = []
    for k in range(3):
           c   += [int(i) for i in df[33+k].rstrip('\n').split(',') if len(i)!=0]
    d       = []
    for k in range(4):
        if k == 0:
            d   += [int(i) for i in df[37+k].rstrip('\n').split(', ') if len(i)!=0]
        else:
            d   += [int(i) for i in df[37+k].rstrip('\n').split(',') if len(i)!=0]
    t       = []
    for k in range(4):
        t   += [float(i) for i in df[42+k].rstrip('\n').split(',') if len(i)!=0]
    alpha   = [int(i) for i in df[47].rstrip('\n').split(',') if len(i)!=0]
    beta    = [float(i) for i in df[49].rstrip('\n').split(',') if len(i)!=0]
    gamma   = [float(i) for i in df[51].rstrip('\n').split(',') if len(i)!=0]
    epsilon = [int(i) for i in df[53].rstrip('\n').split(',') if len(i)!=0]
    a       = [float(i) for i in df[55].rstrip('\n').split(',') if len(i)!=0]
    b       = [float(i) for i in df[57].rstrip('\n').split(',') if len(i)!=0]
    A       = [float(i) for i in df[59].rstrip('\n').split(',') if len(i)!=0]
    B       = [float(i) for i in df[61].rstrip('\n').split(',') if len(i)!=0]
    C       = [float(i) for i in df[63].rstrip('\n').split(',') if len(i)!=0]
    D       = [float(i) for i in df[65].rstrip('\n').split(',') if len(i)!=0]
             
    fid.close()
    var=[R,Tc,Pc,rhoc,n0,gamma0,n,c,d,t,alpha,beta,gamma,epsilon,a,b,A,B,C,D]
    varname=['R','Tc','Pc','rhoc','n0','gamma0','n','c','d','t','alpha','beta','gamma','epsilon','a','b','A','B','C','D']
    coeffs = {}
    for i in range(len(var)):
        coeffs['%s' % varname[i]] = var[i]
    
    return coeffs

# @timer
def readAqspecdb(dbaccess): 
    """
    This function reads direct access thermodynamic database and can add other database sources at the bottom
     returns all constants of dG [cal/mol], dH [cal/mol], S [cal/mol-K], V [cm3/mol]
     a [cal/mol-K], b [*10**3 cal/mol/K^2], c [*10^-5 cal/mol/K] ) etc.
     for minerals, gases and aqueous species packed into a dbacessdictionary
    parameters:
        dbaccess        filename of the direct-access database
    results:
        dbacessdic      dictionary of minerals, gases, redox and aqueous species
        dbaccess        dat file name
    """
    # fid = open(dbaccess, 'r')
    
    # check if its a single liner data
    with open(dbaccess) as g:
        Rd = g.readlines()
    
    dbacessdic = {}; counter = 0 
    # if it is single line data like for dpeq20
    if len(Rd) <= 1:    
        width = re.search('                     3 ', Rd[0]).start()
        Rd = textwrap.wrap(Rd[0], width=width)
        
        for i in range(len(Rd)): #
            s1 = Rd[i]
            if not s1.startswith('*', 0) | s1.startswith(' ',0) | (s1.rstrip('\n').lstrip('0123456789.- ') == "") | (s1[0] == "-") :
                if (s1.strip()[:3] != 'ref') and (s1.strip()[:3] != 'REF'):
                    name = s1.strip().split()[0]
                    if len(s1.strip().split()) > 1:
                        formula = s1.strip().split()[1]
                    else:
                        formula = ''
                    s2 = Rd[i + 1]
                    if (s2.strip()[:3] != 'ref') and (s2.strip()[:3] != 'REF') and (s2.split()[0].lstrip('0123456789.,- ') != ''):
                        s3 = Rd[i+2]; s4 = Rd[i+3]; s5 = Rd[i+4];
                        if s3.split()[0].lstrip('0123456789.,- ') != '':
                            if i >= len(Rd) - 5:
                                s6 = 'Null'; s7 = 'Null'; s8 = 'Null';
                                s9 = 'Null'; s10 = 'Null'; s11 = 'Null';
                            elif i >= len(Rd) - 6:
                                s6  = Rd[i + 5]; s7 = 'Null'; s8 = 'Null';
                                s9 = 'Null'; s10 = 'Null'; s11 = 'Null';
                            else:
                                s6  = Rd[i + 5]; s7 = Rd[i + 6]; s8 = Rd[i + 7];
                                s9 = Rd[i + 8]; s10 = Rd[i + 9]; s11 = Rd[i + 10];
                                
                            if s3.strip()[:3] == 'ref':
                                ref = s3.split()[0][4:]
                            else:
                                ref = s3.split()[0]
                            if (s6.lower().islower() == True) | (s6.strip().split()[0] == name):
                                params = s4.split() + s5.split()
                            elif (s7.lower().islower() == True) | (s7.strip().split()[0] == name):
                                params = s4.split() + s5.split() + s6.split()
                            elif (s8.lower().islower() == True) | (s8.strip().split()[0] == name):
                                params = s4.split() + s5.split() + s6.split() + s7.split()
                            elif (s9.lower().islower() == True) | (s9.strip().split()[0] == name):
                                params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split()
                            elif (s10.lower().islower() == True) | (s10.strip().split()[0] == name):
                                params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split() + \
                                    s9.split()
                            elif (s11.lower().islower() == True) | (s11.strip().split()[0] == name):
                                params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split() + \
                                    s9.split() + s10.split()
                            else:
                                params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split() + \
                                    s9.split() + s10.split() + s11.split()
                            params = [float(i) if float(i) != 999999 else 0 for i in params]
                            counter += 1
                            dbacessdic[name] = [formula, ref] + params
    else:
    # else for multi line data like for speq20
        for i in range(len(Rd)): #
            s1 = Rd[i].rstrip('\n').strip()
            if  (not s1.lstrip().startswith('*', 0)) and (s1.lstrip('0123456789.- \t') != ""):
                if (s1[:3] != 'ref') and (s1[:3] != 'REF') and (s1.split()[0] not in ['minerals', 'gases', 'aqueous']):
                    name = s1.strip().split()[0]
                    if len(s1.split()) > 1:
                        formula = s1.split()[1]
                    else:
                        formula = ''
                    s2 = Rd[i + 1].strip()
                    if (s2[:3] != 'ref') and (s2[:3] != 'REF') and (s2[0].lstrip('0123456789.,- ') != ''):
                        s3 = Rd[i+2]; s4 = Rd[i+3]; s5 = Rd[i+4];
                        if i >= len(Rd) - 5:
                            s6 = 'Null'; s7 = 'Null'; s8 = 'Null';
                            s9 = 'Null'; s10 = 'Null'; s11 = 'Null';
                        elif i >= len(Rd) - 6:
                            s6  = Rd[i + 5]; s7 = 'Null'; s8 = 'Null';
                            s9 = 'Null'; s10 = 'Null'; s11 = 'Null';
                        else:
                            s6  = Rd[i + 5]; s7 = Rd[i + 6]; s8 = Rd[i + 7];
                            s9 = Rd[i + 8]; s10 = Rd[i + 9]; s11 = Rd[i + 10];
                            
                        if s3.strip()[:3] == 'ref':
                            ref = s3.split()[0][4:]
                        else:
                            ref = s3.split()[0]
                        if (s6.lower().islower() == True) | s6.startswith('*', 0):
                            params = s4.split() + s5.split()
                        elif (s7.lower().islower() == True) | s7.startswith('*', 0):
                            params = s4.split() + s5.split() + s6.split()
                        elif (s8.lower().islower() == True) | s8.startswith('*', 0):
                            params = s4.split() + s5.split() + s6.split() + s7.split()
                        elif (s9.lower().islower() == True) | s9.startswith('*', 0):
                            params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split()
                        elif (s10.lower().islower() == True) | s10.startswith('*', 0):
                            params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split() + \
                                s9.split()
                        elif (s11.lower().islower() == True) | s11.startswith('*', 0):
                            params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split() + \
                                s9.split() + s10.split()
                        else:
                            params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split() + \
                                s9.split() + s10.split() + s11.split()
                        params = [float(i) if float(i) != 999999 else 0 for i in params]
                        counter += 1
                        dbacessdic[name] = [formula, ref] + params
                        if s5.strip() == '' and s6.strip() == '' :
                            break
                        elif s6.strip() == '' and s7.strip() == '' :
                            break
                        elif s7.strip() == '' and s8.strip() == '' :
                            break
                        elif s8.strip() == '' and s9.strip() == '' :
                            break
                        elif s9.strip() == '' and s10.strip() == '' :
                            break
        
    #%% other sources aside speq20 for solid solution calculation
    #dG dH S V a1 a2 a3 a4 a5
    # dG, dH, S from Arnorsson 1999, V and Cp from Robie and Hemingway #1995
    _Anorthite_ = ['Ca(Al2Si2)O8', 'R&H95, Stef2001',-4002095, -4227830, 199.30, 100.790*J_to_cal, 
                   5.168e2, -9.249e-2, -1.408e6, -4.589e3, 4.188e-5]
    dbacessdic['ss_Anorthite'] = [x/J_to_cal if type(x)!=str else x for x in _Anorthite_ ]

    dbacessdic['ss_Albite_high'] = ['NaAlSi3O8', 'R&H95, Stef2001', -887368.32+8413/J_to_cal, 
                                    -940769.52, 224.14/J_to_cal, 100.07, 139.56, -0.0221916826, 
                                    401051.6, -1535.37, 5.430210e-06]

    _Ferrosilite_ = ['FeSiO3', 'R&H95, Stef2001', -1118000, -1195200, 94.6, 33.0*J_to_cal, 
                   1.243e2, 1.454e-2, -3.378e6, 0, 0]
    dbacessdic['ss_Ferrosilite'] = [x/J_to_cal if type(x)!=str else x for x in _Ferrosilite_]

    _Enstatite_=['MgSiO3', 'R&H95, Stef2001', -1458300, -1545600,  66.3, 31.31*J_to_cal, 
                 3.507e2, -1.472e-1, 1.769e6, -4.296e3, 5.826e-5]
    dbacessdic['ss_Enstatite'] = [x/J_to_cal if type(x)!=str else x for x in _Enstatite_]

    _Clinoenstatite_=['MgSiO3', 'R&H95, Stef2001', -1458100, -1545000,  67.9, 31.28*J_to_cal, 
                 2.056e2, -1.280e-2, 1.193e6, -2.298e3, 0]
    dbacessdic['ss_Clinoenstatite'] = [x/J_to_cal if type(x)!=str else x for x in _Clinoenstatite_]

    _Hedenbergite_=['CaFeSi2O6', 'R&H95, Stef2001', -2676300, -2839900,  174.2, 67.950*J_to_cal, 
                 3.1046e2, 1.257e-2, -1.846e6, -2.040e3, 0]           
    dbacessdic['ss_Hedenbergite'] = [x/J_to_cal if type(x)!=str else x for x in _Hedenbergite_]

    _Diopside_=['CaMgSi2O6', 'R&H95, Stef2001', -3026800, -3201500,  142.7 , 66.090*J_to_cal, 
                 4.7025e2, -9.864e-2, 0.2454e6, -4.823e3, 2.813e-5]                
    dbacessdic['ss_Diopside'] = [x/J_to_cal if type(x)!=str else x for x in _Diopside_]

    #dG dH S V a1 a2 a3 a4 a5
    _Forsterite_ = ['Mg2SiO4', 'R&H95, Stef2001', -2053600, -2171850, 94.1, 43.65*J_to_cal, 
                  8.736e1, 8.717e-2, -3.699e6, 8.436e2, -2.237e-5]
    dbacessdic['ss_Forsterite'] = [x/J_to_cal if type(x)!=str else x for x in _Forsterite_]

    _Fayalite_ = ['Fe2SiO4', 'R&H95, Stef2001', -1379100, -1478200, 151, 46.31*J_to_cal, 1.7602e2, 
                -8.808e-3, -3.889e6, 0, 2.471e-5]
    dbacessdic['ss_Fayalite'] = [x/J_to_cal if type(x)!=str else x for x in _Fayalite_]

    _Fluorapatite_ = ['Ca5(PO4)3F', 'R&H95', -6489700, -6872000, 387.9, 157.56*J_to_cal, 
                  7.543e2, -3.026e-2, -0.9084e6, -6.201e3, 0]
    dbacessdic['Fluorapatite'] = [x/J_to_cal if type(x)!=str else x for x in _Fluorapatite_]
    
    _Hydroxyapatite_ = ['Ca5(OH)(PO4)3', 'R&H95', -6337100, -6738500, 390.4, 159.6*J_to_cal, 
                        3.878e2, 11.186e-2, -12.70e6, 1.811e3, 0]
    dbacessdic['Hydroxyapatite'] = [x/J_to_cal if type(x)!=str else x for x in _Hydroxyapatite_]

    #pos = re.search('/([^<>]*)\s*$', dbaccess).start()   #re.findall('/(.*?)\s*$', dbaccess)

    #%% other sources aside speq20 for clay minerals calculation
    #dG dH S V a1 a2 a3 a4 a5
    data_clay= np.array([['Montmorillonite_Lc_MgK', -1274534.0481955067,
                            -1363171.6061185468, 65.25812619502868, 134.69365335689045,
                            83.2950834070901, 39.40000472944213, -18.346808893378107],
                           ['Montmorillonite_Lc_MgNa', -1272072.1433150095,
                            -1360040.6309751433, 66.41491395793498, 133.96435335689046,
                            79.01704803424114, 54.47856591491058, -18.695828969859946],
                           ['Montmorillonite_Lc_MgCa', -1272139.0715786328,
                            -1360011.9502868068, 64.25669216061186, 135.57935335689046,
                            79.8544524319467, 51.208585035369474, -19.576302201217498],
                           ['Montmorillonite_Lc_MgMg', -1268881.8715690728,
                            -1356598.948374761, 64.41682600382408, 130.8199,
                            82.17496016571063, 36.25846080305927, -18.026011790949646],
                           ['Montmorillonite_Hc_K', -1287876.3541347992, -1376132.8871892926,
                            70.82695984703632, 138.75366666666667, 85.92751929750014,
                            40.32366687911621, -19.096708802492735],
                           ['Montmorillonite_Hc_Na', -1283533.5599665393, -1370609.464627151,
                            72.868068833652, 137.46666666666667, 78.37804511011966,
                            66.93289250053112, -19.71262658451951],
                           ['Montmorillonite_Hc_Ca', -1283653.180293977, -1370559.273422562,
                            69.06309751434034, 140.31666666666666, 79.85581757665885,
                            61.16233800722328, -21.26640287515048],
                           ['Montmorillonite_Hc_Mg', -1277904.3000119503,
                            -1364536.3288718928, 69.34273422562141, 131.921,
                            84.03433715742511, 34.66204588910134, -18.58787444231995],
                           ['Saponite_K', -1346295.9714698854, -1436517.6864244742,
                            70.25812619502867, 141.6886, 95.07148661567878,
                            30.711711281070748, -21.581596558317408],
                           ['Saponite_Na', -1343946.3992853728, -1433499.0439770555,
                            71.41491395793498, 140.95929999999998, 90.79345124282983,
                            45.79027246653919, -21.93061663479924],
                           ['Saponite_Ca', -1344205.2447335082, -1433661.567877629,
                            69.25908221797322, 142.5743, 91.63085564053537,
                            42.52029158699809, -22.811089866156784],
                           ['Saponite_Mg', -1340990.3531608507, -1430291.5869980878,
                            69.41682600382408, 137.8313, 94.35922562141492,
                            26.995195984703635, -21.54055927342256],
                           ['Saponite_FeK', -1262900.4494813576, -1349314.053537285,
                            81.74952198852773, 144.2686, 95.8482552581262, 32.91932759719566,
                            -20.645490758444872],
                           ['Saponite_FeNa', -1260786.780380019, -1346532.0267686425,
                            82.90391969407266, 143.5393, 91.57021988527725,
                            47.99788878266412, -20.994510834926714],
                           ['Saponite_FeCa', -1260806.6200920169, -1346455.5449330783,
                            80.7480879541109, 145.15429999999998, 92.4076242829828,
                            44.72790790312301, -21.874984066284256],
                           ['Saponite_FeMg', -1257594.8311723229, -1343087.9541108985,
                            80.90822179732312, 140.59577949526812, 95.23199615781317,
                            29.112492761971396, -20.653244823301634],
                           ['Nontronite_K', -1108622.5436245222, -1193659.1778202676,
                            77.3565965583174, 132.8512, 72.61948693435309, 66.82311982154239,
                            -11.257847355003193],
                           ['Nontronite_Na', -1106272.2588444073, -1190640.5353728489,
                            78.51099426386233, 132.12189999999998, 68.34145156150414,
                            81.90168100701084, -11.606867431485028],
                           ['Nontronite_Ca', -1106531.104292543, -1190803.0592734225,
                            76.35516252390057, 133.7369, 69.1788559592097, 78.63170012746973,
                            -12.487340662842582],
                           ['Nontronite_Mg', -1103316.9253154877, -1187433.0783938814,
                            76.51529636711281, 129.4132476340694, 71.0803877321447,
                            58.00452961107659, -9.04589279574887],
                           ['Beidellite_K', -1285032.566833174, -1374249.5219885276,
                            63.730879541108976, 133.21859999999998, 80.76858667941364,
                            42.991826003824094, -17.259576163161256],
                           ['Beidellite_Na', -1282682.9946486615, -1371230.8795411089,
                            64.8876673040153, 132.4893, 76.49055130656471, 58.07038718929254,
                            -17.608596239643095],
                           ['Beidellite_Ca', -1282941.8400967973, -1371393.4034416825,
                            62.73183556405354, 134.1043, 77.32795570427025,
                            54.80040630975143, -18.489069471000647],
                           ['Beidellite_Mg', -1279726.9485241396, -1368023.4225621414,
                            62.889579349904395, 129.34360283911673, 79.6176307821293,
                            39.893747474229606, -16.917630405148653],
                           ['Illite_Mg', -1316688.7059094168, -1405685.946462715,
                            73.20267686424474, 140.06155434782607, 84.17529512012635,
                            47.30345415246487, -18.019232168093776],
                           ['Illite_FeII', -1297011.3190786329, -1385346.5583173996,
                            75.1027724665392, 140.66916304347825, 84.35002909635048,
                            47.87366468534375, -17.77531642281154],
                           ['Illite_FeIII', -1296190.3295052582, -1385131.4531548757,
                            73.6376673040153, 138.92149999999998, 81.08634082217972,
                            53.52800350541746, -16.31357552581262],
                           ['Illite_Al', -1323461.9677402007, -1413396.2715105163,
                            70.36567877629064, 138.9765, 82.30626593371574,
                            49.96044455066921, -17.21203792224347],
                           ['Vermiculite_K', -1384534.0908628106, -1475480.4015296367,
                            77.04588910133843, 147.5594, 96.63931644359465,
                            37.81696940726577, -21.533126195028682],
                           ['Vermiculite_Na', -1379020.2544431167, -1468274.378585086,
                            79.97131931166348, 145.7147, 85.8184034416826, 75.95685946462714,
                            -22.41594168260038],
                           ['Vermiculite_Ca', -1380410.7834930688, -1469421.6061185468,
                            74.5172084130019, 149.7997, 87.93654397705545, 67.68573135755257,
                            -24.64302103250478],
                           ['Vermiculite_Mg', -1372450.5722454588, -1461068.3556405352,
                            74.91873804971318, 137.80270000000002, 94.83771510516254,
                            28.41637189292543, -21.42932600382409],
                           ['Berthierine_FeIII', -753610.8487296845, -826489.0057361376,
                            68.82648183556405, 103.26511081081081, 68.6930003617384,
                            69.62887846020017, -17.256270347785645],
                           ['Berthierine_FeII', -825519.2596558317, -901161.5678776291,
                            60.48518164435946, 103.86357142857143, 68.33225211690795,
                            48.46387599016662, -13.354786943458068],
                           ['Glauconite', -1147252.7444311662, -1231149.617590822,
                            87.61472275334607, 139.75227272727273, 80.2245295932557,
                            69.0576927255345, -16.417141056839906],
                           ['GlauconiteQZ', -1248112.3056976816, -1334059.3795411089,
                            78.26773422562141, 137.73075628051993, 80.21039732636001,
                            52.815893139624364, -15.99589269125246]])

    for i in range(len(data_clay)):
        dbacessdic[data_clay[i, 0]] = ['nan', 'B2015'] + \
            [float(x)*J_to_cal/J_to_cal if (j == 3) else float(x) for j, x in enumerate(data_clay[i, 1:])]    
    
    return dbacessdic, dbaccess.split('/')[-1]

def Molecularweight():
    """
    This function stores the Molecular weight of all elements
    """
    MW = {'O': 15.9994,
         'Ag': 107.8682,
         'Al': 26.98154,
         'Am': 243.0,
         'Ar': 39.948,
         'Au': 196.96654,
         'B': 10.811,
         'Ba': 137.327,
         'Be': 9.01218,
         'Br': 79.904,
         'Ca': 40.078,
         'Cd': 112.411,
         'Ce': 140.115,
         'Cl': 35.4527,
         'Co': 58.9332,
         'Cr': 51.9961,
         'Cs': 132.90543,
         'Cu': 63.546,
         'Dy': 162.5,
         'Er': 167.26,
         'Eu': 151.965,
         'F': 18.9984,
         'Fe': 55.847,
         'Ga': 69.723,
         'Gd': 157.25,
         'H': 1.00794,
         'As': 74.92159,
         'C': 12.011,
         'P': 30.97376,
         'He': 4.0026,
         'Hg': 200.59,
         'Ho': 164.93032,
         'I': 126.90447,
         'In': 114.82,
         'K': 39.0983,
         'Kr': 83.8,
         'La': 138.9055,
         'Li': 6.941,
         'Lu': 174.967,
         'Mg': 24.305,
         'Mn': 54.93805,
         'Mo': 95.94,
         'N': 14.00674,
         'Na': 22.98977,
         'Nd': 144.24,
         'Ne': 20.1797,
         'Ni': 58.69,
         'Np': 237.048,
         'Pb': 207.2,
         'Pd': 106.42,
         'Pr': 140.90765,
         'Pu': 244.0,
         'Ra': 226.025,
         'Rb': 85.4678,
         'Re': 186.207,
         'Rn': 222.0,
         'Ru': 101.07,
         'S': 32.066,
         'Sb': 121.75,
         'Sc': 44.95591,
         'Se': 78.96,
         'Si': 28.0855,
         'Sm': 150.36,
         'Sn': 118.71,
         'Sr': 87.62,
         'Tb': 158.92534,
         'Tc': 98.0,
         'Th': 232.0381,
         'Ti': 47.88,
         'Tl': 204.3833,
         'Tm': 168.93421,
         'U': 238.0289,
         'V': 50.9415,
         'W': 183.85,
         'Xe': 131.29,
         'Y': 88.90585,
         'Yb': 173.04,
         'Zn': 65.39}
    
    return MW

# @timer
def readSourcedb(sourcedb):
    """
    This function reads source thermodynamic database and 
    reaction coefficients of 'eh' and 'e-' has been added at the bottom
     returns all reaction coefficients and species, group species into 
     redox, minerals, gases, oxides and aqueous species
    parameters:
        sourcedb        filename of the source database
    results:
        sourcedic      dictionary of reaction coefficients and species
        specielist      list of species segmented into the different categories
                            [element, basis, redox, aqueous, minerals, gases, oxides]
        chargedic       dictionary of charges of species
        MWdic           dictionary of MW of species
        Mineraltype     mineral type for minerals
    """
    f = open(sourcedb, 'r')
            
    unwanted = ['elements', 'basis species', 'redox couples', 'aqueous species', 
                'minerals', 'gases', 'oxides', '-end-', 'stop.']
    #capture line numbers with line break     
    d=[]
    with open(sourcedb) as fid:
        for idx, line in enumerate(fid, 1):
            if line.strip().rstrip('\n').lstrip('0123456789.- ') in unwanted:
                x=idx
                d.append(x-1)
    
    #skip first 11 lines of database  .lstrip('0123456789.- ')  
    for i in range(d[1]+2):
      s1 = f.readline()

    sourcedic = {} # initialize dictionary
    for i in range(d[7]-d[1]):   #
        s1 = f.readline()
        if not s1.startswith('*',0) | s1.startswith(' ',0) | (s1.rstrip('\n') == "") | (s1[0] == "-") :
            if (s1.rstrip('\n') == "references"):
                break
            else:
                specie_name = s1.strip().split()[0]
                s2 = f.readline(); s3 = f.readline()
                s4 = f.readline(); s5 = f.readline(); 
                if (s5.rstrip('\n') != ""):
                    s6 = f.readline()
                    if (s6.rstrip('\n') != ""):
                        s7 = f.readline()
                        if not s2.startswith('*',0):
                            if (s2.split()[0] != 'formula='):
                                specie_formula = ""
                                species_num = int(s3.split()[0])
                                if species_num <= 3:
                                    reactant = s4.split()
                                elif species_num <= 6:
                                    reactant = s4.split() + s5.split()
                                else:
                                    reactant = s4.split() + s5.split() + s6.split()
                            else:
                                if len(s2.split('formula=')) < 1:
                                    specie_formula = ""
                                else:
                                    specie_formula = s2.split('formula=')[1]
                                species_num = int(s4.split()[0])
                                if species_num <= 3:
                                    reactant = s5.split()
                                elif species_num <= 6:
                                    reactant = s5.split() + s6.split()
                                else:
                                    reactant = s5.split() + s6.split() + s7.split()
                        else:
                            specie_formula = s2.split()[2]
                            species_num = int(s4.split()[0])          
                            if species_num <= 3:
                                reactant = s5.split()
                            elif species_num <= 6:
                                reactant = s5.split() + s6.split()
                            else:
                                reactant = s5.split() + s6.split() + s7.split()
                    else:
                        specie_formula = ""
                        species_num = int(s3.split()[0])
                        if species_num <= 3:
                            reactant = s4.split()
                        elif species_num <= 6:
                            reactant = s4.split() + s5.split()
                        else:
                            reactant = s4.split() + s5.split() + s6.split()
                else:
                    specie_formula = ""
                    species_num = int(s3.split()[0])
                    if species_num <= 3:
                        reactant = s4.split()
                    elif species_num <= 6:
                        reactant = s4.split() + s5.split()
                    else:
                        reactant = s4.split() + s5.split() + s6.split()
                    
        sourcedic[specie_name] = [specie_formula, species_num] + reactant
    sourcedic['eh'] = ['eh', 3, '-2.0000', 'H2O', '1.0000', 'O2(g)', '4.0000', 'H+']
    sourcedic['e-'] = ['e-', 3, '0.50000', 'H2O', '-0.2500', 'O2(g)', '-1.0000', 'H+']
        
    element = []; basis = []; redox = []; aqueous = []; minerals = []; gases = []; oxides = [];
    charge = []; MW = []; Mineraltype = {}
    with open(sourcedb) as fid:
        for i, line in enumerate(fid, 1):
            if (line.rstrip('\n') == "references"):
                break
            if not line.startswith('*',0) | line.startswith(' ',0) | (line.rstrip('\n') == "") | (line[0] == "-") :
                if not line.split()[0].replace('.','',1).isnumeric():
                    if d[0] < i < d[1]:
                        element.append(line.split()[0])
                    elif d[1] < i < d[2]:
                        basis.append(line.split()[0])
                    elif d[2] < i < d[3]:
                        redox.append(line.split()[0])
                    elif d[3] < i < d[4]:
                        aqueous.append(line.split()[0])
                    elif d[4] < i < d[5]:
                        minerals.append(line.split()[0])
                    elif d[5] < i < d[6]:
                        gases.append(line.split()[0])
                    elif i > d[6]:
                        oxides.append(line.split()[0])
            if re.compile(r"charge").search(line) != None:
                charge.append(line)
            if re.compile(r"mole wt.=").search(line) != None:
                MW.append(re.sub('[^0123456789\.]', '', line.strip('\n').split('wt.=')[1]))
            if re.compile(r"type=").search(line) != None:
                if len(line.split()) <= 2:
                    Mineraltype[line.split()[0]] = ''
                else:
                    Mineraltype[line.split()[0]] = line.split()[2]

    res = basis + redox + aqueous
    chargedic = {res[i]: charge[i].rstrip('\n') for i in range(len(charge))}
    res = element + basis + redox + aqueous + minerals + gases + oxides
    MWdic = {res[i]: float(MW[i]) for i in range(len(MW))}
    specielist = [element, basis, redox, aqueous, minerals, gases, oxides]
    fid.close()  
    
    return sourcedic, specielist, chargedic, MWdic, Mineraltype

# @timer
def importconfile(filename, *Rows):
    """
    This function imports numeric data from a text file as column vectors.
       [Var1, Var2] = importconfile(filename) Reads data from text file
       filename for the default selection.
    
       [Var1, Var2] = importconfile(filename, StartRow, EndRow) Reads data
       from rows StartRow through EndRow of text file filename.
    
     Example:
       [Var1, Var2] = importconfile('100bar.con', 6, 13);
    """        
    
    # %% Initialize variables.
    if (len(Rows) == 0):
        startRow = 5
        endRow = np.inf
    else:
        startRow, endRow = Rows
            
    # %% Open the text file.
    fileID = open(filename,'r');
    
    df=fileID.readlines()
    Var1 = []
    Var2 = []
    for block in range(startRow,len(df)):
        if not df[block].startswith('\n'):
            dataArray = df[block].split()
            Var1.append(float(dataArray[0]))
            Var2.append(float(dataArray[1]))
       
    Var1 = np.asarray(Var1)
    Var2 = np.asarray(Var2)
    # %% Close the text file.
    fileID.close()  
    # %%
    
    return Var1, Var2


def var_name(var):
    callers_local_vars = inspect.currentframe().f_back.f_locals.items()
    print(str([k for k, v in callers_local_vars if v is var][0])+': '+str(var))

def feval(funcName, *args):  
    return eval(funcName)(*args)

def read_specific_lines(file, lines_to_read):
   lines = set(lines_to_read)
   last = max(lines)
   for n, line in enumerate(file):
      if n + 1 in lines:
          yield line
      if n + 1 > last:
          return
      if not line:
          continue

# %-------------------------------------------------------- 

IAPWS95_COEFFS = readIAPWS95data()
MW = Molecularweight()

# To check for naming convention of the species
# print([i for i in list(dic.keys()) if i.startswith('ZnC')==True])

def celsiusToKelvin(T):
    """
    converts temperatures from Celsius to Kelvin
    parameters:
        T        temperature in °C
    results:
        T        temperature in K
    """
    return T + 273.15

#%%---------------------------------------------------------
# specific dimensionless Helmholtz free energy (phi) and its derivatives
## IAPWS95.residual
def phir(delta, tau):
    """
     residual part of free energy, dimensionless
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK           dimensionless inverse temperature
    """
    
    # % unpack coefficients
    n = np.array(IAPWS95_COEFFS['n']);          c = np.array(IAPWS95_COEFFS['c'])
    d = np.array(IAPWS95_COEFFS['d']);          t = np.array(IAPWS95_COEFFS['t'])
    alpha = np.array(IAPWS95_COEFFS['alpha']);  beta = np.array(IAPWS95_COEFFS['beta'])
    gamma = np.array(IAPWS95_COEFFS['gamma']);  epsilon = np.array(IAPWS95_COEFFS['epsilon'])
    # a = np.array(IAPWS95_COEFFS['a']);          
    b = np.array(IAPWS95_COEFFS['b'])
          
    y = np.dot(n[:7], (delta**d[:7] * tau**t[:7]))
    
    y = y + np.dot(n[7:51], (delta**d[7:51] * tau**t[7:51] * np.exp(-delta**c[7:51])))
    
    y = y + np.dot(n[51:54], ( delta**d[51:54] * tau**t[51:54] * np.exp(-alpha*(delta - epsilon)**2 \
                                                                        - beta[:3]*(tau - gamma)**2) ))
    
    y = y + np.dot(n[54:56], (Delta(delta,tau)**b * delta * Psi(delta,tau)))
    
    return y

def phir_t(delta, tau):
    """
     partial derivative for tau of phir
     where phir = residual part of free energy, dimensionless
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK           dimensionless inverse temperature
    """
    
    # % unpack coefficients
    n = np.array(IAPWS95_COEFFS['n']);          c = np.array(IAPWS95_COEFFS['c'])
    d = np.array(IAPWS95_COEFFS['d']);          t = np.array(IAPWS95_COEFFS['t'])
    alpha = np.array(IAPWS95_COEFFS['alpha']);  beta = np.array(IAPWS95_COEFFS['beta'])
    gamma = np.array(IAPWS95_COEFFS['gamma']);  epsilon = np.array(IAPWS95_COEFFS['epsilon'])
    # a = np.array(IAPWS95_COEFFS['a']);          
    b = np.array(IAPWS95_COEFFS['b'])
          
    y = np.dot(n[:7], (t[:7]*delta**d[:7] * tau**(t[:7]-1)))
    
    y = y + np.dot(n[7:51], (t[7:51]*delta**d[7:51] * tau**(t[7:51]-1) * np.exp(-delta**c[7:51])))
    
    y = y + np.dot(n[51:54], ( delta**d[51:54] * tau**t[51:54] * \
                              np.exp(-alpha*(delta - epsilon)**2 - beta[:3]*(tau - gamma)**2)* \
                              (t[51:54]/tau - 2*beta[:3]*(tau - gamma)) ))
    
    y = y + np.dot(n[54:56], (delta * (Delta_b_t(delta,tau) * Psi(delta,tau) + Delta(delta,tau)**b * \
                                       Psi_t(delta,tau))))
    
    return y

def phir_tt(delta, tau):
    """
     second partial derivative for tau of phir
     where phir = residual part of free energy, dimensionless
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK           dimensionless inverse temperature
    """
    
    # % unpack coefficients
    n = np.array(IAPWS95_COEFFS['n']);          c = np.array(IAPWS95_COEFFS['c'])
    d = np.array(IAPWS95_COEFFS['d']);          t = np.array(IAPWS95_COEFFS['t'])
    alpha = np.array(IAPWS95_COEFFS['alpha']);  beta = np.array(IAPWS95_COEFFS['beta'])
    gamma = np.array(IAPWS95_COEFFS['gamma']);  epsilon = np.array(IAPWS95_COEFFS['epsilon'])
    # a = np.array(IAPWS95_COEFFS['a']);          
    b = np.array(IAPWS95_COEFFS['b'])
          
    y = np.dot(n[:7], (t[:7]*(t[:7]-1)*delta**d[:7] * tau**(t[:7]-2)))
    
    y = y + np.dot(n[7:51], (t[7:51]*(t[7:51]-1)*delta**d[7:51] * tau**(t[7:51]-2) * np.exp(-delta**c[7:51])))
    
    y = y + np.dot(n[51:54], ( delta**d[51:54] * tau**t[51:54] * \
                              np.exp(-alpha*(delta - epsilon)**2 - beta[:3]*(tau - gamma)**2)* \
                              ( (t[51:54]/tau - 2*beta[:3]*(tau - gamma))**2 - t[51:54]/tau**2 - 2*beta[:3] )))
    
    y = y + np.dot(n[54:56], (delta * (Delta_b_tt(delta,tau) * Psi(delta,tau) +\
                                       2*Delta_b_t(delta,tau) * Psi_t(delta,tau) +\
                                       Delta(delta,tau)**b * Psi_tt(delta,tau) )))

    return y

def phir_d(delta, tau):
    """
     partial derivative for delta of phir
     where phir = residual part of free energy, dimensionless
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    n = np.array(IAPWS95_COEFFS['n']);          c = np.array(IAPWS95_COEFFS['c'])
    d = np.array(IAPWS95_COEFFS['d']);          t = np.array(IAPWS95_COEFFS['t'])
    alpha = np.array(IAPWS95_COEFFS['alpha']);  beta = np.array(IAPWS95_COEFFS['beta'])
    gamma = np.array(IAPWS95_COEFFS['gamma']);  epsilon = np.array(IAPWS95_COEFFS['epsilon'])
    b = np.array(IAPWS95_COEFFS['b'])
          
    y = np.dot(n[:7], (d[:7]*delta**(d[:7]-1) * tau**t[:7]))
    
    y = y + np.dot(n[7:51], ( np.exp(-delta**c[7:51]) * (delta**(d[7:51]-1) * tau**t[7:51] \
                                                   * (d[7:51] - c[7:51]*delta**c[7:51]))))
    
    y = y + np.dot(n[51:54], (delta**d[51:54] * tau**t[51:54] \
                              * np.exp(-alpha*(delta - epsilon)**2 - beta[:3]*(tau - gamma)**2) \
                                  * (d[51:54]/delta - 2*alpha*(delta - epsilon))))
    
    tPsi = Psi(delta,tau)
    y = y + np.dot(n[54:56], (Delta(delta,tau)**b * (tPsi + delta*Psi_d(delta,tau)) \
                              + (Delta_b_d(delta,tau)*delta*tPsi)))
    
    return y

def phir_dd(delta, tau):
    """
     second partial derivative for delta of phir
     where phir = residual part of free energy, dimensionless
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    n = np.array(IAPWS95_COEFFS['n']);          c = np.array(IAPWS95_COEFFS['c'])
    d = np.array(IAPWS95_COEFFS['d']);          t = np.array(IAPWS95_COEFFS['t'])
    alpha = np.array(IAPWS95_COEFFS['alpha']);  beta = np.array(IAPWS95_COEFFS['beta'])
    gamma = np.array(IAPWS95_COEFFS['gamma']);  epsilon = np.array(IAPWS95_COEFFS['epsilon'])
    b = np.array(IAPWS95_COEFFS['b'])
          
    y = np.dot(n[:7], (d[:7]*(d[:7]-1)*delta**(d[:7]-2) * tau**t[:7]))
    
    y = y + np.dot(n[7:51], ( np.exp(-delta**c[7:51]) * ( delta**(d[7:51]-2) * tau**t[7:51] * \
                                                         ((d[7:51] - c[7:51]*delta**c[7:51]) * \
                                                          (d[7:51] - 1 - c[7:51]*delta**c[7:51]) - \
                                                              (c[7:51])**2*delta**c[7:51]) ) ) )
    
    y = y + np.dot(n[51:54], ( tau**t[51:54]* np.exp(-alpha*(delta - epsilon)**2 \
                                                     -beta[:3]*(tau - gamma)**2) \
                              * (-2*alpha*delta**d[51:54] + 4*alpha**2*delta**d[51:54]*(delta-epsilon)**2 \
                                 - 4*d[51:54]*alpha*delta**(d[51:54]-1)*(delta-epsilon) \
                                     + d[51:54]*(d[51:54]-1)*delta**(d[51:54]-2))) )
    
    tPsi = Psi(delta,tau)
    dPsi = Psi_d(delta,tau)
    y = y + np.dot(n[54:56], ( Delta(delta,tau)**b * (2*dPsi + delta* Psi_dd(delta,tau)) \
                              + 2*Delta_b_d(delta,tau)*(tPsi + delta*dPsi) \
                                  + Delta_b_dd(delta,tau)*delta*tPsi) )        
    return y

def phir_dt(delta, tau):
    """
     partial derivative for delta and tau of phir
     where phir = residual part of free energy, dimensionless
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    n = np.array(IAPWS95_COEFFS['n']);          c = np.array(IAPWS95_COEFFS['c'])
    d = np.array(IAPWS95_COEFFS['d']);          t = np.array(IAPWS95_COEFFS['t'])
    alpha = np.array(IAPWS95_COEFFS['alpha']);  beta = np.array(IAPWS95_COEFFS['beta'])
    gamma = np.array(IAPWS95_COEFFS['gamma']);  epsilon = np.array(IAPWS95_COEFFS['epsilon'])
    b = np.array(IAPWS95_COEFFS['b'])
          
    y = np.dot(n[:7], (d[:7]*t[:7]*delta**(d[:7]-1) * tau**(t[:7]-1)))
    
    y = y + np.dot(n[7:51], (t[7:51]*  delta**(d[7:51]-1) * tau**(t[7:51]-1) \
                             * (d[7:51] - c[7:51]*delta**c[7:51]) *np.exp(-delta**c[7:51]) ))
    
    y = y + np.dot(n[51:54], ( delta**d[51:54] * tau**t[51:54] \
                              * np.exp(-alpha*(delta - epsilon)**2 - beta[:3]*(tau - gamma)**2) \
                                  * (d[51:54]/delta - 2*alpha*(delta - epsilon)) \
                                      *(t[51:54]/tau - 2*beta[:3]*(tau-gamma) )))
    
    tPsi = Psi(delta,tau)
    ttPsi = Psi_t(delta,tau)
    y = y + np.dot(n[54:56], ( Delta(delta,tau)**b * \
                              (Psi_t(delta,tau) + delta*Psi_dt(delta,tau)) \
                                  + (Delta_b_d(delta,tau)*delta*ttPsi) \
                                      + Delta_b_t(delta,tau)*(tPsi+delta*Psi_d(delta,tau)) \
                                          + Delta_b_dt(delta,tau)*delta*tPsi)) 
    return y

## IAPWS95.idealgas 
# Equation 6.5
def phi0(delta, tau):
    """
     ideal gas part of free energy, dimensionless
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # unpack coefficients
    n0 = np.asarray(IAPWS95_COEFFS['n0'])
    gamma0 = np.asarray(IAPWS95_COEFFS['gamma0'])
          
    y = np.log(delta) + n0[0] + n0[1]*tau + n0[2]*np.log(tau)
    y = y + np.dot(n0[3:], np.log(1 - np.exp(-gamma0[3:]*tau)))
    return y

# derivatives from Table 6.4
def phi0_t(delta, tau):
    """
     partial derivative for tau of phi0
     where phi0 = ideal gas part of free energy, dimensionless
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # unpack coefficients
    n0 = np.asarray(IAPWS95_COEFFS['n0'])
    gamma0 = np.asarray(IAPWS95_COEFFS['gamma0'])
          
    y = n0[1] + n0[2]/tau 
    y = y + np.dot(n0[3:], gamma0[3:]*(1/(1 - np.exp(-gamma0[3:]*tau))- 1.0))
    
    return y

def phi0_tt(delta, tau):
    """
     second partial derivative for tau of phi0
     where phi0 = ideal gas part of free energy, dimensionless
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # unpack coefficients
    n0 = np.asarray(IAPWS95_COEFFS['n0'])
    gamma0 = np.asarray(IAPWS95_COEFFS['gamma0'])
              
    y = -n0[2]/tau**2
    y = y - np.dot(n0[3:], (gamma0[3:]**2*np.exp(-gamma0[3:]*tau)*(1-np.exp(-gamma0[3:]*tau))**-2 ))

    return y

def phi0_d(delta, tau):
    """
     partial derivative for delta of phi0
     where phi0 = ideal gas part of free energy, dimensionless
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK           dimensionless inverse temperature
    """    
    y = 1/delta
    return y

def phi0_dd(delta, tau):
    """
     second partial derivative for delta of phi0
     where phi0 = ideal gas part of free energy, dimensionless
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """    
    y = -1/delta**2
    return y

def phi0_dt(delta, tau):
    """
     partial derivative for tau and delta of phi0
     where phi0 = ideal gas part of free energy, dimensionless
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """    
    y = 0
    return y

# supporting functions for calculating the ideal-gas and residual parts in the IAPWS-95 formulation
def Delta(delta, tau):
    """
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    a = np.array(IAPWS95_COEFFS['a']);  B = np.array(IAPWS95_COEFFS['B'])
    
    return theta(delta, tau)**2 + B*((delta - 1)**2)**a

def Delta_d(delta, tau):
    """
     Delta_d = (d Delta)/(d delta)
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/T           dimensionless inverse temperature
    """
    
    # % unpack coefficients
    a = np.array(IAPWS95_COEFFS['a']); beta = np.array(IAPWS95_COEFFS['beta'])
    A = np.array(IAPWS95_COEFFS['A']); B = np.array(IAPWS95_COEFFS['B'])
    
    d1 = delta - 1
    y = d1 * ( A*theta(delta,tau)*2/beta[-2:]* (d1**2)**(1/(2*beta[-2:])-1)+ 2*B*a*(d1**2)**(a-1) )
    
    return y

def Delta_dd(delta, tau):
    """
     Delta_dd = (d2 Delta)/(d delta2)
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/T           dimensionless inverse temperature
    """
    
    # % unpack coefficients
    a = np.array(IAPWS95_COEFFS['a']); beta = np.array(IAPWS95_COEFFS['beta'])
    A = np.array(IAPWS95_COEFFS['A']); B = np.array(IAPWS95_COEFFS['B'])
    
    d1 = delta - 1    
    y = 1/d1 * Delta_d(delta, tau) + \
        d1**2*( 4*B*a*(a-1)*(d1**2)**(a-2) + \
               2*A**2*beta[-2:]**-2*((d1**2)**(1/(2*beta[-2:])-1))**2 + \
                   A*theta(delta,tau)*4/beta[-2:]*(1/(2*beta[-2:])-1)* (d1**2)**(1/(2*beta[-2:])-2))  
    return y

def Delta_b_d(delta, tau):
    """
     Delta_b_d = (d Delta^b)/(d delta)
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    b = np.array(IAPWS95_COEFFS['b'])
    
    return b * Delta(delta,tau)**(b-1) * Delta_d(delta,tau)

def Delta_b_dd(delta, tau):
    """
     Delta_b_dd = (d2 Delta^b)/(d delta2)
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    b = np.array(IAPWS95_COEFFS['b'])
    y = b * ( Delta(delta,tau)**(b-1)*Delta_dd(delta,tau) + \
              (b-1)*Delta(delta,tau)**(b-2)*(Delta_d(delta,tau))**2)
    
    return y

def Delta_b_dt(delta, tau):
    """
     Delta_b_dt = (d2 Delta^b)/(d delta tau)
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    b = np.array(IAPWS95_COEFFS['b'])
    beta = np.array(IAPWS95_COEFFS['beta'])
    A = np.array(IAPWS95_COEFFS['A'])
    
    d1 = delta - 1
    
    y = -A*b*2/beta[-2:]*Delta(delta,tau)**(b-1) * d1*(d1**2)**(1/(2*beta[-2:])-1) \
              - 2*theta(delta,tau)*b*(b-1)*Delta(delta,tau)**(b-2) * Delta_d(delta,tau)
           
    return y

def Delta_b_t(delta, tau):
    """
     Delta_b_t = (d Delta^b)/(d tau)
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    b = np.array(IAPWS95_COEFFS['b'])
    
    return -2*theta(delta,tau)*b*Delta(delta,tau)**(b-1)

def Delta_b_tt(delta, tau):
    """
     Delta_b_tt = (d2 Delta^b)/(d tau2)
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    b = np.array(IAPWS95_COEFFS['b'])
    
    return 2*b*Delta(delta,tau)**(b-1) + 4*(theta(delta,tau))**2*b*(b-1)*Delta(delta,tau)**(b-2)

def theta(delta, tau):
    """
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    beta = np.array(IAPWS95_COEFFS['beta']);  A = np.array(IAPWS95_COEFFS['A'])
    
    return (1 - tau) + A*((delta - 1)**2)**(1./(2*beta[3:5]))

def Psi(delta, tau):
    """
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    C = np.array(IAPWS95_COEFFS['C'])
    D = np.array(IAPWS95_COEFFS['D'])
    
    return np.exp(-C*(delta-1)**2 - D*(tau - 1)**2)

def Psi_d(delta, tau):
    """
     Psi_d = (d Psi)/(d delta)
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    C = np.array(IAPWS95_COEFFS['C'])
    
    return -2*C*(delta - 1)*Psi(delta, tau)

def Psi_t(delta, tau):
    """
     Psi_t = (d Psi)/(d tau)
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    D = np.array(IAPWS95_COEFFS['D'])
    
    return -2*D*(tau - 1)*Psi(delta,tau)

def Psi_tt(delta, tau):
    """
     Psi_tt = (d2 Psi)/(d tau2)
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    D = np.array(IAPWS95_COEFFS['D'])
    
    return (2*D*(tau - 1)**2 - 1)*2*D*Psi(delta,tau)

def Psi_dd(delta, tau):
    """
     Psi_dd = (d2 Psi)/(d delta2)
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    C = np.array(IAPWS95_COEFFS['C'])
    
    return (2*C*(delta - 1)**2 - 1)*2*C*Psi(delta, tau)

def Psi_dt(delta, tau):
    """
     Psi_dt = (d Psi)/(d delta tau)
     auxiliary function in IAPWS95 formulation
     parameters:
         delta = rho/rhoc     dimensionless density
         tau = Tc/TK          dimensionless inverse temperature
    """
    
    # % unpack coefficients
    C = np.array(IAPWS95_COEFFS['C'])
    D = np.array(IAPWS95_COEFFS['D'])
    
    return 4*C*D*(delta - 1)*(tau - 1)*Psi(delta, tau)

#%%--------------------------------------------------------

def GZTTBR(delta):
    """
    This function is another means of calculating the zeta function
    utilized in waterviscosity function. Not really used here
    """
    cfz = np.zeros([5, 2])
    cfz[:, 0] = [0.152966739132486,  0.127637911731621,  0.098454718356579,
                 -0.304726588463916,  0.114242784534772] 
    cfz[:, 1] = [0.436894921256486, -0.256891980869883, -0.010426094984877, 
                 0.032776263097026, -0.005480033061907]
    j = -1
    if (delta <= 2.0):
        j = 0
    elif (delta <= 2.869):
        j = 1
        
    if j <= -1:
        #ier = 1   #Here delta is out of range
        gx = 0
        return gx
        
    gx = np.sum([x*delta**idx for idx, x in enumerate(cfz[:, j])])
    
    return gx

def heatcap(T, P, species):
    """
     returns heat capacity as a function of temperature and pressure for any specie
     parameters:
       T             vector format of temperature T[C]
       P             vector format of pressure P[bar]
       species       properties such as [dG [cal/ml], dH [cal/mol], S [cal/mol-K], V [cm3/mol]
                       a [cal/mol-K],b [*10^3 cal/mol/K^2],c [*10^-5 cal/mol/K] )
     results:
       delGfPT       Gibbs Energy [cal/mol]
    """
    if (np.ndim(T) == 0) | (np.ndim(P) == 0):
        T = np.array(celsiusToKelvin(T)).ravel()
        P = np.array(P).ravel()
    else:
        T = celsiusToKelvin(T).ravel()
        P = P.ravel()
    a = species[6]
    b = species[7]*10**-3
    c = species[8]*10**5
    delGref = species[2]
    Sr = species[4]
    Vr = species[5]*0.0239005736   # conversion: cal/bar/cm3  See Johnson et al., 1992
    Tr = 298.15
    Pr = 1.0
    # delCp = a + b*T + c*T**-2
    delGfPT = delGref - Sr*(T-Tr) + a*(T-Tr-T*np.log(T/Tr)) + ((-c-b*T*Tr**2)*(T-Tr)**2)/(2*T*Tr**2) + Vr*(P-Pr)
        
    return delGfPT #, delCp

def heatcapusgscal(T, P, species):
    """
     returns heat capacity as a function of temperature and pressure for any specie
     parameters:
       T             vector format of temperature T[C]
       P             vector format of pressure P[bar]
       species       properties such as [dG [cal/mol], dH [cal/mol], S [cal/mol-K], V [cm3/mol]
                       a [cal/mol-K],b [*10^3 cal/mol/K^2],c [*10^-5 cal/mol/K] )
     results:
       delGfPT       Gibbs Energy [cal/mol]
    """
    if (np.ndim(T) == 0) | (np.ndim(P) == 0):
        T = np.array(celsiusToKelvin(T)).ravel()
        P = np.array(P).ravel()
    else:
        T = celsiusToKelvin(T).ravel()
        P = P.ravel()
    Tr = 298.15
    Pr = 1.0
    delGfPT = np.nan*np.ones(len(T))
    delCp = delGfPT
    if len(species[2:]) == 9:
        a = species[6] #
        b = species[7] # T
        c = species[8] # T^-2
        g = species[9] # T^-.5
        f = species[10] # T^2
        delGref = species[2] #cal/mol
        Sr = species[4] # cal/mol/K
        Vr = species[5]*0.02390054 # conversion: cal/bar/cm3  See Johnson et al., 1992
        
        delCp = a + b*T + c*(T**-2.0) + g*(T**-.5) + f*(T**2.0)
        delGfPT = delGref-Sr*(T-Tr)+Vr*(P-Pr)+a*(T-Tr-T*np.log(T/Tr))+(b/2)*(-T**2-Tr**2+2*T*Tr) + \
            c*((-T**2-Tr**2+2*T*Tr)/(2*T*Tr**2)) +(f/6)*(-T**3-2*Tr**3+3*T*Tr**2) + \
                2*g*(2*T**0.5-Tr**0.5-(T/(Tr**0.5)))   
                #Corrected 6/26/2013
    elif len(species[2:]) == 15:  #There is one phase transition to be accounted for    
        delGref = species[2] #cal/mol
        Sr = species[4]  # cal/mol/K
        
        Vr = species[5]*0.02390054 # conversion: cal/bar/cm3  See Johnson et al., 1992
        a = species[6] #
        b = species[7] # T
        c = species[8] # T^-2
        g = species[9] # T^-.5
        f = species[10] # T^2
        Ttrans = species[11] #K
        
        delGfPT = delGref-Sr*(T-Tr)+Vr*(P-Pr)+a*(T-Tr-T*np.log(T/Tr))+(b/2)*(-T**2-Tr**2+2*T*Tr) \
            +c*((-T**2-Tr**2+2*T*Tr)/(2*T*Tr**2))+(f/6)*(-T**3-2*Tr**3+3*T*Tr**2)  \
                +2*g*(2*T**.5-Tr**.5-(T/(Tr**.5))) #%Corrected 6/26/2013
        
        # %For T greater than the phase transition:        
        # %Get the heat capacity coefficients for T>Ttrans
        a=species[12] #
        b=species[13] # T
        c=species[14] # T^-2
        f=species[16] # T^2
        g=species[15] # T^-.5
        
        delGfPT[T>Ttrans] = delGfPT[T>Ttrans]+a*(T[T>Ttrans]-Ttrans-T[T>Ttrans]*np.log(T[T>Ttrans]/Ttrans)) \
            +(b/2)*(-T[T>Ttrans]**2-Ttrans**2+2*T[T>Ttrans]*Ttrans)   \
                +c*((-T[T>Ttrans]**2-Ttrans**2 +2*T[T>Ttrans]*Ttrans)/(2*T[T>Ttrans]*Ttrans**2))  \
                    +(f/6)*(-T[T>Ttrans]**3-2*Ttrans**3+3*T[T>Ttrans]*Ttrans**2)   \
                        +2*g*(2*T[T>Ttrans]**.5-Ttrans**.5-(T[T>Ttrans]/(Ttrans**.5)))  #Corrected 6/26/2013
        
    return delGfPT, delCp

# %--------------------------------------------------------
def EOSIAPW95(TK, rho):   
    """
     This function evaluates the basic equation of state, which is written as
      a function of:
           reduced density: delta = rho/rhoc
           inverse reduced temperature: tau = Tc/TK
     The pressure px is an output. To obtain properties as a function of temperature and 
     pressure, it is necessary to iterate on pressure (adjusting density).
    parameters:
        TK      temperature [K]
        rho     density [kg/m3]
    results:
        px      pressure [kPa]
        ax      Helmholtz energy (kJ/kg-K)
        ux      Internal energy (kJ/kg)
        hx      Enthalpy (kJ/kg)
        sx      Entropy (kJ/kg/K)
        gx      Gibbs energy (kJ/kg)
        vx      Volume (m3/kg)
        cpx     Isobaric heat capacity (kJ/kg/K)
        cvx     Isochoric heat capacity (kJ/kg/K)
        mux     Joule-Thomsen coefficient (K-m3/kJ)
        dtx     Isothermal throttling coefficient (kJ/kg/MPa)
        bsx     Isentropic temperature-pressure coefficient (K-m3/kJ)
        wx      Speed of sound (m/s)
        ktx     Compressibility
        avx     Thermal expansion coefficient (thermal expansivity)
    """

    Tc = IAPWS95_COEFFS['Tc']
    Pc = IAPWS95_COEFFS['Pc']
    rhoc = IAPWS95_COEFFS['rhoc']
    # specific and molar gas constants
    R = IAPWS95_COEFFS['R']/1000 # kJ kg-1 K-1
    
    delta = rho/rhoc
    tau = Tc/TK
        
    epxc = 100*eps* delta
    if (delta <= epxc):
        delta = epxc
        rho = rhoc*delta
    
    #     Avoid a singularity at the critical density (at any temperature).
    #     If delta = rho/rhoc is unity, then delta - 1 is zero. This
    #     cannot be used with (delta - 1)**n where n is negative, or in a
    #     division (x/(d -1)).

    x1 = 1.0 - epxc
    x2 = 1.00 + epxc
    if (delta > x1) & (delta < x2):
        if (delta < 1.0):
            delta = x1
        else:
            delta = x2
        rho = rhoc*delta
            
    # delsq = delta**2
    # tausq = tau**2
    rtx = R*TK

    #     Calculate thermodynamic functions.    
    #     Helmholtz energy. The value is in J/kg-K.
    ax = float(rtx*( phi0(delta, tau) + phir(delta, tau) )   )
    #     Pressure. The value here is in kPa.
    px = float(rho*rtx*( 1 + delta*phir_d(delta, tau) )  )  
    #     Internal energy.
    # ux = rtx*tau*( phi0_t(delta, tau) + phir_t(delta, tau) )    
    #     Entropy.
    sx = float(R*( tau*(phi0_t(delta, tau) + phir_t(delta, tau)) - phi0(delta, tau) - phir(delta, tau) ))
    #     Enthalpy.
    hx = float(rtx*( 1 + tau*(phi0_t(delta, tau) + phir_t(delta, tau)) + delta*phir_d(delta, tau) ) )   
    #     Gibbs energy.
    gx = float(rtx*( 1 + phi0(delta, tau) + phir(delta, tau) + delta*phir_d(delta, tau) ))
    #     Alternate formulas for the Gibbs energy.   
    #     gx = hx - TK*sx
    #     gx = ax + hx - ux
    #     Volume.
    vx = float(1/rho)
    #     Isochoric heat capacity.
    # cvx = -R*tausq*( phi0_tt(delta, tau) + phir_tt(delta, tau) )
    #     Isobaric heat capacity.
    # x1 = ( 1 + delta*phir_d(delta, tau) - delta*tau*phir_dt(delta, tau) )**2
    # x2 = 1 + 2*delta*phir_d(delta, tau) + delsq*phir_dd(delta, tau)
    # if (abs(x2) > 1e-15):
    #     cpx = float(cvx + R*(x1/x2))
    # else:
    #     cpx = 1.0 + 100
    #     Speed of sound.
    # x1 = ( 1 + delta*phir_d(delta, tau) - delta*tau*phir_dt(delta, tau) )**2
    # x2 = tausq*( phi0_tt(delta, tau) + phir_tt(delta, tau) )
    # x3 = x1/x2
    # xxt = rtx*( 1 + 2*delta*phir_d(delta, tau) + delsq*phir_dd(delta, tau) - x3 )
    # if (xxt > 0):
    #     wx = np.sqrt(xxt)
    # else:
    #     wx = 0.0
    #     Joule-Thomsen coefficient.
    # x1 = delta*phir_d(delta, tau) + delsq*phir_dd(delta, tau) + delta*tau*phir_dt(delta, tau)
    # x2 = ( 1 + delta*phir_d(delta, tau) - delta*tau*phir_dt(delta, tau) )**2
    # x3 = ( phi0_tt(delta, tau) + phir_tt(delta, tau) )*( 1.0 + 2*delta*phir_d(delta, tau) + \
    #                                                     delsq*phir_dd(delta, tau) )
    # mux = float(( - x1/( x2 - tausq*x3 ) )/(R*rho) )   
    #     Isothermal throttling coefficient.    
    # x1 = 1 + delta*phir_d(delta, tau) - delta*tau*phir_dt(delta, tau)
    # x2 = 1 + 2*delta*phir_d(delta, tau) + delsq*phir_dd(delta, tau)
    # dtx = float(( 1 - ( x1/x2 ) )/rho )   
    #     Isentropic temperature-pressure coefficient.
    # x1 = 1.0 + delta*phir_d(delta, tau) - delta*tau*phir_dt(delta, tau)
    # x2 = x1**2
    # x3 = ( phi0_tt(delta, tau) + phir_tt(delta, tau) )*( 1 + 2*delta*phir_d(delta, tau) + \
    #                                                     delsq*phir_dd(delta, tau) )
    # bsx = float(( x1/( x2 - tausq*x3 ) )/(R*rho) )   
    #     Derivative of pressure with respect to delta (needed to perform Newton-Raphson 
    #     iteration to matched desired pressure). Here the value   is in kPa. Recall that:
    #     px = rho*rtx*( 1.0d0 + delta*phird )
    pdx = ( px/delta ) + delta*rhoc*rtx*( phir_d(delta, tau) + delta*phir_dd(delta, tau) )    
    #     Derivative of pressure with respect to tau (needed to calculate the thermal expansion 
    #     coefficient). Here the value is again     in kPa.
    # ptx = ( -px/tau ) + px*delta*phir_dt(delta, tau)/(1 + delta*phir_d(delta, tau))    
    #     Compressibility. Here the value is in /Kpa.
    # ktx = 1.0/(delta*pdx)    
    #     Calculate ztx (needed to calculate viscosity). Note: Pc here is in MPa, 
    #     but pdx is still in /kPa. Hence there is the need to include the factor of 
    #     1000 kPa/MPa. Note that ztx itself is dimensionless, so pressure unit correction 
    #     needs to be made here.
    ztx = float(1000.0*Pc/pdx    )
    #     An alternative formula is: ztx = 1000.0d0*delta*Pc*ktx
    #     This can be useful for getting zeta from a calculator that gives
    #     the compressibility (but not zeta).    
    #     Thermal expansion coefficient (thermal expansivity).
    #     This calculation is based on the Maxwell relation:
    #     (del P/del T) at const V = alpha/kappa
    # avx = float(ktx*ptx*( -tau/TK ))    
    #     Parts needed to calculate residuals and Jacobian elements
    #     if refining saturation properties at fixed temperature.    
    #     Helmholtz energy.    
    #     ax = rtx*( phi0 + phir )
    adx = float(rtx*( phi0_d(delta, tau) + phir_d(delta, tau) ))
    #     Gibbs energy.    
    #     gx = rtx*( 1.0d0 + phi0 + phir + delta*phird )
    # gdx = float(rtx*( phi0_d(delta, tau) + 2*phir_d(delta, tau) + delta*phir_dd(delta, tau) ))
   
    #     Unit conversions. Direct evaluation of the IAPWS-95
    #     model equations gives some of the results in in odd
    #     units. Conversions are therefore desirable for most
    #     usage. Prior to conversion, the units are determined
    #     by the units adopted by the model for temperature (K),
    #     density (kg/m3), and the gas constant (kJ/kg/K).
    #     See Table 3.
    
    #       Note that density is input as kg/m3.
    #       Volume is therefore in m3/kg.
    
    #       Pressure comes out in kPA. Divide by 1000 to obtain
    #       pressure in MPa. One could divide instead by 100 to
    #       obtain pressure in bars. Here we will use MPa.
    #       The pdx (the partial derivative of pressure with respect
    #       to delta) and ktx (the isothermal compressibility)
    #       must also be corrected to be consistent with pressure in MPa.
    
    #       Internal energy, enthalpy, and Helmholtz energy come out in kJ/kg.
    
    #       Entropy and the two heat capacity functions come out in kJ/kg/K.
    
    #       Gibbs energy is therefore in kJ/kg.
    
    #       Speed of sound comes out in sqrt(kJ/kg). Multiply by
    #       sqrt(1000) to obtain the speed of sound in m/s.
    
    #       Joule-Thomson coefficient comes out in K-m3/kJ.
    #       These units are equivalent to the usual K/MPa.
    
    #       Isothermal throttling coefficient comes out in m3/kg.
    #       Divide by 1000 to obtain the result in the usual kJ/kg/MPa.
    
    #       Isentropic temperature-pressure coefficient comes out
    #       K-m3/kJ (the same units as for the Joule-Thomson coefficient).
    #       These units are equivalent to the usual K/MPa.

    px = 0.0010*px
    pdx = 0.0010*float(pdx)
    # ptx = 0.0010*float(ptx)
    # ktx = 1000.0*float(ktx)

    # wx = float(wx)*np.sqrt(1000.00)
    # dtx = 0.0010*dtx

    return px, ax, sx, hx, gx, vx, pdx, adx, ztx

vEOSIAPW95 = np.vectorize(EOSIAPW95) # vectorize the function above to permit array variables

def auxMeltingPressure(TK, P):
    """
    #    This function gets the melting pressure of ice as a function
    #    of temperature. This model is described in IAPWS R14-08(2011),
    #    The International Association for the Properties of Water and
    #    Steam, 2011, Revised Release on the Pressure along the Melting
    #    and Sublimation Curves of Ordinary Water Substance. This document may be found at:
    #    http://www.iapws.org/relguide/MeltSub.html
    
    #    Five ice phases are covered here. The melting pressure is
    #    not a single-valued function of temperature as there is some
    #    overlap in the temperature ranges of the individual phases.
    #    There is no overlap in the temperature ranges of Ices III,
    #    V, VI, and VII, which together span the range 251.165-715K.
    #    The melting pressure is continuous and monotonically increasing
    #    over this range, albeit with discontinuities in slope at
    #    the triple points where two ice phases and liquid are in
    #    equilibrium. The problem comes in with Ice Ih, whose
    #    temperature range completely overlaps that of Ice III and
    #    partially overlaps that of Ice V. For a temperature In the
    #    range for Ice Ih, there are two possible melting pressures.
    
    #    The possible ambiguity here in the meaning of melting pressure
    #    is not present if the temperature is greater than or equal to
    #    the triple point temperature of 273.16K, or if the pressure is
    #    greater than or equal to 208.566 MPa (the triple point pressure
    #    for Ice Ih-Ice III-liquid). If neither of these conditions
    #    are satisfied, then the Ice Ih-liquid curve will be used.
    #    To deal with the pressure condition noted above, this function
    #    assumes that an actual pressure is specified.
    
    #    IAPWS R14-08 makes reference to the melting temperature for
    #    pressure between the triple point pressure and 300 MPa, which
    #    includes the region of ambiguity. However, it does not discuss
    #    the ambiguity or make any suggestion of how to deal with it.
    #    presents the overall model.
    parameters:
        P        pressure [MPa]
        TK       temperature [K]
    results:
        Pmelt    melting pressure [MPa]

    """
    #     Coefficients for calculating the melting pressure of Ice Ih.
    
    a = np.array([0.119539337e+07, 0.808183159e+05, 0.333826860e+04])
    b = np.array([0.300000e+01, 0.257500e+02, 0.103750e+03])
    
    if (P <= 208.566):
        if (251.165 <=  TK <= 273.16):
            #         Ice Ih.
            theta = TK/273.16
            pimelt = 1 + np.sum(a*( 1 - theta**b))
            Pmelt = pimelt*611.647e-06
        else:
            Pmelt = np.nan #208.566
    else:
        if (251.165 <= TK < 256.164):
            #          Ice III.
            theta = TK/251.165
            pimelt = 1 - 0.299948*( 1 - theta**60 )
            Pmelt = pimelt*208.566
        elif (256.164 <= TK < 273.31):
            #          Ice V.
            theta = TK/256.164
            pimelt = 1 - 1.18721*( 1 - theta**8 )
            Pmelt = pimelt*350.1
        elif (273.31 <= TK < 355.0):
            #          Ice VI.
            theta = TK/273.31
            pimelt = 1 - 1.07476*( 1 - theta**4.6 )
            Pmelt = pimelt*632.4
        elif (355.0 <= TK < 715.0):
            #          Ice VII.
            theta = TK/355.0
            px =   0.173683e+01*( 1 - theta**(-1) ) - 0.544606e-01*( 1 - theta**5 ) \
                + 0.806106e-07*( 1 - theta**22 )
            pimelt = np.exp(px)
            Pmelt = pimelt*2216.0
        elif (715.0 <= TK <= 2000.0):
            #          This is out of range. Ice VII, extrapolated.
            theta = TK/355.0
            px =   0.173683e+01*( 1 - theta**(-1) ) - 0.544606e-01*( 1 - theta**5 ) \
                + 0.806106e-07*( 1 - theta**22 )
            pimelt = np.exp(px)
            Pmelt = pimelt*2216.0
        else:    
            Pmelt = np.nan
            
    return Pmelt
    
def auxMeltingTemp(P):
    """
    #     This function gets the melting temperature of ice as a function
    #     of pressure. This inverts the model for the melting pressure
    #     as a function of temperature. That model is described in
    #     IAPWS R14-08(2011), The International Association for the
    #     Properties of Water and Steam, 2011, Revised Release on the
    #     Pressure along the Melting and Sublimation Curves of Ordinary
    #     Water Substance. This document may be found at:
    #     http://www.iapws.org/relguide/MeltSub.html
    
    #     The above model for the melting pressure is treated in
    #     FUNCTION auxMeltingPressure. The actual pressure must be specified in
    #     order to resolve the problem of overlap in the temperature
    #     ranges of the individual phases. See discussion in FUNCTION
    #     auxMeltingPressure.
    
    #     Inversion of the model for the melting pressure is done
    #     here using the secant method. This is chosen instead of the
    #     Newton-Raphson method to avoid potential problems with slope
    #     discontinuites at boundaries between the ice phases for
    #     pressures above 208.566 MPa, which is the equilibrium pressure
    #     for Ice Ih-Ice III-liquid. The corresponding equlibrium
    #     temperature is 251.165K. Putative melting temperatures should
    #     not be less than this for pressures above 208.566 MPa, nor
    #     more than this for pressures less than this.
    parameters:
        P        pressure [MPa]
    results:
        Tmelt    temperature [K]

    """
    #     Find the melting temperature that corresponds to the actual
    #     pressure. The variables t0 and t1 are initial values for
    #     secant method iteration.
    if (P < 208.566):
        #       In the Ice Ih field.
        tlim0 = 251.165
        tlim1 = 273.16
        t0 = tlim0
        t1 = tlim1
    elif (208.566 <= P < 350.1):
        #       In the Ice III field.
        tlim0 = 251.165
        tlim1 = 256.164
        t0 = tlim0
        t1 = tlim1
    elif (350.1 <= P < 632.4):
        #       In the Ice V field.
        tlim0 = 256.164
        tlim1 = 273.31
        t0 = tlim0
        t1 = tlim1
    elif (632.4 <= P < 2216.0):
        #       In the Ice VI field.
        tlim0 = 273.31
        tlim1 = 355.0
        tx = tlim1 - tlim0
        t0 = tlim0 + 0.3*tx
        t1 = tlim0 + 0.7*tx
    elif (2216.0 <= P <= 10000.0):
        #       In the Ice VII field.
        #       Note: the upper limit here of 10000 MPa is an arbitrary
        #       cutoff suggested by Figure 1 from IAPWS R14, but this
        #       is not part of the IAPWS R14 standard.
        tlim0 = 355.0
        tlim1 = 1000.0
        tx = tlim1 - tlim0
        t0 = tlim0 + 0.3*tx
        t1 = tlim0 + 0.7*tx
    elif (P > 20000.0):
        Tm = np.nan
    
    funct_melt = lambda t: auxMeltingPressure(t, P) - P    
    Tm = root_scalar(funct_melt, method = 'secant', 
                     bracket=[t0, t1], x0=t0, x1=t1, xtol = 1.0e-10).root
            
    # n = 0; tt = 0; res = 0
    # rlx = 1.0
    # #     In general, t0 is the temperature to be tested nad
    # #     t1 is the other temperature used in refining t0.
    # #     Here test the starting t1 in case it is the solution.
    # while (n < 50):
    #     Pmelt1 = auxMeltingPressure(t1, P)
    #     res = Pmelt1 - P
    #     if (abs(res) <= 1.0e-6):
    #         Tm = t1
    #         break
    #     Pmelt0 = auxMeltingPressure(t0, P)
    #     res = Pmelt0 - P
    #     dp = Pmelt1 - Pmelt0
    #     dt = t1 - t0
    #     if (abs(res) <= 1.0e-6):
    #         Tm = t0
    #         break
    #     delt = - (dt/dp)*res  
    #     tt = t0 + rlx*delt    
    #     # Check to see that tt is within the specified temperature limits. If not, apply under-relaxation.
    #     if not (tlim0 < tt < tlim1): 
    #         rlx = 0.5*rlx
    #         tt = t0 + rlx*delt            
    #     #     Determine whether tt is closer to t0 or t1 Discard the more distant point.
    #     if (abs(tt - t0) <= abs(tt - t1)):
    #         #       Make make t0 the new t1.
    #         t1 = t0
    #         Pmelt1 = Pmelt0
    #     #     Make tt the new t0.
    #     t0 = tt
    #     #print(n, t0, res)
    #     n = n + 1
    Tmelt = Tm        
    
    return Tmelt

def waterviscosity(T, P, rho):
    """
    #     This function calculates the viscosity of water using the
    #     "IAPWS Formulation 2008 for the Viscosity of Ordinary Water
    #     Substance" (IAPWS R12-08). The corresponding journal reference
    #     is: Huber M.L., Perkins R.A., Laesecke A., Friend D.G.,
    #     Sengers J.V., Assael M.J., Metaxa I.N., Vogel E., Mares R.,
    #     and Miyagawa K. (2009) New International Formulation for the
    #     Viscosity of H2O. J. Phys. Chem. Ref. Data 38, 101-125.
    #     Equations referenced below are from IAPWS R12-08.
    # parameters:
        rho      density
        P        pressure [bar]
        T        temperature [C]
    # results:
        visc     viscosity [Pa.s]
    """
    TK=celsiusToKelvin(T)  #convert to Kelvin  
    PPa = P*1e5  
        
    Tc = IAPWS95_COEFFS['Tc'] # K
    # Pc = IAPWS95_COEFFS['Pc']*1e6 # Pa
    rhoc = IAPWS95_COEFFS['rhoc']
    
    H0 = [1.67752e+00, 2.20462e+00, 0.6366564e+00, -0.241605e+00]
    I1 = [0, 1, 2, 3, 0, 1, 2, 3, 5, 0, 1, 2, 3, 4, 0, 1, 0, 3, 4, 3, 5]
    J1 = [0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 4, 4, 5, 6, 6]
    H1 = [0.520094,  0.0850895, -1.08374, -0.289555, 0.222531, 0.999115, 1.8879700000000001,
          1.26613, 0.120573, -0.281378, -0.9068510000000001, -0.772479, -0.48983699999999997,
          -0.25704, 0.161913, 0.257399, -0.0325372, 0.0698452, 0.00872102, -0.00435673, -0.000593264]
    
    delta = rho/rhoc
    Tbar = TK/Tc
    # Pbar = PPa/Pc
    muref = 1.00e-06   # Pa.s
    
    # Check range of validity.
    chk = False
    # triple point
    Ttr = 273.16 # K
    Ptr = 611.657 # Pa
    Ttmltx = auxMeltingTemp(PPa*1e-6)
    
    if (PPa < Ptr):
        if (Ttr <= TK <= 1173.15):
            chk = True
    elif Ttmltx > 0.0:
        if (Ptr <= PPa <= 300.0e6):
            if (Ttmltx <= TK <= 1173.15):
              chk = True
        elif (300.0e6 < PPa <= 350.0e6):
            if (Ttmltx <= TK <= 873.15):
              chk = True
        elif (350.0e6 < PPa <= 500.0e6):
            if (Ttmltx <= TK <= 433.15):
              chk = True
        elif (500.0e6 < PPa <= 1000.0e6):
            if (Ttmltx <= TK <= 373.15):
              chk = True

    if (not chk) | (Ttmltx <= 0):
        mubar = np.nan
    else:
        # Calculate the viscosity in the dilute gas limit (mubar0)
        mubar0 = 100*np.sqrt(Tbar)/np.sum([x/Tbar**idx for idx, x in enumerate(H0)])
    
        s1=np.zeros([len(H1), 1])
        for x in range(len(H1)):
            s1[x] = H1[x]*((1/Tbar) -1)**I1[x] * (delta-1)**J1[x]
        # Calculate the contribution to viscosity due to finite density
        mubar1 = np.exp(delta * np.sum(s1))
    
        # critical enhancement
        xmu = 0.068
        qc = 1/1.9      #/nm
        qD = 1/1.1      #/nm
        nvc = 0.63
        gamma = 1.239
        xicl0 = 0.13 
        gam0 = 0.06
        TR = 1.5
        Tkr = TR*Tc
        
        # Get zeta at the reference temperature
        zettbr = GZTTBR(delta)  #Estimate zeta at the reference temperature using fitted polynomials
        
        [_, _, _, _, _, _, _, _, ztxr] = EOSIAPW95(Tkr, rho) # Estimate the reference zeta value by directly evaluating the EOS model
        
        # Compare the reference zeta value obtained by the two methods.
        # zdx = zettbr - ztxr        
        [_, _, _, _, _, _, _, _, ztx] = EOSIAPW95(TK, rho)
        
        # Get delchb (DELTA chibar, equation 21).
        delchb = delta*( ztx - (ztxr*TR*Tbar) )
        if (delchb < 0.0):
            delchb = 0.0
        #     Get xicl (equation 20).
        xicl = xicl0*( (delchb/gam0)**(nvc/gamma) )
        #  Get psid (equation 17).
        psid = math.acos(( 1.0 + (qD*xicl)**2 )**(-0.5))
        #  Get www (w in equation 19).
        w = np.sqrt(abs( ((qc*xicl) - 1.0)/((qc*xicl) + 1.0) )) * math.tan(0.5*psid)
        # Get lcap (L(w) in equation 18)
        if ((qc*xicl) > 1):
            lcap = np.log( (1 + w)/(1 - w) )
        else:
            lcap = 2*math.atan( abs(w) )
            
        if (xicl <= 0.3817016416):
            ycap  = 0.2*(qc*xicl)*((qD*xicl)**5)* \
                (1 - (qc*xicl) + (qc*xicl)**2  - ( 765.0/504.0 )*(qD*xicl)**2)
        else:
            ycap = ( 1/12 )*math.sin(3*psid) - ( 1/(4*(qc*xicl)) )*math.sin(2*psid) +\
                ( 1/(qc*xicl)**2 )*( 1.0 - 1.25*(qc*xicl)**2 ) *math.sin(psid) \
                    -( 1.0/(qc*xicl)**3 ) *( ( 1.0 - 1.5*(qc*xicl)**2 ) \
                                            - ( abs((qc*xicl)**2 - 1) )**1.5 * lcap)
    
        if (645.91 < TK < 650.77):
            if (245.8 < rho < 405.3):
                mubar2 = np.exp(xmu * ycap)
        else:
            mubar2 = 1
    
        mubar = mubar0 * mubar1 *mubar2
    
    return mubar*muref, ztx, zettbr

def fDescr(P, TK, *rho):
    """
    #     Given the temperature and pressure, find the appropriate
    #     description of the H2O fluid. A problem may occur if the
    #     pressure is equal or nearly equal to the saturation pressure.
    #     Here comparing the pressure with the saturation pressure
    #     pressure may lead to the wrong description, as vapor and
    #     liquid coexist at the saturation pressure. It then becomes
    #     neccesary to compare the fluid density with the saturated
    #     vapor and saturated liquid densities. If the density is
    #     known (as following a CALDLT calculation), it will be used.
    #     If it is not known (as in a CALPRE calculation), it will
    #     not be used. In that case, the results obtained here will
    #     determine the starting density estimate, thus in essence
    #     choosing "vapor" or "liquid" for pressures close to the
    #     saturation pressure.   
    parameters:
        P        pressure [MPa]
        TK       temperature [K]
        rho      density [kg/m3] (optional)
    results:
        udescr   fluid description
        rhosl    liquid density [kg/m3]
        rhosv    vapor density [kg/m3]
    """
    Tc = IAPWS95_COEFFS['Tc'] # K
    Pc = IAPWS95_COEFFS['Pc'] # MPa
    # rhoc = IAPWS95_COEFFS['rhoc']
    btxtol = 1e-10
    rhotol = 1.0e-8
    
    if (TK < 273.15):
        #       Note that the allowed temperature range has been extended a bit
        #       on the low end to include 0C.
        udescr = 'unknown'
        rhosv = 0; rhosl = 0
    elif (TK <= Tc):
        #       Calculate the saturation curve properties.
        [Psat, rhosv, rhosl] = CALSCT(TK)
        if (P > Pc):
            udescr = 'compressed liquid'
            if Psat == np.nan:
                rhosl = 1.05 # if CALSCT failed, an arbitrary liquid-like density will be assigned as a starting value for compressed liquid
        else:
            if Psat == np.nan:  #if CALSCT failed, the vapor and liquid states cannot be distinguished from one another. Liquid is assigned arbitrarily
                udescr = 'liquid'
                rhosl = 1.05
            else:
                if (P >= Psat):
                    udescr = 'liquid'
                else:
                    udescr = 'vapor'
                #    Use density (rho) if available and pressure is close to psat.
                if len(rho) != 0:
                    Ptest = (P - Psat)/Psat
                    btest = 10*btxtol
                    if (abs(Ptest) <= btest):
                        # Here press is very close to psat. Use rho to determine vapor or liquid.
                        rtestl = (rho - rhosl)/rhosl
                        rtestv = (rho - rhosv)/rhosv
                        if (abs(rtestl) <= rhotol):
                            udescr = 'liquid'
                        elif (abs(rtestv) <= rhotol):
                            udescr = 'vapor'
                        else:
                            udescr = 'unknown'
    else:
        rhosv = 0; rhosl = 0
        if (P > Pc):
            udescr = 'supercritical fluid'
        else:
          udescr = 'hot vapor'
    
    return udescr, float(rhosl), float(rhosv)

vfDescr = np.vectorize(fDescr) # vectorize the function above to permit array variables

def APXSCT(TK):
    """   Approximate
    #     Evaluate the pressure (psat) as a function of temperature along
    #     the vapor-liquid equilibrium curve, using equation 2.5 of Wagner
    #     and Pruss (2002). Also calculate the densities of the liquid and
    #     vapor phases using equations 2.6 and 2.7 from the same source.
    #     Results are not fully consistent with IAPWS-95 to high precision,
    #     but may serve as close approximations or starting values for
    #     refinement.
    parameters:
        Tsat     saturation temperature [K]
    results:
        Psat    saturation pressure [MPa]
        Psat_t  Derivative of saturation pressure with respect to temperature
        rhosv   density of vapor [kg/m3]
        rhosl   density of liquid [kg/m3]
    """
    Tc = IAPWS95_COEFFS['Tc'] # K
    Pc = IAPWS95_COEFFS['Pc'] # MPa
    a1 = -7.85951783
    a2 = 1.84408259
    a3 = -11.7866497
    a4 = 22.6807411
    a5 = -15.9618719
    a6 = 1.80122502
    
    rhoc = IAPWS95_COEFFS['rhoc']    
    b1 = 1.99274064
    b2 = 1.09965342
    b3 = -0.510839303
    b4 = -1.75493479
    b5 = -45.5170352
    b6 = -6.74694450e5
    
    c1 = -2.03150240
    c2 = -2.68302940
    c3 = -5.38626492
    c4 = -17.2991605
    c5 = -44.7586581
    c6 = -63.9201063
    #T = np.min([Tc, np.max([TK, 0])])
    #     Check to see that the temperature is in the allowed range.
    if (TK < 273.15) | (TK > Tc):
        Psat = np.nan
        Psat_t = np.nan
        rhosl = np.nan
        rhosv = np.nan
    else:   
        #     Saturation pressure.
        th = 1 - TK/Tc
        t1 = a1*th + a2*th**1.5 + a3*th**3 + a4*th**3.5 + a5*th**4 + a6*th**7.5
        Psat = Pc*np.exp(Tc*t1/TK)
        #     Derivative of saturation pressure with respect to temperature.
        x1 = a1 + 1.5*a2*th**0.5 + 3.0*a3*th**2.0 + 3.5*a4*th**2.5 + 4.0*a5*th**3.0 + 7.5*a6*th**6.5
        x2 = np.log( Psat/Pc ) + x1
        Psat_t = -( Psat/TK )*x2
    
        
        #  Density of liquid.
        th = (1 - TK/Tc)**(1/3)
        t1 = 1 + b1*th + b2*th**2 + b3*th**5 + b4*th**16 + b5*th**43 + b6*th**110
        rhosl = rhoc*t1
        
        #  Density of vapor.
        th = np.sqrt(th)
        t2 = c1*th**2 + c2*th**4 + c3*th**8 + c4*th**18 + c5*th**37 + c6*th**71
        rhosv = rhoc*np.exp(t2)
    
    return Psat, Psat_t, rhosl, rhosv

vAPXSCT = np.vectorize(APXSCT) # vectorize the function above to permit array variables

def APXSCP(P):
    """   Approximate
    #     Evaluate the temperature (tsat) as a function of pressure along
    #     the vapor-liquid equilibrium curve, using equation 2.5 of Wagner
    #     and Pruss (2002). Also calculate the densities of the liquid and
    #     vapor phases using equations 2.6 and 2.7 from the same source.
    #     Results may not be fully consistent with IAPWS-95, but may serve
    #     as close approximations or starting values for refinement.
    #     This routine is similar to APXSCT, but evaluates the inverse
    #     problem (tsat as a function of press instead of psat as a function
    #     of tempk). Newton-Raphson itation is used. This routine calls
    #     APXSCT to calclate the pressure for a given temperature value and
    #     the derivative of pressure with respect to temperature.
    parameters:
        P        pressure [MPa]
    results:
        Tsat     saturation temperature [K]
    """
    Tc = IAPWS95_COEFFS['Tc'] # K
    Pc = IAPWS95_COEFFS['Pc'] # MPa
    # triple point
    Ttr = 273.16 # K
    Ptr = 611.657e-6 # MPa
    P1atm = 1.01325e-1  # MPa
    Ts1atm = 373.124  # K
    
    #     Check to see that the pressure is in the allowed range.
    if (P < Ptr) | (P > Pc):
        Tsat = np.nan
    else:   
        #  Choose a starting temperature value. The exact value is not that important.
        if (P >= P1atm):
            # Interpolate between 100C, 1.01325 bar and tcr, pcr.
            dtdp = (Tc - Ts1atm)/(Pc - P1atm)
            TK = Ts1atm + dtdp*(P - P1atm)
        else:
            # Interpolate between the triple point and 100C, 1.013 bar
            dtdp = (Ts1atm - Ttr)/(P1atm - Ptr)
            TK = Ttr + dtdp*(P - Ptr)
    
        funct_tsat = lambda T: APXSCT(T)[0] - P    
        if P < 14:
            Tsat = newton(funct_tsat, TK*1.2, fprime=None, args=(), tol=1.48e-10, maxiter=100, fprime2=None)
        else:
            Tsat = newton(funct_tsat, TK, fprime=None, args=(), tol=1.48e-10, maxiter=100, fprime2=None)
    
    return float(Tsat)

vAPXSCP = np.vectorize(APXSCP) # vectorize the function above to permit array variables

def CALSCP(P):
    """
    #     This routine calculates the saturation properties as a function
    #     of specified pressure. This is done by iterating on pressure to
    #     obtain the desired temperature. This routine calls CALSCT to
    #     calculate the saturation pressure and densities (of vapor and
    #     liquid). There is no easy way to calculate the requisite
    #     derivative (dTsat/dPsat) in the refinement process.  Therefore,
    #     the secant method is used instead of the Newton- Raphson method.
    parameters:
       P       pressure [MPa]
    results:
       Tsat    temperature [K]
       rhosv   density of vapor [kg/m3]
       rhosl   density of liquid [kg/m3]
    """
    btxtol = 1.0e-10 
    itermx = 50
    # arelax = 1
    
    #  Calculate approximate saturation temperature to use as a starting estimate
    #  These results are not those of the IAPWS-95 model itself, but can serve as starting estimates.
    Tsat = APXSCP(P)
    # tau = Tc/Tsat
    #     Using the current temperature estimate, calculate
    #     the corresponding saturation properties. This will
    #     include a calculated pressure to compare with the specified pressure.
    #[Psat, rhosv, rhosl] = CALSCT(Tsat)
    
    #betamx = np.max(beta)
    funct_tsat = lambda T: CALSCT(T)[0] - P    
    Tsat = newton(funct_tsat, Tsat, fprime=None, args=(), tol=btxtol, maxiter=itermx, fprime2=None)
    
    [_, rhosv, rhosl] = CALSCT(Tsat)
    
    return float(Tsat), float(rhosv), float(rhosl)

vCALSCP = np.vectorize(CALSCP) # vectorize the function above to permit array variables

def CALSCT(TK):
    """
    #     This routine calculates the saturation properties as a function
    #     of specified temperature. This is done using Newton-Raphson
    #     iteration to refine values of pressure, vapor density, and
    #     liquid density, starting with results obtained using approximate
    #     equations included by Wagner and Pruss (2002) in their description
    #     of the IAPWS-95 model.
    parameters:
       TK      temperature [K]
    results:
       Psat    saturation pressure [MPa]
       rhosv   density of vapor [kg/m3]
       rhosl   density of liquid [kg/m3]
    """
    Tc = IAPWS95_COEFFS['Tc'] # K
    rhoc = IAPWS95_COEFFS['rhoc']
    bettl1 = 1.0e-8 
    bettl2 = 1.0e-7 
    btxtol = 1.0e-10 
    qxiter = False
    #tau = Tc/TK
    alpha = np.zeros([3, 1]); beta = alpha
    aamatr = np.zeros([3, 3])
    deltas = np.zeros([3, 1]); arelax = 1
    
    if (TK <= 298.15):
        btxtol = bettl1
    elif (647.090 < TK < Tc):
        qxiter = True
    elif (TK > 647.090):
        btxtol = bettl2

    #icutv = 0; icutl = 0;
    #  Calculate approximate saturation pressure and corresponding densities of liquid and vapor.
    #  These results are not those of the IAPWS-95 model itself, but can serve as starting estimates.
    [Psat, _, rhosl, rhosv] = APXSCT(TK)
    #  Save the values from the approximation.
    delta_svq = rhosv/rhoc
    delta_slq = rhosl/rhoc
    
    it = 0
    itermx = 35
    Psat0 = Psat
    deltasv0 = delta_svq
    deltasv = delta_svq
    deltasl0 = delta_slq
    deltasl = delta_slq
    

    while True:
        # Below is the return point to refine the saturation curve properties.
        # First calculate the vapor properties by calling EOSIAPW95 with the vapor density.
        
        [Pxv, axv, _, _, _, _, Pdxv, adxv, _] = EOSIAPW95(TK, rhosv)
        # Now calculate the liquid properties by calling EOSIAPW95 with the liquid density.
        [Pxl, axl, _, _, _, _, Pdxl, adxl, _] = EOSIAPW95(TK, rhosl)
        # The pdx for vapor cannot be negative. Under-relax to prevent this.
        if (Pdxv < 0):
            if (it <= 0):
                #if (icutv >= 30):
                #icutv = icutv + 1
                rhosv = 0.995*rhosv
                deltasv = rhosv/rhoc
            else:
                #icutv = icutv + 1
                arelax = 0.25
        #  The revised delta for vapor cannot be less than a good
        #  fraction of the value obtained from the intial approximation.
        if (deltasv < 0.9*delta_svq):
            arelax = 0.25
    
        #  The pdx for liquid cannot be negative. Under-relax to prevent this.
        if (Pdxl < 0):
            if (it <= 0):
                #if (icutl >= 30):
                #icutl = icutl + 1
                rhosl = 1.001*rhosl
                deltasl = rhosl/rhoc
            else:
                #icutl = icutl + 1
                arelax = 0.25
        # The revised delta for liquid cannot be less than a good
        # fraction of the value obtained from the intial approximation.
        if (deltasl > 1.1*delta_slq):
            arelax = 0.25
            
        #  The delta for liquid cannot be less than the delta for vapor.
        #  Corrected delta values must be positive to avoid
        #  a singularity in the equation-of-state model equations.
        #  Under-relax to prevent this.
        if (deltasl < deltasv) | (deltasv <= 0 or deltasl <= 0):
            arelax = 0.25
        
        deltas = deltas*arelax
        Psat = Psat0 + deltas[2]
        deltasl = deltasl0 + deltas[1]
        deltasv = deltasv0 + deltas[0]
        rhosl = rhoc*deltasl
        rhosv = rhoc*deltasv
            
    # =============================================================================
    #     Have obtained valid (outside the unstable zone) vapor and liquid properties for the current iteration. Improve the
    #     calculated saturation properties by solving three equations in three unknowns. The equations are all in terms of pressure.
    #     The unknowns to be found are Psat, deltasv, and deltasl.    
    #     Calculate the Maxwell crition pressure (Gibbs energy equality expressed through the
    #     Helmholtz energies and the pressure)     
    # =============================================================================
    
        dix = (1/deltasl) - (1/deltasv)
        # dltx = deltasl - deltasv
        if (abs(dix) > 1e-15):
            if (abs(axv - axl) > 1e-15):
                # Normal calculation, result in kPa.  
                Pxm = rhoc*( axv - axl )/dix
                Pxm = 0.001*Pxm     #    Convert from kPa to MPa.
            else:
                # There is no difference in the Helmholtz energies of the vapor and the liquid.
                Pxm = 0
        else:
            # Exception intended for the critical point.
            if (abs(TK - Tc) <= 1e-10) & (abs(deltasv - 1.0) <= 1e-10) & (abs(deltasl - 1) <= 1e-10): 
                #   Am at the critical point.    
                Pxm = Pxv
            else:    
                # Not at the critical point, but the vapor and liquid densities have converged.  
                Pxm = 0
    
        # Calculate residual functions.
        alpha[0] = Pxm - Psat
        alpha[1] = Pxv - Psat
        alpha[2] = Pxl - Psat
    
        beta = abs(alpha/Psat)
        betamx = np.max(beta)
        # Note: using a convergence tolerance below 1.0d-11
        # may lead to non-convergence due to the limitations of 64-bit arithmetic.
        
        # print(it, ' Psat: %.6e' % Psat[0], 'betamx:  %.6e' % betamx)
        if (betamx <= btxtol) | (it >= itermx):
            # Iteration has converged.
            # P = Psat
            break
        elif (qxiter and it >= 5):
            break
            
        # Since this matrix is only 3x3, solve using Gaussian
        # elimination with partial pivoting. There is no need
        # to use the "Jordan" part of the Gauss-Jordan algorithm for a matrix of this size.    
        # The simultaneous equations have the form:    
        # aamatr(i,1)*deltas(1) + aamatr(i,2)*deltas(2) + aamatr(i,3)*deltas(3) = -alpha(i), i = 1,3
        # The Jacobian matrix J here is aamatr(kdim,kdim).    
        aamatr[0, :] = [Pxm*(-(1/(dix*deltasv**2)) + (adxv/(axv - axl))), 
                        Pxm*((1/(dix*deltasl**2)) - (adxl/(axv - axl))),
                        -1]
        aamatr[1, :] = [Pdxv, 
                        0, 
                        -1]
        aamatr[2, :] = [0, 
                        Pdxl, 
                        -1]
        
        deltas = -lu_solve(lu_factor(aamatr), alpha)
        #  Save current values.
        Psat0 = Psat
        deltasv0 = deltasv
        deltasl0 = deltasl
        Psat = Psat0 + deltas[2]
        deltasl = deltasl0 + deltas[1]
        deltasv = deltasv0 + deltas[0]
        rhosl = rhoc*deltasl
        rhosv = rhoc*deltasv

        it = it + 1
    
    return float(Psat), float(rhosv), float(rhosl)

vCALSCT = np.vectorize(CALSCT) # vectorize the function above to permit array variables

def CALPRE(T, P, *rho0):
    """
    #     Find the thermodynamic properties of water at given temperature
    #     and pressure. The problem reduces to finding the value of reduced
    #     density (delta) that is consistent with the desired pressure.
    #     The Newton-Raphson method is employed. Under-relaxation techniques
    #     are required to assure that delta or other requisite parameters
    #     do not take on out-of-bounds values in the iteration process.
    #     Small negative values of calculated pressure are okay. Zero or
    #     negative values for calculated "pdx" (pressure derivative with
    #     respect to delta) imply the unstable zone and must be avoided.
    parameters:
        T       temperature [C]
        P       pressure [bar]
        rho0    starting estimate of density [kg/m3] (optional)
    results:
        rho     density [kg/m3]
        gx      Gibbs energy (cal/mol)
        hx      Enthalpy (cal/mol)
        sx      Entropy (cal/mol/K)
        vx      Volume (m3/mol)
        Pout    pressure [bar]
        Tout    temperature [C]
     #  ux      Internal energy (cal/mol)
     #  cpx     Isobaric heat capacity (cal/mol/K)
     #  visc    viscosity (Pa-s)
    The general usage of CALPRE is as follows:
    (1) Not on steam saturation curve:
        out = CALPRE(T, P), where T is temperature in celsius and P is pressure in bar;
    (2) On steam saturation curve:
        out = CALPRE(T,'T'), where T is temperature in celsius, followed with a quoted char 'T'; 
        out = CALPRE(P,'P'), where P is pressure in bar, followed with a quoted char 'P'.
    """ #rho, gxcu, hxcu, sxcu, vxcu

    if type(P) == str:
        if P == 'T':
            P, _, _ = vCALSCT(celsiusToKelvin(T))
            P = np.ravel(P)*10  #Convert MPa to bars            
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = T   # Assign first input T to pressure in bar
            T, _, _ = vCALSCP(P*0.1)
            T = T - 273.15 # convert from K to celcius

    if (np.ndim(T) == 0) | (type(P) != str and np.ndim(P) == 0):
        T = np.array(T).ravel()
        P = np.array(P).ravel()

    Pout = P # cases where 'T' is used as input
    Tout = T # cases where 'P' is used as input
    TK = celsiusToKelvin(T)
    P = P * 0.1  #Convert bars to MPa  
    Tc = IAPWS95_COEFFS['Tc'] # K
    Pc = IAPWS95_COEFFS['Pc'] # MPa
    rhoc = IAPWS95_COEFFS['rhoc']
    R = IAPWS95_COEFFS['R']*1e-3
    if len(rho0) == 0:
        itermx = 60
    else:
        itermx = 80
    btxtol = 1.0e-10
    # deltol = 1.0e-12
    # icdlta = 0
    
    # Obtain a description (udescr) of the H2O fluid.
    rho = np.zeros([len(TK), 1]).ravel()
    for i in range(len(TK)):
        if len(rho0) == 0:
            [udescr, rhosl, rhosv] = fDescr(P[i], TK[i])
        else:
            [udescr, rhosl, rhosv] = fDescr(P[i], TK[i], rho0[i])
    
        if (udescr == 'vapor'):
            #   Vapor: assume ideal gas behavior.
            #   Ideal gas.
            rho[i] = 1000.0*P[i]/(TK[i]*R)
            #   Alternate: ideal gas correction to saturation density (slightly different).
            #   rho = (P/Psat)*rhosv
        elif (udescr == 'liquid'):
            #   Liquid: use a liquid-like density.
            #   The liquid density on the saturation curve.
            rho[i] = rhosl
            #   Something slightly greater than the saturation density.
            #   rho = 1.0002*rhosl
        elif (udescr == 'compressed liquid'):
            #   Estimate the density of compressed liquid.
            #   Ideal gas correction to critical point density.
            #   This produces one of two false solutions at 500K and 25 MPa. Do not use this.
            #   rho = (P/Pc)*(Tc/TK)*rhoc
            
            #   Twice the ideal gas correction to the critical point density.
            #   rho = 2.0*(P/Pc)*(Tc/TK)*rhoc      
            #   The saturated liquid density is a minimum value.
            rho[i] = 1.10*rhosl
            #       Close to the upper limit for this field
            #       (T near the triple point, 1000 MPa).
            #       For higher pressure, a higher value might be needed.
            #       rho = 1250.0d0
            rho[i] = np.maximum(rho[i], rhosl)
            rho[i] = np.minimum(rho[i], 1400)
        elif (udescr == 'supercritical fluid'):
            #   Estimate the density of supercritical fluid.
            #   Ideal gas.
            #   rho = 1000.0*P/(TK*R)
            #   SUPCRT92 estimate, about 15% higher than ideal gas.
            #   rho = 2500.0*P/TK
            #   Ideal gas correction to critical point density.
            #   rho = (P/Pc)*(Tc/TK)*rhoc
            #   Twice the ideal gas correction to the critical point density.
            rho[i] = 2.0*(P[i]/Pc)*(Tc/TK[i])*rhoc
            #   Close to the upper limit for this P, T field.
            #   (T near the critical point, 1000 MPa).
            #   Calculated value of 1068.7 kg/m3 is rounded up.
            #   For higher pressure, a higher value might be needed.
            #   rho = 1100.0d0
            rho[i] = np.minimum(rho[i], 1100.0)
        elif (udescr == 'hot vapor'):
            #   Estimate the density of hot vapor.
            #   Ideal gas.
            rhoidg = 1000.0*P[i]/(TK[i]*R)
            #   SUPCRT92 estimate, about 15% higher than ideal gas.
            rhosup = 2500.0*P[i]/TK[i]
            #   Ideal gas correction to critical point density.
            rhocpa = (P[i]/Pc)*(Tc/TK[i])*rhoc
            #   The upper limit for this field, the critical pressure (rhocr), 22.064 MPa.
            #   rho = rhocr
            if (P[i] <= 1.0):
                rho[i] = rhoidg
            elif (P[i] <= 18.0):
                rho[i] = rhosup
            else:
                rho[i] = rhocpa
            rho[i] = np.minimum(rho[i], rhoc)
        else:
            #  The H2O fluid type could not be determined. A good starting estimate of density could not be established
            #  Will try four times the critical density.
            rho[i] = 4*rhoc
        
    funct_tsat = lambda rho: vEOSIAPW95(TK, rho)[0] - P    
    rho = newton(funct_tsat, rho, fprime=None, args=(), tol=btxtol, maxiter=itermx, fprime2=None)
    #delta = rho/rhoc
    [Px, _, sx, hx, gx, vx,  _, _, _] = vEOSIAPW95(TK, rho)
    # visc = waterviscosity(TK-273.15, P, rho)[0]
    
    # The following data are in kilogram units.
    ds = 3.5156150 # reference state Entropy (kJ/kg/K)
    # du = -15767.19391 # reference state Internal energy (kJ/kg)
    dh = -15970.89538 # reference state Enthalpy (kJ/kg)
    # da = -11906.84446 # reference state Helmholtz energy (kJ/kg-K)
    dg = -12110.54592 # reference state Gibbs energy (kJ/kg)
    htripl = 0.000611782 # reference state 
    mwH2O = 18.01528
    #     Calculate the entropy, internal energy, enthalpy, Helmholtz
    #     energy, and Gibbs energy on the standard thermochemical scale
    #     using offset parameters. Results are still in kilogram units.
    sxcu = sx + ds
    # uxcu = ux + du
    hxcu = (hx - htripl) + dh
    # axcu = ux - TK*sxcu + da
    gxcu = hx - TK*sxcu + dg

    #     Calculate the entropy, internal energy, enthalpy, Helmholtz
    #     energy, Gibbs energy, volume, heat capacity at constant volume,
    #     heat capacity at constant pressure, and isothermal throttling
    #     coefficient (dt) in molar units.
    sxcu = (mwH2O/1000)*sxcu
    # uxcu = (mwH2O/1000)*uxcu
    hxcu = (mwH2O/1000)*hxcu
    # axcu = (mwH2O/1000)*axcu
    gxcu = (mwH2O/1000)*gxcu
    vxcu = (mwH2O/1000)*vx
    # cpxcu = (mwH2O/1000)*cpx

    #     Adjust all values from kJ units to cal.
    sxcu = 1000*(sxcu)/J_to_cal
    # uxcu = 1000*(uxcu)/J_to_cal
    hxcu = 1000*(hxcu)/J_to_cal
    # axcu = 1000*(axcu)/J_to_cal
    gxcu = 1000*(gxcu)/J_to_cal
    vxcu = 1000*(vxcu)/J_to_cal
    # cpxcu = 1000*(cpx)/J_to_cal
    
    return rho, gxcu, hxcu, sxcu, vxcu, Pout, Tout

def DIELEC(T, rho):
    """
    #     This function calculates the dielectric constant of water
    #     (epsx) as a function of temperature and density. The model is
    #     described in "Release on the Static Dielectric Constant of
    #     Ordinary Water Substance for Temperatures from 238 K to 873 K
    #     and Pressures up to 1000 MPa" (IAPWS R8-97, 1997). The
    #     corresponding journal reference is Fernandez D. P.,
    #     Goodwin A. R. H., Lemmon E. W., Levelt Sengers J. M. H.,
    #     and Williams R. C. (1997) A Formulation for the Static
    #     Permittivity of Water and Steam at Temperatures from 238 K
    #     to 873 K at Pressures up to 1200 MPa, Including Derivatives
    #     and Debye-HÂckel Coefficients. J. Phys. Chem. Ref. Data 26,
    #     1125-1166.  The equations used for the dielectric constant
    #     here are taken directly from IAPWS R8-97.
    
    #     This routine also calculates some closely related water
    #     properties. Equations for The temperature and pressure
    #     derivatives of the dielectric constant and the Debye-Huckel
    #     "A" parameters are taken from Fernandez et al. (1997). These
    #     equations are not sanctioned by the IAPWS. Equations for the
    #     Debye-Huckel "B" parameters and some Born functions are taken
    #     from Helgeson H. C. and Kirkham D. H. (1974) Theoretical
    #     Prediction of the Thermodynamic Behavior of Aqueous Electrolytes
    #     at High Pressures and Temperatures: II. Debye-Huckel Parameters
    #     for Activity Coefficients and Relative Partial Molal Properties.
    #     Am. J. Sci. 274, 1199-1251. The values of associated constants
    #     are updated to values consistent with Fernandez et al. (1997).
    # parameters:
    #   rho      density [kg/m3]
    #   T        temperature [C]
    # results:
    #   epsx     dielectric constant of water [cal/mol] 
    #   Ah       Debye-Huckel "A" parameters [kg^1/2 mol^-1/2]
    #   Bh       Debye-Huckel "B" parameters [kg^1/2 mol^-1/2 Angstrom^-1]
    """
    if np.ndim(T) == 0 | np.ndim(rho) == 0:
        T = np.array(T).ravel()
        rho = np.array(rho).ravel()
    else:
        T = T.ravel()
        rho = rho.ravel()
    TK = celsiusToKelvin(T)
    Tc = IAPWS95_COEFFS['Tc'] # K
    # Pc = IAPWS95_COEFFS['Pc'] # MPa
    rhoc = IAPWS95_COEFFS['rhoc'] # kg/m3
    mwH2O = 18.01528/1000  # kg/mol
    Nh = np.array([0.978224486826, -0.957771379375e0, 0.237511794148e0, 0.714692244396e0,
          -0.298217036956, -0.108863472196e0, 0.949327488264e-01, -0.980469816509e-02,
        0.165167634970e-04, 0.937359795772e-04, -0.123179218720e-09, 0.196096504426e-02])
    ih = np.array([1, 1, 1, 2, 3, 3, 4, 5, 6, 7, 10])
    jh = np.array([0.25, 1.0, 2.5, 1.5, 1.5, 2.5, 2.0, 2.0, 5.0, 0.5, 10.0])

    alphammp = 1.636e-40    # Mean molecular polarizability C^2 J^-1 m^2
    xmudp = 6.138e-30       # Molecular dipole moment C.m
    e = 1.6021773310e-19    # elementary charge e is 1.602176634 x 10^-19 coulomb (C)
    kB = 1.380658e-23        # Boltzmann constant k is 1.380649 x 10^-23 J K^-1
    NA = 6.0221367e23       # Avogadro constant NA is 6.02214076 x 10^23 mol^-1

    #  expression for calculating the permittivity of free space, eps0.
    c = 2997924580.0 # the speed of light in classical vacuum  m/s
    mu0 = 4.0e-07 * np.pi # the permeability of free space N Angstrom^-2

    #  Slightly different values for some of these constants were used by Helgeson & Kirkham (1974b). 
    #  Relevant 2019 redefinitions (for information only):
    rhom = rho/mwH2O
    rhocrm = rhoc/mwH2O

    # Get the permittivity of free space (eps0). eps0 = 1/(mu0*c^2)
    # Note: the constant below is c/10.
    eps0 = 1/( mu0 * (c * 0.1)**2) # C^2 J^-1 m^-1
    delta = rhom/rhocrm
    tau = Tc/TK

    # Get the Harris and Alder g factor (gha).
    g = 1 + np.sum(Nh[:-1].reshape(-1,1)*(delta**ih.reshape(-1,1))*(tau**jh.reshape(-1,1)),0) + \
        Nh[-1]*delta*((TK/228) - 1)**(-1.2)
    # Get A and B.
    A = ( NA*(xmudp**2)*rhom*g )/( eps0*kB*TK )
    B = ( NA*alphammp*rhom )/( 3*eps0 )
    # Get the dielectric constant (Epsx).
    Epsx = ( 1 + A + 5*B + np.sqrt(9 + 2*A + 18*B + A**2 + 10*A*B + 9*B**2) )/( 4 - 4*B )

    # Calculates the set of Debye-Huckel coefficients and bdot at given temperature T and pressure P
    # Get the Debye-Huckel Agammae, Aphi and Agamma10 parameters
    # Units are kg^0.5 mol^-0.5 for all three.
    Agammae = np.sqrt(2*np.pi*NA*rho)*((e**2/( 4*np.pi*Epsx*eps0*kB*TK ))**1.5)
    Agamma10 = Agammae/np.log(10)
    Ah = Agamma10
    # Aphi = Agammae/3

    #     Get the Debye-Huckel B(gamma) parameter (bgm).
    #     Units are kg^0.5 mol^-0.5 cm^-1 (Helgeson and Kirkham, 1974b). The values tabulated by Helgeson
    #     and Kirkham in their Table 2 are consistent with units of kg^0.5 mol^-0.5 Angstrom^-1 (see below).    
    #     The tabulated values are described by these authors as "Bh in (kg^0.5 mol^-0.5 cm^-1) x 10^-8".
    #     Technically these values correspond to Bh x 10^+8 for the units specified ("... cm-1")
    #     in the table's title. There is a similar issue
    #     with power of 10 multipliers in regard to BV in their Table 12 and BH in their Table 4.

    kB_erg = kB * 1e7  # Get Boltzmann's constant in erg K^-1. The constant represented by k is in J K^-1.
    rho = rho/1000     # converts rho from kg m^-3 to g cm^-3

    #     Get Bh in units of kg^1/2 mol^-1/2 Angstrom^-1.
    #     This is more traditional (e.g., Helgeson and Kirkham, 1974b)
    #     for usage in calculating activity coefficients.
    #     1 Angstrom(A) = 10^-8 cm. The multiplier below is 10^-8 cm Angstrom^-1.
    Bh = 1.0e-08*np.sqrt((8*np.pi*NA*rho*((c*e)**2))/(1000*Epsx*kB_erg*TK))
    
    # Calculates bdot  Ref: Helgeson H.C.,1969, American Journal of Science, Vol.267, pp:729-804
    b = [0.0374e0, 1.3569e-4, 2.6411e-7, -4.6103e-9]    
    bdot = np.where(T>=300, 0, b[0] + b[1]*(T-25.0) + b[2]*(T-25.0)**2 + b[3]*(T-25.0)**3)
    
    # b = [15698.4, 41.8088, 0.0367626, 974169.0, 268.902] 
    # bdot = np.where(T>=300, 0, b[0]*(TK)**(-1) + b[1]*np.log(TK) - \
    #                  b[2]*(TK) - b[3]*(TK)**(-2) - b[4])

    return Epsx, Ah, Bh, bdot

def drummondgamma(TK, I):
    """
    function to model solubility of gases in brine using Drummond equation
    # parameters:
    #   TK          Temperature [K]
    #   I           ionic strength
    # results:
    #   gamma       co2 aqueous activities
    """
    
    # A=20.244
    # B=-0.016323
    C=-1.0312
    # D=-3629.7
    E=0.4445
    F=0.0012806
    G=255.9
    H=-0.001606
    
    #gamma=(((C + F*TK + (G/TK))*I - (E + H*TK)*(I / (I + 1)))/np.log(10))
    gamma = np.log10(np.exp((C + F*TK + (G/TK))*I - (E + H*TK)*(I / (I + 1))))
    return gamma

def Henry_duan_sun(TK, P, I):
    """
    function to model solubility of gases in brine, mainly Duan_Sun
    Solubility of CO2 in NaCl solutions in mol/kg water Duan & Sun
    (2003) Chemical Geology 193 (2003) 257-271
    # parameters:
    #   TK          Temperature [K]
    #   P           pressure [bar]
    #   I           ionic strength
    # results:
    #   co2_coef    co2 aqueous activities
    """
    # convert Pressure to CO2 partial pressure
    [fCO2, denCO2, SI, pCO2] = co2fugacity(TK, P)
    
    mNaCl = 2 * I/ ((1)**2 + (-1)**2)
    # par1 = np.array([28.9447706, -0.0354581768, -4770.67077, 0.0000102782768,
    #                  33.8126098, 0.0090403714, -0.00114934031, -0.307405726,
    #                  -0.0907301486, 0.000932713393, 0])

    par2 = np.array([-0.411370585, 0.000607632013, 97.5347708, 0, 0, 0, 0,
                     -0.0237622469, 0.0170656236, 0, 0.0000141335834])

    par3 = np.array([0.000336389723, -0.000019829898, 0, 0, 0, 0, 0,
                     0.0021222083, -0.00524873303, 0, 0])
    
    fTP = [1*np.ones([1, len(TK)]), TK, 1/TK, TK**2, 1/(630-TK), pCO2, pCO2*np.log(TK), 
           pCO2/TK, pCO2/(630-TK), (pCO2/(630-TK))**2, TK*np.log(pCO2)]
            
    
    # muCO2 = np.sum(par1 * fTP)      # mu0/RT
    laCO2Na = np.sum(par2 * fTP)    # lambda_CO2-Na Pitzer 2nd order int. param.
    xiCO2NaCl = np.sum(par3 * fTP)  # zeta_CO2-Na-Cl Pitzer 3rd order int. param.
    
    # activity coef. aqueous co2
    # Honoring limits for Duan and Sun - P-T-X range (0– 2000 bar, 0–260°C, 0–4.3 m NaCl)
    lngamco2 = np.zeros([len(I), len(TK)])
    for j in range(len(I)):
        for i in range(len(TK)):
            if (TK[i] <= celsiusToKelvin(260)) and (I[j] <= 4.3) and (P[i] <= 2000):
                lngamco2[j, i] = 2*mNaCl[j]*laCO2Na.ravel()[i] + xiCO2NaCl.ravel()[i]*mNaCl[j]**2
            else:
                lngamco2[j, i] = 0

    co2coef =  np.log10(np.exp(lngamco2))  #  ((lngamco2)/np.log(10))  #  
    
    return co2coef

def co2fugacity(TK, P):
    """
    function to Computes the fugacity and density of CO2 by Duan and Sun 2003
    Also Calculate the Saturation Index SI and partial pressure of CO2(g) given T, P and using 
    the Duan equation of state. A Poynting correction factor is also applied.
    # parameters:
    #   TK          Temperature [K]
    #   P           pressure [bar]
    # results:
    #   fCO2       co2 fugacity
    #   denCO2     co2 density [g/cm3]
    #   SI         co2 saturation index [bar]
    #   pCO2       co2 partial pressure [bar]
    """
    R = 0.08314467  #bar*L/mol/K
    PcCO2 = 73.8    ## bar
    TcCO2 = 304.15  ## K
    VcCO2 = R * TcCO2 / PcCO2  # L/mol
    
    Pr = P / PcCO2 # bar
    Tr = TK / TcCO2
    xmwc = 4.40098e2  # g/mol
    
    a1  =  0.0899288497
    a2  = -0.494783127
    a3  =  0.0477922245
    a4  =  0.0103808883
    a5  = -0.0282516861
    a6  =  0.0949887563
    a7  =  0.00052060088
    a8  = -0.000293540971
    a9  = -0.00177265112
    a10 = -0.0000251101973
    a11 =  0.0000893353441
    a12 =  0.0000788998563
    alpha = -0.0166727022
    beta =  1.398
    gamma =  0.0296
    
    
    zfun = lambda Z : -Z + (1 + (a1 + a2 / Tr ** 2 + a3 / Tr ** 3) / Z / Tr * Pr +
                            (a4 + a5 / Tr ** 2 + a6 / Tr ** 3) / Z ** 2 / Tr ** 2 * Pr ** 2 +
                            (a7 + a8 / Tr ** 2 + a9 / Tr ** 3) / Z ** 4 / Tr ** 4 * Pr ** 4 +
                            (a10 + a11 / Tr ** 2 + a12 / Tr ** 3) / Z ** 5 / Tr ** 5 * Pr**5 +
                            alpha / Tr ** 3 / Z ** 2 / Tr ** 2 * Pr ** 2 * (beta + gamma / 
                                                                            Z ** 2 / Tr ** 2 * Pr ** 2)
                            * np.exp(-gamma / Z ** 2 / Tr ** 2 * Pr ** 2))

    Z = fsolve(zfun, [0.001]*len(TK), xtol=1.0e-10, factor=10)    
    Vr = Z * Tr / Pr
    V = Vr * VcCO2  ## L / mol
    
    phi = np.exp(Z - 1 - np.log(Z) + (a1 + a2 / (Tr ** 2) + a3 / (Tr ** 3)) / Vr + \
                 (a4 + a5 / (Tr ** 2) + a6 / (Tr ** 3)) / (2 * Vr ** 2) + \
                     ((a7 + a8 / (Tr ** 2) + a9 / (Tr ** 3)) / (4 * Vr ** 4) + \
                      (a10 + a11 / (Tr ** 2) + a12 / (Tr ** 3)) / (5 * Vr ** 5) + \
                          alpha / (2 * Tr ** 3 * gamma) * (beta + 1 - (beta + 1 + gamma / Vr ** 2) * \
                                                           np.exp(-gamma / Vr ** 2))))
    #-----fugacity
    fCO2 = phi * P  #bar
    
    #-----density
    denCO2 = xmwc*1e-3 / V # g/cm^3
    Poy = 1
    Patm = P / 1.01325
    poy = True
    if (poy):
        R =   8.205746E-2 ## L atm /K/mol
        Vm = np.where(V < 0, 32.0e-3, V)   ## L / mol
        Poy = np.exp(-(Patm - 1)*Vm/R/TK)
    pCO2 = phi*Patm*Poy  # atm
    SI = np.log10(pCO2)
    pCO2 = pCO2 * 1.01325  # bar
    
    return fCO2, denCO2, SI, pCO2
                
def gamma_correlation(T, P, method):
    """
      * Calculates the CO2 activity correlation coefficients at given temperature T and pressure P
      ******************************************************************
      ******************************************************************
      *  Ref1:
      *  Segal Edward Drummond, 1981, Boiling and Mixing of Hydrothermal
      *  Fluids: Chemical Effects on Mineral Precipitation, page 19
      *  Ref2:
      *  Wolery, T. J., Lawrence Livermore National Laboratory, United 
      *  States Dept. of Energy, 1992. EQ3/6: A software package for 
      *  geochemical modeling of aqueous systems: package overview and 
      *  installation guide (version 7.0)
      ******************************************************************
    # parameters:
    #   T           Temperature [C]
    #   P           pressure [bar]
    #   method      activity model [Duan_Sun or Drummond]
    # results:
    #   cco2       co2 correlation coefficients
    """
    
    if np.ndim(T) == 0 | np.ndim(P) == 0:
        T = np.array(T).ravel()
        P = np.array(P).ravel()
        
    TK=celsiusToKelvin(T)
    #   assign ionic strength from 0 to 3
    N = 100
    I = np.zeros([N, 1])
    for i in range(N):
        I[i] = 3 * i /(N - 1)
        
    
    A = np.zeros([3, 3])
    B = np.zeros([3, 1])
    cco2 = np.zeros([4, len(TK)])
    if method == 'Duan_Sun':
        ccoef = Henry_duan_sun(TK, P, I)
    elif method == 'Drummond':
        ccoef = drummondgamma(TK, I)
        
    for i in range(len(TK)):
        for ii in range(3):
            for jj in range(3):
                A[ii, jj] = np.sum(I**((ii+1) + (jj+1)))
                B[ii] = np.sum(ccoef[:, i]*I.ravel()**(ii+1))
        Coef = lu_solve(lu_factor(A), B)
        cco2[:3, i] = Coef[0], Coef[1], Coef[2]

    return cco2

def wateract(T, P, I, chargedic, **DB):
    # work in progress   
    if DB.__len__() != 0:
        Ah = DB['Ah'].ravel()
        Bh = DB['Bh'].ravel()
        bdot = DB['bdot'].ravel()
    else:
        [rho, _, _, _, _, P, T] = CALPRE(T, P)
        [_, Ah, Bh, bdot] = DIELEC(T, rho) 

    z = [float(chargedic['Cl-'].split()[1]), float(chargedic['Na+'].split()[1])]
    mNaCl = 2 * I/ ((z[0])**2 + (z[1])**2)
    cp = np.concatenate([mNaCl, mNaCl], axis=1)
    # ion_size = 4 # Angstrom(A)
    mwH2O = 18.01528/1000  # kg/mol
    xH2O = 1/mwH2O
    
    #  Polynomial regression coefficients a, b, c, d, e, and f are below to calculate 
    #  parameters as:   parameter(T in C) = a + b*T + c*T**2 + d*T**3 + e*T**4
    # aft = [0.49276542e+00, 0.31857945e-03, 0.11628933e-04,
    #        -0.52038832e-07, 0.12633045e-09]
    # bft = [0.32476341e+00, 0.12018502e-03, 0.79530646e-06,
    #        -0.30410531e-08, 0.56931304e-11]
    bift = [0.26538636e+01, -0.52889569e-02, -0.11009615e-03, 
            0.46820513e-06, -0.11104895e-08]
    bilft = [-0.14769091e+02,0.22563951e+00,-0.10225385e-02,
             0.30349650e-05,-0.35468531e-08]
    R = 1.9872041
    #  bhat NaCl (b NaCl = bhat/(2.303RT)) from Table 29 (bi here) in kg/mol * 1e+3
    #  b Na+Cl- from Table 30 (bil here) in kg/mol * 1e+2
    #  Data range from 0 to 300 C, but the data available to fit bi and bil did not 
    #  go down to T = 0 C (first point at 25 C) so the extrapolation may not be too good 
    #  below 25 C. However, the extrapolated bi and bil values below 25 C vary smoothly down to 0 C.
    polyfunc = lambda x, *a: a[0] + (a[1] + (a[2] + (a[3] + a[4]*T)*T)*T)*T
    bihat = polyfunc(T, *bift)*1e-3
    bi = bihat/(2.303*R*celsiusToKelvin(T))  #[kg cal/mol]
    bil = polyfunc(T, *bilft)*1e-2
    # Ah = polyfunc(T, *aft)
    # Bh = polyfunc(T, *bft)
    
    #  Rej from Table 3 (input in thermodynamic database to calculate a0 using eq. 125,
    #  assuming other dominant anion (for cations) is Cl- (rej = 1.81 A)
    #  and cation (for anions) is Na+ (rej = 1.91 A)
    
    # ---calculate ionic strength and other global concentrations
    #    all concentrations must be in molal scale (mol/kgH2O) 
    summ=0.              #to compute true ionic strength
    mstar=0             #total solute in solution
    mchr=0              #total solute excluding neutral species
    cpion = np.ones_like(cp)
    zi_sqrd = np.ones_like(z)
    #     primary species contribution 
    for i in range(2):
        zi_sqrd[i] = z[i]*z[i]
        summ = summ + zi_sqrd[i] * cp[:, i]
        mstar = mstar + cp[:, i]
        if (z[i] != 0): mchr = mchr + cp[:, i]
        cpion[:, i] = cp[:, i]
    summ = np.where(summ < 0, 0, summ)
    strI = 0.5*summ
    stroot = np.sqrt(strI)
    
    capgam = -np.log10(1 + mwH2O * mstar)

    # ---activity and osmotic coefficients     
    summt = 0.           #eq. 190 sum term for osmotic coef
    azero = 0.  
    for i in range(2):
        rej=1.810                    #       Rej of Cl-
        if (z[i] > 0): rej=1.910     #       Rej of sodium
        zabsi = np.abs(z[i])
        azero = 2*(azero + zabsi*rej)/(zabsi + 1)
            
        lambda1 = 1 + Bh*azero*stroot.reshape(-1,1)
        omega = 1.66027e+5*zi_sqrd[i]/rej
    #     if (z[i] != 0):
    # #   --- charged species (gammas from eq. 165/166 simplified as in 298
    # #       without making assumption of only monovalent species;
    # #       osmotic coefficient from 190, with same simplifications as for
    # #       gammas but using stoich.ioni# strength - see summt below) 
    # #         omega is absolute Born coefficient in [cal/mol]
    #         omega = 1.66027e+5*zi_sqrd[i]/rej
    #         gamlog = -Ah.reshape(1, -1)*zi_sqrd[i]*stroot.reshape(-1, 1)/lambda1  +  capgam.reshape(-1, 1)  + \
    #             (omega*bi + (bil - 0.19*(zabsi - 1)))*strI.reshape(-1, 1)
 
    #           summt is summation term for osmotic coef and water
    #           activity calculation.  Use stoich ionic str here so
    #           that effect of ion association is minimized (i.e. there
    #           are no other solvent species to compensate as would be
    #           the case for solute species).
        summt = summt + cpion[:,i].reshape(-1, 1) * ( Ah.reshape(1, -1)*zi_sqrd[i] / ((azero*Bh)**3*strI.reshape(-1, 1)**2) *\
            (lambda1 - 1/lambda1 - 2*np.log(lambda1)) + \
                (capgam/(mwH2O*mstar) - \
                 (0.5*(omega*bi*strI.reshape(-1, 1) + \
                       (bil - 0.19*(zabsi - 1))*mchr.reshape(-1, 1)*0.5)).T).T)
    #     activity of water (Note: value of mstar here will
    #     strongly affect the value of the osmotic coefficient but
    #     does not affect the activity of water because mstar cancels out in the final expression)
    osmo = -np.log(10)*summt/mstar.reshape(-1, 1)
    osmo = np.where(np.isnan(osmo), 0, osmo)
    #     **note: for water, gamp is activity, NOT the coefficient!
    aw = np.exp(-osmo*mstar.reshape(-1, 1)/xH2O)
    aw = np.where(np.isnan(aw), 1, aw)

    # sigma = lambda x: (3/x**3)*(1 + x - (1/(1 + x)) - 2*np.log(1 + x))
    # sigma_x = np.where(I==0, 0, sigma(azero*Bh*np.sqrt(I)))
    # aw2 = 10**(mwH2O * ((-2*mNaCl/np.log(10)) + (2/3)*Ah*I**(3/2)*sigma_x - Bh*I**2))

    return aw, osmo

def aw_correlation(T, P, chargedic, **DB):
    """
      * Calculates the water activity correlation coefficients 
    at given temperature T and pressure P
    # parameters:
    #   T           Temperature [C]
    #   P           pressure [bar]
    #   method      co2 activity model [Duan_Sun or Drummond]
    # results:
    #   cco2       co2 correlation coefficients
    """
    
    if DB.__len__() != 0:
        Ah = DB['Ah'].ravel()
        Bh = DB['Bh'].ravel()
        bdot = DB['bdot'].ravel()
    else:
        [rho, _, _, _, _, P, T] = CALPRE(T, P)
        [_, Ah, Bh, bdot] = DIELEC(T, rho) 
        DB = {'Ah': Ah, 'Bh': Bh, 'bdot': bdot}

    if np.ndim(T) == 0 | np.ndim(P) == 0:
        T = np.array(T).ravel()
        P = np.array(P).ravel()
        
    #   assign ionic strength from 0 to 3
    N = 100
    Is = np.zeros([N, 1])
    for i in range(N):
        Is[i] = 3 * i /(N - 1)
    mwH2O = 18.01528/1000  # kg/mol
    xH2O = 1/mwH2O
        
    # %% Water Activity
    aw, osmo = wateract(T, P, Is, chargedic, **DB)
    ch20 = np.zeros([4, len(T)])
    for i in range(len(T)):
        ch20func = lambda Is, Aw, *x: (-Is/xH2O * \
                                         (1 - (np.log(10)*Ah[i]/(x[0]**3*Is)) * \
                                          ((1 + x[0]*Is**0.5) - 2*np.log(1 + x[0]*Is**0.5) - \
                                           (1/(1 + x[0]*Is**0.5))) + \
                                              (x[1]*Is/2) + (2/3*x[2]*Is**2) + (3/4*x[3]*Is**3) ) ) - np.log(Aw)
        ch20[:, i], pcov = curve_fit(ch20func, Is.ravel(), np.log(aw[:, i]), 
                                     p0=[1.454, 0.02236, 9.380e-3, -5.362e-4], 
                                     # bounds=[[1, 0.01, 1e-3, -4], [2, 0.2, 2e-2, 4]], 
                                     maxfev = 1000000)    
    return ch20

# %--------------------------------------------------------

def supcrtaq(TC, P, species, **rhoE):
    """
    function for calculating Gibbs free energy of aqueous species at T and P 
    using the revised HKF equation of state.
    Written by Ben Tutolo at UMN.  Last updated 21 Feb 2013
    # parameters:
       P        pressure [bar]
       T        temperature [C]
       species  properties such as [dG [cal/ml], dH [cal/mol], S [cal/mol-K], V [cm3/mol]
                       a [cal/mol-K],b [*10**3 cal/mol/K**2],c [*10^-5 cal/mol/K] )
       rhoE     dictionary of water properties like density (rho) and dielectric factor (E) 
                   (optional)
    # results:
       delG     Gibbs energy 
    The general usage of supcrtaq without the optional arg is as follows:
    (1) Not on steam saturation curve:
        out = supcrtaq(TC, P, species), where T is temperature in celsius and P is pressure in bar;
    (2) On steam saturation curve:
        out = supcrtaq(TC, 'T', species), where T is temperature in celsius, followed with a quoted char 'T'; 
        out = supcrtaq(P, 'P', species), where P is pressure in bar, followed with a quoted char 'P'.
    """        
    delGref=species[2] 
    # Href=species[3] 
    Sref=species[4] 
    a1=species[5]*1e-1 
    a2=species[6]*1e2 
    a3=species[7] 
    a4=species[8]*1e4 
    c1=species[9] 
    c2=species[10]*1e4 
    omegaref=species[11]*1e5 
    Z=species[12] #Z is formal charge
    
    if rhoE.__len__() != 0:
        rhohat = rhoE['rho'].ravel()
        E = rhoE['E'].ravel()
    else:
        [rhohat, _, _, _, _, P, TC] = CALPRE(TC, P)
        [E, _, _, _] = DIELEC(TC, rhohat) 
    
    if np.ndim(TC) == 0 | np.ndim(P) == 0:
        TC = np.array(TC).ravel()
        P = np.array(P).ravel()
        length = 1
    else:
        TC= TC.ravel()
        P = P.ravel()
        length = len(P)
        
    TK=celsiusToKelvin(TC)
    Tr=298.15 #K    
        
    g=np.zeros([length, 1]).ravel()
    theta=228 #K
    psi=2600 #bars
    Pr=1 #bars
    Yref=-5.802E-5 #K^-1
    Eref=78.2396 #(calculated at 298.15K)
    n=1.66027e5 
    f1_T = TK*np.log(TK/Tr) - TK + Tr 
    f2_T = (((1/(TK - theta)) - (1/(Tr - theta)))*((theta - TK)/(theta)) - \
            (TK/(theta**2))*np.log(Tr*(TK - theta)/(TK*(Tr - theta)))) 
    f1_P = (P-Pr) 
    f2_P = np.log((psi+P)/(psi+Pr)) 
    f1_PT = ((P-Pr)/(TK-theta)) 
    f2_PT = (1/(TK-theta))*np.log((psi+P)/(psi+Pr)) 
    f3_PT = ((1/E)-(1/Eref) + Yref*(TK-Tr)) 
    
    if Z == 0:
        r = 0
        f4_PT = 0         
    else:                
        ag1 = -2.037662 
        ag2 = 5.747e-3 
        ag3 = -6.557892e-6 
        bg1 = 6.107361 
        bg2 = -1.074377e-2 
        bg3 = 1.268348e-5         
            
        for k in range(length):
            if rhohat[k]<1:
                ag = ag1 + ag2*TC[k] + ag3*TC[k]**2 
                bg = bg1 + bg2*TC[k] + bg3*TC[k]**2 
                negrhohat = 1 - rhohat[k] 
                g[k] = ag*negrhohat**bg 
                            
                if ((TC[k]<155) | (P[k] > 1000) | (TC[k] >355)):
                    g[k]=g[k] 
                else:
                    ag_1 = 0.3666666e2 
                    ag_2 = -1.504956e-10 
                    ag_3 = 5.01799e-14 
                    Tg = (TC[k]-155.0)/300
                    Pg = 1000-P[k] 
                    f_T = Tg**4.8 + ag_1*Tg**16
                    f_P = ag_2*Pg**3 + ag_3*Pg**4 
                    
                    f_PT = f_T*f_P 
                    g[k] = g[k] - f_PT 
        r = Z**2*(omegaref/n + Z/3.082)**-1 + abs(Z)*g 
        omega = n*((Z**2/r)-(Z/(3.082+g))) 
        f4_PT = (omega-omegaref)*((1/E)-1) 
    
    
    delG = delGref - Sref*(TK-Tr) - c1*f1_T - c2*f2_T + a1*f1_P + a2*f2_P + a3*f1_PT + \
        a4*f2_PT + omegaref*f3_PT + f4_PT 
    
    DMINCA=0.35 
    DMINNA=0.05 
    PMAXX1=500 
    # PMAXX2=1000 
    TMINX=350 
    TMAXX=400 
    
    if Z != 0:
        xall=((rhohat < DMINCA) | (P < PMAXX1) & (TC > TMINX) & (TC < TMAXX)) 
    else: 
        xall = (rhohat < DMINNA)  

    # print(xall),print(rhohat)
    delG[xall] = np.nan 
    if xall.any():
        print('Some T,P out of aqueous species eqns regions of applicability')
        
    return delG

def calclogKAnAb( XAn, TC, P, dbacessdic, **rhoEG):
    """
    function for calculating solid solution of Plagioclase minerals
    # parameters:
       XAn          volume fraction of Anorthite
       TC           temperature [C]
       P            pressure [bar]
       dbacessdic   dictionary of species from direct-access database
       rhoEG        dictionary of water properties like  density (rho), 
                       dielectric factor (E) and Gibbs Energy  (optional)
    # results:
       logKplag     logarithmic K values 
       Rxn          properties 
    The general usage of calclogKAnAb without the optional arg is as follows:
    (1) Not on steam saturation curve:
        out = calclogKAnAb(XAn, TC, P, dbacessdic), 
        where T is temperature in celsius and P is pressure in bar;
    (2) On steam saturation curve:
        out = calclogKAnAb(XAn, TC, 'T', dbacessdic), 
        where T is temperature in celsius, followed with a quoted char 'T'; 
        out = calclogKAnAb(XAn, P, 'P', dbacessdic), 
        where P is pressure in bar, followed with a quoted char 'P'.
    """
    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = CALPRE(TC, P)
        [E, _, _, _] = DIELEC(TC, rho) 
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}
    if np.ndim(TC) == 0 | np.ndim(P) == 0:
        TC = np.array(TC).ravel()
        P = np.array(P).ravel()

    TK=celsiusToKelvin(TC)
    Rxn = {}
    Rxn['type']='plag'
    XAb = 1-XAn
    nH = (8-4*XAb)
    nAl = (2-XAb)
    nH2O = (4-2.*XAb)
    nNa = XAb
    nCa = (1-XAb)
    nSi = (2+XAb)
    if XAn == 1:
        Rxn['name']='Anorthite'
        Rxn['formula']='CaAl2(SiO4)2'
    elif XAn == 0:
        Rxn['name']='Albite'
        Rxn['formula']='NaAlSi3O8'
    else:
        Rxn['name'] = 'An%d' % (XAn*100)
        Rxn['formula']= 'Ca%1.2f' % nCa + 'Na%1.2f' % nNa + 'Al%1.2f' % nAl + 'Si%1.2f' % nSi + 'O8'

    Rxn['MW'] = nCa*MW['Ca'] + nNa* MW['Na'] + nAl*MW['Al'] + nSi*MW['Si'] + 8*MW['O']
    R = 1.9872041


    dGAl = supcrtaq(TC, P, dbacessdic['Al+++'], **rhoEG)
    dGSiO2aq  = supcrtaq(TC, P, dbacessdic['SiO2(aq)'], **rhoEG)    
    
    dGH = supcrtaq(TC, P, dbacessdic['H+'], **rhoEG)
    dGNa = supcrtaq(TC, P, dbacessdic['Na+'], **rhoEG)
    dGCa = supcrtaq(TC, P, dbacessdic['Ca++'], **rhoEG)
    
    
    if (XAn < 1) & (XAn != 0):
        Smix=-R*(XAb*np.log((XAb*(4-XAb**2)*(2+XAb)**2)/27) + \
                 XAn*np.log((XAn*(1+XAn)**2*(3-XAn)**2)/16))
    else:
        Smix=0
    
    dGplag = XAb*dbacessdic['ss_Albite_high'][2] + XAn*dbacessdic['ss_Anorthite'][2] + R*298.15*Smix
    Splag = XAb*dbacessdic['ss_Albite_high'][4] + XAn*dbacessdic['ss_Anorthite'][4] - R*Smix
    Vplag = XAb*dbacessdic['ss_Albite_high'][5] + XAn*dbacessdic['ss_Anorthite'][5]
    Cpplag = [a + b for a, b in zip([XAb*x for x in dbacessdic['ss_Albite_high'][6:11] ], 
                                    [XAn*y for y in dbacessdic['ss_Anorthite'][6:11] ])]
    Rxn['min'] = [dGplag, np.nan, Splag, Vplag] + Cpplag
    Rxn['min'].insert(0, Rxn['formula'])
    Rxn['min'].insert(1,' R&H95, Stef2001')
    plag = Rxn['min']
    dGplagTP, _ = heatcapusgscal(TC, P, plag)
    coeff = [-nH, nAl, nNa, nCa, nSi, nH2O]
    spec = ['H+', 'Al+++', 'Na+', 'Ca++', 'SiO2(aq)', 'H2O']
    Rxn['spec'] = [x for x, y in zip(spec, coeff) if y!=0] 
    Rxn['coeff'] = [y for y in coeff if y!=0]
    Rxn['nSpec']=len(Rxn['coeff'])
    
    dGrxn = -dGplagTP - nH*dGH + nAl*dGAl + nH2O*dGH2O + nNa*dGNa+ nCa*dGCa + nSi*dGSiO2aq
    logKplag = np.log10(np.exp(-dGrxn/R/(TK)))
    
    Rxn['V'] = Rxn['min'][5]  #,'%8.3f')
    Rxn['source'] = ' R&H95, Stef2001'
        
    return logKplag, Rxn

def calclogKFoFa( XFo, TC, P, dbacessdic, **rhoEG ):
    """
    This function calculates solid solution properties of Fo and Fa
    # parameters:
       XFo          volume fraction of Forsterite
       TC           temperature [C]
       P            pressure [bar]
       dbacessdic   dictionary of species from direct-access database
       rhoEG        dictionary of water properties like  density (rho), 
                       dielectric factor (E) and Gibbs Energy  (optional)
    # results:
       logK_ol      logarithmic K values 
       Rxn          properties 
    The general usage of calclogKFoFa without the optional arg is as follows:
    (1) Not on steam saturation curve:
        out = calclogKFoFa(XFo, TC, P, dbacessdic), 
        where T is temperature in celsius and P is pressure in bar;
    (2) On steam saturation curve:
        out = calclogKFoFa(XFo, TC, 'T', dbacessdic), 
        where T is temperature in celsius, followed with a quoted char 'T'; 
        out = calclogKFoFa(XFo, P, 'P', dbacessdic), 
        where P is pressure in bar, followed with a quoted char 'P'.
    """
    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = CALPRE(TC, P)
        [E, _, _, _] = DIELEC(TC, rho) 
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    if np.ndim(TC) == 0 | np.ndim(P) == 0:
        TC = np.array(TC).ravel()
        P = np.array(P).ravel()
        length = 1
    else:
        length = len(TC)
        
    Tref = 25; Pref = 1
    if len(TC[TC==Tref]) == 0: # if no reference Temperature is found, append to the bottom
        TC = np.concatenate([TC, np.ravel(Tref)])
        P = np.concatenate([P,np.ravel(Pref)])
        [rho, dGH2O, _, _, _, _, _] = CALPRE(TC, P)
        [E, _, _, _] = DIELEC(TC, rho) 
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    TK=celsiusToKelvin(TC)
    Rxn = {}
    Rxn['type']='ol'
    XFa = 1-XFo
    nH = 4
    nMg = 2*XFo
    nFe = 2*XFa
    nSi = 1
    nH2O = 2
    if XFo == 1:
        Rxn['name']='Forsterite'
        Rxn['formula']='Mg2SiO4'
    elif XFo == 0:
        Rxn['name']='Fayalite'
        Rxn['formula']='Fe2SiO4'
    else:
        Rxn['name'] = 'Fo%d' % (XFo*100)
        Rxn['formula']= 'Mg%1.2f' % nMg + 'Fe%1.2f' % nFe + 'Si%s' % nSi + 'O4'

    Rxn['MW'] = nMg*MW['Mg'] + nFe*MW['Fe'] + nSi*MW['Si'] + 4*MW['O']
    R=1.9872041
    WH=10366.2/J_to_cal
    WS=4/J_to_cal

    dGSiO2aq  = supcrtaq(TC, P, dbacessdic['SiO2(aq)'], **rhoEG)    
    dGH = supcrtaq(TC, P, dbacessdic['H+'], **rhoEG)
    dGFe = supcrtaq(TC, P, dbacessdic['Fe++'], **rhoEG)
    dGMg = supcrtaq(TC, P, dbacessdic['Mg++'], **rhoEG)    
    
    if (XFo == 1) | (XFo == 0):
        Sconf = 0
    else:
        Sconf = -2*R*(XFo*np.log(XFo) + XFa*np.log(XFa))
    
    dG_ol = XFa*dbacessdic['ss_Fayalite'][2] + XFo*dbacessdic['ss_Forsterite'][2]
    S_ol = XFa*dbacessdic['ss_Fayalite'][4] + XFo*dbacessdic['ss_Forsterite'][4] + Sconf
    V_ol = XFa*dbacessdic['ss_Fayalite'][5] + XFo*dbacessdic['ss_Forsterite'][5]
    Cp_ol = [a + b for a, b in zip([XFa*x for x in dbacessdic['ss_Fayalite'][6:11] ], 
                                    [XFo*y for y in dbacessdic['ss_Forsterite'][6:11] ])]
    Rxn['min']=[dG_ol, np.nan, S_ol, V_ol]+Cp_ol
    Rxn['min'].insert(0, Rxn['formula'])
    Rxn['min'].insert(1,' R&H95, Stef2001')
    coeff = [-nH, nMg, nFe, nSi, nH2O]
    spec = ['H+', 'Mg++', 'Fe++', 'SiO2(aq)', 'H2O']
    Rxn['spec'] = [x for x, y in zip(spec, coeff) if y!=0] 
    Rxn['coeff'] = [y for y in coeff if y!=0]
    Rxn['nSpec'] = len(Rxn['coeff'])
    
    # because of the entropy term in WG, Gex and dGol are T-dependent, thus must do the logK calculation on a piecewise basis.
    WG = WH - TK*WS
    Gex = WG*XFo*XFa
    dG_ol = dG_ol + Gex - 298.15*Sconf  # this works, do not touch!!
    logK_ol=np.zeros([length, 1])
    ol = Rxn['min']
    ol[2] = dG_ol
    dGolTP, _ = heatcapusgscal(TC, P, ol)
    dGrxn = -dGolTP - nH*dGH + nMg*dGMg + nFe*dGFe + nH2O*dGH2O + nSi*dGSiO2aq        
    logK_ol = np.log10(np.exp(-dGrxn/R/(TK)))

    Rxn['min'][2] = dG_ol[TC == 25][0]
    Rxn['V'] = Rxn['min'][5]  #,'%8.3f')
    Rxn['source'] = ' R&H95, Stef2001'
    
    if (TC[-1] == Tref) & (P[-1] == Pref):
        logK_ol = logK_ol[:-1]
    
    return logK_ol, Rxn

def calclogKEnFe( XEn, TC, P, dbacessdic, **rhoEG ):
    """
    This function calculates solid solution properties of En and Fe
    # parameters:
       XEn          volume fraction of Enstatite
       TC           temperature [C]
       P            pressure [bar]
       dbacessdic   dictionary of species from direct-access database
       rhoEG        dictionary of water properties like  density (rho), 
                       dielectric factor (E) and Gibbs Energy  (optional)
    # results:
       logK_opx     logarithmic K values 
       Rxn          properties 
    The general usage of calclogKEnFe without the optional arg is as follows:
    (1) Not on steam saturation curve:
        out = calclogKEnFe(XEn, TC, P, dbacessdic), 
        where T is temperature in celsius and P is pressure in bar;
    (2) On steam saturation curve:
        out = calclogKEnFe(XEn, TC, 'T', dbacessdic), 
        where T is temperature in celsius, followed with a quoted char 'T'; 
        out = calclogKEnFe(XEn, P, 'P', dbacessdic), 
        where P is pressure in bar, followed with a quoted char 'P'.
    """
    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = CALPRE(TC, P)
        [E, _, _, _] = DIELEC(TC, rho) 
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}
    if np.ndim(TC) == 0 | np.ndim(P) == 0:
        TC = np.array(TC).ravel()
        P = np.array(P).ravel()

    Tref = 25; Pref = 1
    if len(TC[(TC == Tref)&(P == Pref)]) == 0: # if no reference Temperature is found, append to the bottom
        TC = np.concatenate([TC, np.ravel(Tref)])
        P = np.concatenate([P,np.ravel(Pref)])
        [rho, dGH2O, _, _, _, _, _] = CALPRE(TC, P)
        [E, _, _, _] = DIELEC(TC, rho) 
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    TK=celsiusToKelvin(TC)
    Rxn = {}
    Rxn['type']='opx'
    XFe = 1-XEn
    nH = 2
    nMg = XEn
    nFe = XFe
    nSi = 1
    nH2O = 1
    if XEn == 1:
        Rxn['name']='Enstatite'
        Rxn['formula']='MgSiO3'
    elif XEn == 0:
        Rxn['name']='Ferrosilite'
        Rxn['formula']='FeSiO3'
    else:
        Rxn['name'] = 'En%d' % (XEn*100)
        Rxn['formula']= 'Mg%1.2f' % nMg + 'Fe%1.2f' % nFe + 'Si%s' % nSi + 'O3'

    Rxn['MW'] = nMg*MW['Mg'] + nFe*MW['Fe'] + nSi*MW['Si'] + 3*MW['O']
    R = 1.9872041
    WH = -2600.4/J_to_cal
    WS = -1.34/J_to_cal

    dGSiO2aq  = supcrtaq(TC, P, dbacessdic['SiO2(aq)'], **rhoEG)    
    dGH = supcrtaq(TC, P, dbacessdic['H+'], **rhoEG)
    dGFe = supcrtaq(TC, P, dbacessdic['Fe++'], **rhoEG)
    dGMg = supcrtaq(TC, P, dbacessdic['Mg++'], **rhoEG)    
    
    if (XEn == 1) | (XEn == 0):
        Sconf = 0
    else:
        Sconf = -1*R*(XEn*np.log(XEn) + XFe*np.log(XFe))
    
    dG_opx = XEn*dbacessdic['ss_Enstatite'][2] + XFe*dbacessdic['ss_Ferrosilite'][2]
    S_opx = XEn*dbacessdic['ss_Enstatite'][4] + XFe*dbacessdic['ss_Ferrosilite'][4] + Sconf
    V_opx = XEn*dbacessdic['ss_Enstatite'][5] + XFe*dbacessdic['ss_Ferrosilite'][5]
    Cp_opx = [a + b for a, b in zip([XEn*x for x in dbacessdic['ss_Enstatite'][6:11] ], 
                                    [XFe*y for y in dbacessdic['ss_Ferrosilite'][6:11] ])]
    Rxn['min']=[dG_opx, np.nan, S_opx, V_opx]+Cp_opx
    Rxn['min'].insert(0,Rxn['formula'])
    Rxn['min'].insert(1,' R&H95, Stef2001')
    coeff = [-nH, nMg, nFe, nSi, nH2O]
    spec = ['H+', 'Mg++', 'Fe++', 'SiO2(aq)', 'H2O']
    Rxn['spec'] = [x for x, y in zip(spec, coeff) if y!=0] 
    Rxn['coeff'] = [y for y in coeff if y!=0]
    Rxn['nSpec'] = len(Rxn['coeff'])
    
    # because of the entropy term in WG, Gex and dGol are T-dependent, thus must do the logK calculation on a piecewise basis.
    WG = WH-TK*WS
    Gex = WG*XEn*XFe
    dG_opx = dG_opx + Gex - 298.15*Sconf  # this works, do not touch!!
    opx = Rxn['min']
    opx[2] = dG_opx
    dGopxTP, _ = heatcapusgscal(TC, P, opx)
    dGrxn = -dGopxTP - nH*dGH + nMg*dGMg + nFe*dGFe + nH2O*dGH2O + nSi*dGSiO2aq        
    logK_opx = np.log10(np.exp(-dGrxn/R/(TK)))

    Rxn['min'][2]=dG_opx[TC == 25][0]
    Rxn['V'] = Rxn['min'][5]  #,'%8.3f')
    Rxn['source'] = ' R&H95, Stef2001'
        
    if (TC[-1] == Tref) & (P[-1] == Pref):
        logK_opx = logK_opx[:-1]

    return logK_opx, Rxn

def calclogKDiHedEnFe( nCa, XMg, TC, P, dbacessdic, **rhoEG ):
    """
    This function calculates solid solution properties of Di, Hed, En and Fe
    # parameters:
       nCa          number of moles of Ca in formula unit (=1 for Di, Hed)
                       and must be greater than zero
       XMg          mole fraction of Mg (XMg=(nMg/(nFe+nMg)))
       TC           temperature [C]
       P            pressure [bar]
       dbacessdic   dictionary of species from direct-access database
       rhoEG        dictionary of water properties like  density (rho), 
                       dielectric factor (E) and Gibbs Energy  (optional)
    # results:
       logK_cpx     logarithmic K values 
       Rxn          properties 
    The general usage of calclogKDiHedEnFe without the optional arg is as follows:
    (1) Not on steam saturation curve:
        out = calclogKDiHedEnFe(nCa, XMg, TC, P, dbacessdic), 
        where T is temperature in celsius and P is pressure in bar;
    (2) On steam saturation curve:
        out = calclogKDiHedEnFe(nCa, XMg, TC, 'T', dbacessdic), 
        where T is temperature in celsius, followed with a quoted char 'T'; 
        out = calclogKDiHedEnFe(nCa, XMg, P, 'P', dbacessdic), 
        where P is pressure in bar, followed with a quoted char 'P'.
    """
    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = CALPRE(TC, P)
        [E, _, _, _] = DIELEC(TC, rho) 
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}
    if np.ndim(TC) == 0 | np.ndim(P) == 0:
        TC = np.array(TC).ravel()
        P = np.array(P).ravel()

    Tref = 25; Pref = 1
    if len(TC[TC==Tref]) == 0: # if no reference Temperature is found, append to the bottom
        TC = np.concatenate([TC, np.ravel(Tref)])
        P = np.concatenate([P,np.ravel(Pref)])
        [rho, dGH2O, _, _, _, _, _] = CALPRE(TC, P)
        [E, _, _, _] = DIELEC(TC, rho) 
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}


    TK=celsiusToKelvin(TC)
    R = 1.9872041

    if nCa >= 1:
        XDi = XMg
        XHed = 1-XDi
        XEn = 0
        XFs = 0
    else:
        XDi=XMg*nCa
        XHed=(1-XMg)*nCa
        Xopx=1-nCa
        XEn=XMg*Xopx
        XFs=(1-XMg)*Xopx
        
    #start on M1 parms
    WH_M1 = -1910/J_to_cal
    WS_M1 = -1.05/J_to_cal        
    if nCa < 1:
        # for the M1 (i.e., opx) component of cpx
        Mg_M1=XEn*2 #  %multiplying by 2 b/c thermo props multiplied by 2
        Fe_M1=XFs*2 #  %multiplying by 2 b/c thermo props multiplied by 2
        
        XFe_M1=Fe_M1/(Fe_M1+Mg_M1)
        XMg_M1=Mg_M1/(Fe_M1+Mg_M1)
        if (XEn==0) | (XFs==0):
            Sconf_M1 = 0 # if you don't do this, you take log of 0
        else:
            Sconf_M1 = -1*R*(XFe_M1*np.log(XFe_M1)+XMg_M1*np.log(XMg_M1))
    else: #No M1 if nCa >=1
        XMg_M1 = 0; Mg_M1 = 0
        XFe_M1 = 0; Fe_M1 = 0
        Sconf_M1=0
        
    WG_M1 = WH_M1 - TK*WS_M1
    Gex_M1 = WG_M1*XFe_M1*XMg_M1    
    WG_M1_25 = WH_M1 - 298.15*WS_M1
    Gex_M1_25 = WG_M1_25*XFe_M1*XMg_M1

    # now start on the M2 part
    WH_M2=0./J_to_cal
    WS_M2=0./J_to_cal    
    Fe_M2=(XHed)
    Mg_M2=(XDi)
    Ca_M2=(XHed+XDi)
    Tot_M2 = Fe_M2 + Mg_M2 + Ca_M2 

    XFe_M2=Fe_M2/Tot_M2
    XMg_M2=Mg_M2/Tot_M2
    XCa_M2=Ca_M2/Tot_M2
    if (XHed==0) | (XDi==0):
        Sconf_M2 = 0
    else:
        #because of the entropy term in WG, Gex and dGol are T-dependent, thus must
        #do the logK calculation on a piecewise basis.        
        Sconf_M2=-1*R*( XFe_M2*np.log(XFe_M2) + XMg_M2*np.log(XMg_M2)+\
                       XCa_M2*np.log(XCa_M2))
    WG_M2 = WH_M2 - TK*WS_M2
    Gex_M2= WG_M2*XDi*XHed
    WG_M2_25=WH_M2 - 298.15*WS_M2
    Gex_M2_25 = WG_M2_25*XFe_M2*XMg_M2            

    Rxn = {}
    Rxn['type']='cpx'
    nH = 4
    nMg = round(Mg_M1 + Mg_M2, 2)
    nFe = round(Fe_M1 + Fe_M2, 2)
    nCa = round(Ca_M2, 2)
    nSi = 2
    nH2O = 2
    if (XEn == 0):
        if (XFs == 0):
            if XDi == 1:
                Rxn['name'] = 'Diopside'
            elif XHed == 1:
                Rxn['name'] = 'Hedenbergite'
            else:
                Rxn['name'] = 'Di%d' % round(XDi*100) + 'Hed%d' % round(XHed*100)
        elif (XHed == 0):
            Rxn['name'] = 'Di%d' % round(XDi*100) + 'Fe%d' % round(XFs*100)
        elif (XDi == 0):
            Rxn['name'] = 'Hed%d' % round(XHed*100) + 'Fe%d' % round(XFs*100)
        else:
            Rxn['name'] = 'Di%d' % round(XDi*100) + 'Hed%d' % round(XHed*100) + 'Fe%d' % round(XFs*100)        
    elif (XFs == 0):
        if (XHed == 0):
            Rxn['name'] = 'Di%d' % round(XDi*100) + 'En%d' % round(XEn*100) 
        elif (XDi == 0):
            Rxn['name'] = 'Hed%d' % round(XHed*100) + 'En%d' % round(XEn*100) 
        else:
            Rxn['name'] = 'Di%d' % round(XDi*100) + 'Hed%d' % round(XHed*100) + 'En%d' % round(XEn*100)       
    elif (XHed == 0):
        Rxn['name'] = 'Di%d' % round(XDi*100) + 'En%d' % round(XEn*100) + 'Fe%d' % round(XFs*100)        
    elif (XDi == 0):
        Rxn['name'] = 'Hed%d' % round(XHed*100) + 'En%d' % round(XEn*100) + 'Fe%d' % round(XFs*100)        
    else:
        Rxn['name'] = 'Di%d' % round(XDi*100) + 'Hed%d' % round(XHed*100) + 'En%d' % round(XEn*100) + 'Fe%d' % round(XFs*100)

    if (nCa == 0):
        Rxn['formula'] = 'Mg%1.2f' % nMg + 'Fe%1.2f' % nFe + 'Si%s' % nSi + 'O6'
    elif (nMg == 0):
        if (nCa == 1) and (nFe == 1):
            Rxn['formula'] = 'CaFe' + 'Si%s' % nSi + 'O6'
        else:
            Rxn['formula'] = 'Ca%1.2f' % nCa + 'Fe%1.2f' % nFe + 'Si%s' % nSi + 'O6'
    elif (nFe == 0):
        if (nCa == 1) and (nMg == 1):
            Rxn['formula'] = 'CaMg' + 'Si%s' % nSi + 'O6'
        else:
            Rxn['formula'] = 'Ca%1.2f' % nCa + 'Mg%1.2f' % nMg + 'Si%s' % nSi + 'O6'
    else:
        Rxn['formula'] = 'Ca%1.2f' % nCa + 'Mg%1.2f' % nMg + 'Fe%1.2f' % nFe + 'Si%s' % nSi + 'O6'
    Rxn['MW'] = nCa*MW['Ca'] + nMg*MW['Mg'] + nFe*MW['Fe'] + nSi*MW['Si'] + 6*MW['O']
    R = 1.9872041
    WH_M2 = -1910/J_to_cal     #cal/mol
    WS_M2 = -1.05/J_to_cal         #cal/K/mol
    
    dGSiO2aq  = supcrtaq(TC, P, dbacessdic['SiO2(aq)'], **rhoEG)    
    dGH = supcrtaq(TC, P, dbacessdic['H+'], **rhoEG)
    dGFe = supcrtaq(TC, P, dbacessdic['Fe++'], **rhoEG)
    dGMg = supcrtaq(TC, P, dbacessdic['Mg++'], **rhoEG)    
    dGCa = supcrtaq(TC, P, dbacessdic['Ca++'], **rhoEG)  
    
    # Addition of M1 and M2 sites
    Sconf = Sconf_M1 + Sconf_M2
    dG_cpx_noGex = 2*XFs*dbacessdic['ss_Ferrosilite'][2] + 2*XEn*dbacessdic['ss_Clinoenstatite'][2] + \
        XHed*dbacessdic['ss_Hedenbergite'][2] + XDi*dbacessdic['ss_Diopside'][2] 
    dG_cpx_25 = dG_cpx_noGex + Gex_M1_25 + Gex_M2_25        
    # dG_cpx = dG_cpx_25 + Gex_M1 + Gex_M2 - 298.15*(Sconf)       
    
    S_cpx = 2*XFs*dbacessdic['ss_Ferrosilite'][4] + 2*XEn*dbacessdic['ss_Clinoenstatite'][4] + \
        XHed*dbacessdic['ss_Hedenbergite'][4] + XDi*dbacessdic['ss_Diopside'][4] + Sconf
    V_cpx = 2*XFs*dbacessdic['ss_Ferrosilite'][5] + 2*XEn*dbacessdic['ss_Clinoenstatite'][5] + \
        XHed*dbacessdic['ss_Hedenbergite'][5] + XDi*dbacessdic['ss_Diopside'][5]
    Cp_cpx = [i + j + k + l for i, j, k, l in zip([2*XFs*a for a in dbacessdic['ss_Ferrosilite'][6:11] ], 
                                          [2*XEn*b for b in dbacessdic['ss_Clinoenstatite'][6:11] ],
                                          [XHed*c for c in dbacessdic['ss_Hedenbergite'][6:11] ],
                                          [XDi*d for d in dbacessdic['ss_Diopside'][6:11] ] )]
    Rxn['min']=[dG_cpx_25, np.nan, S_cpx, V_cpx]+Cp_cpx
    Rxn['min'].insert(0,Rxn['formula'])
    Rxn['min'].insert(1,' R&H95, Stef2001')
    coeff = [-nH, nCa, nMg, nFe, nSi, nH2O]
    spec = ['H+', 'Ca++', 'Mg++', 'Fe++', 'SiO2(aq)', 'H2O']
    Rxn['spec'] = [x for x, y in zip(spec, coeff) if y!=0] 
    Rxn['coeff'] = [y for y in coeff if y!=0]
    Rxn['nSpec'] = len(Rxn['coeff'])
    
    # because of the entropy term in WG, Gex and dGol are T-dependent, thus must do the logK calculation on a piecewise basis.
    dG_cpx = dG_cpx_25 + Gex_M1 + Gex_M2 - TK*Sconf  
    cpx = Rxn['min']    
    cpx[2] = dG_cpx
    dGcpxTP, _ = heatcapusgscal(TC, P, cpx)
    dGrxn = -dGcpxTP - nH*dGH + nCa*dGCa + nMg*dGMg + nFe*dGFe + nH2O*dGH2O + nSi*dGSiO2aq        
    logK_cpx = np.log10(np.exp(-dGrxn/R/(TK)))

    Rxn['min'][2] = dG_cpx[TC == 25][0]
    Rxn['V'] = Rxn['min'][5]  #,'%8.3f')
    Rxn['source'] = ' R&H95, Stef2001'
    
    if (TC[-1] == Tref) & (P[-1] == Pref):
        logK_cpx = logK_cpx[:-1]
    
    return logK_cpx, Rxn

def calcRxnlogK( TC, P, Prod, dbacessdic, sourcedic, specielist, **rhoEG):
    """
    This function calculates logK values of any reaction
      parameters:
          TC            temperature [C]
          P             pressure [bar]
          Prod          Product species of the reaction
          dbacessdic    direct-acess database dictionary
          sourcedic     source database reactions dictionary
          specielist    source database species grouped into 
                            [element, basis, redox, aqueous, minerals, gases, oxides]
          rhoEG         dictionary of water properties like  density (rho), 
                           dielectric factor (E) and Gibbs Energy  (optional)
      results:
          logK          logarithmic K values 
    """
    R = 1.9872041 # cal/mol/K
    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = CALPRE(TC, P)
        [E, _, _, _] = DIELEC(TC, rho) 
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    if np.ndim(TC) == 0 | np.ndim(P) == 0:
        TC = np.array(TC).ravel()
        P = np.array(P).ravel() 
        
    TK=celsiusToKelvin(TC)
    rxnspecies = sourcedic[Prod]

    if Prod == 'e-' or Prod == 'eh':
        dGP = 0
    elif Prod == 'H2O':
        dGP = dGH2O
    elif Prod in specielist[2] or Prod in specielist[3]:
        dGP  = supcrtaq(TC, P, dbacessdic[Prod], **rhoEG) 
    elif Prod in ['Hydroxylapatite', 'Fluorapatite']:
        dGP, _ = heatcapusgscal(TC, P, dbacessdic[Prod])
    else:
        dGP = heatcap(TC, P, dbacessdic[Prod])
    
    
    nR1 = float(rxnspecies[2])
    R1 = rxnspecies[3]
    if R1 == 'H2O':
        dGR1 = dGH2O
    elif R1 in specielist[2] or R1 in specielist[3]:
        dGR1  = supcrtaq(TC, P, dbacessdic[R1], **rhoEG) 
    else:
        dGR1 = heatcap(TC, P, dbacessdic[R1])
    dGrxn = -dGP + nR1*dGR1        
    if rxnspecies[1] > 1:
        nR2 = float(rxnspecies[4])
        R2 = rxnspecies[5]
        if R2 == 'H2O':
            dGR2 = dGH2O
        elif R2 in specielist[2] or R2 in specielist[3]:
            dGR2  = supcrtaq(TC, P, dbacessdic[R2], **rhoEG) 
        else:
            dGR2 = heatcap(TC, P, dbacessdic[R2])
        dGrxn = -dGP + nR1*dGR1 + nR2*dGR2        
        if rxnspecies[1] > 2:
            nR3 = float(rxnspecies[6])
            R3 = rxnspecies[7]
            if R3 == 'H2O':
                dGR3 = dGH2O
            elif R3 in specielist[2] or R3 in specielist[3]:
                dGR3  = supcrtaq(TC, P, dbacessdic[R3], **rhoEG) 
            else:
                dGR3 = heatcap(TC, P, dbacessdic[R3])
            dGrxn = -dGP + nR1*dGR1 + nR2*dGR2 + nR3*dGR3        
            if rxnspecies[1] > 3:
                nR4 = float(rxnspecies[8])
                R4 = rxnspecies[9]
                if R4 == 'H2O':
                    dGR4 = dGH2O
                elif R4 in specielist[2] or R4 in specielist[3]:
                    dGR4  = supcrtaq(TC, P, dbacessdic[R4], **rhoEG) 
                else:
                    dGR4 = heatcap(TC, P, dbacessdic[R4])
                dGrxn = -dGP + nR1*dGR1 + nR2*dGR2 + nR3*dGR3 + nR4*dGR4        
                if rxnspecies[1] > 4:
                    nR5 = float(rxnspecies[10])
                    R5 = rxnspecies[11]
                    if R5 == 'H2O':
                        dGR5 = dGH2O
                    elif R5 in specielist[2] or R5 in specielist[3]:
                        dGR5  = supcrtaq(TC, P, dbacessdic[R5], **rhoEG) 
                    else:
                        dGR5 = heatcap(TC, P, dbacessdic[R5])
                    dGrxn = -dGP + nR1*dGR1 + nR2*dGR2 + nR3*dGR3 + nR4*dGR4 + nR5*dGR5       
                    if rxnspecies[1] > 5:
                        nR6 = float(rxnspecies[12])
                        R6 = rxnspecies[13]
                        if R6 == 'H2O':
                            dGR6 = dGH2O
                        elif R6 in specielist[2] or R6 in specielist[3]:
                            dGR6  = supcrtaq(TC, P, dbacessdic[R6], **rhoEG) 
                        else:
                            dGR6 = heatcap(TC, P, dbacessdic[R6])
                        dGrxn = -dGP + nR1*dGR1 + nR2*dGR2 + nR3*dGR3 + nR4*dGR4 + nR5*dGR5 + nR6*dGR6       
                        if rxnspecies[1] > 6:
                            nR7 = float(rxnspecies[14])
                            R7 = rxnspecies[15]
                            if R7 == 'H2O':
                                dGR7 = dGH2O
                            elif R7 in specielist[2] or R7 in specielist[3]:
                                dGR7  = supcrtaq(TC, P, dbacessdic[R7], **rhoEG) 
                            else:
                                dGR7 = heatcap(TC, P, dbacessdic[R7])
                            dGrxn = -dGP + nR1*dGR1 + nR2*dGR2 + nR3*dGR3 + nR4*dGR4 + nR5*dGR5 + nR6*dGR6 + nR7*dGR7       
                            if rxnspecies[1] > 7:
                                nR8 = float(rxnspecies[16])
                                R8 = rxnspecies[17]
                                if R8 == 'H2O':
                                    dGR8 = dGH2O
                                elif R8 in specielist[2] or R8 in specielist[3]:
                                    dGR8  = supcrtaq(TC, P, dbacessdic[R8], **rhoEG) 
                                else:
                                    dGR8 = heatcap(TC, P, dbacessdic[R8])
                                dGrxn = -dGP + nR1*dGR1 + nR2*dGR2 + nR3*dGR3 + nR4*dGR4 + nR5*dGR5 + nR6*dGR6 + nR7*dGR7 + nR8*dGR8
    
    logK = (-dGrxn/R/(TK)/np.log(10))    #np.log10(np.exp(-dGrxn/R/(TK)))
            
    return logK #, dGrxn, dGP, dGR1, dGR2, dGR3, dGR4

def outputtxt(fid, logK, Rxn):
    """
    function that writes logK and Rxn data to a text file 
     Example:
       outputtxt(open('logKdata.txt','w'), logK, Rxn);
    
    """
            
    # %% Open the text file.
    f = fid
    
    f.writelines("%s                       " % Rxn['name'])
    f.writelines( "%s= " %  list(Rxn.keys())[0])
    if Rxn['type'].find('plag') == 0:
        f.writelines( "plagioclase\n")
    else:
        f.writelines( "%s\n" %  Rxn['type'])
    f.writelines( "     %s= " %  list(Rxn.keys())[2])
    f.writelines( "%s\n" %  Rxn['formula'])
    f.writelines( "     mole vol.=   %1.3f cc" %  Rxn['V'])
    f.writelines( "      mole wt.=  %1.4f g\n" %  Rxn['MW'])
    f.writelines( "     %s species in reaction\n" %  Rxn['nSpec'])
    for i in range(len(Rxn['spec'])):
        i = i + 1
        f.writelines( "%8.4f " %  Rxn['coeff'][i-1])
        f.writelines( "%-8s          " %  Rxn['spec'][i-1])
        if (i % 3 == 0) | (i % 6 == 0) | (i == len(Rxn['spec'])):
            f.writelines( "\n")
    for i in range(len(logK)):
        i = i + 1
        if (i == 1) | (i == 5) | (i == 9):
            f.writelines("       %8.4f" %  logK[i-1])
        else:
            f.writelines("  %8.4f" %  logK[i-1])
        if (i % 4 == 0) | (i == 8) | (i == len(logK)):
            f.writelines( "\n")

    f.writelines( "*    gflag = 1 [reported delG0f used]\n" )
    if Rxn['type'].find('plag') == 0:
        f.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
    else:
        f.writelines( "*    extrapolation algorithm: supcrt92/water95\n" )
    if Rxn['type'].find('serp') == 0:
        f.writelines( "*    reference-state data source = Blancetal 2015\n" )
    elif 'source' in Rxn:
        f.writelines( "*    reference-state data source = %s\n" % Rxn['source'])
    else:
        f.writelines( "*    reference-state data source = supcrt92?\n" ) 

    f.write( "*         delG0f =   %8.3f  kcal/mol\n" % (Rxn['min'][2]/1000) )
    f.writelines( "*         delH0f =   NaN  kcal/mol\n")
    f.writelines( "*         S0PrTr =   %8.3f  cal/mol\n" % Rxn['min'][4])
    f.writelines( "\n")

    return 
            
# @timer        
def write_db(T, P, dbaccess, sourcedb, objdb, *nCa_cpx, solid_solution = True, co2actmodel = 'Drummond' ): 
    """
    function that writes the new database
      parameters:
        T                   temperature [°C] 
        P                   pressure [bar]
        dbaccess            direct-access database filename and location
        sourcedb            source database filename and location
        objdb               new database filename and location
        solid_solution      bool to specify the inclusion of solid-solution [True or False]
        co2actmodel         co2 activity model equation [Duan_Sun or Drummond]
    
    """
    dbacessdic, dbname = readAqspecdb(dbaccess)
    sourcedic, specielist, chargedic, MWdic, Mineraltype = readSourcedb(sourcedb)
    
    
    missing_species = []
    all_species_source = [[i]+k for i, k in sourcedic.items() if i not in specielist[1]]
    for num in range(len(all_species_source)):
        if num < len(all_species_source)-2:
            lst = all_species_source[num]
            lst = [i for j, i in enumerate(lst) if j not in [1, 2]] # remove formula and specie number
            lst = [v for i, v in enumerate(lst) if i % 2 == 0 ] # remove all coefficients
            missing_species.append([j for j in lst if j not in dbacessdic.keys() and j != 'H2O'])
    missingfile = open('./spxNotFound.txt', 'w')
    #missing_species = [i for i in missing_species if len(i)<1]
    for line in missing_species:
        if len(line) > 0:
            missingfile.writelines(line[0])
            missingfile.writelines('\n')
            for i in range(len(line)):
                missingfile.writelines('   %s' % line[i])
                missingfile.writelines('\n')
    missingfile.close()
    missing_species = [item for sublist in missing_species for item in sublist]
    missing_species = [i for n, i in enumerate(missing_species) if i not in missing_species[:n]]
    
    # sourcedb='./thermo.com.dat'
    fid = open(sourcedb, 'r')

    # objdb='./thermo.new.dat'
    # fout = open(objdb, 'w+')
    if os.path.exists(os.path.join(os.getcwd(), 'output')) == False:
        os.makedirs(os.path.join(os.getcwd(), 'output'))
        
    fout = open(os.path.join(os.path.dirname(os.path.abspath(__file__)),'output', objdb + '.dat'), 'w+')
    #Rd = fid.readlines()
    
    s = fid.readline()
    fout.writelines(s)
    s = fid.readline()
    fout.writelines(s)
    s = fid.readline()
    fout.writelines(s)
    s = fid.readline()
    fout.writelines(s[:27] + dbname + '\n')
    s = fid.readline()
    fout.writelines(s[:15] + ': DBCreate, ' + time.ctime() + '\n')
    s = fid.readline()
    fout.writelines(s)
    s = fid.readline()
    fout.writelines(s)
    fout.writelines('*  Note: coefficients for calculating the activity coefficients \n' + \
                    '*    for CO2 are based on Ref:\n' + \
                        '*    S.E.Drummond,1981. Boiling and Mixing of Hydrothermal\n' + \
                            '*    Fluids: Chemical Effects on Mineral Precipitation.\n')
    
    #skip lines till temperature rows
    for i in range(50) :
        s = fid.readline()
        # print(s)
        if s[2:-1] == 'temperatures':
            break
    fout.writelines( "\n")
    fout.writelines(s[:-1] + '(degC)\n')

    [rho, dGH2O, dHH2O, SH2O, _, P, T] = CALPRE(T, P)
    if np.ndim(T) == 0 | np.ndim(P) == 0:
        T = np.ravel(T)
        P = np.ravel(P)
        
    for i in range(len(T)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "       %8.4f" %  T[i-1])
        else:
            fout.writelines( "   %8.4f" %  T[i-1])
        if (i % 4 == 0) | (i == len(T)):
            fout.writelines( "\n")

    #skip lines till pressure rows
    for i in range(50) :
        s = fid.readline()
        # print(s)
        if s[2:-1] == 'pressures':
            break
    fout.writelines(s[:-1] + '(bar)\n')
    for i in range(len(P)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "       %8.4f" %  P[i-1])
        else:
            fout.writelines( "   %8.4f" %  P[i-1])
        if (i % 4 == 0) | (i == len(P)):
            fout.writelines( "\n")
    
    #%% Calculation for debye huckel and bdot and water properties
    [E, Adh, Bdh, bdot] = DIELEC(T, rho) 
    rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}
    # DB = {'Ah': Adh, 'Bh': Bdh, 'bdot': bdot}
    #skip lines till adh rows
    for i in range(50) :
        s = fid.readline()
        # print(s)
        if s.strip('\n')[-5:] == '(adh)':
            break
    fout.writelines(s)
    for i in range(len(Adh)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "       %8.4f" %  Adh[i-1])
        else:
            fout.writelines( "   %8.4f" %  Adh[i-1])
        if (i % 4 == 0) | (i == len(Adh)):
            fout.writelines( "\n")
    #skip lines till bdh rows
    for i in range(50) :
        s = fid.readline()
        # print(s)
        if s.strip('\n')[-5:] == '(bdh)':
            break
    fout.writelines(s)
    for i in range(len(Bdh)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "       %8.4f" %  Bdh[i-1])
        else:
            fout.writelines( "   %8.4f" %  Bdh[i-1])
        if (i % 4 == 0) | (i == len(Bdh)):
            fout.writelines( "\n")
    #skip lines till bdot rows
    for i in range(50) :
        s = fid.readline()
        # print(s)
        if s.strip('\n')[-5:] == ' bdot':
            break
    fout.writelines(s)
    for i in range(len(bdot)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "       %8.4f" %  bdot[i-1])
        else:
            fout.writelines( "   %8.4f" %  bdot[i-1])
        if (i % 4 == 0) | (i == len(bdot)):
            fout.writelines( "\n")

    #%% Calculation for co2 fitting coefs
    cco2 = gamma_correlation(T, P, co2actmodel) #co2act(T)
    cco2 = cco2.T#.ravel()
    for j in range(4):
        fout.writelines('* c co2 %s\n' % (j+1))
        for i in range(len(cco2)):
            i = i + 1
            if (i == 1) | (i == 5):
                fout.writelines( "       %8.4f" %  cco2[i-1, j])
            else:
                fout.writelines( "   %8.4f" %  cco2[i-1, j])
            if (i % 4 == 0) | (i == len(cco2)):
                fout.writelines( "\n")

    #skip lines till c h2o rows
    for i in range(50) :
        s = fid.readline()
        # print(s)
        if s[:7] == '* c h2o':
            break
    fout.writelines(s)
    #Copy and replace c h2o values
    for i in range(50) :
        s = fid.readline()
        # print(s)
        if s[:7] == '* log k':
            break
        fout.writelines(s)

    #%% Calculations for "log k for eh" rows
    fout.writelines(s)
    logK = calcRxnlogK( T, P, s[12:14], dbacessdic, sourcedic, specielist, **rhoEG)
    for i in range(len(logK)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "       %8.4f" %  logK[i-1])
        else:
            fout.writelines( "   %8.4f" %  logK[i-1])
        if (i % 4 == 0) | (i == len(logK)):
            fout.writelines( "\n")

    #%% Calculations for "log k for o2"
    #skip lines till "log k for o2" rows
    for i in range(50) :
        s = fid.readline()
        # print(s)
        if s[:14] == '* log k for o2':
            break
    fout.writelines(s)
    logK = calcRxnlogK( T, P, 'O2(g)', dbacessdic, sourcedic, specielist, **rhoEG)
    for i in range(len(logK)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "       %8.4f" %  logK[i-1])
        else:
            fout.writelines( "   %8.4f" %  logK[i-1])
        if (i % 4 == 0) | (i == len(logK)):
            fout.writelines( "\n")

    #%% Calculations for "log k for h2"
    #skip lines till "log k for h2" rows
    for i in range(50) :
        s = fid.readline()
        # print(s)
        if s[:14] == '* log k for h2':
            break
    fout.writelines(s)
    logK = calcRxnlogK( T, P, 'H2(g)', dbacessdic, sourcedic, specielist, **rhoEG)
    for i in range(len(logK)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "       %8.4f" %  logK[i-1])
        else:
            fout.writelines( "   %8.4f" %  logK[i-1])
        if (i % 4 == 0) | (i == len(logK)):
            fout.writelines( "\n")

    #%% Calculations for "log k for n2"
    #skip lines till "log k for n2" rows
    for i in range(50) :
        s = fid.readline()
        # print(s)
        if s[:14] == '* log k for n2':
            break
    fout.writelines(s)
    logK = calcRxnlogK( T, P, 'N2(g)', dbacessdic, sourcedic, specielist, **rhoEG)
    for i in range(len(logK)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "       %8.4f" %  logK[i-1])
        else:
            fout.writelines( "   %8.4f" %  logK[i-1])
        if (i % 4 == 0) | (i == len(logK)):
            fout.writelines( "\n")
    fout.writelines( "\n")

    #%% Elements
    #skip lines till "elements" rows
    for i in range(50) :
        s = fid.readline()
        # print(s)
        if s.rstrip('\n').lstrip('0123456789.- ') == 'elements':
            break
    fout.writelines( "   %s elements" % len(specielist[0]))
    fout.writelines( "\n")

    #copy and paste lines till "basis" rows
    for i in range(200) :
        s = fid.readline()
        if s.rstrip('\n').lstrip('0123456789.- ') == 'basis species':
            break
        fout.writelines(s)
    # fout.writelines( "-end-\n\n")

    #only used for counter
    counter = 0
    for j in specielist[1]:
        if (j not in missing_species):
            counter +=1
    fout.writelines( "   %s basis species\n\n" % counter)

    # print('-> Processing basis output')
    #%% Basis reactions
    #skip lines till "redox couples" rows
    for i in range(2000):
        s = fid.readline()
        # print(s)
        if s.rstrip('\n').lstrip('0123456789.- ') == 'redox couples':
            break
        
    for j in specielist[1]:
        if j not in missing_species:
            fout.writelines('%s\n' % j)
            fout.writelines('%s\n' % chargedic[j])
            fout.writelines( "     %s elements in species\n" % sourcedic[j][1])
            Rxn = sourcedic[j][2:]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 7):
                    fout.writelines( "    %8.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%8.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( "%-8s     " %  (Rxn[i - 1]))
                if (i % 6 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
            fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
            if j != 'H2O':
                ref, dG, dH, S = dbacessdic[j][1], dbacessdic[j][2], dbacessdic[j][3], dbacessdic[j][4]
            else:
                dG, dH, S, ref = dGH2O[0], dHH2O[0], SH2O[0], 'IAWPS95'
            fout.writelines( "*    reference-state data source = %s\n" % ref)           
            fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dG/1000) )
            fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dH/1000))
            fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % S)
            fout.writelines( "\n")
        else:
            continue
    fout.writelines( "-end-\n\n")

    #only used for counter
    counter = 0
    for j in specielist[2]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            counter +=1
    fout.writelines( "   %s redox couples\n\n" % counter)
    
    # print('-> Processing redox output')
    #%% Redox reactions
    #skip lines till "aqueous species" rows
    for i in range(2000):
        s = fid.readline()
        # print(s)
        if s.rstrip('\n').lstrip('0123456789.- ') == 'aqueous species':
            break
        
    for j in specielist[2]:     #[:5]
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([i for i in rxnlst if i not in missing_species])==len(rxnlst)):
            fout.writelines('%s\n' % j)
            fout.writelines('*    formula= %s\n' % dbacessdic[j][0])
            fout.writelines('%s\n' % chargedic[j])
            fout.writelines( "     %s species in reaction\n" % sourcedic[j][1])
            Rxn = sourcedic[j][2:]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 7):
                    fout.writelines( "    %8.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%8.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( "%-8s     " %  (Rxn[i - 1]))
                if (i % 6 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist, **rhoEG)
            for i in range(len(logK)):
                i = i + 1
                if (i == 1) | (i == 5):
                    fout.writelines( "     %8.4f" %  logK[i-1])
                else:
                    fout.writelines( "  %8.4f" %  logK[i-1])
                if (i % 4 == 0) | (i == len(logK)):
                    fout.writelines( "\n")
            fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
            fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
            if j != 'H2O':
                ref, dG, dH, S = dbacessdic[j][1], dbacessdic[j][2], dbacessdic[j][3], dbacessdic[j][4]
            else:
                dG, dH, S, ref = dGH2O[0], dHH2O[0], SH2O[0], 'IAWPS95'
            fout.writelines( "*    reference-state data source = %s\n" % ref)           
            fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dG/1000) )
            fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dH/1000))
            fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % S)
            fout.writelines( "\n")
        else:
            continue
    fout.writelines( "-end-\n\n")

    #only used for counter
    counter = 0
    for j in specielist[3]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            counter +=1
    fout.writelines( "   %s aqueous species\n\n" % counter)

    # print('-> Processing aqueous output')
    #%% Aqueous reactions
    #skip lines till "minerals" rows
    for i in range(20000):
        s = fid.readline()
        # print(s)
        if s.rstrip('\n').lstrip('0123456789.- ') == 'minerals':
            break
        
    for j in specielist[3]:     #[:5]
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([i for i in rxnlst if i not in missing_species])==len(rxnlst)):
            fout.writelines('%s\n' % j)
            fout.writelines('*    formula= %s\n' % dbacessdic[j][0])
            fout.writelines('%s\n' % chargedic[j])
            fout.writelines( "     %s species in reaction\n" % sourcedic[j][1])
            Rxn = sourcedic[j][2:]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 7) | (i == 13):
                    fout.writelines( "    %8.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%8.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( "%-8s     " %  (Rxn[i - 1]))
                if (i % 6 == 0) | (i % 12 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist, **rhoEG)
            for i in range(len(logK)):
                i = i + 1
                if (i == 1) | (i == 5):
                    fout.writelines( "     %8.4f" %  logK[i-1])
                else:
                    fout.writelines( "  %8.4f" %  logK[i-1])
                if (i % 4 == 0) | (i == len(logK)):
                    fout.writelines( "\n")
            fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
            fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
            if j != 'H2O':
                ref, dG, dH, S = dbacessdic[j][1], dbacessdic[j][2], dbacessdic[j][3], dbacessdic[j][4]
            else:
                dG, dH, S, ref = dGH2O[0], dHH2O[0], SH2O[0], 'IAWPS95'
            fout.writelines( "*    reference-state data source = %s\n" % ref)           
            fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dG/1000) )
            fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dH/1000))
            fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % S)
            fout.writelines( "\n")
        else:
            continue

    fout.writelines( "-end-\n\n")

    #only used for counter
    solidsolution_no = 11
    if solid_solution == True:
        if len(nCa_cpx) == 0:
            nCa = 0
            counter = 3*solidsolution_no # no cpx
        else:
            nCa = nCa_cpx[0]
            if nCa > 0:
                counter = 4*solidsolution_no
            else:
                counter = 3*solidsolution_no  # no cpx
    else:
        counter = 0
    for j in specielist[4]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            if solid_solution == False:
                counter += 1
            elif solid_solution == True:
                if (j not in ['Anorthite', 'Albite', 'Forsterite', 'Fayalite', 'Enstatite', 'Ferrosilite']): # | ((nCa == 1) and (j not in ['Diopside', 'Hedenbergite'])):
                    counter += 1
                else:
                    continue

    if (solid_solution == True) and (nCa == 1):
        fout.writelines( "   %s minerals\n\n" % (counter-2))
    else:
        fout.writelines( "   %s minerals\n\n" % (counter))

    # print('-> Processing mineral output')
    #%% Mineral reactions
    #skip lines till "gases" rows
    for i in range(20000):
        s = fid.readline()
        if s.rstrip('\n').lstrip('0123456789.- ') == 'gases':
            break
    if solid_solution == True:
        mineralcount = 0
        for XAn in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKAnAb(XAn, T, P, dbacessdic, **rhoEG)
            outputtxt(fout, logK, Rxn)
            mineralcount += 1
        for XFo in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKFoFa(XFo, T, P, dbacessdic, **rhoEG)
            outputtxt(fout, logK, Rxn)
            mineralcount += 1
        for XEn in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKEnFe(XEn, T, P, dbacessdic, **rhoEG)
            outputtxt(fout, logK, Rxn)
            mineralcount += 1
        if nCa > 0:
            for XMg in np.round(np.linspace(1, 0, solidsolution_no), 1):
                logK, Rxn = calclogKDiHedEnFe(nCa, XMg, T, P, dbacessdic, **rhoEG)
                outputtxt(fout, logK, Rxn)
                mineralcount += 1
        
    for j in specielist[4]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            if solid_solution == True and j in ['Anorthite', 'Albite', 'Forsterite', 'Fayalite', 'Enstatite', 'Ferrosilite']:
                continue
            elif solid_solution == True and (nCa == 1) and j in ['Diopside', 'Hedenbergite']:
                continue
            else:
                fout.writelines("%s                       " % j)
                fout.writelines( "type= %s\n" %  Mineraltype[j])
                if dbacessdic[j][0] == 'nan':
                    fout.writelines('     formula= %s' % sourcedic[j][0])
                else:
                    fout.writelines('     formula= %s\n' % dbacessdic[j][0])
                fout.writelines("     mole vol.=   %1.3f cc" %  dbacessdic[j][5])
                fout.writelines("      mole wt.=  %1.4f g\n" %  MWdic[j])
                fout.writelines("     %s species in reaction\n" % sourcedic[j][1])
                Rxn = sourcedic[j][2:]
                for i in range(len(Rxn)):
                    i = i + 1
                    if (i == 1) | (i == 7) | (i == 13):
                        fout.writelines("%8.4f " %  float(Rxn[i - 1]))
                    elif i % 2 != 0:
                        fout.writelines("%8.4f " %  float(Rxn[i - 1]))
                    else:
                        fout.writelines("%-8s          " %  (Rxn[i - 1]))
                    if (i % 6 == 0) | (i % 12 == 0) | (i == len(Rxn)):
                        fout.writelines( "\n")
                logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist, **rhoEG)
                for i in range(len(logK)):
                    i = i + 1
                    if (i == 1) | (i == 5) | (i == 9):
                        fout.writelines("       %8.4f" %  logK[i-1])
                    else:
                        fout.writelines("  %8.4f" %  logK[i-1])
                    if (i % 4 == 0) | (i == len(logK)):
                        fout.writelines( "\n")        
            
                fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
                if dbacessdic[j][0] == 'nan':
                    fout.writelines( "*    extrapolation algorithm: supcrt92/water95\n" )
                else:
                    fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
                fout.writelines( "*    reference-state data source = %s\n" % dbacessdic[j][1])           
                fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dbacessdic[j][2]/1000) )
                fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dbacessdic[j][3]/1000))
                fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % dbacessdic[j][4])
                fout.writelines( "\n")
        else:
            continue
            
    fout.writelines( "-end-\n\n")

    #only used for counter
    counter = 0
    for j in specielist[5]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            counter +=1
    fout.writelines( "   %s gases\n\n" % counter )
     
    # print('-> Processing gases output')
    #%% Gas reactions
    #skip lines till "oxides" rows
    for i in range(20000):
        s = fid.readline()
        # print(s)
        if s.rstrip('\n').lstrip('0123456789.- ') == 'oxides':
            break
        
    for j in specielist[5]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            fout.writelines("%s\n" % j)
            fout.writelines( "      mole wt.=   %1.4f g\n" %  MWdic[j])
            fout.writelines( "     %s species in reaction\n" % sourcedic[j][1])
            Rxn = sourcedic[j][2:]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 7) | (i == 13):
                    fout.writelines( "    %8.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%8.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( "%-8s     " %  (Rxn[i - 1]))
                if (i % 6 == 0) | (i % 12 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist, **rhoEG)
            for i in range(len(logK)):
                i = i + 1
                fout.writelines( "      %8.4f" %  logK[i-1])
                if (i % 4 == 0) | (i == len(logK)):
                    fout.writelines( "\n")        
        
            fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
            fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
            fout.writelines( "*    reference-state data source = %s\n" % dbacessdic[j][1])           
            fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dbacessdic[j][2]/1000) )
            fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dbacessdic[j][3]/1000))
            fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % dbacessdic[j][4])
            fout.writelines( "\n")
        else:
            continue
            
    fout.writelines( "-end-\n\n")

    #only used for counter
    counter = 0
    for j in specielist[6]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            counter +=1
    fout.writelines( "   %s oxides\n\n" % counter )
        
    # print('-> Processing oxides output')
    #%% Gas reactions
    #skip lines till "references" rows
    for i in range(20000):
        s = fid.readline()
        # print(s)
        if s.rstrip('\n').lstrip('0123456789.- ') == 'references':
            break
        
    for j in specielist[6]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            fout.writelines("%s\n" % j)
            fout.writelines( "      mole wt.=   %1.4f g\n" %  MWdic[j])
            fout.writelines( "     %s species in reaction\n" % sourcedic[j][1])
            Rxn = sourcedic[j][2:]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 7) | (i == 13):
                    fout.writelines( "    %8.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%8.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( "%-8s     " %  (Rxn[i - 1]))
                if (i % 6 == 0) | (i % 12 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            fout.writelines( "\n")
        else:
            continue
    fout.writelines( "-end-\n\n")
    fout.writelines('references\n')
    fout.writelines('** Please copy references to here from the corresponding\n')
    fout.writelines('** sequential-access version of the direct-access SUPCRT database.\n stop.\n\n')

    #%% close all files
    fid.close()
    fout.close()
        
    return print('Success, your new database is ready for download in the "output" folder')

    
