# -*- coding: utf-8 -*-
"""
Created on Wed Jul 28 16:45:32 2021

@author: adedapo.awolayo
"""

import re
import textwrap
J_to_cal = 4.184


def readAqspecdb(dbaccess):
    """
    This function reads direct access thermodynamic database and can add other database sources
    at the bottom returns all constants of dG [cal/mol], dH [cal/mol], S [cal/mol-K], V [cm3/mol]
    a [cal/mol-K], b [*10**3 cal/mol/K^2], c [*10^-5 cal/mol/K] ) etc. for minerals, gases and
    aqueous species packed into a dbacess dictionary
    Parameters
    ----------
        dbaccess        filename of the direct-access database
    Returns
    ----------
        dbacessdic      dictionary of minerals, gases, redox and aqueous species
        dbaccess        dat file name
    Usage:
    ----------
    [dbacessdic, dbname] = readAqspecdb(dbaccess)
    """
    # check if its a single liner data
    with open(dbaccess) as g:
        Rd = g.readlines()

    dbacessdic = {}; counter = 0
    # if it is single line data like for dpeq20
    if len(Rd) <= 1:
        width = re.search('                     3 ', Rd[0]).start()
        Rd = textwrap.wrap(Rd[0], width=width)

        for i in range(len(Rd)): #
            s1 = Rd[i]
            if not s1.startswith(('*', ' '),0) | (s1.rstrip('\n').lstrip('0123456789.- ') == "") | (s1[0] == "-") :
                if (s1.strip()[:3] != 'ref') and (s1.strip()[:3] != 'REF'):
                    name = s1.strip().split()[0]
                    if len(s1.strip().split()) > 1:
                        formula = s1.strip().split()[1]
                    else:
                        formula = ''
                    s2 = Rd[i + 1]
                    if (s2.strip()[:3] != 'ref') and (s2.strip()[:3] != 'REF') and (s2.split()[0].lstrip('0123456789.,- ') != ''):
                        s3 = Rd[i+2]; s4 = Rd[i+3]; s5 = Rd[i+4];
                        if s3.split()[0].lstrip('0123456789.,- ') != '':
                            if i >= len(Rd) - 5:
                                s6 = 'Null'; s7 = 'Null'; s8 = 'Null';
                                s9 = 'Null'; s10 = 'Null'; s11 = 'Null';
                            elif i >= len(Rd) - 6:
                                s6  = Rd[i + 5]; s7 = 'Null'; s8 = 'Null';
                                s9 = 'Null'; s10 = 'Null'; s11 = 'Null';
                            else:
                                s6  = Rd[i + 5]; s7 = Rd[i + 6]; s8 = Rd[i + 7];
                                s9 = Rd[i + 8]; s10 = Rd[i + 9]; s11 = Rd[i + 10];

                            if s3.strip()[:3] == 'ref':
                                ref = s3#.split()[0][4:]
                            else:
                                ref = s3#.split()[0]
                            if (s6.lower().islower() == True) | (s6.strip().split()[0] == name):
                                params = s4.split() + s5.split()
                            elif (s7.lower().islower() == True) | (s7.strip().split()[0] == name):
                                params = s4.split() + s5.split() + s6.split()
                            elif (s8.lower().islower() == True) | (s8.strip().split()[0] == name):
                                params = s4.split() + s5.split() + s6.split() + s7.split()
                            elif (s9.lower().islower() == True) | (s9.strip().split()[0] == name):
                                params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split()
                            elif (s10.lower().islower() == True) | (s10.strip().split()[0] == name):
                                params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split() + \
                                    s9.split()
                            elif (s11.lower().islower() == True) | (s11.strip().split()[0] == name):
                                params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split() + \
                                    s9.split() + s10.split()
                            else:
                                params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split() + \
                                    s9.split() + s10.split() + s11.split()
                            params = [float(i) if float(i) != 999999 else 0 for i in params]
                            counter += 1
                            if name in dbacessdic.keys():
                                print('Duplicate found for species "%s" in %s' % (name, dbaccess.split('/')[-1]))
                                continue
                            else:
                                dbacessdic[name] = [formula, ref] + params
    else:
    # else for multi line data like for speq20
        for i in range(len(Rd)): #
            s1 = Rd[i].rstrip('\n').strip()
            if  (not s1.lstrip().startswith('*', 0)) and (s1.lstrip('0123456789.- \t') != ""):
                if (s1[:3] != 'ref') and (s1[:3] != 'REF') and (s1.split()[0] not in ['minerals', 'gases', 'gas', 'aqueous', 'abandoned']):
                    name = s1.strip().split()[0]
                    if len(s1.split()) > 1:
                        formula = s1.split()[1]
                    else:
                        formula = ''
                    s2 = Rd[i + 1].strip()
                    if (s2[:3] != 'ref') and (s2[:3] != 'REF') and (s2[0].lstrip('0123456789.,- ') != ''):
                        s3 = Rd[i+2]; s4 = Rd[i+3]; s5 = Rd[i+4];
                        if i >= len(Rd) - 5:
                            s6 = 'Null'; s7 = 'Null'; s8 = 'Null';
                            s9 = 'Null'; s10 = 'Null'; s11 = 'Null';
                        elif i >= len(Rd) - 6:
                            s6  = Rd[i + 5]; s7 = 'Null'; s8 = 'Null';
                            s9 = 'Null'; s10 = 'Null'; s11 = 'Null';
                        else:
                            s6  = Rd[i + 5]; s7 = Rd[i + 6]; s8 = Rd[i + 7];
                            s9 = Rd[i + 8]; s10 = Rd[i + 9]; s11 = Rd[i + 10];

                        if s3.strip()[:3] == 'ref':
                            ref = s3#.split()[0][4:]
                        else:
                            ref = s3#.split()[0]
                        if (s6.lower().islower() == True) | s6.startswith('*', 0):
                            params = s4.split() + s5.split()
                        elif (s7.lower().islower() == True) | s7.startswith('*', 0):
                            params = s4.split() + s5.split() + s6.split()
                        elif (s8.lower().islower() == True) | s8.startswith('*', 0):
                            params = s4.split() + s5.split() + s6.split() + s7.split()
                        elif (s9.lower().islower() == True) | s9.startswith('*', 0):
                            params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split()
                        elif (s10.lower().islower() == True) | s10.startswith('*', 0):
                            params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split() + \
                                s9.split()
                        elif (s11.lower().islower() == True) | s11.startswith('*', 0):
                            params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split() + \
                                s9.split() + s10.split()
                        else:
                            params = s4.split() + s5.split() + s6.split() + s7.split() + s8.split() + \
                                s9.split() + s10.split() + s11.split()
                        params = [float(i) if float(i) != 999999 else 0 for i in params]
                        counter += 1
                        if name in dbacessdic.keys():
                            print('Duplicate found for species "%s" in %s' % (name, dbaccess.split('/')[-1]))
                            continue
                        else:
                            dbacessdic[name] = [formula, ref] + params

                        if len(s5.split()) != 0 and len(s6.split()) and (s5.strip() == '' and s6.strip() == ''):
                            break
                        elif len(s7.split()) != 0 and len(s6.split()) and (s6.strip() == '' and s7.strip() == ''):
                            break
                        elif len(s7.split()) != 0 and len(s8.split()) and (s7.strip() == '' and s8.strip() == ''):
                            break
                        elif len(s8.split()) != 0 and len(s9.split()) and (s8.strip() == '' and s9.strip() == ''):
                            break
                        elif len(s9.split()) != 0 and len(s10.split()) and (s9.strip() == '' and s10.strip() == ''):
                            break
                        elif len(s6.split()) != 0 and (s6.split()[-1] in ['(nmin1)', '(nmin2)', '(nmin3)', '(nmin4)', '(ngas)', '(naqs)']):
                            break
                        elif len(s7.split()) != 0 and (s7.split()[-1] in ['(nmin1)', '(nmin2)', '(nmin3)', '(nmin4)', '(ngas)', '(naqs)']):
                            break
                        elif len(s8.split()) != 0 and (s8.split()[-1] in ['(nmin1)', '(nmin2)', '(nmin3)', '(nmin4)', '(ngas)', '(naqs)']) :
                            break
                        elif len(s9.split()) != 0 and (s9.split()[-1] in ['(nmin1)', '(nmin2)', '(nmin3)', '(nmin4)', '(ngas)', '(naqs)']) :
                            break
                        elif len(s10.split()) != 0 and (s10.split()[-1] in ['(nmin1)', '(nmin2)', '(nmin3)', '(nmin4)', '(ngas)', '(naqs)']):
                            break

    #%% other sources aside speq20 for solid solution calculation
    #dG dH S V a1 a2 a3 a4 a5
    # dG, dH, S from Arnorsson 1999, V and Cp from Robie and Hemingway #1995
    _Anorthite_ = ['Ca(Al2Si2)O8', 'R&H95, Stef2001',-4002095, -4227830, 199.30, 100.790*J_to_cal,
                   5.168e2, -9.249e-2, -1.408e6, -4.589e3, 4.188e-5]
    dbacessdic['ss_Anorthite'] = [x/J_to_cal if type(x)!=str else x for x in _Anorthite_ ]

    dbacessdic['ss_Albite_high'] = ['NaAlSi3O8', 'R&H95, Stef2001', -887368.32+8413/J_to_cal,
                                    -940769.52, 224.14/J_to_cal, 100.07, 139.56, -0.0221916826,
                                    401051.6, -1535.37, 5.430210e-06]

    _K_feldspar_ = ['KAlSi3O8', 'R&H95, A&S99', -3745958, -3965730, 232.90, 108.960*J_to_cal,
                    6.934e2, -1.717e-1, 3.462e6, -8.305e3, 4.919e-5]
    dbacessdic['ss_K-feldspar'] = [x/J_to_cal if type(x)!=str else x for x in _K_feldspar_ ]

    _Ferrosilite_ = ['FeSiO3', 'R&H95, Stef2001', -1118000, -1195200, 94.6, 33.0*J_to_cal,
                   1.243e2, 1.454e-2, -3.378e6, 0, 0]
    dbacessdic['ss_Ferrosilite'] = [x/J_to_cal if type(x)!=str else x for x in _Ferrosilite_]

    _Enstatite_=['MgSiO3', 'R&H95, Stef2001', -1458300, -1545600,  66.3, 31.31*J_to_cal,
                 3.507e2, -1.472e-1, 1.769e6, -4.296e3, 5.826e-5]
    dbacessdic['ss_Enstatite'] = [x/J_to_cal if type(x)!=str else x for x in _Enstatite_]

    _Clinoenstatite_=['MgSiO3', 'R&H95, Stef2001', -1458100, -1545000,  67.9, 31.28*J_to_cal,
                 2.056e2, -1.280e-2, 1.193e6, -2.298e3, 0]
    dbacessdic['ss_Clinoenstatite'] = [x/J_to_cal if type(x)!=str else x for x in _Clinoenstatite_]

    _Hedenbergite_=['CaFeSi2O6', 'R&H95, Stef2001', -2676300, -2839900,  174.2, 67.950*J_to_cal,
                 3.1046e2, 1.257e-2, -1.846e6, -2.040e3, 0]
    dbacessdic['ss_Hedenbergite'] = [x/J_to_cal if type(x)!=str else x for x in _Hedenbergite_]

    _Diopside_=['CaMgSi2O6', 'R&H95, Stef2001', -3026800, -3201500,  142.7 , 66.090*J_to_cal,
                 4.7025e2, -9.864e-2, 0.2454e6, -4.823e3, 2.813e-5]
    dbacessdic['ss_Diopside'] = [x/J_to_cal if type(x)!=str else x for x in _Diopside_]

    #dG dH S V a1 a2 a3 a4 a5
    _Forsterite_ = ['Mg2SiO4', 'R&H95, Stef2001', -2053600, -2171850, 94.1, 43.65*J_to_cal,
                  8.736e1, 8.717e-2, -3.699e6, 8.436e2, -2.237e-5]
    dbacessdic['ss_Forsterite'] = [x/J_to_cal if type(x)!=str else x for x in _Forsterite_]

    _Fayalite_ = ['Fe2SiO4', 'R&H95, Stef2001', -1379100, -1478200, 151, 46.31*J_to_cal, 1.7602e2,
                -8.808e-3, -3.889e6, 0, 2.471e-5]
    dbacessdic['ss_Fayalite'] = [x/J_to_cal if type(x)!=str else x for x in _Fayalite_]

    _Fluorapatite_ = ['Ca5(PO4)3F', 'R&H95', -6489700, -6872000, 387.9, 157.56*J_to_cal,
                  7.543e2, -3.026e-2, -0.9084e6, -6.201e3, 0]
    dbacessdic['Fluorapatite'] = [x/J_to_cal if type(x)!=str else x for x in _Fluorapatite_]

    _Hydroxyapatite_ = ['Ca5(OH)(PO4)3', 'R&H95', -6337100, -6738500, 390.4, 159.6*J_to_cal,
                        3.878e2, 11.186e-2, -12.70e6, 1.811e3, 0]
    dbacessdic['Hydroxyapatite'] = [x/J_to_cal if type(x)!=str else x for x in _Hydroxyapatite_]

    dbacessdic['Ankerite'] = ['CaFe(CO3)2', 'HP2011               31.DEC.11\n', -434945.7, -471178.3,
                              45.043, 66.060,  81.500956, -0.277486, 0, -730.114720, 0]

    dbacessdic['Acmite'] = ['NaFeSi2O6', 'HP2011               31.DEC.11\n', -577476.5, -617454.6,
                            40.774, 64.590,  1.994e2, 6.197e-2, -4.267e6, 0, 0]

    _Annite_ = ['KFe3AlSi3O10(OH)2', 'R&H95', -4798300, -5149300, 415.0, 154.3*J_to_cal,
                6.366e2, 8.208e-2, -4.860e6, -3.731e3, 0]
    dbacessdic['ss_Annite'] = [x/J_to_cal if type(x)!=str else x for x in _Annite_]

    _Phlogopite_ = ['KMg3AlSi3O10(OH)2', 'R&H95', -5860500, -6246000, 315.9, 149.65*J_to_cal,
                    8.639e2, -7.6076e-2, 3.5206e5, -8.470e3, 0]
    dbacessdic['ss_Phlogopite'] = [x/J_to_cal if type(x)!=str else x for x in _Phlogopite_]


    return dbacessdic, dbaccess.split('/')[-1]


def readSourceGWBdb(sourcedb):
    """
    This function reads source GWB thermodynamic database and reaction coefficients of 'eh'
    and 'e-' has been added at the bottom returns all reaction coefficients and species,
    group species into redox, minerals, gases, oxides and aqueous species
    Parameters
    ----------
        sourcedb      :     filename of the source database
    Returns
    ----------
        sourcedic     :     dictionary of reaction coefficients and species
        specielist    :     list of species segmented into the different categories
                            [element, basis, redox, aqueous, minerals, gases, oxides]
        chargedic     :     dictionary of charges of species
        MWdic         :     dictionary of MW of species
        Mineraltype   :     mineral type for minerals
        fugacity_info :     fugacity information as stored in new tdat database for chi and critical ppts
    Usage:
    ----------
    [sourcedic, specielist, chargedic, MWdic, Mineraltype, fugacity_info, activity_model] = readSourceGWBdb(sourcedb)
    """
    with open(sourcedb) as g:
        for line in g:
            if line.startswith('activity model'):
                break

    with open(sourcedb) as g:
        Rd = g.readlines()
    activity_model = line.strip('\n').split()[-1]
    data_fmt = [x for x in Rd if 'dataset format' in x][0].strip('\n').split(':')[-1].strip()

    unwanted = ['elements', 'basis species', 'redox couples', 'aqueous species',
                'free electron', 'minerals', 'gases', 'oxides', 'stop.' ]
    #capture line numbers with line break
    d=[]; previousline = ''
    with open(sourcedb) as fid:
        for idx, line in enumerate(fid, 1):
            if line.strip().rstrip('\n').lstrip('0123456789.- ') in unwanted:
                x=idx
                d.append(x-1)
            #elif line.strip(' \n*').startswith(('virial coefficients', 'Virial coefficients', 'SIT epsilon coefficients', 'Pitzer parameters')):
            elif previousline.startswith('-end-') and line.startswith('*'):
                x=idx; #print(x)
                d.append(x+1)
                break
            previousline = line

    if activity_model == 'h-m-w':
        d_act = [i for i, x in enumerate(Rd[d[-1]:]) if x.strip('\n') ==''][0]
    f = open(sourcedb, 'r')
    #skip first 11 lines of database  .lstrip('0123456789.- ')
    for i in range(d[1]+2):
      s1 = f.readline()

    sourcedic = {} # initialize dictionary
    for i in range(d[-1]-d[1]):   #
        s1 = f.readline()
        if s1.strip(' \n*').startswith(("references", 'virial coefficients', 'Virial coefficients', 'SIT epsilon coefficients', 'Pitzer parameters')) :
            break
        if not s1.startswith((' ', '*'), 0) | (s1.rstrip('\n') == "") | (len(s1) !=0 and s1[0] == "-") :
            if s1.rstrip('\n').lstrip('0123456789.- ') in unwanted:
                continue
            elif s1.rstrip('\n').lstrip('0123456789.- ') == '':
                continue
            else:
                specie_name = s1.strip().split()[0]
                s2 = f.readline(); s3 = f.readline()
                s4 = f.readline(); s5 = f.readline();
                if (s5.rstrip('\n') != ""):
                    s6 = f.readline()
                    if (s6.rstrip('\n') != ""):
                        s7 = f.readline()
                        if not s2.startswith('*',0):
                            if (len(s2.split()) > 1) and (s2.split()[0] != 'formula='):
                                if len(s1.split('formula=')) <= 1:
                                    specie_formula = ""
                                else:
                                    specie_formula = s1.rstrip('\n').split('formula=')[1]
                                if not s3.lstrip().startswith(('chi', 'Pcrit'), 0):
                                    species_num = int(s3.split()[0])
                                    if species_num <= 3:
                                        reactant = s4.split()
                                    elif species_num <= 6:
                                        reactant = s4.split() + s5.split()
                                    else:
                                        reactant = s4.split() + s5.split() + s6.split()
                                else:
                                    if not s4.lstrip().startswith(('chi','Pcrit'),0):
                                        species_num = int(s4.split()[0])
                                        if species_num <= 3:
                                            reactant = s5.split()
                                        elif species_num <= 6:
                                            reactant = s5.split() + s6.split()
                                        else:
                                            reactant = s5.split() + s6.split() + s7.split()
                                    else:
                                        species_num = int(s5.split()[0])
                                        if species_num <= 3:
                                            reactant = s6.split()
                                        else:
                                            reactant = s6.split() + s7.split()
                            else:
                                if len(s2.split('formula=')) <= 1:
                                    specie_formula = ""
                                else:
                                    specie_formula = s2.rstrip('\n').split('formula=')[1]
                                species_num = int(s4.split()[0])
                                if species_num <= 3:
                                    reactant = s5.split()
                                elif species_num <= 6:
                                    reactant = s5.split() + s6.split()
                                else:
                                    reactant = s5.split() + s6.split() + s7.split()
                        else:
                            specie_formula = s2.split()[2]
                            species_num = int(s4.split()[0])
                            if species_num <= 3:
                                reactant = s5.split()
                            elif species_num <= 6:
                                reactant = s5.split() + s6.split()
                            else:
                                reactant = s5.split() + s6.split() + s7.split()
                    else:
                        specie_formula = ""
                        species_num = int(s3.split()[0])
                        if species_num <= 3:
                            reactant = s4.split()
                        elif species_num <= 6:
                            reactant = s4.split() + s5.split()
                        else:
                            reactant = s4.split() + s5.split() + s6.split()
                else:
                    specie_formula = ""
                    species_num = int(s3.split()[0])
                    if species_num <= 3:
                        reactant = s4.split()
                    elif species_num <= 6:
                        reactant = s4.split() + s5.split()
                    else:
                        reactant = s4.split() + s5.split() + s6.split()

        sourcedic[specie_name] = [specie_formula, species_num] + reactant
    sourcedic['eh'] = ['eh', 3, '-2.0000', 'H2O', '1.0000', 'O2(g)', '4.0000', 'H+']
    sourcedic['e-'] = ['e-', 3, '0.50000', 'H2O', '-0.2500', 'O2(g)', '-1.0000', 'H+']

    element = []; basis = []; redox = []; aqueous = []; minerals = []; gases = []; oxides = [];
    charge = []; MW = []; electron = []; Mineraltype = {}; fugacity_chi = {}; fugacity_Pcrit = {}
    with open(sourcedb) as fid:
        for i, line in enumerate(fid, 1):
            previousline = line
            if (line.strip(' \n*').startswith(("references", 'Pitzer parameters', 'virial coefficients', 'Virial coefficients', 'SIT epsilon coefficients'))):
                break
            # elif previousline.startswith('-end-') and line.startswith('*'):
            #     break
            if not line.startswith((' ','*'),0) | (line.rstrip('\n') == "") | (line[0] == "-") :
                if not line.split()[0].replace('.','',1).isnumeric():
                    if not line.startswith(('charge', 'mole', 'formula'), 0):
                        if data_fmt == 'oct94':
                            if d[0] < i < d[1]:
                                element.append(line.split()[0])
                            elif d[1] < i < d[2]:
                                basis.append(line.split()[0])
                            elif d[2] < i < d[3]:
                                redox.append(line.split()[0])
                            elif d[3] < i < d[4]:
                                aqueous.append(line.split()[0])
                            elif d[4] < i < d[5]:
                                minerals.append(line.split()[0])
                            elif d[5] < i < d[6]:
                                gases.append(line.split()[0])
                            elif i > d[6]:
                                oxides.append(line.split()[0])
                        elif data_fmt == 'apr20':
                            if d[0] < i < d[1]:
                                element.append(line.split()[0])
                            elif d[1] < i < d[2]:
                                basis.append(line.split()[0])
                            elif d[2] < i < d[3]:
                                redox.append(line.split()[0])
                            elif d[3] < i < d[4]:
                                aqueous.append(line.split()[0])
                            elif d[4] < i < d[5]:
                                electron.append(line.split()[0])
                            elif d[5] < i < d[6]:
                                minerals.append(line.split()[0])
                            elif d[6] < i < d[7]:
                                gases.append(line.split()[0])
                            elif i > d[7]:
                                oxides.append(line.split()[0])
            if re.compile(r"charge").search(line) != None:
                charge.append(line)
            if re.compile(r"mole wt.=").search(line) != None:
                MW.append(re.sub('[^0123456789\.]', '', line.strip('\n').split('wt.=')[1]))
            if re.compile(r"type=").search(line) != None:
                if len(line.split()) <= 2:
                    Mineraltype[line.split()[0]] = ''
                else:
                    Mineraltype[line.split()[0]] = line.split()[2]
            if re.compile(r"chi=").search(line) != None:
                fugacity_chi[gases[-1]] = line
            if re.compile(r"Pcrit=").search(line) != None:
                fugacity_Pcrit[gases[-1]] = line

    act_list = []; #previousline = ''
    if activity_model == 'h-m-w':
        with open(sourcedb) as fid:
            for i, line in enumerate(fid, 1):
                if i > d[-1] + d_act:
                    if not line.startswith(('  ', '\n', '-end-', '*')):
                        act_list.append(line)

    act_param = {'activity_model': activity_model, 'act_list': act_list, 'dataset_format' : data_fmt}
    fugacity_info = {'fugacity_chi': fugacity_chi,'fugacity_Pcrit': fugacity_Pcrit}
    res = basis + redox + aqueous + electron
    chargedic = {res[i]: charge[i].rstrip('\n') for i in range(len(charge))}
    res = element + basis + redox + aqueous + electron + minerals + gases + oxides
    MWdic = {res[i]: float(MW[i]) for i in range(len(MW))}
    specielist = [element, basis, redox, aqueous, electron, minerals, gases, oxides]
    fid.close()

    return sourcedic, specielist, chargedic, MWdic, Mineraltype, fugacity_info, act_param


def readSourceEQ36db(sourcedb):
    """
    This function reads source EQ3/6 thermodynamic database and reaction coefficients of 'eh'
    and 'e-' has been added at the bottom returns all reaction coefficients and species,
    group species into basis, auxiliary basis, minerals, gases, liquids and aqueous species
    Parameters
    ----------
        sourcedb      :     filename of the source database
    Returns
    ----------
        sourcedic     :     dictionary of reaction coefficients and species
        specielist    :     list of species segmented into the different categories
                            [element, basis, redox, aqueous, minerals, gases, oxides]
        chargedic     :     dictionary of charges of species and DHazero info
        MWdic         :     dictionary of MW of species
        Sptype        :     specie types and eq3/6 and revised date info
        Elemlist      :     dictionary of elements and coefficients
    Usage:
    ----------
    [sourcedic, specielist, chargedic, MWdic, block_info, Elemlist, act_param] = readSourceEQ36db(sourcedb)
    """
    with open(sourcedb) as g:
        for line in g:
            if line.startswith(('bdot parameters', 'single-salt parameters', 'ca combinations')):
                break

    if line.startswith('bdot parameters'):
        activity_model = 'debye-huckel'
    elif line.startswith(('single-salt parameters', 'ca combinations')):
        activity_model = 'h-m-w'

    subheading = ['elements', 'basis species', 'auxiliary basis species', 'aqueous species',
                'solids', 'liquids', 'gases', 'solid solutions', 'references', 'stop.']
    d = []
    with open(sourcedb) as f:
        for idx, line in enumerate(f, 1):
            if line.strip().rstrip('\n').lstrip('0123456789.- ') in subheading:
                x=idx
                d.append(x-1)
            if line.startswith(('bdot parameters', 'single-salt parameters', 'ca combinations')):
                x=idx
                d.append(x-1)

    element = []; basis = []; auxiliary = []; aqueous = []; minerals = []; MW = []; #DH = []
    gases = []; liquids = []; charge = []; solid_solutions = []; act_params = []

    with open(sourcedb) as f:
        for i, line in enumerate(f, 1):
            if (line.rstrip('\n') == "references"):
                break
            if not line.startswith('*',0) | line.startswith(' ',0) | (line.rstrip('\n') == "") | (line[0] == "-") :
                if not line.split()[0].replace('.','',1).isnumeric() and not line.startswith('+',0):
                    if line.strip().rstrip('\n').lstrip('0123456789.- ') not in subheading:
                        line = line.strip().rstrip('\n')
                        if d[1] < i < d[2]:
                            element.append(line.split()[0])
                            MW.append(float(line.split()[1]))
                        elif d[2] < i < d[3]:
                            basis.append(line.split('  ')[0])
                        elif d[3] < i < d[4]:
                            if any(re.findall(r'|'.join(('acid', 'high', 'low')), line.split('  ')[0], re.IGNORECASE)):
                                auxiliary.append(line.split('  ')[0].replace(' ', '_'))
                            else:
                                auxiliary.append(line.split('  ')[0])
                        elif d[4] < i < d[5]:
                            if any(re.findall(r'|'.join(('acid', 'high', 'low')), line.split('  ')[0], re.IGNORECASE)):
                                aqueous.append(line.split('  ')[0].replace(' ', '_'))
                            else:
                                aqueous.append(line.split('  ')[0])
                        elif d[5] < i < d[6]:
                            if any(re.findall(r'|'.join(('acid', 'high', 'low', 'anhyd', 'hydr')), line.split('  ')[0], re.IGNORECASE)):
                                minerals.append(line.split('  ')[0].replace(' ', '_'))
                            else:
                                minerals.append(line.split('  ')[0])
                        elif d[6] < i < d[7]:
                            liquids.append(line.split('  ')[0])
                        elif d[7] < i < d[8]:
                            gases.append(line.split('  ')[0])
                        elif i > d[8]:
                            solid_solutions.append(line.split('  ')[0])
            if re.compile(r"charge").search(line) != None:
                charge.append(line)
            if d[0] < i < d[1]:
                act_params.append(line)

    MWdic = {element[i]: float(MW[i]) for i in range(len(MW))}
    res = basis + auxiliary + aqueous + minerals + liquids + gases + solid_solutions
    lstname = []; block_info = {}; Elemlist = {}; sourcedic = {}
    act_param = {'activity_model': activity_model, 'act_list': None, 'alpha_beta' : {},
                 'theta':{}, 'lambda':{}, 'psi':{},'zeta':{},'mu':{}, 'beta0' : {},
                 'beta1' : {}, 'beta2' : {}, 'alpha1' : {}, 'alpha2' : {}, 'cphi' : {}}
    keywords = [["+" + "-"*30,   "+" + "-"*30]  ]
    for num, k in enumerate(keywords):
        lst = []; counter = 0
        f = open(sourcedb, 'r')

        for i in range(d[1] + len(element)):#
            s1 = f.readline()
            if (s1.rstrip('\n') == "elements") :
                break
            act_list = [act_params[i +1] for i,x in enumerate(act_params) if x.rstrip('\n').startswith(k[0])]
            act_list = [x for x in act_list if not x.startswith(('*', 'nn', 'nc', 'cc', 'mixture'))]
            if (activity_model == 'h-m-w') and any(s1.lstrip().rstrip('\n').startswith(x) for x in act_list):
                lst = []; lst.append(s1)
                for j in range(50):
                    s = f.readline()
                    if s.lstrip().rstrip('\n').startswith(k[1]):
                        break
                    elif not s.lstrip().startswith(('****', '*',  'single-salt parameters', 'ca combinations')) | (s.rstrip('\n').strip('0123456789.- ') in ["", 'Miscellaneous parameters']+subheading):
                        lst.append(s)
                if len(lst) > 2:
                    if any(['mu' in x for x in lst]):
                        checker = [x.strip(' \n').split()[-1] for i, x in enumerate(lst) if x.lstrip().startswith('mu')]
                        if re.sub('[^0123456789\.]', '', checker[0]) == '':
                            first_a = [i for i, x in enumerate(lst) if x.lstrip().startswith('a')][0]
                            mu = float(lst[first_a].split()[-1])
                        else:
                            mu = float(re.sub('[^0123456789\.]', '', checker[0]))
                        act_param['mu'][lst[0].rstrip('\n')] = mu
                    if any(['zeta' in x for x in lst]):
                        checker = [x.strip(' \n').split()[-1] for i, x in enumerate(lst) if x.lstrip().startswith('zeta')]
                        if re.sub('[^0123456789\.]', '', checker[0]) == '':
                            first_a = [i for i, x in enumerate(lst) if x.lstrip().startswith('a')][0]
                            zeta = float(lst[first_a].split()[-1])
                        else:
                            zeta = float(re.sub('[^0123456789\.]', '', checker[0]))
                        act_param['zeta'][lst[0].rstrip('\n')] = zeta
                    if any([('alpha' or 'beta') in x for x in lst]):
                        act_param['alpha_beta'][lst[0].rstrip('\n')] = lst
                        lster = ['beta0', 'beta1', 'beta2', 'alpha1', 'alpha2', 'cphi']
                        for pos in lster:
                            checker = [x.strip(' \n') for i, x in enumerate(lst) if pos in re.sub('[()]', '', x).lstrip().lower()]
                            if all([x in checker[0] for x in lster[:3]]):
                                checker = checker[0].replace("=", "").split()
                                par = float(checker[checker.index(pos) + 1])
                            elif all([x in checker[0] for x in lster[3:5]]):
                                checker = checker[0].replace("=", "").split()
                                par = float(checker[checker.index(pos) + 1])
                            else:
                                # checker = checker.split()[-1]
                                if re.sub('[^0123456789\.]', '', re.sub('[(012)]', '', checker[0])) == '':
                                    first_a = [i for i, x in enumerate(lst) if pos in re.sub('[()]', '', x).lstrip().lower() ][0] + 1
                                    par = float(lst[first_a].split()[-1])
                                else:
                                    par = float(re.sub('[^0123456789\.]', '', re.sub('[()]', '', checker[0])))
                            act_param[pos][lst[0].rstrip('\n')] = par
                    if any(['psi' in x for x in lst]):
                        checker = [x.strip(' \n') for i, x in enumerate(lst) if 'psi' in x.lstrip() ]
                        if all([x in checker[0] for x in [' psi', 'theta']]):
                            checker = checker[0].replace("=", "").split()
                            psi = float(checker[checker.index('psi') + 1])
                        else:
                            if re.sub('[^0123456789\.]', '', checker[0]) == '':
                                first_a = [i for i, x in enumerate(lst) if x.lstrip().startswith('a')][0]
                                psi = float(lst[first_a].split()[-1])
                            else:
                                psi = float(re.sub('[^0123456789\.]', '', checker[0]))
                        act_param['psi'][lst[0].rstrip('\n')] = psi
                    if any(['theta' in x for x in lst]):
                        checker = [x.strip(' \n') for i, x in enumerate(lst) if 'theta' in x.lstrip() ]
                        if all([x in checker[0] for x in ['theta', ' psi']]):
                            checker = checker[0].replace("=", "").split()
                            theta = float(checker[checker.index('theta') + 1])
                        else:
                            if re.sub('[^0123456789\.]', '', checker[0]) == '':
                                first_a = [i for i, x in enumerate(lst) if x.lstrip().startswith('a')][0]
                                theta = float(lst[first_a].split()[-1])
                            else:
                                theta = float(re.sub('[^0123456789\.]', '', checker[0]))
                        act_param['theta'][lst[0].rstrip('\n')] = theta
                    if any(['lambda' in x for x in lst]):
                        checker = [x.strip(' \n').split()[-1] for i, x in enumerate(lst) if 'lambda' in x.lstrip()]
                        if re.sub('[^0123456789\.]', '', checker[0]) == '':
                            first_a = [i for i, x in enumerate(lst) if x.lstrip().startswith('a')][0]
                            lambdaa = float(lst[first_a].split()[-1])
                        else:
                            lambdaa = float(re.sub('[^0123456789\.]', '', checker[0]))
                        act_param['lambda'][lst[0].rstrip('\n')] = lambdaa

        for i in range(d[-1]) :  #
            s = f.readline()
            if (s.rstrip('\n') == "references") :
                break
            s = s.replace(" acid", "_acid").replace(" high", "_high").replace(" low", "_low").replace(" anhyd", "_anhyd").replace(" hydr", "_hydr")

            if any(s.lstrip().rstrip('\n').startswith(x) for x in res):
                lst = []; lst.append(s); counter += 1
                for j in range(50):
                    s = f.readline()
                    if s.lstrip().rstrip('\n').startswith(k[1]):
                        break
                    elif not s.lstrip().startswith('****'):
                        lst.append(s)
                lst[0] = lst[0].rstrip('\n')
                if lst[0] not in subheading:
                    if lst[0].split('  ')[0] in res or lst[0].split()[0].replace(' ', '_') in res:
                        if any(re.findall(r'|'.join(('acid', 'high', 'low', 'anhyd', 'hydr')), lst[0].split('  ')[0], re.IGNORECASE)):
                            specie_name = lst[0].split('  ')[0].replace(' ', '_')
                        else:
                            specie_name = lst[0].split('  ')[0]
                        lstname.append(specie_name)
                        if specie_name not in solid_solutions:
                            indx, elem_numb =[(i,int(re.sub('[^0123456789\.]', '', x))) for i,x in enumerate(lst)
                                              if x.strip('0123456789.,-: ').startswith('element(s)')][0]
                            elem_rows = list(range(indx + 1, indx + 2)) if elem_numb <= 3 else list(range(indx + 1, indx + 3)) if elem_numb <= 6 else list(range(indx + 1, indx + 4)) if elem_numb <= 9 else list(range(indx + 1, indx + 5))
                            reactant = [lst[x].rstrip('\n').split('  ') for x in elem_rows]
                            reactant = [item for sublist in reactant for item in sublist] # convert list of list to list
                            reactant = [y for x in reactant for y in x.split() if x != '' and x != '****']
                            Elemlist[specie_name] = reactant
                        else:
                            elem_rows = [20]
                        if specie_name not in basis[:-1] + solid_solutions and counter > len(basis):
                            indx, species_num =[(i,int(re.sub('[^0123456789\.]', '', x))) for i,x in enumerate(lst)
                                                if x.strip('0123456789.,-: ').startswith('species in')][0]
                            spec_rows = list(range(indx + 1, indx + 2)) if species_num < 3 else list(range(indx + 1, indx + 3)) if species_num < 5 else list(range(indx + 1, indx + 4)) if species_num < 7 else list(range(indx + 1, indx + 5)) if species_num < 9 else  list(range(indx + 1, indx + 6))
                            reactants = [lst[x].rstrip('\n').split('  ') for x in spec_rows]
                            reactants = [item for sublist in reactants for item in sublist] # convert list of list to list
                            reactants = [y for x in reactants for y in x.split() if x != '' and x != '****']
                            reactants = [j.replace(' ', '_') if any(re.findall(r'|'.join(('acid', 'high', 'low', 'anhyd', 'hydr')),
                                                                                j, re.IGNORECASE)) else j for j in reactants]
                        else:
                            spec_rows = [20]
                        if specie_name in solid_solutions:
                            indx, species_num =[(i,int(re.sub('[^0123456789\.]', '', x))) for i,x in enumerate(lst)
                                                if x.strip('0123456789.,-: ').startswith('components')][0]
                            spec_rows = list(range(indx + 1, indx + 2)) if species_num < 3 else list(range(indx + 1, indx + 3)) if species_num < 5 else list(range(indx + 1, indx + 4)) if species_num < 7 else list(range(indx + 1, indx + 5)) if species_num < 9 else  list(range(indx + 1, indx + 6))
                            reactants = [lst[x].rstrip('\n').split('  ') for x in spec_rows]
                            reactants = [item for sublist in reactants for item in sublist] # convert list of list to list
                            reactants = [y for x in reactants for y in x.split() if x != '' and x != '****']
                        mw = [x for x in lst if x.strip('*    ').startswith('mol.wt.')]
                        mw = float(re.sub('[^0123456789\.]', '', mw[0].split()[-2])) if len(mw) != 0 else []
                        if len(lst[0].split()) <= 1:
                            specie_formula = ""
                        elif len(lst[0].split()) > 2:
                            specie_formula = lst[0].split()[2]
                        else:
                            specie_formula = lst[0].split()[1]
                        if (specie_name == 'O2(g)') and counter <= len(basis):
                            block_info['%s_b' % specie_name] = lst[1:min(elem_rows,spec_rows)[0] - 1]
                        elif specie_name in basis[:-1] + auxiliary + aqueous + minerals + liquids + gases:
                            block_info[specie_name] = lst[1:min(elem_rows,spec_rows)[0] - 1]
                        elif specie_name in solid_solutions:
                            block_info[specie_name] = [lst[1:min(spec_rows) - 1], lst[(max(spec_rows) + 1):]]
                        if specie_name in basis and counter <= len(basis):
                            sourcedic[specie_name] = [specie_formula, elem_numb] + reactant
                        elif specie_name == 'O2(g)':
                            sourcedic[specie_name] = [specie_formula, species_num] + reactants
                        else:
                            sourcedic[specie_name] = [specie_formula, species_num] + reactants
                        MWdic[specie_name] = mw

    sourcedic['eh'] = ['eh', 4, '-1.0000', 'eh', '-2.0000', 'H2O', '1.0000', 'O2(g)', '4.0000', 'H+']
    sourcedic['e-'] = ['e-', 4, '-1.0000', 'e-', '0.50000', 'H2O', '-0.2500', 'O2(g)', '-1.0000', 'H+']
    specielist = [element, basis, auxiliary, aqueous, minerals, liquids, gases, solid_solutions]
    res = basis + auxiliary + aqueous
    chargedic = {res[i]: charge[i].rstrip('\n') for i in range(len(charge))}
    act_param['act_list'] = act_list if activity_model == 'h-m-w' else ''
    act_param['activity_model'] = activity_model

    f.close()
    return sourcedic, specielist, chargedic, MWdic, block_info, Elemlist, act_param


def readSourceEQ36db_old(sourcedb):
    """
    This function reads source EQ3/6 thermodynamic database and reaction coefficients of 'eh'
    and 'e-' has been added at the bottom returns all reaction coefficients and species,
    group species into basis, auxiliary basis, minerals, gases, liquids and aqueous species
    Parameters
    ----------
        sourcedb      :     filename of the source database
    Returns
    ----------
        sourcedic     :     dictionary of reaction coefficients and species
        specielist    :     list of species segmented into the different categories
                            [element, basis, redox, aqueous, minerals, gases, oxides]
        chargedic     :     dictionary of charges of species and DHazero info
        MWdic         :     dictionary of MW of species
        Sptype        :     specie types and eq3/6 and revised date info
        Elemlist      :     dictionary of elements and coefficients
    Usage:
    ----------
    [sourcedic, specielist, chargedic, MWdic, Sptype, Elemlist] = readSourceEQ36db2(sourcedb)
    """

    with open(sourcedb) as g:
        for line in g:
            if line.startswith(('bdot parameters', 'single-salt parameters', 'ca combinations')):
                break

    subheading = ['elements', 'basis species', 'auxiliary basis species', 'aqueous species',
                'solids', 'liquids', 'gases', 'solid solutions', 'references', 'stop.']
    d = []
    with open(sourcedb) as fid:
        for idx, line in enumerate(fid, 1):
            if line.strip().rstrip('\n').lstrip('0123456789.- ') in subheading:
                x=idx
                d.append(x-1)
            if line.startswith(('bdot parameters', 'single-salt parameters', 'ca combinations')):
                x=idx
                d.append(x-1)


    element = []; basis = []; auxiliary = []; aqueous = []; minerals = []; MW = []; #DH = []
    gases = []; liquids = []; charge = []; solid_solutions = []; act_param = []

    with open(sourcedb) as fid:
        for i, line in enumerate(fid, 1):
            if (line.rstrip('\n') == "references"):
                break
            if not line.startswith('*',0) | line.startswith(' ',0) | (line.rstrip('\n') == "") | (line[0] == "-") :
                if not line.split()[0].replace('.','',1).isnumeric() and not line.startswith('+',0):
                    if line.strip().rstrip('\n').lstrip('0123456789.- ') not in subheading:
                        line = line.strip().rstrip('\n')
                        if d[1] < i < d[2]:
                            element.append(line.split()[0])
                            MW.append(float(line.split()[1]))
                        elif d[2] < i < d[3]:
                            basis.append(line.split('  ')[0])
                        elif d[3] < i < d[4]:
                            if 'acid' in line.split('  ')[0] or 'high' in line.split('  ')[0] or 'low' in line.split('  ')[0]:
                                auxiliary.append(line.split('  ')[0].replace(' ', '_'))
                            else:
                                auxiliary.append(line.split('  ')[0])
                        elif d[4] < i < d[5]:
                            if 'acid' in line.split('  ')[0] or 'high' in line.split('  ')[0] or 'low' in line.split('  ')[0]:
                                aqueous.append(line.split('  ')[0].replace(' ', '_'))
                            else:
                                aqueous.append(line.split('  ')[0])
                        elif d[5] < i < d[6]:
                            if 'acid' in line.split('  ')[0] or 'high' in line.split('  ')[0] or 'low' in line.split('  ')[0]:
                                minerals.append(line.split('  ')[0].replace(' ', '_'))
                            elif 'anhyd' in line.split('  ')[0] or 'hydr' in line.split('  ')[0]:
                                minerals.append(line.split('  ')[0].replace(' ', '_'))
                            else:
                                minerals.append(line.split('  ')[0])
                        elif d[6] < i < d[7]:
                            liquids.append(line.split('  ')[0])
                        elif d[7] < i < d[8]:
                            gases.append(line.split('  ')[0])
                        elif i > d[8]:
                            solid_solutions.append(line.split('  ')[0])
            if re.compile(r"charge").search(line) != None:
                charge.append(line)
            if d[0] < i < d[1]:
                act_param.append(line)

    MWdic = {element[i]: float(MW[i]) for i in range(len(MW))}
    f = open(sourcedb, 'r')
    for i in range(d[1]):
        s1 = f.readline()

    sourcedic, Sptype, elem = {}, {}, [] # initialize dictionary
    res = basis + auxiliary + aqueous + minerals + liquids + gases + solid_solutions
    for i in range(d[-1]):#1000
        s1 = f.readline()
        if not s1.startswith('*',0)|s1.startswith(' ',0)|(s1.rstrip('\n') == "")|(len(s1) !=0 and s1[0] == "+") :
            if (s1.rstrip('\n') == "references") :
                break
            elif s1 not in subheading:
                s1 = s1.rstrip('\n')
                if s1.split('  ')[0] in res or s1.split('  ')[0].replace(' ', '_') in res:
                    if 'acid' in s1.split('  ')[0] or 'high' in s1.split('  ')[0] or 'low' in s1.split('  ')[0] or 'anhyd' in s1.split('  ')[0] or 'hydr' in s1.split('  ')[0]:
                        specie_name = s1.split('  ')[0].replace(' ', '_')
                    else:
                        specie_name = s1.split('  ')[0]
                    if len(s1.split()) <= 1:
                        specie_formula = ""
                    elif len(s1.split()) > 2:
                        specie_formula = s1.split()[2]
                    else:
                        specie_formula = s1.split()[1]
                    s2 = f.readline()
                    if s2.startswith('*',0):
                        s2 = f.readline().rstrip('\n')
                    s3 = f.readline().rstrip('\n'); s4 = f.readline().rstrip('\n'); s5 = f.readline().rstrip('\n');
                    s6 = f.readline().rstrip('\n'); s7 = f.readline().rstrip('\n');
                    s8 = f.readline().rstrip('\n'); s9 = f.readline().rstrip('\n');
                    if not s9.startswith('+----', 0):
                        s10 = f.readline().rstrip('\n')
                        if not s10.startswith('+----', 0):
                            s11 = f.readline().rstrip('\n')
                            if not s11.startswith('+----', 0):
                                s12 = f.readline().rstrip('\n')
                                if not s12.startswith('+----', 0):
                                    s13 = f.readline().rstrip('\n')
                                    if not s13.startswith('+----', 0):
                                        s14 = f.readline().rstrip('\n')
                                        if not s14.startswith('+----', 0):
                                            s15 = f.readline().rstrip('\n')
                                            if not s15.startswith('+----', 0):
                                                s16 = f.readline().rstrip('\n')
                                                if not s16.startswith('+----', 0):
                                                    s17 = f.readline().rstrip('\n')
                                                    if not s17.startswith('+----', 0):
                                                        s18 = f.readline().rstrip('\n')
                    specie_type = s2; eq = s3
                    if s3.lstrip() != '[ ]':
                        revised = s4; dh = s6; mw = float(s5.split()[-2])
                        if s8 != '****' and s8.rstrip('\n').strip('0123456789.,-: ') == 'element(s)':
                            elem_numb = int(re.sub('[^0123456789\.]', '', s8))
                            if elem_numb < 3:
                                reactant = s9.split('  ')
                            elif elem_numb < 5:
                                reactant = s9.split('  ') + s10.split('  ')
                            elif elem_numb < 7:
                                reactant = s9.split('  ') + s10.split('  ') + s11.split('  ')
                            else:
                                reactant = s9.split('  ') + s10.split('  ') + s11.split('  ') + s12.split('  ')
                        elif s9 != '****' and s9.rstrip('\n').strip('0123456789.,-: ') == 'element(s)':
                            elem_numb = int(re.sub('[^0123456789\.]', '', s9))
                            if elem_numb < 3:
                                reactant = s10.split('  ')
                            elif elem_numb < 5:
                                reactant = s10.split('  ') + s11.split('  ')
                            elif elem_numb < 7:
                                reactant = s10.split('  ') + s11.split('  ') + s12.split('  ')
                            else:
                                reactant = s10.split('  ') + s11.split('  ') + s12.split('  ') + s13.split('  ')
                        elif s10 != '****' and s10.rstrip('\n').strip('0123456789.,-: ') == 'element(s)':
                            elem_numb = int(re.sub('[^0123456789\.]', '', s10))
                            if elem_numb < 3:
                                reactant = s11.split('  ')
                            elif elem_numb < 5:
                                reactant = s11.split('  ') + s12.split('  ')
                            elif elem_numb < 7:
                                reactant = s11.split('  ') + s12.split('  ') + s13.split('  ')
                            else:
                                reactant = s11.split('  ') + s12.split('  ') + s13.split('  ') + s14.split('  ')
                        reactant = [x for x in reactant if x != '' and x != '****']
                        reactant = [word for line in reactant for word in line.split()]
                        elem.append(reactant)
                        if s11.rstrip('\n').strip('0123456789.,-: ')[:10] == 'species in':
                            species_num = int(re.sub('[^0123456789\.]', '', s11))
                            if species_num < 3:
                                reactants = s12.split('  ')
                            elif species_num < 5:
                                reactants = s12.split('  ') + s13.split('  ')
                            elif species_num < 7:
                                reactants = s12.split('  ') + s13.split('  ') + s14.split('  ')
                            elif species_num < 9:
                                reactants = s12.split('  ') + s13.split('  ') + s14.split('  ') + s15.split('  ')
                            else:
                                reactants = s12.split('  ') + s13.split('  ') + s14.split('  ') + s15.split('  ') + s16.split('  ')
                        elif s12.rstrip('\n').strip('0123456789.,-: ')[:10] == 'species in':
                            species_num = int(re.sub('[^0123456789\.]', '', s12))
                            if species_num < 3:
                                reactants = s13.split('  ')
                            elif species_num < 5:
                                reactants = s13.split('  ') + s14.split('  ')
                            elif species_num < 7:
                                reactants = s13.split('  ') + s14.split('  ') + s15.split('  ')
                            elif species_num < 9:
                                reactants = s13.split('  ') + s14.split('  ') + s15.split('  ') + s16.split('  ')
                            else:
                                reactants = s13.split('  ') + s14.split('  ') + s15.split('  ') + s16.split('  ') + s17.split('  ')
                        elif s13.rstrip('\n').strip('0123456789.,-: ')[:10] == 'species in':
                            species_num = int(re.sub('[^0123456789\.]', '', s13))
                            if species_num < 3:
                                reactants = s14.split('  ')
                            elif species_num < 5:
                                reactants = s14.split('  ') + s15.split('  ')
                            elif species_num < 7:
                                reactants = s14.split('  ') + s15.split('  ') + s16.split('  ')
                            elif species_num < 9:
                                reactants = s14.split('  ') + s15.split('  ') + s16.split('  ') + s17.split('  ')
                            else:
                                reactants = s14.split('  ') + s15.split('  ') + s16.split('  ') + s17.split('  ') + s18.split('  ')
                        # solid_solutions
                        if s6.rstrip('\n').strip('0123456789.,-: ') == 'components':
                            species_num = int(re.sub('[^0123456789\.]', '', s6))
                            if species_num < 3:
                                reactants = s7.split('  ')
                                ss = [s8] + [s9] + [s10] + [s11]
                            elif species_num < 5:
                                reactants = s7.split('  ') + s8.split('  ')
                                ss = [s9] + [s10] + [s11] + [s12]
                            elif species_num < 7:
                                reactants = s7.split('  ') + s8.split('  ') + s9.split('  ')
                                ss = [s10] + [s11] + [s12] + [s13]
                            elif species_num < 9:
                                reactants = s7.split('  ') + s8.split('  ') + s9.split('  ') + s10.split('  ')
                                ss = [s11] + [s12] + [s13] + [s14]
                            else:
                                reactants = s7.split('  ') + s8.split('  ') + s9.split('  ') + s10.split('  ') + s11.split('  ')
                                ss = [s12] + [s13] + [s14] + [s15]
                        elif s7.rstrip('\n').strip('0123456789.,-: ') == 'components':
                            species_num = int(re.sub('[^0123456789\.]', '', s7))
                            if species_num < 3:
                                reactants = s8.split('  ')
                                ss = [s9] + [s10] + [s11] + [s12]
                            elif species_num < 5:
                                reactants = s8.split('  ') + s9.split('  ')
                                ss = [s10] + [s11] + [s12] + [s13]
                            elif species_num < 7:
                                reactants = s8.split('  ') + s9.split('  ') + s10.split('  ')
                                ss = [s11] + [s12] + [s13] + [s14]
                            elif species_num < 9:
                                reactants = s8.split('  ') + s9.split('  ') + s10.split('  ') + s11.split('  ')
                                ss = [s12] + [s13] + [s14] + [s15]
                            else:
                                reactants = s8.split('  ') + s9.split('  ') + s10.split('  ') + s11.split('  ') + s12.split('  ')
                                ss = [s13] + [s14] + [s15] + [s16]
                        elif s8.rstrip('\n').strip('0123456789.,-: ') == 'components':
                            species_num = int(re.sub('[^0123456789\.]', '', s8))
                            if species_num < 3:
                                reactants = s9.split('  ')
                                ss = [s10] + [s11] + [s12] + [s13]
                            elif species_num < 5:
                                reactants = s9.split('  ') + s10.split('  ')
                                ss = [s11] + [s12] + [s13] + [s14]
                            elif species_num < 7:
                                reactants = s9.split('  ') + s10.split('  ') + s11.split('  ')
                                ss = [s12] + [s13] + [s14] + [s15]
                            elif species_num < 9:
                                reactants = s9.split('  ') + s10.split('  ') + s11.split('  ') + s12.split('  ')
                                ss = [s13] + [s14] + [s15] + [s16]
                            else:
                                reactants = s9.split('  ') + s10.split('  ') + s11.split('  ') + s12.split('  ') + s13.split('  ')
                                ss = [s14] + [s15] + [s16] + [s17]
                    else:
                        revised = '[ ]'; dh = ''; mw = ''
                        if s6 != '****' and s6.rstrip('\n').strip('0123456789.,-: ') == 'element(s)':
                            elem_numb = int(re.sub('[^0123456789\.]', '', s6))
                            if elem_numb < 3:
                                reactant = s7.split('  ')
                            elif elem_numb < 5:
                                reactant = s7.split('  ') + s8.split('  ')
                            elif elem_numb < 7:
                                reactant = s7.split('  ') + s8.split('  ') + s9.split('  ')
                            else:
                                reactant = s7.split('  ') + s8.split('  ') + s9.split('  ') + s10.split('  ')
                        elif s7 != '****' and s7.rstrip('\n').strip('0123456789.,-: ') == 'element(s)':
                            elem_numb = int(re.sub('[^0123456789\.]', '', s7))
                            if elem_numb < 3:
                                reactant = s8.split('  ')
                            elif elem_numb < 5:
                                reactant = s8.split('  ') + s9.split('  ')
                            elif elem_numb < 7:
                                reactant = s8.split('  ') + s9.split('  ') + s10.split('  ')
                            else:
                                reactant = s8.split('  ') + s9.split('  ') + s10.split('  ') + s11.split('  ')
                        elif s8 != '****' and s8.rstrip('\n').strip('0123456789.,-: ') == 'element(s)':
                            elem_numb = int(re.sub('[^0123456789\.]', '', s8))
                            if elem_numb < 3:
                                reactant = s9.split('  ')
                            elif elem_numb < 5:
                                reactant = s9.split('  ') + s10.split('  ')
                            elif elem_numb < 7:
                                reactant = s9.split('  ') + s10.split('  ') + s11.split('  ')
                            else:
                                reactant = s9.split('  ') + s10.split('  ') + s11.split('  ') + s12.split('  ')
                        reactant = [x for x in reactant if x != '' and x != '****']
                        reactant = [word for line in reactant for word in line.split()]
                        elem.append(reactant)
                        if s9.rstrip('\n').strip('0123456789.,-: ')[:10] == 'species in':
                            species_num = int(re.sub('[^0123456789\.]', '', s9))
                            if species_num < 3:
                                reactants = s10.split('  ')
                            elif species_num < 5:
                                reactants = s10.split('  ') + s11.split('  ')
                            elif species_num < 7:
                                reactants = s10.split('  ') + s11.split('  ') + s12.split('  ')
                            elif species_num < 9:
                                reactants = s10.split('  ') + s11.split('  ') + s12.split('  ') + s13.split('  ')
                            else:
                                reactants = s10.split('  ') + s11.split('  ') + s12.split('  ') + s13.split('  ') + s14.split('  ')
                        elif s10.rstrip('\n').strip('0123456789.,-: ')[:10] == 'species in':
                            species_num = int(re.sub('[^0123456789\.]', '', s10))
                            if species_num < 3:
                                reactants = s11.split('  ')
                            elif species_num < 5:
                                reactants = s11.split('  ') + s12.split('  ')
                            elif species_num < 7:
                                reactants = s11.split('  ') + s12.split('  ') + s13.split('  ')
                            elif species_num < 9:
                                reactants = s11.split('  ') + s12.split('  ') + s13.split('  ') + s14.split('  ')
                            else:
                                reactants = s13.split('  ') + s12.split('  ') + s13.split('  ') + s14.split('  ') + s15.split('  ')
                        elif s11.rstrip('\n').strip('0123456789.,-: ')[:10] == 'species in':
                            species_num = int(re.sub('[^0123456789\.]', '', s11))
                            if species_num < 3:
                                reactants = s12.split('  ')
                            elif species_num < 5:
                                reactants = s12.split('  ') + s13.split('  ')
                            elif species_num < 7:
                                reactants = s12.split('  ') + s13.split('  ') + s14.split('  ')
                            elif species_num < 9:
                                reactants = s12.split('  ') + s13.split('  ') + s14.split('  ') + s15.split('  ')
                            else:
                                reactants = s12.split('  ') + s13.split('  ') + s14.split('  ') + s15.split('  ') + s16.split('  ')
                        # solid_solutions
                        if s4.rstrip('\n').strip('0123456789.,-: ') == 'components':
                            species_num = int(re.sub('[^0123456789\.]', '', s4))
                            if species_num < 3:
                                reactants = s5.split('  ')
                                ss = [s6] + [s7] + [s8] + [s9]
                            elif species_num < 5:
                                reactants = s5.split('  ') + s6.split('  ')
                                ss = [s7] + [s8] + [s9] + [s10]
                            elif species_num < 7:
                                reactants = s5.split('  ') + s6.split('  ') + s7.split('  ')
                                ss = [s8] + [s9] + [s10] + [s11]
                            elif species_num < 9:
                                reactants = s5.split('  ') + s6.split('  ') + s7.split('  ') + s8.split('  ')
                                ss = [s9] + [s10] + [s11] + [s12]
                            else:
                                reactants = s5.split('  ') + s6.split('  ') + s7.split('  ') + s8.split('  ') + s9.split('  ')
                                ss = [s10] + [s11] + [s12] + [s13]
                        elif s5.rstrip('\n').strip('0123456789.,-: ') == 'components':
                            species_num = int(re.sub('[^0123456789\.]', '', s5))
                            if species_num < 3:
                                reactants = s6.split('  ')
                                ss = [s7] + [s8] + [s9] + [s10]
                            elif species_num < 5:
                                reactants = s6.split('  ') + s7.split('  ')
                                ss = [s8] + [s9] + [s10] + [s11]
                            elif species_num < 7:
                                reactants = s6.split('  ') + s7.split('  ') + s8.split('  ')
                                ss = [s9] + [s10] + [s11] + [s12]
                            elif species_num < 9:
                                reactants = s6.split('  ') + s7.split('  ') + s8.split('  ') + s9.split('  ')
                                ss = [s10] + [s11] + [s12] + [s13]
                            else:
                                reactants = s6.split('  ') + s7.split('  ') + s8.split('  ') + s9.split('  ') + s10.split('  ')
                                ss = [s11] + [s12] + [s13] + [s14]
                        elif s6.rstrip('\n').strip('0123456789.,-: ') == 'components':
                            species_num = int(re.sub('[^0123456789\.]', '', s6))
                            if species_num < 3:
                                reactants = s7.split('  ')
                                ss = [s8] + [s9] + [s10] + [s11]
                            elif species_num < 5:
                                reactants = s7.split('  ') + s8.split('  ')
                                ss = [s9] + [s10] + [s11] + [s12]
                            elif species_num < 7:
                                reactants = s7.split('  ') + s8.split('  ') + s9.split('  ')
                                ss = [s10] + [s11] + [s12] + [s13]
                            elif species_num < 9:
                                reactants = s7.split('  ') + s8.split('  ') + s9.split('  ') + s10.split('  ')
                                ss = [s11] + [s12] + [s13] + [s14]
                            else:
                                reactants = s7.split('  ') + s8.split('  ') + s9.split('  ') + s10.split('  ') + s11.split('  ')
                                ss = [s12] + [s13] + [s14] + [s15]

                    if specie_name in basis and i < d[2]:
                        sourcedic[specie_name] = [specie_formula, elem_numb] + reactant
                    elif specie_name == 'O2(g)':
                        reactants = [x for x in reactants if x != '' and x != '****']
                        sourcedic[specie_name] = [specie_formula, species_num] + reactants
                    else:
                        reactants = [x for x in reactants if x != '' and x != '****']
                        reactants = [j.replace(' ', '_') if 'acid' in j or 'high' in j or 'low' in j or 'anhyd' in j or 'hydr' in j else j for j in reactants]
                        sourcedic[specie_name] = [specie_formula, species_num] + reactants
                    if specie_name in basis + auxiliary + aqueous:
                        Sptype[specie_name] = [specie_type] + [eq] + [revised] + [dh]
                    elif specie_name in solid_solutions:
                        Sptype[specie_name] = [specie_type] + [eq] + [revised] + ss
                    else:
                        Sptype[specie_name] = [specie_type] + [eq] + [revised]
                    MWdic[specie_name] = mw


    sourcedic['eh'] = ['eh', 4, '-1.0000', 'eh', '-2.0000', 'H2O', '1.0000', 'O2(g)', '4.0000', 'H+']
    sourcedic['e-'] = ['e-', 4, '-1.0000', 'e-', '0.50000', 'H2O', '-0.2500', 'O2(g)', '-1.0000', 'H+']

    Elemlist = {res[i]: elem[i] for i in range(len(res))}
    res = basis + auxiliary + aqueous
    chargedic = {res[i]: charge[i].rstrip('\n') for i in range(len(charge))}
    res = element + basis + auxiliary + aqueous + minerals + liquids + gases
    specielist = [element, basis, auxiliary, aqueous, minerals, liquids, gases, solid_solutions]

    f.close()
    return sourcedic, specielist, chargedic, MWdic, Sptype, Elemlist #, activity_model
