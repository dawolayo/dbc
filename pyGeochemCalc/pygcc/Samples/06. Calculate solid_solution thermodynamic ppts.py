# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 18:48:55 2021

@author: adedapo.awolayo
"""
import os, sys, numpy as np
sys.path.append(os.path.abspath('.'))

try:
    from pygcc.pygcc_utils import calclogKAnAb, outputfmt, readAqspecdb, iapws95, densitylogKextrap
except ImportError:
    # append full path
    parentdir = os.path.dirname(os.path.dirname(os.path.abspath('.')))
    sys.path.append(parentdir)
    from pygcc.pygcc_utils import calclogKAnAb, outputfmt, readAqspecdb, iapws95, densitylogKextrap


#%% specify the direct access thermodynamic database
dbaccess ='./default_db/speq20.dat'
dbacessdic, _ = readAqspecdb(dbaccess)


#%% Vectors for Temperature (K) and Pressure (bar) inputs
T = np.array([  0.00,   25.0000 ,  60.0000,  100.0000, 150.0000,  200.0000,  250.0000,  300.0000])
P = np.array([  1.0132,    1.0132,    1.0132,    1.0132, 4.7572,   15.5365,   39.7365,   85.8378])
nCa = 1


#%% Examples of calculating Plagioclase mineral thermodynamics
filename = './output/logK.txt'
fid = open(filename, 'w')

logK, Rxn = calclogKAnAb(0.634, T, P, dbacessdic)

# including density-LogK extrapolation
rho = iapws95(T = T, P = P)[0]
if any(rho < 350):
    subBornptrs = rho < 350
    logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'AnAb',
                                          dbacessdic = dbacessdic, X = 0.634)

# output in EQ36 format
outputfmt(fid, logK, Rxn, dataset = 'EQ36')
# output in GWB format
outputfmt(fid, logK, Rxn, dataset = 'GWB')
# output in Pflotran format
outputfmt(fid, logK, Rxn, dataset = 'Pflotran')

fid.close()




