# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 18:48:55 2021

@author: adedapo.awolayo
"""

import os, sys, numpy as np
sys.path.append(os.path.abspath('.'))

try:
    from pygcc.pygcc_utils import write_database
except ImportError:
    # append full path
    parentdir = os.path.dirname(os.path.dirname(os.path.abspath('.')))
    sys.path.append(parentdir)
    from pygcc.pygcc_utils import write_database

# Vectors for Temperature (K) and Pressure (bar) inputs
T = np.array([  0.010,   25.0000 ,  60.0000,  100.0000, 150.0000,  200.0000,  250.0000,  300.0000])
P = 250*np.ones(np.size(T))


#%% Examples of generating EQ3/6 thermodynamic database
# 1. write EQ3/6 using default sourced database
write_database(T, P, nCa_cpx = 1, solid_solution = 'Yes', clay_thermo = 'Yes', dataset = 'EQ36')

# 2. write EQ3/6 user-specified sourced database
write_database([0, 600], 350, nCa_cpx = 0.5, solid_solution = 'Yes', clay_thermo = 'Yes',
                sourcedb = './Testing/data0.geo', dataset = 'EQ36')

# 3. write EQ3/6 using user-specified sourced Pitzer database
write_database([0, 350], 200, sourcedb = './Testing/data0.hmw', dataset = 'EQ36')

# 4. write EQ3/6 user-specified sourced database and FE97 dielectric constant
write_database([0, 600], 300, nCa_cpx = 0.1, sourcedb = './Testing/data0.geo', dataset = 'EQ36',
               solid_solution = 'Yes', Dielec_method = 'FE97', clay_thermo = 'Yes')
