#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 18:48:55 2021

@author: adedapo.awolayo
"""
import os, sys, numpy as np
sys.path.append(os.path.abspath('..'))

try:
    from pygcc.pygcc_utils import write_database
except ImportError:
    # append full path
    parentdir = os.path.dirname(os.path.dirname(os.path.abspath('.')))
    sys.path.append(parentdir)
    from pygcc.pygcc_utils import write_database


# Vectors for Temperature (K) and Pressure (bar) inputs
T = np.array([  0.010,   25.0000 ,  60.0000,  100.0000, 120.0000,  150.0000,  250.0000,  300.0000])
P = 350*np.ones(np.size(T))
nCa = 1

#%% Examples of generating GWB thermodynamic database

# 1.  write GWB using default sourced database
write_database(T, P, nCa_cpx = nCa, solid_solution = 'Yes', clay_thermo = 'Yes', dataset = 'GWB')

# 2.  write GWB using user-specified sourced database
write_database(T, 175, nCa_cpx = nCa, solid_solution = 'Yes', clay_thermo = 'Yes',
                sourcedb = './Testing/thermo.29Sep15.dat', dataset = 'GWB')

# 3.  write GWB using Jan2020 formatted sourced database
write_database(T, 125, nCa_cpx = nCa, solid_solution = 'Yes', clay_thermo = 'Yes',
                sourcedb =  './default_db/thermo.com.tdat', dataset = 'GWB')

# 4.  write GWB using Jan2020 formatted sourced database with logK as polynomial coefficients, using Tmax and Tmin
write_database([0, 400], 200, nCa_cpx = nCa, solid_solution = 'Yes', clay_thermo = 'Yes',
                logK_form = 'polycoeffs', sourcedb =  './default_db/thermo.com.tdat', dataset = 'GWB')

# 5. write GWB using user-specified sourced database and direct-access database (spronsbl)
write_database([0, 600], 300, nCa_cpx = 0.5, solid_solution = 'Yes', dbaccess ='./Testing/dpumn2012.dat',
                sourcedb = './Testing/thermo.29Sep15.dat', dataset = 'GWB')

# 6. write GWB using user-specified sourced Pitzer database and default direct-access database
write_database([0, 350], 'T', dataset = 'GWB',  sourcedb = './Testing/thermo_hmw.tdat')


# 7. write GWB using user-specified sourced EQ3/6 Pitzer database and default direct-access database
write_database([0, 350], 250, dataset = 'GWB', sourcedb = './Testing/data0.fmt', sourceformat = 'EQ36')


