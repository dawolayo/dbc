# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 18:48:55 2021

@author: adedapo.awolayo
"""
import os, sys, numpy as np
sys.path.append(os.path.abspath('.'))

try:
    from pygcc.pygcc_utils import write_database
except ImportError:
    # append full path
    parentdir = os.path.dirname(os.path.dirname(os.path.abspath('.')))
    sys.path.append(parentdir)
    from pygcc.pygcc_utils import write_database


T = np.array([  0.010,   25.0000 ,  60.0000,  100.0000, 150.0000,  200.0000,  250.0000,  300.0000])
P = 250*np.ones(np.size(T))
nCa = 0.4


#%% Pflotran thermodynamic database
# 1. write Pflotran using user-specified EQ3/6 database
write_database(T, P, clay_thermo = 'Yes', sourcedb = './default_db/data0.dat',
                dataset = 'Pflotran', sourceformat = 'EQ36')

# 2. write Pflotran using user-specified GWB database
write_database([0, 500], 500, nCa_cpx = nCa, solid_solution = 'Yes', clay_thermo = 'Yes',
                sourcedb = './default_db/thermo.com.tdat', dataset = 'Pflotran', sourceformat = 'GWB')


