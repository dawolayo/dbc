#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 16:02:22 2021

@author: adedapo.awolayo and Ben Tutolo, University of Calgary
"""
from .read_db import readSourceEQ36db, readSourceGWBdb, readAqspecdb
import warnings
import re
import os, json
import numpy as np
import pandas as pd
import functools
import time
from scipy.optimize import root_scalar, fsolve, curve_fit, brentq
from scipy.linalg import lu_factor, lu_solve
from scipy.interpolate import splev, splrep, Rbf
import inspect
import math
np.random.seed(4321)
warnings.filterwarnings("ignore", message="divide by zero encountered")
warnings.filterwarnings("ignore", message="invalid value encountered")

eps = 2.220446049250313e-16
J_to_cal = 4.184

def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = end_time - start_time    # 3
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer

def readIAPWS95data():
    """
    returns all constants and coefficients needed for the
     IAPWS95 formulation, packed into a dictionary
    """

    # Constants for IAPWS-95-Formulation
    R       = 461.51805      			# R: specific gas constant in J/(kg*K)
    Tc      = 647.096        			# T_c: critical temperature of water in K
    rhoc    = 322            			# rho_c: critical density of water in kg/m^3
    Pc      = 22.06400000000213	        # P_c: critical pressure of water in MPa
    # n0(1:8)
    n0      = [-8.320446483749615, 6.683210527593193, 3.00632, 0.012436,
               0.97315, 1.27950, 0.96956, 0.24873]
    # gamma0(1:8)
    gamma0  = [0.0, 0.0, 0.0, 1.28728967, 3.53734222, 7.74073708, 9.24437796, 27.5075105]
    # n(1:56)
    n       = [0.12533547935523e-1,  0.78957634722828e+1, -0.87803203303561e+1,
               0.31802509345418e+0, -0.26145533859358e+0, -0.78199751687981e-2,
               0.88089493102134e-2, -0.66856572307965e+0,  0.20433810950965e+0,
               -0.66212605039687e-4, -0.19232721156002e+0, -0.25709043003438e+0,
               0.16074868486251e+0, -0.40092828925807e-1,  0.39343422603254e-6,
               -0.75941377088144e-5,  0.56250979351888e-3, -0.15608652257135e-4,
               0.11537996422951e-8,  0.36582165144204e-6, -0.13251180074668e-11,
               -0.62639586912454e-9, -0.10793600908932e+0,  0.17611491008752e-1,
               0.22132295167546e+0, -0.40247669763528e+0,  0.58083399985759e+0,
               0.49969146990806e-2, -0.31358700712549e-1, -0.74315929710341e+0,
               0.47807329915480e+0,  0.20527940895948e-1, -0.13636435110343e+0,
               0.14180634400617e-1,  0.83326504880713e-2, -0.29052336009585e-1,
               0.38615085574206e-1, -0.20393486513704e-1, -0.16554050063734e-2,
               0.19955571979541e-2,  0.15870308324157e-3, -0.16388568342530e-4,
               0.43613615723811e-1,  0.34994005463765e-1, -0.76788197844621e-1,
               0.22446277332006e-1, -0.62689710414685e-4, -0.55711118565645e-9,
               -0.19905718354408e+0,  0.31777497330738e+0, -0.11841182425981e+0,
               -0.31306260323435e+2,  0.31546140237781e+2, -0.25213154341695e+4,
               -0.14874640856724e+0,  0.31806110878444e+0]
    # c(1:51)
    c       = [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
               1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
               2, 2, 3, 3, 3, 3, 4, 6, 6, 6, 6]
    # d(1:54)
    d       = [1,  1,  1,  2,  2,  3,  4,  1,  1,  1,  2,  2,  3,  4,  4,
               5,  7,  9, 10, 11, 13, 15,  1,  2,  2,  2,  3,  4,  4,  4,
               5,  6,  6,  7,  9,  9,  9,  9,  9, 10, 10, 12,  3,  4,  4,
               5, 14,  3,  6,  6,  6,  3,  3,  3]
    # t(1:54)
    t       = [-0.5, 0.875, 1, 0.5, 0.75, 0.375, 1, 4, 6, 12, 1, 5, 4, 2, 13,
               9, 3, 4, 11, 4, 13, 1, 7, 1, 9, 10, 10, 3, 7, 10, 10, 6, 10,
               10, 1, 2, 3, 4, 8, 6, 9, 8, 16, 22, 23, 23, 10, 50, 44, 46, 50, 0, 1, 4]
    # alpha(52:54)
    alpha   = [20, 20, 20]
    # beta(52:56)
    beta    = [150, 150, 250, 0.3, 0.3]
    # gamma(52:54)
    gamma   = [1.21, 1.21, 1.25]
    # epsilon(52:54)
    epsilon = [1, 1, 1]
    # a(55:56)
    a       = [3.5, 3.5]
    # b(55:56)
    b       = [0.85, 0.95]
    # A(55:56)
    A       = [0.32, 0.32]
    # B(55:56)
    B       = [0.2, 0.2]
    # C(55:56)
    C       = [28, 32]
    # D(55:56)
    D       = [700, 800]

    var = [R,Tc,Pc,rhoc,n0,gamma0,n,c,d,t,alpha,beta,gamma,epsilon,a,b,A,B,C,D]
    varname = ['R','Tc','Pc','rhoc','n0','gamma0','n','c','d','t','alpha','beta',
               'gamma','epsilon','a','b','A','B','C','D']
    coeffs = {}
    for i in range(len(var)):
        coeffs['%s' % varname[i]] = var[i]

    return coeffs

# @timer

def Molecularweight():
    """
    This function stores the Molecular weight of all elements
    """
    MW = {'O': 15.9994,
         'Ag': 107.8682,
         'Al': 26.98154,
         'Am': 243.0,
         'Ar': 39.948,
         'Au': 196.96654,
         'B': 10.811,
         'Ba': 137.327,
         'Be': 9.01218,
         'Br': 79.904,
         'Ca': 40.078,
         'Cd': 112.411,
         'Ce': 140.115,
         'Cl': 35.4527,
         'Co': 58.9332,
         'Cr': 51.9961,
         'Cs': 132.90543,
         'Cu': 63.546,
         'Dy': 162.5,
         'Er': 167.26,
         'Eu': 151.965,
         'F': 18.9984,
         'Fe': 55.847,
         'Ga': 69.723,
         'Gd': 157.25,
         'H': 1.00794,
         'As': 74.92159,
         'C': 12.011,
         'P': 30.97376,
         'He': 4.0026,
         'Hg': 200.59,
         'Ho': 164.93032,
         'I': 126.90447,
         'In': 114.82,
         'K': 39.0983,
         'Kr': 83.8,
         'La': 138.9055,
         'Li': 6.941,
         'Lu': 174.967,
         'Mg': 24.305,
         'Mn': 54.93805,
         'Mo': 95.94,
         'N': 14.00674,
         'Na': 22.98977,
         'Nd': 144.24,
         'Ne': 20.1797,
         'Ni': 58.69,
         'Np': 237.048,
         'Pb': 207.2,
         'Pd': 106.42,
         'Pr': 140.90765,
         'Pu': 244.0,
         'Ra': 226.025,
         'Rb': 85.4678,
         'Re': 186.207,
         'Rn': 222.0,
         'Ru': 101.07,
         'S': 32.066,
         'Sb': 121.75,
         'Sc': 44.95591,
         'Se': 78.96,
         'Si': 28.0855,
         'Sm': 150.36,
         'Sn': 118.71,
         'Sr': 87.62,
         'Tb': 158.92534,
         'Tc': 98.0,
         'Th': 232.0381,
         'Ti': 47.88,
         'Tl': 204.3833,
         'Tm': 168.93421,
         'U': 238.0289,
         'V': 50.9415,
         'W': 183.85,
         'Xe': 131.29,
         'Y': 88.90585,
         'Yb': 173.04,
         'Zn': 65.39,
         'Zr': 91.224}

    return MW

def calc_elem_count_molewt(formula, **kwargs):
    """
    This function calculates the molecular mass and the elemental composition of a substance given
    by its chemical formula
    It was extracted from https://github.com/cgohlke/molmass/blob/master/molmass/molmass.py
    Parameters
    ----------
        formula      :     string, chemical formula
        Elementdic   :     dictionary, containing the atomic mass of element database
    Returns
    ----------
        elements     :     dictionary of elemental composition and their respective number of atoms
        molewt       :     Calculated Molecular Weights [g/mol]
    Usage:
    ----------
    [elements, molewt] = calc_elem_count_molewt(formula)
        Examples of valid formulas are "H2O", "[2H]2O", "CH3COOH", "EtOH", "CuSO4:5H2O", "(COOH)2",
        "AgCuRu4(H)2[CO]12{PPh3}2", "CGCGAATTCGCG", and, "MDRGEQGLLK" .
    """
    kwargs = dict({"Elementdic": None   }, **kwargs)
    Elementdic = kwargs['Elementdic']

    if Elementdic is None:
        periodic_table = open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                           'PeriodicTableJSON.json'), encoding='utf8')
        data = json.load(periodic_table)
        Elementdic = {data['elements'][x]['symbol'] : pd.DataFrame([data['elements'][x]['name'],
                                                                    data['elements'][x]['atomic_mass']],
                                                                   index = ['name', 'mass']).T
                   for x in range(len(data['elements']))}
        periodic_table.close()

    validchars = set('([{<123456789ABCDEFGHIKLMNOPRSTUVWXYZ')
    validchars |= set(']})>0abcdefghiklmnoprstuy')

    elements = {}
    ele = ''  # parsed element
    num = 0  # number
    level = 0  # parenthesis level
    counts = [1]  # parenthesis level multiplication
    i = len(formula)
    while i:
        i -= 1
        char = formula[i]

        if char in '([{<':
            level -= 1
        elif char in ')]}>':
            if num == 0:
                num = 1
            level += 1
            if level > len(counts) - 1:
                counts.append(0)
            counts[level] = num * counts[level - 1]
            num = 0
        elif char.isdigit():
            j = i
            while i and (formula[i - 1].isdigit() or formula[i - 1] == '.'):
                i -= 1
            num = float(formula[i : j + 1])
        elif char.islower():
            ele = char
        elif char.isupper():
            ele = char + ele
            if num == 0:
                num = 1
            j = i
            number = num * counts[level]
            if ele in elements.keys():
                elements[ele] = number + elements[ele]
            else:
                elements[ele] = number
            ele = ''
            num = 0
        elif char == ':':
            if num == 0:
                num = 1
            for k in elements.keys():
                elements[k] = elements[k]*num

    molewt = 0
    for symbol in elements:
        molewt += Elementdic[symbol].mass[0] * elements[symbol]

    return elements, molewt

# @timer
def importconfile(filename, *Rows):
    """
    This function imports numeric data from a text file as column vectors.
       [Var1, Var2] = importconfile(filename) Reads data from text file
       filename for the default selection.

       [Var1, Var2] = importconfile(filename, StartRow, EndRow) Reads data
       from rows StartRow through EndRow of text file filename.

     Example:
       [Var1, Var2] = importconfile('100bar.con', 6, 13);
    """

    # %% Initialize variables.
    if (len(Rows) == 0):
        startRow = 5
        endRow = np.inf
    else:
        startRow, endRow = Rows

    # %% Open the text file.
    fileID = open(filename,'r');

    df=fileID.readlines()
    Var1 = []
    Var2 = []
    for block in range(startRow,len(df)):
        if not df[block].startswith('\n'):
            dataArray = df[block].split()
            Var1.append(float(dataArray[0]))
            Var2.append(float(dataArray[1]))

    Var1 = np.asarray(Var1)
    Var2 = np.asarray(Var2)
    # %% Close the text file.
    fileID.close()
    # %%

    return Var1, Var2

def var_name(var):
    callers_local_vars = inspect.currentframe().f_back.f_locals.items()
    print(str([k for k, v in callers_local_vars if v is var][0])+': '+str(var))

def feval(funcName, *args):
    return eval(funcName)(*args)

def roundup_tenth(x):
    return int(math.ceil(x / 10.0)) * 10

def read_specific_lines(file, lines_to_read):
   lines = set(lines_to_read)
   last = max(lines)
   for n, line in enumerate(file):
      if n + 1 in lines:
          yield line
      if n + 1 > last:
          return
      if not line:
          continue

#%%----------------------------------------------------------------

IAPWS95_COEFFS = readIAPWS95data()
MW = Molecularweight()

def info(name, dic):
    """
    This function checks for naming convention of the species in the direct-access database
    """
    lst = [i for i in list(dic.keys()) if i.startswith(name)==True] # starts with name
    lst = lst + [i for i in list(dic.keys()) if i.__contains__(name)] # contains name
    return lst

def celsiusToKelvin(TC):
    """
    This function converts temperatures from Celsius to Kelvin
    Parameters
    ----------
        T        temperature in °C
    Returns
    ----------
        T        temperature in K
    Usage:
    ----------
      TK = celsiusToKelvin( TC)
    """
    return TC + 273.15

def iapws95(**kwargs):
    """
    Implementation of IAPWS Formulation 1995 for ordinary water substance,
    (revised release of 2016)  \n
    Parameters:
    ----------
        T     :  temperature [°C]  \n
        P     :  pressure [bar]  \n
        rho   :  density [kg/m³]  \n
        rho0  :  starting estimate of density [kg/m³]  \n
        rhom  :  molar density [kg/m³]  \n
        delta :  reduced density, rho/rhoc  \n
        tau   :  reduced temperature, Tc/T  \n
        v     :  Specific volume [m³/kg]  \n
        vm    :  Specific molar volume [m³/mol]  \n
    Usage:
    ----------
        The general usage of iapws95 is as follows:  \n
        (1) For water properties at any Temperature and Pressure not on steam saturation curve:  \n
            [rho, gxcu, hxcu, sxcu, vxcu, Pout, Tout] = iapws95(T = T, P = P),   \n
            where T is temperature in celsius and P is pressure in bar
        (2) For water properties at any Temperature and Pressure on steam saturation curve:  \n
            [rho, gxcu, hxcu, sxcu, vxcu, Pout, Tout] = iapws95(T = T, P = 'T'),   \n
            where T is temperature in celsius, followed with a quoted character 'T' to reflect steam saturation pressure  \n
            [rho, gxcu, hxcu, sxcu, vxcu, Pout, Tout] = iapws95(T = 'P', P = P),   \n
            where P is pressure in bar, followed with a quoted character 'P' to reflect steam saturation temperature
        (3) For water properties at any Temperature and density :  \n
            [P, ax, sx, hx, gx, vx, pdx, adx, ztx, ptx, ktx, avx] = iapws95(T = T, rho = rho),   \n
            where T is temperature in celsius and rho is density in kg/m³
        (4) For water properties at any Pressure and density :  \n
            [rho, gxcu, hxcu, sxcu, vxcu, Pout, Tout] = iapws95(P = P, rho = rho),   \n
            where P is pressure in bar and rho is density in kg/m³
        (5) For water saturation properties at any saturation Temperature :  \n
            [P, rhol, rhosv] = iapws95(T = T),   \n
            where T is temperature in celsius
        (6) For water saturation properties at any saturation Pressure :  \n
            [P, rhol, rhosv] = iapws95(P = P),   \n
            where P is pressure in bar
    Examples
    --------
    >>> water = iapws95(T = 200., P = 50)  \n
    >>> rho, G, h, s, v, P, T  \n
        867.2595, -60368.41787, -65091.03895,  25.14869,  4.96478e-03,  50.00000,  200.000  \n

    >>> water = iapws95(T = 200, rho = 996.5560)  \n
    >>> P, ax, sx, hx, gx, vx, pdx, adx, ztx, ptx, ktx, avx  \n
        2872.0634, -234.2045, 2.0518, 1024.7476, 53.9944, 1.0035e-03, 10079.1771, 93.1208, 2.1891e-03, -7.3486e+03,  3.2057e-05,  6.8093e-04  \n

    >>> water = iapws95(T = 350)  \n
    >>> P, rhol, rhosv  \n
        165.2942 574.7065 113.6056  \n

    >>> water = iapws95(P = 100)  \n
    >>> T, rhol, rhosv  \n
        615.3054, 603.5179, 96.7271  \n

    """

    kwargs = dict({"T": None,
                  "P": None,
                  "rho": None,
                  "rho0": None,
                  "v": None}, **kwargs)

    mwH2O = 18.015268     # g/mol

    # Alternative rho input
    if "rhom" in kwargs:
        kwargs["rho"] = kwargs["rhom"]*mwH2O
    elif "delta" in kwargs:
        kwargs["rho"] = kwargs["delta"]*IAPWS95_COEFFS['rhoc']
    elif kwargs.get("v", 0):
        kwargs["rho"] = 1/kwargs["v"]
    elif kwargs.get("vm", 0):
        kwargs["rho"] = mwH2O/kwargs["vm"]

    # Alternative T input
    if "tau" in kwargs:
        kwargs["T"] = IAPWS95_COEFFS['Tc']/kwargs["tau"]
    kwargs.update(kwargs)

    def EOSIAPW95(TK, rho):
        """
        This function evaluates the IAPWS basic equation of state to calculate thermodynamic
        properties of water, which is written as a function of temperature and density.    \n
        Parameters
        ----------
            TK    :  temperature [K]    \n
            rho   :  density [kg/m3]    \n
        Returns
        ----------
            px    :  pressure [bar]    \n
            ax    :  Helmholtz energy [kJ/kg-K]    \n
            sx    :  Entropy [kJ/kg/K]    \n
            hx    :  Enthalpy [kJ/kg]    \n
            gx    :  Gibbs energy [kJ/kg]    \n
            vx    :  Volume [m3/kg]    \n
            pdx   :  Derivative of pressure with respect to delta in bar    \n
            adx   :  Helmholtz energy derivative with respect to delta    \n
            ztx   :  zeta value (needed to calculate viscosity)    \n
            ptx   :  Derivative of pressure with respect to tau in bar    \n
            ktx   :  Compressibility [/bar]    \n
            avx   :  Thermal expansion coefficient (thermal expansivity)    \n
            # ux  :  Internal energy [kJ/kg]    \n
            # cpx :  Isobaric heat capacity [kJ/kg/K]    \n
            # cvx :  Isochoric heat capacity [kJ/kg/K]    \n
            # mux :  Joule-Thomsen coefficient [K-m3/kJ]    \n
            # dtx :  Isothermal throttling coefficient [kJ/kg/bar]    \n
            # bsx :  Isentropic temperature-pressure coefficient [K-m3/kJ]    \n
            # wx  :  Speed of sound [m/s]    \n
        Usage
        ----------
          [px, ax, sx, hx, gx, vx, pdx, adx, ztx, ptx, ktx, avx] = EOSIAPW95( TK, rho)
        """

        Tc = IAPWS95_COEFFS['Tc']
        Pc = IAPWS95_COEFFS['Pc']
        rhoc = IAPWS95_COEFFS['rhoc']
        # specific and molar gas constants
        R = IAPWS95_COEFFS['R']/1000 # kJ kg-1 K-1

        delta = rho/rhoc
        tau = Tc/TK

        # To avoid delta values of zero
        epxc = eps*delta
        if (delta <= epxc):
            delta = epxc
            rho = rhoc*delta

        # To avoid a singularity at the critical density (at any temperature), i.e.
        # if delta is unity, then delta - 1 is zero. This will lead to error when evaluating
        # (delta - 1)**n where n is negative, or in any division like (x/(d -1)).
        x1 = 1.0 - epxc
        x2 = 1.00 + epxc
        if (delta > x1) & (delta < x2):
            if (delta < 1.0):
                delta = x1
            else:
                delta = x2
            rho = rhoc*delta

        #%%---------------------------------------------------------
        # specific dimensionless Helmholtz free energy (phi) and its derivatives
        ## IAPWS95.residual
        def phir(delta, tau):
            """
             residual part of free energy, dimensionless
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK           dimensionless inverse temperature
            """

            # % unpack coefficients
            n = np.array(IAPWS95_COEFFS['n']);          c = np.array(IAPWS95_COEFFS['c'])
            d = np.array(IAPWS95_COEFFS['d']);          t = np.array(IAPWS95_COEFFS['t'])
            alpha = np.array(IAPWS95_COEFFS['alpha']);  beta = np.array(IAPWS95_COEFFS['beta'])
            gamma = np.array(IAPWS95_COEFFS['gamma']);  epsilon = np.array(IAPWS95_COEFFS['epsilon'])
            # a = np.array(IAPWS95_COEFFS['a']);
            b = np.array(IAPWS95_COEFFS['b'])

            y = np.dot(n[:7], (delta**d[:7] * tau**t[:7]))

            y = y + np.dot(n[7:51], (delta**d[7:51] * tau**t[7:51] * np.exp(-delta**c[7:51])))

            y = y + np.dot(n[51:54], ( delta**d[51:54] * tau**t[51:54] * np.exp(-alpha*(delta - epsilon)**2 \
                                                                                - beta[:3]*(tau - gamma)**2) ))

            y = y + np.dot(n[54:56], (Delta(delta,tau)**b * delta * Psi(delta,tau)))

            return y

        def phir_t(delta, tau):
            """
             partial derivative for tau of phir
             where phir = residual part of free energy, dimensionless
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK           dimensionless inverse temperature
            """

            # % unpack coefficients
            n = np.array(IAPWS95_COEFFS['n']);          c = np.array(IAPWS95_COEFFS['c'])
            d = np.array(IAPWS95_COEFFS['d']);          t = np.array(IAPWS95_COEFFS['t'])
            alpha = np.array(IAPWS95_COEFFS['alpha']);  beta = np.array(IAPWS95_COEFFS['beta'])
            gamma = np.array(IAPWS95_COEFFS['gamma']);  epsilon = np.array(IAPWS95_COEFFS['epsilon'])
            # a = np.array(IAPWS95_COEFFS['a']);
            b = np.array(IAPWS95_COEFFS['b'])

            y = np.dot(n[:7], (t[:7]*delta**d[:7] * tau**(t[:7]-1)))

            y = y + np.dot(n[7:51], (t[7:51]*delta**d[7:51] * tau**(t[7:51]-1) * np.exp(-delta**c[7:51])))

            y = y + np.dot(n[51:54], ( delta**d[51:54] * tau**t[51:54] * \
                                      np.exp(-alpha*(delta - epsilon)**2 - beta[:3]*(tau - gamma)**2)* \
                                      (t[51:54]/tau - 2*beta[:3]*(tau - gamma)) ))

            y = y + np.dot(n[54:56], (delta * (Delta_b_t(delta,tau) * Psi(delta,tau) + Delta(delta,tau)**b * \
                                               Psi_t(delta,tau))))

            return y

        def phir_tt(delta, tau):
            """
             second partial derivative for tau of phir
             where phir = residual part of free energy, dimensionless
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK           dimensionless inverse temperature
            """

            # % unpack coefficients
            n = np.array(IAPWS95_COEFFS['n']);          c = np.array(IAPWS95_COEFFS['c'])
            d = np.array(IAPWS95_COEFFS['d']);          t = np.array(IAPWS95_COEFFS['t'])
            alpha = np.array(IAPWS95_COEFFS['alpha']);  beta = np.array(IAPWS95_COEFFS['beta'])
            gamma = np.array(IAPWS95_COEFFS['gamma']);  epsilon = np.array(IAPWS95_COEFFS['epsilon'])
            # a = np.array(IAPWS95_COEFFS['a']);
            b = np.array(IAPWS95_COEFFS['b'])

            y = np.dot(n[:7], (t[:7]*(t[:7]-1)*delta**d[:7] * tau**(t[:7]-2)))

            y = y + np.dot(n[7:51], (t[7:51]*(t[7:51]-1)*delta**d[7:51] * \
                                     tau**(t[7:51]-2) * np.exp(-delta**c[7:51])))

            y = y + np.dot(n[51:54], ( delta**d[51:54] * tau**t[51:54] * \
                                      np.exp(-alpha*(delta - epsilon)**2 - \
                                             beta[:3]*(tau - gamma)**2)* \
                                          ( (t[51:54]/tau - 2*beta[:3]*(tau - gamma))**2 - \
                                           t[51:54]/tau**2 - 2*beta[:3] )))

            y = y + np.dot(n[54:56], (delta * (Delta_b_tt(delta,tau) * Psi(delta,tau) +\
                                               2*Delta_b_t(delta,tau) * Psi_t(delta,tau) +\
                                               Delta(delta,tau)**b * Psi_tt(delta,tau) )))

            return y

        def phir_d(delta, tau):
            """
             partial derivative for delta of phir
             where phir = residual part of free energy, dimensionless
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            n = np.array(IAPWS95_COEFFS['n']);          c = np.array(IAPWS95_COEFFS['c'])
            d = np.array(IAPWS95_COEFFS['d']);          t = np.array(IAPWS95_COEFFS['t'])
            alpha = np.array(IAPWS95_COEFFS['alpha']);  beta = np.array(IAPWS95_COEFFS['beta'])
            gamma = np.array(IAPWS95_COEFFS['gamma']);  epsilon = np.array(IAPWS95_COEFFS['epsilon'])
            b = np.array(IAPWS95_COEFFS['b'])

            y = np.dot(n[:7], (d[:7]*delta**(d[:7]-1) * tau**t[:7]))

            y = y + np.dot(n[7:51], ( np.exp(-delta**c[7:51]) * (delta**(d[7:51]-1) * tau**t[7:51] \
                                                           * (d[7:51] - c[7:51]*delta**c[7:51]))))

            y = y + np.dot(n[51:54], (delta**d[51:54] * tau**t[51:54] \
                                      * np.exp(-alpha*(delta - epsilon)**2 - beta[:3]*(tau - gamma)**2) \
                                          * (d[51:54]/delta - 2*alpha*(delta - epsilon))))

            tPsi = Psi(delta,tau)
            y = y + np.dot(n[54:56], (Delta(delta,tau)**b * (tPsi + delta*Psi_d(delta,tau)) \
                                      + (Delta_b_d(delta,tau)*delta*tPsi)))

            return y

        def phir_dd(delta, tau):
            """
             second partial derivative for delta of phir
             where phir = residual part of free energy, dimensionless
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            n = np.array(IAPWS95_COEFFS['n']);          c = np.array(IAPWS95_COEFFS['c'])
            d = np.array(IAPWS95_COEFFS['d']);          t = np.array(IAPWS95_COEFFS['t'])
            alpha = np.array(IAPWS95_COEFFS['alpha']);  beta = np.array(IAPWS95_COEFFS['beta'])
            gamma = np.array(IAPWS95_COEFFS['gamma']);  epsilon = np.array(IAPWS95_COEFFS['epsilon'])
            b = np.array(IAPWS95_COEFFS['b'])

            y = np.dot(n[:7], (d[:7]*(d[:7]-1)*delta**(d[:7]-2) * tau**t[:7]))

            y = y + np.dot(n[7:51], ( np.exp(-delta**c[7:51]) * ( delta**(d[7:51]-2) * tau**t[7:51] * \
                                                                 ((d[7:51] - c[7:51]*delta**c[7:51]) * \
                                                                  (d[7:51] - 1 - c[7:51]*delta**c[7:51]) - \
                                                                      (c[7:51])**2*delta**c[7:51]) ) ) )

            y = y + np.dot(n[51:54], ( tau**t[51:54]* np.exp(-alpha*(delta - epsilon)**2 \
                                                             -beta[:3]*(tau - gamma)**2) \
                                      * (-2*alpha*delta**d[51:54] + 4*alpha**2*delta**d[51:54]*(delta-epsilon)**2 \
                                         - 4*d[51:54]*alpha*delta**(d[51:54]-1)*(delta-epsilon) \
                                             + d[51:54]*(d[51:54]-1)*delta**(d[51:54]-2))) )

            tPsi = Psi(delta,tau)
            dPsi = Psi_d(delta,tau)
            y = y + np.dot(n[54:56], ( Delta(delta,tau)**b * (2*dPsi + delta* Psi_dd(delta,tau)) \
                                      + 2*Delta_b_d(delta,tau)*(tPsi + delta*dPsi) \
                                          + Delta_b_dd(delta,tau)*delta*tPsi) )
            return y

        def phir_dt(delta, tau):
            """
             partial derivative for delta and tau of phir
             where phir = residual part of free energy, dimensionless
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            n = np.array(IAPWS95_COEFFS['n']);          c = np.array(IAPWS95_COEFFS['c'])
            d = np.array(IAPWS95_COEFFS['d']);          t = np.array(IAPWS95_COEFFS['t'])
            alpha = np.array(IAPWS95_COEFFS['alpha']);  beta = np.array(IAPWS95_COEFFS['beta'])
            gamma = np.array(IAPWS95_COEFFS['gamma']);  epsilon = np.array(IAPWS95_COEFFS['epsilon'])
            b = np.array(IAPWS95_COEFFS['b'])

            y = np.dot(n[:7], (d[:7]*t[:7]*delta**(d[:7]-1) * tau**(t[:7]-1)))

            y = y + np.dot(n[7:51], (t[7:51]*  delta**(d[7:51]-1) * tau**(t[7:51]-1) \
                                     * (d[7:51] - c[7:51]*delta**c[7:51]) *np.exp(-delta**c[7:51]) ))

            y = y + np.dot(n[51:54], ( delta**d[51:54] * tau**t[51:54] \
                                      * np.exp(-alpha*(delta - epsilon)**2 - beta[:3]*(tau - gamma)**2) \
                                          * (d[51:54]/delta - 2*alpha*(delta - epsilon)) \
                                              *(t[51:54]/tau - 2*beta[:3]*(tau-gamma) )))

            tPsi = Psi(delta,tau)
            ttPsi = Psi_t(delta,tau)
            y = y + np.dot(n[54:56], ( Delta(delta,tau)**b * \
                                      (Psi_t(delta,tau) + delta*Psi_dt(delta,tau)) \
                                          + (Delta_b_d(delta,tau)*delta*ttPsi) \
                                              + Delta_b_t(delta,tau)*(tPsi+delta*Psi_d(delta,tau)) \
                                                  + Delta_b_dt(delta,tau)*delta*tPsi))
            return y

        ## IAPWS95.idealgas
        # Equation 6.5
        def phi0(delta, tau):
            """
             ideal gas part of free energy, dimensionless
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # unpack coefficients
            n0 = np.asarray(IAPWS95_COEFFS['n0'])
            gamma0 = np.asarray(IAPWS95_COEFFS['gamma0'])

            y = np.log(delta) + n0[0] + n0[1]*tau + n0[2]*np.log(tau)
            y = y + np.dot(n0[3:], np.log(1 - np.exp(-gamma0[3:]*tau)))
            return y

        # derivatives from Table 6.4
        def phi0_t(delta, tau):
            """
             partial derivative for tau of phi0
             where phi0 = ideal gas part of free energy, dimensionless
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # unpack coefficients
            n0 = np.asarray(IAPWS95_COEFFS['n0'])
            gamma0 = np.asarray(IAPWS95_COEFFS['gamma0'])

            y = n0[1] + n0[2]/tau
            y = y + np.dot(n0[3:], gamma0[3:]*(1/(1 - np.exp(-gamma0[3:]*tau))- 1.0))

            return y

        def phi0_tt(delta, tau):
            """
             second partial derivative for tau of phi0
             where phi0 = ideal gas part of free energy, dimensionless
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # unpack coefficients
            n0 = np.asarray(IAPWS95_COEFFS['n0'])
            gamma0 = np.asarray(IAPWS95_COEFFS['gamma0'])

            y = -n0[2]/tau**2
            y = y - np.dot(n0[3:], (gamma0[3:]**2*np.exp(-gamma0[3:]*tau)*(1-np.exp(-gamma0[3:]*tau))**-2 ))

            return y

        def phi0_d(delta, tau):
            """
             partial derivative for delta of phi0
             where phi0 = ideal gas part of free energy, dimensionless
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK           dimensionless inverse temperature
            """
            y = 1/delta
            return y

        def phi0_dd(delta, tau):
            """
             second partial derivative for delta of phi0
             where phi0 = ideal gas part of free energy, dimensionless
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """
            y = -1/delta**2
            return y

        def phi0_dt(delta, tau):
            """
             partial derivative for tau and delta of phi0
             where phi0 = ideal gas part of free energy, dimensionless
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """
            y = 0
            return y

        # Supporting functions for calculating the ideal-gas and residual parts in the IAPWS-95 formulation
        def Delta(delta, tau):
            """
             auxiliary function in IAPWS95 formulation
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            a = np.array(IAPWS95_COEFFS['a']);  B = np.array(IAPWS95_COEFFS['B'])

            return theta(delta, tau)**2 + B*((delta - 1)**2)**a

        def Delta_d(delta, tau):
            """
             Delta_d = (d Delta)/(d delta)
             auxiliary function in IAPWS95 formulation
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/T           dimensionless inverse temperature
            """

            # % unpack coefficients
            a = np.array(IAPWS95_COEFFS['a']); beta = np.array(IAPWS95_COEFFS['beta'])
            A = np.array(IAPWS95_COEFFS['A']); B = np.array(IAPWS95_COEFFS['B'])

            d1 = delta - 1
            y = d1 * ( A*theta(delta,tau)*2/beta[-2:]* (d1**2)**(1/(2*beta[-2:])-1)+ 2*B*a*(d1**2)**(a-1) )

            return y

        def Delta_dd(delta, tau):
            """
             Delta_dd = (d2 Delta)/(d delta2)
             auxiliary function in IAPWS95 formulation
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/T           dimensionless inverse temperature
            """

            # % unpack coefficients
            a = np.array(IAPWS95_COEFFS['a']); beta = np.array(IAPWS95_COEFFS['beta'])
            A = np.array(IAPWS95_COEFFS['A']); B = np.array(IAPWS95_COEFFS['B'])

            d1 = delta - 1
            y = 1/d1 * Delta_d(delta, tau) + \
                d1**2*( 4*B*a*(a-1)*(d1**2)**(a-2) + \
                       2*A**2*beta[-2:]**-2*((d1**2)**(1/(2*beta[-2:])-1))**2 + \
                           A*theta(delta,tau)*4/beta[-2:]*(1/(2*beta[-2:])-1)* (d1**2)**(1/(2*beta[-2:])-2))
            return y

        def Delta_b_d(delta, tau):
            """
             Delta_b_d = (d Delta^b)/(d delta)
             auxiliary function in IAPWS95 formulation
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            b = np.array(IAPWS95_COEFFS['b'])

            return b * Delta(delta,tau)**(b-1) * Delta_d(delta,tau)

        def Delta_b_dd(delta, tau):
            """
             Delta_b_dd = (d2 Delta^b)/(d delta2)
             auxiliary function in IAPWS95 formulation
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            b = np.array(IAPWS95_COEFFS['b'])
            y = b * ( Delta(delta,tau)**(b-1)*Delta_dd(delta,tau) + \
                      (b-1)*Delta(delta,tau)**(b-2)*(Delta_d(delta,tau))**2)

            return y

        def Delta_b_dt(delta, tau):
            """
             Delta_b_dt = (d2 Delta^b)/(d delta tau)
             auxiliary function in IAPWS95 formulation
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            b = np.array(IAPWS95_COEFFS['b'])
            beta = np.array(IAPWS95_COEFFS['beta'])
            A = np.array(IAPWS95_COEFFS['A'])

            d1 = delta - 1

            y = -A*b*2/beta[-2:]*Delta(delta,tau)**(b-1) * d1*(d1**2)**(1/(2*beta[-2:])-1) \
                      - 2*theta(delta,tau)*b*(b-1)*Delta(delta,tau)**(b-2) * Delta_d(delta,tau)

            return y

        def Delta_b_t(delta, tau):
            """
             Delta_b_t = (d Delta^b)/(d tau)
             auxiliary function in IAPWS95 formulation
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            b = np.array(IAPWS95_COEFFS['b'])

            return -2*theta(delta,tau)*b*Delta(delta,tau)**(b-1)

        def Delta_b_tt(delta, tau):
            """
             ∂delta/∂x|y
             Delta_b_tt = (d2 Delta^b)/(d tau2)
             auxiliary function in IAPWS95 formulation
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            b = np.array(IAPWS95_COEFFS['b'])

            return 2*b*Delta(delta,tau)**(b-1) + 4*(theta(delta,tau))**2*b*(b-1)*Delta(delta,tau)**(b-2)

        def theta(delta, tau):
            """
             auxiliary function in IAPWS95 formulation
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            beta = np.array(IAPWS95_COEFFS['beta']);  A = np.array(IAPWS95_COEFFS['A'])

            return (1 - tau) + A*((delta - 1)**2)**(1./(2*beta[3:5]))

        def Psi(delta, tau):
            """
             auxiliary function in IAPWS95 formulation
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            C = np.array(IAPWS95_COEFFS['C'])
            D = np.array(IAPWS95_COEFFS['D'])

            return np.exp(-C*(delta-1)**2 - D*(tau - 1)**2)

        def Psi_d(delta, tau):
            """
             ∂Psi/∂delta
             Psi_d = (d Psi)/(d delta)
             auxiliary function in IAPWS95 formulation
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            C = np.array(IAPWS95_COEFFS['C'])

            return -2*C*(delta - 1)*Psi(delta, tau)

        def Psi_t(delta, tau):
            """
             ∂Psi/∂tau
             Psi_t = (d Psi)/(d tau)
             auxiliary function in IAPWS95 formulation
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            D = np.array(IAPWS95_COEFFS['D'])

            return -2*D*(tau - 1)*Psi(delta,tau)

        def Psi_tt(delta, tau):
            """
             Psi_tt = (d2 Psi)/(d tau2)
             auxiliary function in IAPWS95 formulation
             parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            D = np.array(IAPWS95_COEFFS['D'])

            return (2*D*(tau - 1)**2 - 1)*2*D*Psi(delta,tau)

        def Psi_dd(delta, tau):
            """
             Psi_dd = (d2 Psi)/(d delta2)
             auxiliary function in IAPWS95 formulation
             Parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            C = np.array(IAPWS95_COEFFS['C'])

            return (2*C*(delta - 1)**2 - 1)*2*C*Psi(delta, tau)

        def Psi_dt(delta, tau):
            """
             Psi_dt = (d Psi)/(d delta tau)
             auxiliary function in IAPWS95 formulation
             Parameters:
                 delta = rho/rhoc     dimensionless density
                 tau = Tc/TK          dimensionless inverse temperature
            """

            # % unpack coefficients
            C = np.array(IAPWS95_COEFFS['C'])
            D = np.array(IAPWS95_COEFFS['D'])

            return 4*C*D*(delta - 1)*(tau - 1)*Psi(delta, tau)

        # Calculate thermodynamic functions.
        # Helmholtz energy. The value is in J/kg-K.
        ax = (R*TK*( phi0(delta, tau) + phir(delta, tau) )   )

        # Pressure kpa to bar.
        px = rho*R*TK*( 1 + delta*phir_d(delta, tau) ) * 0.010

        # Internal energy. kJ/kg
        # ux = R*TK*tau*( phi0_t(delta, tau) + phir_t(delta, tau) )

        # Entropy. kJ/kg/K
        sx = R*( tau*(phi0_t(delta, tau) + phir_t(delta, tau)) - phi0(delta, tau) - phir(delta, tau) )

        # Enthalpy. kJ/kg
        hx = R*TK*( 1 + tau*(phi0_t(delta, tau) + phir_t(delta, tau)) + delta*phir_d(delta, tau) )

        # Gibbs energy. kJ/kg
        # Alternate formulas for the Gibbs energy. gx = hx - TK*sx = ax + hx - ux
        gx = R*TK*( 1 + phi0(delta, tau) + phir(delta, tau) + delta*phir_d(delta, tau) )

        # Volume. m3/kg
        vx = (1/rho)

        ## Isochoric heat capacity. kJ/kg/K
        # cvx = -R*tau**2*( phi0_tt(delta, tau) + phir_tt(delta, tau) )

        ## Isobaric heat capacity. kJ/kg/K
        # x1 = ( 1 + delta*phir_d(delta, tau) - delta*tau*phir_dt(delta, tau) )**2
        # x2 = 1 + 2*delta*phir_d(delta, tau) + delta**2*phir_dd(delta, tau)
        # cpx = x1*0
        # for i in range(len(x2)):
        #     if (abs(x2[i]) > 1e-15):
        #         cpx[i] = float(cvx[i] + R*(x1[i]/x2[i]))
        #     else:
        #         cpx[i] = 1.0 + 100

        ## Speed of sound. m/s
        # x1 = ( 1 + delta*phir_d(delta, tau) - delta*tau*phir_dt(delta, tau) )**2
        # x2 = tau**2*( phi0_tt(delta, tau) + phir_tt(delta, tau) )
        # x3 = x1/x2
        # xxt = R*TK*( 1 + 2*delta*phir_d(delta, tau) + delta**2*phir_dd(delta, tau) - x3 )
        # wx = np.where(xxt > 0, np.sqrt(xxt), 0)
        # wx = wx*np.sqrt(1000.00)  # convert Speed of sound from sqrt(kJ/kg) to m/s

        ## Joule-Thomsen coefficient. K-m3/kJ (equivalent to the usual K/MPa)
        # x1 = delta*phir_d(delta, tau) + delta**2*phir_dd(delta, tau) + delta*tau*phir_dt(delta, tau)
        # x2 = ( 1 + delta*phir_d(delta, tau) - delta*tau*phir_dt(delta, tau) )**2
        # x3 = ( phi0_tt(delta, tau) + phir_tt(delta, tau) )*( 1.0 + 2*delta*phir_d(delta, tau) + \
        #                                                     delta**2*phir_dd(delta, tau) )
        # mux = (( - x1/( x2 - tau**2*x3 ) )/(R*rho) )

        ## Isothermal throttling coefficient.
        # x1 = 1 + delta*phir_d(delta, tau) - delta*tau*phir_dt(delta, tau)
        # x2 = 1 + 2*delta*phir_d(delta, tau) + delta**2*phir_dd(delta, tau)
        # dtx = (( 1 - ( x1/x2 ) )/rho )
        # dtx = 0.010*dtx   # convert Isothermal throttling coefficient from m3/kg to the usual kJ/kg/bar.

        ## Isentropic temperature-pressure coefficient. (the same units as for the Joule-Thomson coefficient)
        # x1 = 1.0 + delta*phir_d(delta, tau) - delta*tau*phir_dt(delta, tau)
        # x2 = x1**2
        # x3 = ( phi0_tt(delta, tau) + phir_tt(delta, tau) )*( 1 + 2*delta*phir_d(delta, tau) + \
        #                                                     delta**2*phir_dd(delta, tau) )
        # bsx = (( x1/( x2 - tau**2*x3 ) )/(R*rho) )

        # Derivative of pressure with respect to delta (needed to perform Newton-Raphson
        # iteration to matched desired pressure) in bar.
        pdx = ( px/delta ) + delta*rhoc*R*TK*( phir_d(delta, tau) + delta*phir_dd(delta, tau) ) * 0.010

        # Derivative of pressure with respect to tau (needed to calculate the thermal expansion
        # coefficient) in bar.
        ptx = ( -px/tau ) + px*delta*phir_dt(delta, tau)/(1 + delta*phir_d(delta, tau))

        # Compressibility. Here the value is in /bar.
        ktx = 1.0/(delta*pdx)

        # Calculate zeta value (needed to calculate viscosity). Note: ztx  is dimensionless
        # An alternative formula is: ztx = delta*Pc*ktx
        ztx = Pc/pdx

        # Thermal expansion coefficient (thermal expansivity). This calculation is based on the Maxwell relation:
        avx = ktx*ptx*( -tau/TK )

        # Helmholtz energy derivative with respect to delta
        adx = (R*TK*( phi0_d(delta, tau) + phir_d(delta, tau) ))
        # # Gibbs energy derivative.
        # gdx = (R*TK*( phi0_d(delta, tau) + 2*phir_d(delta, tau) + delta*phir_dd(delta, tau) ))

        return px, ax, sx, hx, gx, vx, pdx, adx, ztx, ptx, ktx, avx

    vEOSIAPW95 = np.vectorize(EOSIAPW95)

    def auxMeltingPressure(TK, P):
        """
        This function calculates the melting pressure of ice as a function of temperature.

        This model is described in IAPWS R14-08(2011), Revised Release on the Pressure along
        the Melting and Sublimation Curves of Ordinary Water Substance, as may be found at:
        http://www.iapws.org/relguide/MeltSub.html

        Five ice phases are covered here. The melting pressure is not a single-valued function
        of temperature as there is some overlap in the temperature ranges of the individual phases.
        There is no overlap in the temperature ranges of Ices III, V, VI, and VII, which together
        span the range 251.165 - 715K. The melting pressure is continuous and monotonically increasing
        over this range, albeit with discontinuities in slope at the triple points where two ice
        phases and liquid are in equilibrium. The problem comes in with Ice Ih, whose temperature
        range completely overlaps that of Ice III and partially overlaps that of Ice V. For a
        temperature in the range for Ice Ih, there are two possible melting pressures.

        The possible ambiguity here in the meaning of melting pressure is not present if the
        temperature is greater than or equal to the triple point temperature of 273.16K, or
        if the pressure is greater than or equal to 2085.66 bar (the triple point pressure
        for Ice Ih-Ice III-liquid). If neither of these conditions are satisfied, then the Ice
        Ih-liquid curve will be used. To deal with the pressure condition noted above, this function
        assumes that an actual pressure is specified.

        Parameters
        ----------
            P     :   pressure [bar]
            TK    :   temperature [K]
        Returns
        ----------
            Pmelt :   melting pressure [bar]
        Usage
        ----------
          [Pmelt] = auxMeltingPressure( TK, P)

        """
        # Coefficients for calculating the melting pressure of Ice Ih.
        a = np.array([0.119539337e+07, 0.808183159e+05, 0.333826860e+04])
        b = np.array([0.300000e+01, 0.257500e+02, 0.103750e+03])
        PPa = P*1e5  # convert bar to Pa
        if (PPa <= 208.566e6):
            if (251.165 <=  TK <= 273.16):
                # Ice Ih.
                theta = TK/273.16
                pimelt = 1 + np.sum(a*( 1 - theta**b))
                Pmelt = pimelt*611.647   #e-06
            else:
                Pmelt = np.nan #2085.66
        else:
            if (251.165 <= TK < 256.164):
                # Ice III.
                theta = TK/251.165
                pimelt = 1 - 0.299948*( 1 - theta**60 )
                Pmelt = pimelt*208.566e6
            elif (256.164 <= TK < 273.31):
                # Ice V.
                theta = TK/256.164
                pimelt = 1 - 1.18721*( 1 - theta**8 )
                Pmelt = pimelt*350.1e6
            elif (273.31 <= TK < 355.0):
                # Ice VI.
                theta = TK/273.31
                pimelt = 1 - 1.07476*( 1 - theta**4.6 )
                Pmelt = pimelt*632.4e6
            elif (355.0 <= TK < 715.0):
                # Ice VII.
                theta = TK/355.0
                px =   0.173683e+01*( 1 - theta**(-1) ) - 0.544606e-01*( 1 - theta**5 ) \
                    + 0.806106e-07*( 1 - theta**22 )
                pimelt = np.exp(px)
                Pmelt = pimelt*2216.0e6
            elif (715.0 <= TK <= 2000.0):
                # This is out of range. Ice VII, extrapolated.
                theta = TK/355.0
                px =   0.173683e+01*( 1 - theta**(-1) ) - 0.544606e-01*( 1 - theta**5 ) \
                    + 0.806106e-07*( 1 - theta**22 )
                pimelt = np.exp(px)
                Pmelt = pimelt*2216.0e6
            else:
                Pmelt = np.nan

        return Pmelt*1e-5

    def auxMeltingTemp(P):
        """
        This function calculates the melting temperature of ice as a function of pressure.

        This inverts the model for the melting pressure as a function of temperature.
        That model is described in IAPWS R14-08(2011), Revised Release on the Pressure
        along the Melting and Sublimation Curves of Ordinary Water Substance as may be found at:
        http://www.iapws.org/relguide/MeltSub.html

        Inversion of the model for the melting pressure is done here using the secant method.
        This is chosen instead of the Newton-Raphson method to avoid potential problems with slope
        discontinuites at boundaries between the ice phases for pressures above 208.566 MPa,
        which is the equilibrium pressure for Ice Ih-Ice III-liquid. The corresponding equlibrium
        temperature is 251.165K. Putative melting temperatures should not be less than this for
        pressures above 208.566 Mpa, nor more than this for pressures less than this.
        Parameters
        ----------
            P     :   pressure [bar]
        Returns
        ----------
            Tmelt :   temperature [K]
        Usage
        ----------
          [Tmelt] = auxMeltingTemp( P)

        """
        # Calculates the melting temperature that corresponds to the actual pressure.
        # The variables t0 and t1 are initial values for the iteration.
        PPa = P*1e5  # convert bar to Pa
        if (PPa < 208.566e6):
            # In the Ice Ih field.
            tlim0 = 251.165
            tlim1 = 273.16
            t0 = tlim0
            t1 = tlim1
        elif (208.566e6 <= PPa < 350.1e6):
            # In the Ice III field.
            tlim0 = 251.165
            tlim1 = 256.164
            t0 = tlim0
            t1 = tlim1
        elif (350.1e6 <= PPa < 632.4e6):
            # In the Ice V field.
            tlim0 = 256.164
            tlim1 = 273.31
            t0 = tlim0
            t1 = tlim1
        elif (632.4e6 <= PPa < 2216.0e6):
            # In the Ice VI field.
            tlim0 = 273.31
            tlim1 = 355.0
            tx = tlim1 - tlim0
            t0 = tlim0 + 0.3*tx
            t1 = tlim0 + 0.7*tx
        elif (2216.0e6 <= PPa <= 10000.0e6):
            # In the Ice VII field.
            # Note: the upper limit here of 10000 MPa is an arbitrary cutoff suggested
            # by Figure 1 from IAPWS R14, but this is not part of the IAPWS R14 standard.
            tlim0 = 355.0
            tlim1 = 1000.0
            tx = tlim1 - tlim0
            t0 = tlim0 + 0.3*tx
            t1 = tlim0 + 0.7*tx
        elif (PPa > 20000.0e6):
            Tm = np.nan

        funct_melt = lambda t: auxMeltingPressure(t, P) - P
        Tm = root_scalar(funct_melt, method = 'secant',
                         bracket=[t0, t1], x0=t0, x1=t1, xtol = 1.0e-10).root

        Tmelt = Tm

        return Tmelt

    def waterviscosity(TC, P, rho):
        """
        This function calculates the viscosity of water using
        Ref:
            (1) "IAPWS Formulation 2008 for the Viscosity of Ordinary Water Substance" (IAPWS R12-08).
            (2) Huber M.L., Perkins R.A., Laesecke A., Friend D.G., Sengers J.V., Assael M.J., Metaxa I.N.,
                Vogel E., Mares R., and Miyagawa K. (2009) New International Formulation for the Viscosity
                of H2O. J. Phys. Chem. Ref. Data 38, 101-125.    \n
        Parameters
        ----------
            TC       temperature [°C]    \n
            P        pressure [bar]    \n
            rho      density [kg/m3]    \n
        Returns
        ----------
            visc     viscosity [Pa.s]
        Usage
        ----------
          [visc] = waterviscosity( TC, P, rho)
        """
        TK = celsiusToKelvin(TC)  #convert to Kelvin
        PPa = P*1e5

        Tc = IAPWS95_COEFFS['Tc'] # K
        rhoc = IAPWS95_COEFFS['rhoc']

        H0 = [1.67752e+00, 2.20462e+00, 0.6366564e+00, -0.241605e+00]
        I1 = [0, 1, 2, 3, 0, 1, 2, 3, 5, 0, 1, 2, 3, 4, 0, 1, 0, 3, 4, 3, 5]
        J1 = [0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 4, 4, 5, 6, 6]
        H1 = [0.520094,  0.0850895, -1.08374, -0.289555, 0.222531, 0.999115, 1.8879700000000001,
              1.26613, 0.120573, -0.281378, -0.9068510000000001, -0.772479, -0.48983699999999997,
              -0.25704, 0.161913, 0.257399, -0.0325372, 0.0698452, 0.00872102, -0.00435673, -0.000593264]

        delta = rho/rhoc
        Tbar = TK/Tc
        muref = 1.00e-06   # Pa.s

        # Check range of validity.
        chk = False
        # triple point
        Ttr = 273.16 # K
        Ptr = 611.657 # Pa
        Ttmltx = auxMeltingTemp(P)

        if (PPa < Ptr):
            if (Ttr <= TK <= 1173.15):
                chk = True
        elif Ttmltx > 0.0:
            if (Ptr <= PPa <= 300.0e6):
                if (Ttmltx <= TK <= 1173.15):
                  chk = True
            elif (300.0e6 < PPa <= 350.0e6):
                if (Ttmltx <= TK <= 873.15):
                  chk = True
            elif (350.0e6 < PPa <= 500.0e6):
                if (Ttmltx <= TK <= 433.15):
                  chk = True
            elif (500.0e6 < PPa <= 1000.0e6):
                if (Ttmltx <= TK <= 373.15):
                  chk = True

        if (not chk) | (Ttmltx <= 0):
            mubar = np.nan
        else:
            # Calculate the viscosity in the dilute gas limit (mubar0)
            mubar0 = 100*np.sqrt(Tbar)/np.sum([x/Tbar**idx for idx, x in enumerate(H0)])

            s1=np.zeros([len(H1), 1])
            for x in range(len(H1)):
                s1[x] = H1[x]*((1/Tbar) -1)**I1[x] * (delta-1)**J1[x]
            # Calculate the contribution to viscosity due to finite density
            mubar1 = np.exp(delta * np.sum(s1))

            # critical enhancement
            xmu = 0.068
            qc = 1/1.9      #/nm
            qD = 1/1.1      #/nm
            nvc = 0.63
            gamma = 1.239
            xicl0 = 0.13
            gam0 = 0.06
            TR = 1.5
            Tkr = TR*Tc

            # Estimate the reference zeta value by directly evaluating the EOS model
            [_, _, _, _, _, _, _, _, ztxr, _, _, _] = EOSIAPW95(Tkr, rho)

            # Compare the reference zeta value obtained by the two methods.
            # zdx = zettbr - ztxr
            [_, _, _, _, _, _, _, _, ztx, _, _, _] = EOSIAPW95(TK, rho)

            # Get delchb (DELTA chibar, equation 21).
            delchb = delta*( ztx - (ztxr*TR*Tbar) )
            if (delchb < 0.0):
                delchb = 0.0
            # Get xicl (equation 20).
            xicl = xicl0*( (delchb/gam0)**(nvc/gamma) )
            # Get psid (equation 17).
            psid = math.acos(( 1.0 + (qD*xicl)**2 )**(-0.5))
            # Get www (w in equation 19).
            w = np.sqrt(abs( ((qc*xicl) - 1.0)/((qc*xicl) + 1.0) )) * math.tan(0.5*psid)
            # Get lcap (L(w) in equation 18)
            if ((qc*xicl) > 1):
                lcap = np.log( (1 + w)/(1 - w) )
            else:
                lcap = 2*math.atan( abs(w) )

            if (xicl <= 0.3817016416):
                ycap  = 0.2*(qc*xicl)*((qD*xicl)**5)* \
                    (1 - (qc*xicl) + (qc*xicl)**2  - ( 765.0/504.0 )*(qD*xicl)**2)
            else:
                ycap = ( 1/12 )*math.sin(3*psid) - ( 1/(4*(qc*xicl)) )*math.sin(2*psid) +\
                    ( 1/(qc*xicl)**2 )*( 1.0 - 1.25*(qc*xicl)**2 ) *math.sin(psid) \
                        -( 1.0/(qc*xicl)**3 ) *( ( 1.0 - 1.5*(qc*xicl)**2 ) \
                                                - ( abs((qc*xicl)**2 - 1) )**1.5 * lcap)

            if (645.91 < TK < 650.77):
                if (245.8 < rho < 405.3):
                    mubar2 = np.exp(xmu * ycap)
            else:
                mubar2 = 1

            mubar = mubar0 * mubar1 *mubar2
        visc = mubar*muref
        return visc #, ztx, zettbr

    def fluidDescriptor(P, TK, *rho):
        """
        This function calculates the appropriate description of the H2O fluid at any given
        temperature and pressure    \n

        A problem may occur if the pressure is equal or nearly equal to the saturation pressure.
        Here comparing the pressure with the saturation pressure pressure may lead to the wrong
        description, as vapor and liquid coexist at the saturation pressure. It then becomes
        neccesary to compare the fluid density with the saturated vapor and saturated liquid densities.
        If the density is known, it will be used. If it is not known, the results obtained
        here will determine the starting density estimate, thus in essence choosing "vapor" or "liquid"
        for pressures close to the saturation pressure.       \n
        Parameters:
        ----------
            P        pressure [bar]    \n
            TK       temperature [K]    \n
            rho      density [kg/m3] (optional)    \n
        Returns:
        ----------
            phase   fluid description    \n
            rhosl    liquid density [kg/m3]    \n
            rhosv    vapor density [kg/m3]    \n
        Usage:
        ----------
          [udescr, rhosl, rhosv] = fluidDescriptor( P, TK)
        """
        # P = P*10 # MPa to bar
        Tc = IAPWS95_COEFFS['Tc'] # K
        Pc = IAPWS95_COEFFS['Pc']*10 # convert MPa to bar
        # rhoc = IAPWS95_COEFFS['rhoc']
        btxtol = 1e-10
        rhotol = 1.0e-8

        if (TK < 273.15):
            # Note that the allowed temperature range has been extended a bit on the low end to include 0C.
            phase = 'unknown'
            rhosv = 0; rhosl = 0
        elif (TK <= Tc):
            # Calculate the saturation curve properties.
            [Psat, rhosl, rhosv] = calcsatpropT(TK)
            if (P > Pc):
                phase = 'compressed liquid'
                if Psat == np.nan:
                    # if calcsatpropT(TK) failed, an arbitrary liquid-like density will be assigned
                    # as a starting value for compressed liquid
                    rhosl = 1.05
            else:
                # if calcsatpropT(TK) failed, the vapor and liquid states cannot be distinguished
                # from one another. Liquid is assigned arbitrarily
                if Psat == np.nan:
                    phase = 'liquid'
                    rhosl = 1.05
                else:
                    if (P >= Psat):
                        phase = 'liquid'
                    else:
                        phase = 'vapor'
                    # Use density (rho) if available and pressure is close to psat.
                    if len(rho) != 0:
                        Ptest = (P - Psat)/Psat
                        btest = 10*btxtol
                        if (abs(Ptest) <= btest):
                            # Here press is very close to psat. Use rho to determine vapor or liquid.
                            rtestl = (rho - rhosl)/rhosl
                            rtestv = (rho - rhosv)/rhosv
                            if (abs(rtestl) <= rhotol):
                                phase = 'liquid'
                            elif (abs(rtestv) <= rhotol):
                                phase = 'vapor'
                            else:
                                phase = 'unknown'
        else:
            rhosv = 0; rhosl = 0
            if (P > Pc):
                phase = 'supercritical fluid'
            else:
                phase = 'hot vapor'

        return phase, float(rhosl), float(rhosv)

    # vfluidDescr = np.vectorize(fluidDescriptor) # vectorize the function above to permit array variables

    def apxsatpropT(TK):
        """
        This function evaluates the approximate pressure (psat) as a function of temperature along
        the vapor-liquid equilibrium curve, using equation 2.5 of Wagner and Pruss (2002).
        It also calculates the derivative of saturation pressure wrt temperature as well as the
        densities of the liquid and vapor phases using equations 2.6 and 2.7
        from the same source.    \n
        Parameters:
        ----------
            TK       temperature [K] (saturation temperature)    \n
        Returns:
        ----------
            Psat    saturation pressure [bar]    \n
            Psat_t  Derivative of saturation pressure with respect to temperature    \n
            rhosl   density of liquid [kg/m3]    \n
            rhosv   density of vapor [kg/m3]    \n
        Usage:
        ----------
          [Psat, Psat_t, rhosl, rhosv] = apxsatpropT( TK)
        """
        Tc = IAPWS95_COEFFS['Tc'] # K
        Pc = IAPWS95_COEFFS['Pc'] # MPa
        a = [-7.85951783, 1.84408259, -11.7866497, 22.6807411, -15.9618719, 1.80122502]

        rhoc = IAPWS95_COEFFS['rhoc']
        b = [1.99274064, 1.09965342, -0.510839303, -1.75493479, -45.5170352, -6.74694450e5]
        c = [-2.03150240, -2.68302940, -5.38626492, -17.2991605, -44.7586581, -63.9201063]

        # Check to see that the temperature is in the allowed range.
        if (TK < 273.15) | (TK > Tc):
            Psat = np.nan
            Psat_t = np.nan
            rhosl = np.nan
            rhosv = np.nan
        else:
            # Saturation pressure.
            th = 1 - TK/Tc
            t1 = a[0]*th + a[1]*th**1.5 + a[2]*th**3 + a[3]*th**3.5 + a[4]*th**4 + a[5]*th**7.5
            Psat = Pc*np.exp(Tc*t1/TK)
            # Derivative of saturation pressure with respect to temperature.
            x1 = a[0] + 1.5*a[1]*th**0.5 + 3.0*a[2]*th**2.0 + 3.5*a[3]*th**2.5 + 4.0*a[4]*th**3.0 + 7.5*a[5]*th**6.5
            x2 = np.log( Psat/Pc ) + x1
            Psat_t = -( Psat/TK )*x2
            # Density of liquid.
            th = (1 - TK/Tc)**(1/3)
            t1 = 1 + b[0]*th + b[1]*th**2 + b[2]*th**5 + b[3]*th**16 + b[4]*th**43 + b[5]*th**110
            rhosl = rhoc*t1
            # Density of vapor.
            th = np.sqrt(th)
            t2 = c[0]*th**2 + c[1]*th**4 + c[2]*th**8 + c[3]*th**18 + c[4]*th**37 + c[5]*th**71
            rhosv = rhoc*np.exp(t2)

        return Psat*10, Psat_t*10, rhosl, rhosv

    vapxsatpropT = np.vectorize(apxsatpropT) # vectorize the function above to permit array variables

    def apxsatpropP(P):
        """
        This function evaluates the approximate temperature (tsat) as a function of pressure along
        the vapor-liquid equilibrium curve, using equation 2.5 of Wagner and Pruss (2002).
        This is similar to apxsatpropT(TK), but evaluates the inverse problem (Tsat as a function
        of pressure instead of psat as a function of temperature). Newton-Raphson iteration is used.    \n
        Parameters:
        ----------
            P        pressure [bar]    \n
        Returns:
        ----------
            Tsat     saturation temperature [K]    \n
        Usage:
        ----------
          [Tsat] = apxsatpropP( P)
        """
        Tc = IAPWS95_COEFFS['Tc'] # K
        Pc = IAPWS95_COEFFS['Pc']*10 # bar
        # triple point
        Ttr = 273.16 # K
        Ptr = 611.657e-5 # bar
        P1atm = 1.01325e0  # bar
        Ts1atm = 373.124  # K

        # Check to see that the pressure is in the allowed range.
        if (P < Ptr) | (P > Pc):
            Tsat = np.nan
        else:
            # Choose a starting temperature value.
            if (P >= P1atm):
                # Interpolate between 100C, 1.01325 bar and Tcr, Pcr.
                dtdp = (Tc - Ts1atm)/(Pc - P1atm)
                TK = Ts1atm + dtdp*(P - P1atm)
            else:
                # Interpolate between the triple point and 100C, 1.013 bar
                dtdp = (Ts1atm - Ttr)/(P1atm - Ptr)
                TK = Ttr + dtdp*(P - Ptr)

            funct_tsat = lambda T: vapxsatpropT(T)[0]*0.1 - P*0.1
            Tsat = fsolve(funct_tsat, TK, xtol=1.48e-10)[0]

        return float(Tsat)

    vapxsatpropP = np.vectorize(apxsatpropP) # vectorize the function above to permit array variables

    def calcsatpropP(P):
        """
        This function calculates the saturation properties as a function of specified pressure.
        This is done by iterating using Newton method on pressure to obtain the desired
        temperature. This implementation calls calcsatpropT(TK) to calculate the saturation
        pressure, liquid and vapor densities.    \n

        Parameters
        ----------
            P        pressure [bar]    \n
        Returns
        ----------
            Tsat     temperature [K]    \n
            rhosl    liquid density [kg/m3]    \n
            rhosv    vapor density [kg/m3]    \n
        Usage
        ----------
          [Tsat, rhosl, rhosv] = calcsatpropP( P)
        """
        btxtol = 1.0e-10
        itermx = 50
        Tc = IAPWS95_COEFFS['Tc'] # K

        # Calculate approximate saturation temperature to use as a starting estimate
        Tsat = vapxsatpropP(P) - 273.15 # convert to C

        # Iterate to calculate the saturation temperature
        funct_tsat = lambda TC: calcsatpropT(TC + 273.15)[0] - P
        Tsat = brentq(funct_tsat, 0, Tc - 273.15, xtol=btxtol, maxiter = itermx) + 273.15
        #Tsat = newton(funct_tsat, Tsat, fprime=None, args=(), tol=btxtol, maxiter=itermx, fprime2=None)

        # Calculate liquid and vapor densities
        [_, rhosl, rhosv] = calcsatpropT(Tsat)

        return float(Tsat), float(rhosl), float(rhosv)

    vcalcsatpropP = np.vectorize(calcsatpropP) # vectorize the function above to permit array variables

    def calcsatpropT(TK):
        """
        This function calculates the saturation properties as a function of specified temperature.
        This is achieved using Newton-Raphson iteration to refine values of pressure, vapor density,
        and liquid density, starting with results obtained using approximate equations included
        by Wagner and Pruss (2002) in their description of the IAPWS-95 model.    \n
        Parameters
        ----------
           TK      temperature [K]    \n
        Returns
        ----------
           Psat    saturation pressure [bar]    \n
           rhosl   density of liquid [kg/m3]    \n
           rhosv   density of vapor [kg/m3]    \n
        Usage
        ----------
          [Psat, rhosl, rhosv] = calcsatpropT( TK)
        """
        Tc = IAPWS95_COEFFS['Tc'] # K
        rhoc = IAPWS95_COEFFS['rhoc']
        bettl1 = 1.0e-8
        bettl2 = 1.0e-7
        btxtol = 1.0e-10
        qxiter = False

        alpha = np.zeros([3, 1]); beta = alpha
        aamatr = np.zeros([3, 3])
        deltas = np.zeros([3, 1]); arelax = 1

        if (TK <= 298.15):
            btxtol = bettl1
        elif (647.090 < TK < Tc):
            qxiter = True
        elif (TK > 647.090):
            btxtol = bettl2

        #  Calculate approximate saturation pressure and corresponding densities of liquid and vapor.
        #  These results are not those of the IAPWS-95 model itself, but can serve as starting estimates.
        [Psat, _, rhosl, rhosv] = apxsatpropT(TK)
        #  Save the values from the approximation.
        delta_svq = rhosv/rhoc
        delta_slq = rhosl/rhoc

        it = 0
        itermx = 50
        Psat0 = Psat
        deltasv0 = delta_svq
        deltasv = delta_svq
        deltasl0 = delta_slq
        deltasl = delta_slq


        while True:
            # Below is the return point to refine the saturation curve properties.
            # First calculate the vapor properties by calling EOSIAPW95 with the vapor density.
            [Pxv, axv, _, _, _, _, Pdxv, adxv, _, _, _, _] = EOSIAPW95(TK, rhosv)
            # Now calculate the liquid properties by calling EOSIAPW95 with the liquid density.
            [Pxl, axl, _, _, _, _, Pdxl, adxl, _, _, _, _] = EOSIAPW95(TK, rhosl)
            # The pdx for vapor cannot be negative. Under-relax to prevent this.
            if (Pdxv < 0):
                if (it <= 0):
                    #if (icutv >= 30):
                    #icutv = icutv + 1
                    rhosv = 0.995*rhosv
                    deltasv = rhosv/rhoc
                else:
                    #icutv = icutv + 1
                    arelax = 0.25
            #  The revised delta for vapor cannot be less than a good
            #  fraction of the value obtained from the intial approximation.
            if (deltasv < 0.9*delta_svq):
                arelax = 0.25

            #  The pdx for liquid cannot be negative. Under-relax to prevent this.
            if (Pdxl < 0):
                if (it <= 0):
                    #if (icutl >= 30):
                    #icutl = icutl + 1
                    rhosl = 1.001*rhosl
                    deltasl = rhosl/rhoc
                else:
                    #icutl = icutl + 1
                    arelax = 0.25
            # The revised delta for liquid cannot be less than a good
            # fraction of the value obtained from the intial approximation.
            if (deltasl > 1.1*delta_slq):
                arelax = 0.25

            #  The delta for liquid cannot be less than the delta for vapor.
            #  Corrected delta values must be positive to avoid
            #  a singularity in the equation-of-state model equations.
            #  Under-relax to prevent this.
            if (deltasl < deltasv) | (deltasv <= 0 or deltasl <= 0):
                arelax = 0.25

            deltas = deltas*arelax
            Psat = Psat0 + deltas[2]
            deltasl = deltasl0 + deltas[1]
            deltasv = deltasv0 + deltas[0]
            rhosl = rhoc*deltasl
            rhosv = rhoc*deltasv

            # =============================================================================
            # Have obtained valid (outside the unstable zone) vapor and liquid properties for the current iteration.
            # Improve the calculated saturation properties by solving three equations in three unknowns.
            # The equations are all in terms of pressure. The unknowns to be found are Psat, deltasv, and deltasl.
            # Calculate the Maxwell crition pressure (Gibbs energy equality expressed through the
            # Helmholtz energies and the pressure)
            # =============================================================================

            dix = (1/deltasl) - (1/deltasv)
            # dltx = deltasl - deltasv
            if (abs(dix) > 1e-15):
                if (abs(axv - axl) > 1e-15):
                    # Normal calculation, result in kPa.
                    Pxm = rhoc*( axv - axl )/dix
                    Pxm = 0.01*Pxm     #    Convert from kPa to bar.
                else:
                    # There is no difference in the Helmholtz energies of the vapor and the liquid.
                    Pxm = 0
            else:
                # Exception intended for the critical point.
                if (abs(TK - Tc) <= 1e-10) & (abs(deltasv - 1.0) <= 1e-10) & (abs(deltasl - 1) <= 1e-10):
                    # Am at the critical point.
                    Pxm = Pxv
                else:
                    # Not at the critical point, but the vapor and liquid densities have converged.
                    Pxm = 0

            # Calculate residual functions.
            alpha[0] = Pxm - Psat
            alpha[1] = Pxv - Psat
            alpha[2] = Pxl - Psat

            beta = abs(alpha/Psat)
            betamx = np.max(beta)
            # Note: using a convergence tolerance below 1.0d-11
            # may lead to non-convergence due to the limitations of 64-bit arithmetic.

            # print(it, ' Psat: %.6e' % Psat[0], 'betamx:  %.6e' % betamx)
            if (betamx <= btxtol) | (it >= itermx):
                # Iteration has converged.
                # P = Psat
                break
            elif (qxiter and it >= 5):
                break

            # Since this matrix is only 3x3, the simultaneous equations have the form:
            # aamatr(i,1)*deltas(1) + aamatr(i,2)*deltas(2) + aamatr(i,3)*deltas(3) = -alpha(i), i = 1,3
            # The Jacobian matrix J here is aamatr(kdim, kdim).
            aamatr[0, :] = [Pxm*(-(1/(dix*deltasv**2)) + (adxv/(axv - axl))),
                            Pxm*((1/(dix*deltasl**2)) - (adxl/(axv - axl))),
                            -1]
            aamatr[1, :] = [Pdxv,
                            0,
                            -1]
            aamatr[2, :] = [0,
                            Pdxl,
                            -1]

            deltas = -lu_solve(lu_factor(aamatr), alpha)
            #  Save current values.
            Psat0 = Psat
            deltasv0 = deltasv
            deltasl0 = deltasl
            Psat = Psat0 + deltas[2]
            deltasl = deltasl0 + deltas[1]
            deltasv = deltasv0 + deltas[0]
            rhosl = rhoc*deltasl
            rhosv = rhoc*deltasv

            it = it + 1

        return float(Psat), float(rhosl), float(rhosv)

    vcalcsatpropT = np.vectorize(calcsatpropT) # vectorize the function above to permit array variables

    def calcwaterppt(TC, P, *rho0):
        """
        This function evaluates thermodynamic properties of water at given temperature and pressure.
        The problem reduces to finding the value of density that is consistent with the desired pressure.
        The Newton-Raphson method is employed. Small negative values of calculated pressure are okay.
        Zero or negative values for calculated "pdx" (pressure derivative with respect to delta)
        imply the unstable zone and must be avoided.  \n
        Parameters:
        ----------
            T     :  temperature [°C]  \n
            P     :  pressure [bar]  \n
            rho0  :  starting estimate of density [kg/m3] (optional)
        Returns:
        ----------
            rho   :  density [kg/m3]  \n
            gx    :  Gibbs energy [cal/mol]  \n
            hx    :  Enthalpy [cal/mol]  \n
            sx    :  Entropy [cal/mol/K]  \n
            vx    :  Volume [m3/mol]  \n
            Pout  :  pressure [bar]  \n
            Tout  :  temperature [C]  \n
            #ux   :  Internal energy [cal/mol]  \n
            #cpx  :  Isobaric heat capacity [cal/mol/K]  \n
            #visc :  viscosity [Pa-s]
        Usage:
        ----------
            [rho, gxcu, hxcu, sxcu, vxcu, Pout, Tout] = calcwaterppt(T, P),   \n
        """

        if np.ndim(TC) == 0:
            TC = np.array(TC).ravel()
        else:
            TC = TC.ravel()

        if np.ndim(P) == 0:
            P = np.array(P).ravel()
        else:
            P = P.ravel()

        Pout = P # cases where 'T' is used as input
        Tout = TC # cases where 'P' is used as input
        TK = celsiusToKelvin(TC)
        Ppa = P * 0.1  #Convert bars to MPa
        Tc = IAPWS95_COEFFS['Tc'] # K
        Pc = IAPWS95_COEFFS['Pc'] # MPa
        rhoc = IAPWS95_COEFFS['rhoc']
        R = IAPWS95_COEFFS['R']*1e-3  #KJ/(kg*K)
        # if len(rho0) != 0:
        #     itermx = 80
        # else:
        #     itermx = 100
        # btxtol = 1.0e-8

        # Obtain a description (udescr) of the H2O fluid.
        rho = np.zeros([len(TK), 1]).ravel()
        for i in range(len(TK)):
            if len(rho0) == 0:
                [phase, rhosl, rhosv] = fluidDescriptor(P[i], TK[i], rho0[i])
            else:
                [phase, rhosl, rhosv] = fluidDescriptor(P[i], TK[i])

            if (phase == 'vapor'):
                # Vapor: assume ideal gas behavior.
                rho[i] = 1000.0*Ppa[i]/(TK[i]*R)
            elif (phase == 'liquid'):
                # Liquid: use a liquid-like density. The liquid density on the saturation curve.
                rho[i] = rhosl
            elif (phase == 'compressed liquid'):
                # Estimate the density of compressed liquid. The saturated liquid density is a minimum value.
                rho[i] = 1.10*rhosl
                # Close to the upper limit for this field (T near the triple point, 1000 MPa).
                # For higher pressure, a higher value might be needed. rho = 1250.0d0
                rho[i] = np.maximum(rho[i], rhosl)
                rho[i] = np.minimum(rho[i], 1400)
            elif (phase == 'supercritical fluid'):
                # Estimate density of supercritical fluid. Twice the ideal gas correction to critical point density.
                rho[i] = 2.0*(Ppa[i]/Pc)*(Tc/TK[i])*rhoc
                # Close to the upper limit for this P, T field. (T near the critical point, 1000 MPa).
                # For higher pressure, a higher value might be needed.  rho = 1100.0d0
                rho[i] = np.minimum(rho[i], 1100.0)
            elif (phase == 'hot vapor'):
                # Estimate the density of hot vapor. Ideal gas.
                rhoidg = 1000.0*Ppa[i]/(TK[i]*R)
                # SUPCRT92 estimate, about 15% higher than ideal gas.
                rhosup = 2500.0*Ppa[i]/TK[i]
                # Ideal gas correction to critical point density.
                rhocpa = (Ppa[i]/Pc)*(Tc/TK[i])*rhoc
                # The upper limit for this field, the critical pressure (rhocr), 22.064 MPa. rho = rhoc
                if (Ppa[i] <= 1.0):
                    rho[i] = rhoidg
                elif (Ppa[i] <= 18.0):
                    rho[i] = rhosup
                else:
                    rho[i] = rhocpa
                rho[i] = np.minimum(rho[i], rhoc)
            else:
                # If the H2O fluid type could not be determined. A good starting estimate
                # of density could not be established, try three times the critical density.
                rho[i] = 4*rhoc

        funct_tsat = lambda rho: EOSIAPW95(TK, rho)[0]*0.1 - P*0.1
        # rho = root_scalar(funct_tsat, method = 'secant',
        #                   bracket=[0.01, 1e4], x0 = 0.01, x1 = 1e4, xtol = btxtol).root
        #rho = brentq(funct_tsat, 0.01, 1e4, xtol=btxtol)
        rho = fsolve(funct_tsat, rho)[0]
        #rho = newton(funct_tsat, rho, fprime = None, args=(), tol=btxtol, maxiter=itermx, fprime2 = None)

        [Px, _, sx, hx, gx, vx,  _, _, _, _, _, _] = EOSIAPW95(TK, rho)
        # visc = waterviscosity(TK-273.15, P, rho)[0]

        # The following reference state data are in kilogram units.
        ds = 3.5156150 # Entropy (kJ/kg/K)
        # du = -15767.19391 # Internal energy (kJ/kg)
        dh = -15970.89538 #  Enthalpy (kJ/kg)
        # da = -11906.84446 # Helmholtz energy (kJ/kg-K)
        dg = -12110.54592 # Gibbs energy (kJ/kg)
        htripl = 0.000611782 # tripple point Enthalpy (kJ/kg)
        mwH2O = 18.01528/1000    # kg/mol
        # Calculate the entropy, internal energy, enthalpy, Helmholtz energy, and Gibbs energy
        # on the standard thermochemical scale, and volume, heat capacity at constant volume.
        # Results are converted from KJ and kilogram units to cal and molar units.
        sxcu = sx + ds
        # uxcu = mwH2O*(ux + du)*1000/J_to_cal
        hxcu = mwH2O*(hx - htripl + dh)*1000/J_to_cal
        # axcu = mwH2O*(ux - TK*sxcu + da)*1000/J_to_cal
        gxcu = mwH2O*(hx - TK*sxcu + dg)*1000/J_to_cal
        vxcu = mwH2O*vx*1000/J_to_cal
        # cpxcu = mwH2O*cpx*1000/J_to_cal
        sxcu = mwH2O*(sx + ds)*1000/J_to_cal

        return rho, gxcu, hxcu, sxcu, vxcu, Pout, Tout

    vcalcwaterppt = np.vectorize(calcwaterppt) # vectorize the function above to permit array variables

    def calcwaterppt_Prho(P, rho):
        """
        This function evaluates thermodynamic properties of water at given density and pressure.
        The problem reduces to finding the value of temperature that is consistent with the desired pressure.  \n
        Parameters:
        ----------
            P     :  pressure [bar]  \n
            rho   :  density [kg/m3]  \n
        Returns:
        ----------
            rho   :  density [kg/m3]  \n
            gx    :  Gibbs energy [cal/mol]  \n
            hx    :  Enthalpy [cal/mol]  \n
            sx    :  Entropy [cal/mol/K]  \n
            vx    :  Volume [m3/mol]  \n
            Pout  :  pressure [bar]  \n
            Tout  :  temperature [°C]  \n
            #ux   :  Internal energy [cal/mol]  \n
            #cpx  :  Isobaric heat capacity [cal/mol/K]  \n
            #visc :  viscosity [Pa-s]
        Usage:
        ----------
            [rho, gxcu, hxcu, sxcu, vxcu, Pout, Tout] = calcwaterppt_Prho(P, rho),   \n
        """

        if np.ndim(rho) == 0:
            rho = np.array(rho).ravel()
        else:
            rho = rho.ravel()

        if np.ndim(P) == 0:
            P = np.array(P).ravel()
        else:
            P = P.ravel()

        Tc = IAPWS95_COEFFS['Tc'] # K
        Pc = IAPWS95_COEFFS['Pc']*10 # bar

        if P  > Pc:
            TK = Tc
        else:
            TK = calcsatpropP(P)
        funct_tsat = lambda TK: EOSIAPW95(TK, rho)[0] - P
        TK = fsolve(funct_tsat, TK)[0]
        Pout = P; Tout = TK - 273.15
        [Px, _, sx, hx, gx, vx,  _, _, _, _, _, _] = EOSIAPW95(TK, rho)
        # visc = waterviscosity(TK-273.15, P, rho)[0]

        # The following reference state data are in kilogram units.
        ds = 3.5156150 # Entropy (kJ/kg/K)
        # du = -15767.19391 # Internal energy (kJ/kg)
        dh = -15970.89538 #  Enthalpy (kJ/kg)
        # da = -11906.84446 # Helmholtz energy (kJ/kg-K)
        dg = -12110.54592 # Gibbs energy (kJ/kg)
        htripl = 0.000611782 # tripple point Enthalpy (kJ/kg)
        mwH2O = 18.01528/1000    # kg/mol
        # Calculate the entropy, internal energy, enthalpy, Helmholtz energy, and Gibbs energy
        # on the standard thermochemical scale, and volume, heat capacity at constant volume.
        # Results are converted from KJ and kilogram units to cal and molar units.
        sxcu = sx + ds
        # uxcu = mwH2O*(ux + du)*1000/J_to_cal
        hxcu = mwH2O*(hx - htripl + dh)*1000/J_to_cal
        # axcu = mwH2O*(ux - TK*sxcu + da)*1000/J_to_cal
        gxcu = mwH2O*(hx - TK*sxcu + dg)*1000/J_to_cal
        vxcu = mwH2O*vx*1000/J_to_cal
        # cpxcu = mwH2O*cpx*1000/J_to_cal
        sxcu = mwH2O*(sx + ds)*1000/J_to_cal
        return rho, gxcu, hxcu, sxcu, vxcu, Pout, Tout

    vcalcwaterppt_Prho = np.vectorize(calcwaterppt_Prho) # vectorize the function above to permit array variables

    """initialization """
    TC = kwargs["T"]
    rho = kwargs["rho"]
    rho0 = kwargs["rho0"]
    P = kwargs["P"]

    if type(P) == str:
        if P == 'T':
            P = vcalcsatpropT(celsiusToKelvin(TC))[0]
            P[np.isnan(P) | (P < 1)] = 1.0133

    if type(TC) == str:
        if TC == 'P':
            TC = vcalcsatpropP(P)[0]
            TC = TC - 273.15 # convert from K to celcius

    if rho0 is not None:
        if np.size(rho0) < np.size(TC):
            rho0 = (rho0*np.ones_like(TC)).ravel()

    """Check if inputs are enough to define state"""
    if type(P) != str and type(TC) != str:
        if np.ravel(TC).any() and np.ravel(P).any():
            mode = "T_P"
            if np.size(P) < np.size(TC):
                P = (P*np.ones_like(TC)).ravel()
            if np.size(TC) < np.size(P):
                TC = (TC*np.ones_like(P)).ravel()
        elif np.ravel(TC).any() and np.ravel(rho).any():
            mode = "T_rho"
            if np.size(rho) < np.size(TC):
                rho = (rho*np.ones_like(TC)).ravel()
            if np.size(TC) < np.size(rho):
                TC = (TC*np.ones_like(rho)).ravel()
        elif np.ravel(P).any() and np.ravel(rho).any():
            mode = "P_rho"
            if np.size(P) < np.size(rho):
                P = (P*np.ones_like(rho)).ravel()
            if np.size(rho) < np.size(P):
                rho = (rho*np.ones_like(P)).ravel()
        elif np.ravel(TC).any() and np.ravel(P).any() is None:
            mode = "T_x"
        elif np.ravel(P).any() and np.ravel(TC).any() is None:
            mode = "P_x"
        else:
            mode = ""
    else:
        if TC and P:
            mode = "T_P"
    TK = celsiusToKelvin(TC) if TC is not None else None
    # P = np.array(P).ravel(); TC = np.array(TC).ravel(); rho = np.array(rho).ravel()
    # return P, TC

    if mode == "T_P":
        [rho, gxcu, hxcu, sxcu, vxcu, Pout, Tout] = vcalcwaterppt(TC, P, rho0)
        water = rho, gxcu, hxcu, sxcu, vxcu, Pout, Tout
    elif mode == "T_rho":
        [px, ax, sx, hx, gx, vx, pdx, adx, ztx, ptx, ktx, avx] = vEOSIAPW95( TK, rho)
        water = px, ax, sx, hx, gx, vx, pdx, adx, ztx, ptx, ktx, avx
    elif mode == "P_rho":
        [rho, gxcu, hxcu, sxcu, vxcu, Pout, Tout] = vcalcwaterppt_Prho(P, rho)
        water = rho, gxcu, hxcu, sxcu, vxcu, Pout, Tout
    elif mode == "P_x":
        [Tsat, rhosl, rhosv] = vcalcsatpropP(P)
        water = Tsat, rhosl, rhosv
    elif mode == "T_x":
        [Psat, rhosl, rhosv] = vcalcsatpropT(TK)
        water = Psat, rhosl, rhosv
    elif mode == "":
        return ('For water properties at any Temperature and Pressure \n \
                input T = temperature (celsius) and P = pressure (bar)  \n \
                    For water properties at any Temperature and Pressure on steam saturation curve \n' + \
                    'input T = temperature (celsius) and P = "T" to reflect steam saturation pressure  \n' + \
                        'For water properties at any Temperature and density  \n' + \
                            'input T = temperature (celsius) and rho  = density (kg/m³) in kg/m³' +\
                                'For water properties at any Pressure and density \n' + \
                                    'input P = pressure (bar) and rho  = density (kg/m³) \n' + \
                                        'For water saturation properties at any saturation Temperature  \n' + \
                                            'input T = temperature (celsius) \n' + \
                                                'For water saturation properties at any saturation Pressure \n' + \
                                                    'P = pressure (bar)')

    return water

#%%----------------------------------------------------------------

def heatcap(TC, P, species):
    """
    This function evaluates Gibbs energy and heat capacity as a function of temperature
    and pressure for any mineral specie using Maier-Kelley power function for heat capacity
    References
    ----------
        (1) Kelley K. K (1960) Contributions to the data on theoretical metallurgy. XIII. High
            temperature heat content, heat capacity and entropy data for elements and inorganic compounds US Bur Mines Bull 548: 232 p
    Parameters
    ----------
       TC            temperature [°C]
       P             pressure [bar]
       species       properties such as [dG [cal/ml], dH [cal/mol], S [cal/mol-K], V [cm³/mol]
                       a [cal/mol-K],b [*10^3 cal/mol/K^2],c [*10^-5 cal/mol/K] )
    Returns
    ----------
       delGfPT       Gibbs Energy [cal/mol]
       delCp         heat capacity [cal/mol/K]
    Usage
    ----------
      [delGfPT, delCp] = heatcap( TC, P, species)
    """
    if (np.ndim(TC) == 0) | (np.ndim(P) == 0):
        T = np.array(celsiusToKelvin(TC)).ravel()
        P = np.array(P).ravel()
    else:
        T = celsiusToKelvin(TC).ravel()
        P = P.ravel()
    a = species[6]
    b = species[7]*10**-3
    c = species[8]*10**5
    delGref = species[2]
    Sr = species[4]
    Vr = species[5]*0.0239005736   # conversion: cal/bar/cm3  See Johnson et al., 1992
    Tr = 298.15
    Pr = 1.0
    delCp = a + b*T + c*T**-2
    delGfPT = delGref - Sr*(T-Tr) + a*(T-Tr-T*np.log(T/Tr)) + ((-c-b*T*Tr**2)*(T-Tr)**2)/(2*T*Tr**2) + Vr*(P-Pr)

    return delGfPT, delCp

def heatcapusgscal(TC, P, species):
    """
    This function evaluates Gibbs energy and heat capacity as a function of temperature and pressure
    for any mineral specie using Haas and Fisher (1976)'s heat capacity parameter fit (utilized for solid-solutions)
    References
    ----------
        (1) Haas JL Jr, Fisher JR (1976) Simultaneous evaluation and correlation of thermodynamic data.
            American Journal of Science 276:525-545
    ----------
       TC            temperature [°C]
       P             pressure [bar]
       species       properties such as [dG [cal/ml], dH [cal/mol], S [cal/mol-K], V [cm³/mol]
                       a [cal/mol-K], b [cal/mol/K^2], c [cal/mol/K], g [cal/mol/K^0.5],
                       f [cal/mol/K^3] )
    Returns
    ----------
       delGfPT       Gibbs Energy [cal/mol]
       delCp         heat capacity [cal/mol/K]
    Usage
    ----------
      [delGfPT, delCp] = heatcapusgscal( TC, P, species)
    """
    if (np.ndim(TC) == 0) | (np.ndim(P) == 0):
        T = np.array(celsiusToKelvin(TC)).ravel()
        P = np.array(P).ravel()
    else:
        T = celsiusToKelvin(TC).ravel()
        P = P.ravel()
    Tr = 298.15 # K
    Pr = 1.0    # bar
    delGref = species[2] #cal/mol
    Sr = species[4] # cal/mol/K
    Vr = species[5]*0.02390054 # conversion: cal/bar/cm3  See Johnson et al., 1992
    a = species[6] #
    b = species[7] # T
    c = species[8] # T^-2
    g = species[9] # T^-.5
    f = species[10] # T^2

    delCp = a + b*T + c*(T**-2.0) + g*(T**-.5) + f*(T**2.0)
    delGfPT = np.nan*np.ones(len(T))
    if len(species[2:]) == 9:

        delGfPT = delGref - Sr*(T-Tr) + Vr*(P-Pr) + a*(T-Tr-T*np.log(T/Tr)) + (b/2)*(-T**2-Tr**2+2*T*Tr) \
            + c*((-T**2-Tr**2+2*T*Tr)/(2*T*Tr**2)) + (f/6)*(-T**3-2*Tr**3+3*T*Tr**2) \
                + 2*g*(2*T**0.5-Tr**0.5-(T/(Tr**0.5)))

    elif len(species[2:]) == 15:  #There is one phase transition to be accounted for

        delGfPT = delGref - Sr*(T-Tr) + Vr*(P-Pr) + a*(T-Tr-T*np.log(T/Tr)) + (b/2)*(-T**2-Tr**2+2*T*Tr) \
            + c*((-T**2-Tr**2+2*T*Tr)/(2*T*Tr**2)) + (f/6)*(-T**3-2*Tr**3+3*T*Tr**2)  \
                + 2*g*(2*T**.5-Tr**.5-(T/(Tr**.5)))

        Ttrans = species[11] #K
        # For T greater than the phase transition:
        # Get the heat capacity coefficients for T>Ttrans
        a = species[12] #
        b = species[13] # T
        c = species[14] # T^-2
        f = species[16] # T^2
        g = species[15] # T^-.5

        delGfPT[T>Ttrans] = delGfPT[T>Ttrans] + a*(T[T>Ttrans]-Ttrans-T[T>Ttrans]*np.log(T[T>Ttrans]/Ttrans)) \
            + (b/2)*(-T[T>Ttrans]**2-Ttrans**2+2*T[T>Ttrans]*Ttrans)   \
                + c*((-T[T>Ttrans]**2-Ttrans**2 +2*T[T>Ttrans]*Ttrans)/(2*T[T>Ttrans]*Ttrans**2))  \
                    + (f/6)*(-T[T>Ttrans]**3-2*Ttrans**3+3*T[T>Ttrans]*Ttrans**2)   \
                        + 2*g*(2*T[T>Ttrans]**.5-Ttrans**.5-(T[T>Ttrans]/(Ttrans**.5)))

    return delGfPT, delCp

def dielec_FE97(TC, P):
    """
    This function calculates the dielectric constant of water (E), the Debye-Huckel "A" parameters
    and Debye-Huckel "B" parameters (3)  and their derivatives as a
    function of temperature and pressure \n
    Parameters
    ----------
       TC      : temperature [°C] \n
       P       : pressure [bar] \n
    Returns
    ----------
       E       : dielectric constant of water [cal/mol]  \n
       rhohat  : density [g/cm³] \n
       Ah      : Debye-Huckel "A" parameters [kg^1/2 mol^-1/2] \n
       Bh      : Debye-Huckel "B" parameters [kg^1/2 mol^-1/2 Angstrom^-1] \n
       bdot    : bdot at any given temperature T
       Adhh    : Debye-Huckel "A" parameters associated with apparent molar enthalpy
       Adhv    : Debye-Huckel "A" parameters associated with apparent molar volume
       Bdhh    : Debye-Huckel "B" parameters associated with apparent molar enthalpy
       Bdhv    : Debye-Huckel "B" parameters associated with apparent molar volume
       dEdP_T  : Partial derivative of dielectric constant with respect to pressure at constant temperature
       dEdT_P  : Partial derivative of dielectric constant with respect to temperature at constant pressure
    Usage
    ----------
       [E, rhohat, Ah, Bh, bdot, Adhh, Adhv, Bdhh, Bdhv, dEdP_T, dEdT_P] = dielec_FE97( TC, P)
    References
    ----------
        (1) Release on the Static Dielectric Constant of Ordinary Water Substance for
            Temperatures from 238 K to 873 K and Pressures up to 1000 MPa" (IAPWS R8-97, 1997).
        (2) Fernandez D. P., Goodwin A. R. H., Lemmon E. W., Levelt Sengers J. M. H.,
            and Williams R. C. (1997) A Formulation for the Permittivity of Water and
            Steam at Temperatures from 238 K to 873 K at Pressures up to 1200 MPa, including
            Derivatives and Debye-Hückel Coefficients. J. Phys. Chem. Ref. Data 26, 1125-1166.
        (3) Helgeson H. C. and Kirkham D. H. (1974) Theoretical Prediction of the Thermodynamic
            Behavior of Aqueous Electrolytes at High Pressures and Temperatures: II.
            Debye-Huckel Parameters for Activity Coefficients and Relative Partial Molal Properties.
            Am. J. Sci. 274, 1199-1251.
    Notes
    ------
    Temperature and Pressure input limits:
        * -35 ≤ TC ≤ 600 and 0 ≤ P ≤ 12000
    """
    if type(P) == str:
        if P == 'T':
            P, _, _ = iapws95(T = TC)
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = TC   # Assign first input T to pressure in bar
            TC, _, _ = iapws95(P = P)
            TC = TC - 273.15 # convert from K to celcius
    if np.ndim(TC) == 0:
        T = np.array(TC).ravel()
        P = np.array(P).ravel()
    else:
        T = TC.ravel()
        P = P.ravel()
    TK = celsiusToKelvin(T)
    [rho, _, _, _, _, _, _] = iapws95(T = TC, P = P)

    [_, _, _, _, _, _,  _, _, _, ptx, ktx, avx] = iapws95(T = T, rho = rho)
    ptx = ptx*0.1  # convert to MPa units
    ktx = ktx/0.1  # convert to /MPa units

    Tc = IAPWS95_COEFFS['Tc'] # K
    # Pc = IAPWS95_COEFFS['Pc'] # MPa
    rhoc = IAPWS95_COEFFS['rhoc'] # kg/m3
    mwH2O = 18.01528/1000  # kg/mol
    R = IAPWS95_COEFFS['R']/1000* mwH2O # kJ mol-1 K-1
    Nh = np.array([0.978224486826, -0.957771379375e0, 0.237511794148e0, 0.714692244396e0,
          -0.298217036956, -0.108863472196e0, 0.949327488264e-01, -0.980469816509e-02,
        0.165167634970e-04, 0.937359795772e-04, -0.123179218720e-09, 0.196096504426e-02])
    ih = np.array([1, 1, 1, 2, 3, 3, 4, 5, 6, 7, 10])
    jh = np.array([0.25, 1.0, 2.5, 1.5, 1.5, 2.5, 2.0, 2.0, 5.0, 0.5, 10.0])
    Nh = Nh.reshape(-1, 1); ih = ih.reshape(-1, 1); jh = jh.reshape(-1, 1)

    alphammp = 1.636e-40    # Mean molecular polarizability C^2 J^-1 m^2
    xmudp = 6.138e-30       # Molecular dipole moment C.m
    e = 1.6021773310e-19    # elementary charge e is 1.602176634 x 10^-19 coulomb (C)
    kB = 1.380658e-23        # Boltzmann constant k is 1.380649 x 10^-23 J K^-1
    NA = 6.0221367e23       # Avogadro constant NA is 6.02214076 x 10^23 mol^-1

    #  expression for calculating the permittivity of free space, eps0.
    c = 2997924580.0 # the speed of light in classical vacuum  m/s
    mu0 = 4.0e-07 * np.pi # the permeability of free space N Angstrom^-2

    rhom = rho/mwH2O     # molar density mol/m3
    rhocrm = rhoc/mwH2O  # critical molar density mol/m3

    # Get the permittivity of free space (eps0). eps0 = 1/(mu0*c^2)
    eps0 = 1/( mu0 * (c * 0.1)**2) # C^2 J^-1 m^-1
    delta = rhom/rhocrm
    tau = Tc/TK

    # Get the Harris and Alder g factor (g).
    g = 1 + np.sum(Nh[:-1]*(delta**ih)*(tau**jh), 0) + Nh[-1]*delta*((TK/228) - 1)**(-1.2)
    # Get A and B.
    A = ( NA*(xmudp**2)*rhom*g )/( eps0*kB*TK )
    B = ( NA*alphammp*rhom )/( 3*eps0 )
    # Get the dielectric constant (E).
    E = ( 1 + A + 5*B + np.sqrt(9 + 2*A + 18*B + A**2 + 10*A*B + 9*B**2) )/( 4 - 4*B )

    # Calculates the set of Debye-Huckel coefficients, derivatives and bdot
    # at given temperature T and pressure P
    # Debye-Huckel Agammae, Aphi and Agamma10 parameters, Units are kg^0.5 mol^-0.5
    Agammae = np.sqrt(2*np.pi*NA*rho)*((e**2/( 4*np.pi*E*eps0*kB*TK ))**1.5)
    Agamma10 = Agammae/np.log(10)
    Ah = Agamma10
    Aphi = Agammae/3

    # Debye-Huckel B(gamma) Bh parameter, Units are kg^1/2 mol^-1/2 Angstrom^-1
    kB_erg = kB * 1e7  # converts Boltzmann's constant from J K^-1 to erg K^-1.
    rho = rho/1000     # converts rho from kg m^-3 to g cm^-3
    rhohat = rho

    Bh = 1.0e-08*np.sqrt((8*np.pi*NA*rho*((c*e)**2))/(1000*E*kB_erg*TK))

    # Calculates bdot  Ref: Helgeson H.C.,1969, American Journal of Science, Vol.267, pp:729-804
    b = [0.0374e0, 1.3569e-4, 2.6411e-7, -4.6103e-9]
    bdot = np.where(T>=300, 0, b[0] + b[1]*(T-25.0) + b[2]*(T-25.0)**2 + b[3]*(T-25.0)**3)

    # Partial derivative of the g factor with respect to density at constant temperature.
    dgdr_T = np.sum((Nh[:-1]*ih/rhocrm)*(delta**(ih - 1))*(tau**jh), 0) + \
        (Nh[11]/rhocrm)*((TK/228) - 1)**(-1.2)

    # Partial derivative of the g factor with respect to temperature at constant density.
    dgdT_r = np.sum(Nh[:-1]*(delta**ih)*jh*(tau**(jh - 1))*(-tau/TK), 0) + \
        Nh[11]*delta*(-1.2*((TK/228) - 1)**(-2.2) )/228

    # Partial derivative of A with respect to density.
    A1 = (A/rhom) + (A/g)*dgdr_T
    # Partial derivative of A with respect to temperature.
    A2 = -(A/TK) + (A/g)*dgdT_r
    # Partial derivative of B with respect to density.
    B1 = B/rhom
    C = 9 + 2*A + 18*B + A**2 + 10*A*B + 9*B**2

    # Partial derivative of E with respect to density at constant temperature.
    dEdr_T = (4*B1*E/(4 - 4*B)) + (A1 + 5*B1 + 0.5*C**(-0.5)*\
                                   (2*A1 + 18*B1 + 2*A*A1 + 10*(A1*B + A*B1) + 18*B*B1))/(4 - 4*B)

    # Partial derivative of E with respect to temperature at constant density.
    dEdT_r = (A2 + 0.5*C**(-0.5)*A2*(2 + 2*A + 10*B))/(4 - 4*B)

    # Partial derivative of density with respect to Pressure at constant temperature
    # using the compressibility (ktx)
    drdP_T = rhom*ktx

    # Partial derivative of E with respect to Pressure at constant temperature
    dEdP_T = dEdr_T*drdP_T

    # Partial derivative of Pressure with respect to temperature at constant density
    #  using "ptx" which is the partial derivative of Pressure with respect to tau
    dPdT_r = -tau*ptx/TK

    # Partial derivative of E with respect to temperature at constant Pressure
    dEdT_P = dEdT_r - dEdr_T*dPdT_r*drdP_T

    # Debye-Huckel AV constant. Units are cm^3 kg^1/2 mol^-3/2
    # Multiply RT in kJ mol^-1 by 1000 to get R in cm^3 MPa mol^-1
    Adhv = 2*Aphi*1000*R*TK*( (3*dEdP_T/E) - ktx )

    # Debye-Huckel AH constant. Units are kJ kg^1/2 mol^-3/2.
    # AH/RT are kg^1/2 mol^-1/2
    Ahrt = -6*Aphi*TK*((dEdT_P/E) + (1/TK) + (avx/3))
    Adhh = R*TK*Ahrt
    # convert Adhh from kJ kg^1/2 mol^-3/2 to kcal kg^1/2 mol^-3/2
    Adhh = Adhh/J_to_cal

    # Get the Debye-Huckel BV constant. Units are cm^3 kg^1/2 mol^-3/2 Angstrom^-1
    # Multiply RT in kJ mol^-1 by 1000 to get R in cm^3 MPa mol^-1
    Bdhv = 2*np.log(10)*1000*R*TK*(Bh/2)*( ktx - dEdP_T/E )
    # convert Bdhv from cm^3 kg^1/2 mol^-3/2 Angstrom^-1 to cm^2 kg^1/2 mol^-3/2 10^-6
    Bdhv = 100*Bdhv

    # Get the Debye-Huckel BH constant. Units are kJ kg^1/2 mol^-3/2 Angstrom^-1
    # BH/RT parameter is expressed below as kg^1/2 mol^-1/2 Angstrom^-1.
    Bhrt = -2*np.log(10)*TK*(Bh/2)*((dEdT_P/E) + (1/TK) + avx )
    Bdhh = R*TK*Bhrt   #
    # convert Bdhh from kJ kg^1/2 mol^-3/2 Angstrom^-1 to cal kg^1/2 mol^-3/2 cm^-1  10^-9
    Bdhh = Bdhh*1000/J_to_cal*0.10

    return E, rhohat, Ah, Bh, bdot, Adhh, Adhv, Bdhh, Bdhv, dEdP_T, dEdT_P

def dielec_JN91(TC, P):
    """
    This watercalc implementation employs the JN91 formulation embedded in SUPCRT92 to calculate the
    dielectric properties of water and steam, the Debye-Huckel "A" parameters and
    Debye-Huckel "B" parameters  and their derivatives
    References
    ----------
        (1) Johnson JW, Norton D (1991) Critical phenomena in hydrothermal systems: State, thermodynamic,
            electrostatic, and transport properties of H2O in the critical region. American Journal of Science 291:541-648
        (2) Helgeson H. C. and Kirkham D. H. (1974) Theoretical Prediction of the Thermodynamic
            Behavior of Aqueous Electrolytes at High Pressures and Temperatures: II.
            Debye-Huckel Parameters for Activity Coefficients and Relative Partial Molal Properties.
            Am. J. Sci. 274, 1199-1251.
    Parameters
    ----------
       TC      : temperature [°C]
       P       : pressure [bar]
     Returns
    ----------
       E       : density
       rhohat  : density [g/cm³]
       Ah      : Debye-Huckel "A" parameters [kg^1/2 mol^-1/2]
       Bh      : Debye-Huckel "B" parameters [kg^1/2 mol^-1/2 Angstrom^-1]
       bdot    : bdot at any given temperature T
       Adhh    : Debye-Huckel "A" parameters associated with apparent molar enthalpy
       Adhv    : Debye-Huckel "A" parameters associated with apparent molar volume
       Bdhh    : Debye-Huckel "B" parameters associated with apparent molar enthalpy
       Bdhv    : Debye-Huckel "B" parameters associated with apparent molar volume
       dEdP_T  : Partial derivative of dielectric constant with respect to pressure at constant temperature
       dEdT_P  : Partial derivative of dielectric constant with respect to temperature at constant pressure
     Usage
    ----------
       [E, rhohat, Ah, Bh, bdot, Adhh, Adhv, Bdhh, Bdhv, dEdP_T, dEdT_P] = dielec_JN91( TC, P)
    Notes
    ------
    Temperature and Pressure input limits:
        * 0 ≤ TC ≤ 1000 and 0 ≤ P ≤ 5000
    """
    if type(P) == str:
        if P == 'T':
            P, _, _ = iapws95(T = TC)
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = TC   # Assign first input T to pressure in bar
            TC, _, _ = iapws95(P = P)
            TC = TC - 273.15 # convert from K to celcius

    if (np.ndim(TC) == 0) | (type(P) != str and np.ndim(P) == 0):
        TC = np.array(TC).ravel()
        P = np.array(P).ravel()
    else:
        TC = np.ravel(TC)
        P = np.ravel(P)
    if np.size(TC) < np.size(P):
        TC = TC*np.ones_like(P)
    if np.size(P) < np.size(TC):
        P = P*np.ones_like(TC)

    TK = celsiusToKelvin(TC)
    Tr = 298.15 #K
    # Str = 15.132                        #S at the triple point
    # Gtr = -56290                        #G at the triple point
    mwH2O = 18.01528/1000  # kg/mol
    R = IAPWS95_COEFFS['R']/1000* mwH2O # kJ mol-1 K-1
    # Ttr = 273.16                        #T at triple point
    [rho, dGH2O, dHH2O, SH2O, _, _, _] = iapws95(T = TC, P = P)

    # S = SH2O + Str #Calculate S at T P
    # TS = TK*S - Ttr*Str
    # delGf = dGH2O - TS + Gtr  #Calculate G at TP
    # Convert density to dimensionless g/cm3
    rhohat = rho/1000  #g/cm^3
    #  Dielectric constant of water calculation.  Johnson et al 1992- SUPCRT92
    ae = [0.1470333593*100, 0.2128462733*1000, -0.1154445173*1000,
          0.1955210915*100, -0.8330347980*100, 0.3213240048*100,
          -0.6694098645*10, -0.3786202045*100, 0.6887359646*100,
          -0.2729401652*100]
    # Dielectric constants from UEMATSU and FRANCK
    # ae = [7.62571, 2.44003E2, -1.40569E2, 2.77841E1, -9.62805E1,
    #       4.17909E1, -1.02099E1, -4.52059E1, 8.46395E1, -3.58644]

    That = TK/Tr
    k = [0]*5
    k[0] = [1]*len(That)
    k[1] = ae[0]/That
    k[2] = ae[1]/That + ae[2] + ae[3]*That
    k[3] = ae[4]/That + ae[5]*That + ae[6]*That**2
    k[4] = ae[7]*That**(-2) + ae[8]/That + ae[9]
    # E = ((k[0]*rhohat**0)+(k[1]*rhohat**1)+(k[2]*rhohat**2)+(k[3]*rhohat**3)+(k[4]*rhohat**4))
    E = np.zeros(np.size(TK))
    for a in range(len(E)):
        for b in range(5):
            E[a] = E[a] + k[b][a]*rhohat[a]**b


    Ar, Br = 1.8248292380e6, 50.291586490
    Ah = Ar*np.sqrt(rhohat)/(E*TK*np.sqrt(E*TK))
    Bh = Br*np.sqrt(rhohat)/np.sqrt(E*TK)
    b = [0.0374e0, 1.3569e-4, 2.6411e-7, -4.6103e-9]
    bdot = np.where(TC>=300, 0, b[0] + b[1]*(TC-25.0) + b[2]*(TC-25.0)**2 + b[3]*(TC-25.0)**3)

    [_, _, _, _, _, _,  _, _, _, ptx, ktx, avx] = iapws95(T = TC, rho = rho)
    ptx = ptx*0.1  # convert to MPa units
    ktx = ktx/0.1  # convert to /MPa units
    Aphi = Ah*np.log(10)/3

    dkdT = [0]*5
    dkdT[0] = [0]*len(That)
    dkdT[1] = -ae[0]*Tr/TK**2
    dkdT[2] = -ae[1]*Tr/TK**2 + ae[3]/Tr
    dkdT[3] = -ae[4]*Tr/TK**2 + ae[5]/Tr + 2*ae[6]*TK/Tr**2
    dkdT[4] = -2*ae[7]*Tr**2/TK**3 - ae[8]*Tr/TK**2

    # Partial derivative of E with respect to Pressure at constant temperature
    dEdP_T = np.zeros(np.size(TK))
    for a in range(len(E)):
        for b in range(5):
            dEdP_T[a] = dEdP_T[a] + b*k[b][a]*rhohat[a]**b
    dEdP_T = dEdP_T*ktx

    # Partial derivative of E with respect to temperature at constant Pressure
    dEdT_P = np.zeros(np.size(TK))
    for a in range(len(E)):
        for b in range(5):
            dEdT_P[a] = dEdT_P[a] + rhohat[a]**b*(dkdT[b][a] - b*avx[a]*k[b][a])

    # Debye-Huckel AV constant. Units are cm^3 kg^1/2 mol^-3/2
    # Multiply RT in kJ mol^-1 by 1000 to get R in cm^3 MPa mol^-1
    Adhv = 2*Aphi*1000*R*TK*( (3*dEdP_T/E) - ktx )

    # Debye-Huckel AH constant. Units are kJ kg^1/2 mol^-3/2.
    # AH/RT are kg^1/2 mol^-1/2
    Ahrt = -6*Aphi*TK*((dEdT_P/E) + (1/TK) + (avx/3))
    Adhh = R*TK*Ahrt
    # convert Adhh from kJ kg^1/2 mol^-3/2 to kcal kg^1/2 mol^-3/2
    Adhh = Adhh/J_to_cal

    # Get the Debye-Huckel BV constant. Units are cm^3 kg^1/2 mol^-3/2 Angstrom^-1
    # Multiply RT in kJ mol^-1 by 1000 to get R in cm^3 MPa mol^-1
    Bdhv = 2*np.log(10)*1000*R*TK*(Bh/2)*( ktx - dEdP_T/E )
    # convert Bdhv from cm^3 kg^1/2 mol^-3/2 Angstrom^-1 to cm^2 kg^1/2 mol^-3/2 10^-6
    Bdhv = 100*Bdhv

    # Get the Debye-Huckel BH constant. Units are kJ kg^1/2 mol^-3/2 Angstrom^-1
    # BH/RT parameter is expressed below as kg^1/2 mol^-1/2 Angstrom^-1.
    Bhrt = -2*np.log(10)*TK*(Bh/2)*((dEdT_P/E) + (1/TK) + avx )
    Bdhh = R*TK*Bhrt   #
    # convert Bdhh from kJ kg^1/2 mol^-3/2 Angstrom^-1 to cal kg^1/2 mol^-3/2 cm^-1  10^-9
    Bdhh = Bdhh*1000/J_to_cal*0.10

    return E, rhohat, Ah, Bh, bdot, Adhh, Adhv, Bdhh, Bdhv, dEdP_T, dEdT_P

def drummondgamma(TK, I):
    """
    This function models solubility of CO2 gas in brine using Drummond equation
    Parameters
    ----------
       TK            Temperature [K]
       I             ionic strength
     Returns
    ----------
       log10_gamma   co2 aqueous activities in log10
     Usage
    ----------
       log10_gamma = drummondgamma( TK, I)
    """

    # A=20.244
    # B=-0.016323
    C=-1.0312
    # D=-3629.7
    E=0.4445
    F=0.0012806
    G=255.9
    H=-0.001606

    #gamma=(((C + F*TK + (G/TK))*I - (E + H*TK)*(I / (I + 1)))/np.log(10))
    log10_gamma = np.log10(np.exp((C + F*TK + (G/TK))*I - (E + H*TK)*(I / (I + 1))))

    return log10_gamma

def Henry_duan_sun(TK, P, I):
    """
    This function evaluates the solubility of CO2 gase in brine, mainly Duan_Sun
    Ref: Solubility of CO2 in NaCl solutions in mol/kg water Duan & Sun
            Chemical Geology 193 (2003) 257-271
    Parameters
    ----------
       TK       :   Temperature [K]
       P        :   pressure [bar]
       I        :   ionic strength
     Returns
    ----------
       log10_co2_gamma   co2 aqueous activities in log10
       mco2             co2 aqueous molalities
     Usage
    ----------
       log10_co2_gamma, mco2 = Henry_duan_sun( TK, P, I)
    """
    if (np.ndim(TK) == 0):
        TK = np.array(TK).ravel()
    if (np.ndim(P) == 0):
        P = np.array(P).ravel()
    if (np.ndim(I) == 0):
        I = np.array(I).ravel()
    if np.size(TK) < np.size(P):
        TK = TK*np.ones_like(P)
    if np.size(P) < np.size(TK):
        P = P*np.ones_like(TK)

    # calculate CO2 fugacity and partial pressure
    vco2fugacity = np.vectorize(co2fugacity)
    [fCO2, denCO2, SI, pCO2] = vco2fugacity(TK, P)

    # Equation B1 of Duan & Sun model to calculate vapor pressure of water
    c1 = -38.640844
    c2 = 5.894842
    c3 = 59.876516
    c4 = 26.654627
    c5 = 10.637097
    Pc = 220.85     ## bar
    Tc = 647.29     ## K
    t = (TK - Tc) / Tc
    PH2O = Pc * TK/Tc*(1 + c1*(-t)**1.9 + c2*t + c3*t**2 + c4*t**3 + c5*t**4)
    yCO2 = (P - PH2O) / P ## gas phase molar fraction of CO2

    mNaCl = 2 * I/ ((1)**2 + (-1)**2)
    par_mu = np.array([28.9447706, -0.0354581768, -4770.67077, 0.0000102782768,
                       33.8126098, 0.0090403714, -0.00114934031, -0.307405726,
                        -0.0907301486, 0.000932713393, 0])

    par_lambda = np.array([-0.411370585, 0.000607632013, 97.5347708, 0, 0, 0, 0,
                           -0.0237622469, 0.0170656236, 0, 0.0000141335834])

    par_xi = np.array([0.000336389723, -0.000019829898, 0, 0, 0, 0, 0,
                       0.0021222083, -0.00524873303, 0, 0])

    fTP = [1*np.ones([1, len(TK)]), TK, 1/TK, TK**2, 1/(630-TK), P, P*np.log(TK),
           P/TK, P/(630-TK), (P/(630-TK))**2, TK*np.log(P)]
    fTP = np.vstack(fTP)

    muCO2 = np.sum(par_mu.reshape(-1,1) * fTP, 0)      # mu0/RT
    lambdaCO2Na = np.sum(par_lambda.reshape(-1,1) * fTP, 0)    # lambda_CO2-Na Pitzer 2nd order int. param.
    xiCO2NaCl = np.sum(par_xi.reshape(-1,1) * fTP, 0)  # zeta_CO2-Na-Cl Pitzer 3rd order int. param.

    # activity coef. aqueous co2
    # Honoring limits for Duan and Sun - P-T-X range (0– 2000 bar, 0–260°C, 0–4.3 m NaCl)
    lngamco2 = np.zeros([len(I), len(TK)]); mco2 = np.zeros([len(I), len(TK)])
    for j in range(len(I)):
        for i in range(len(TK)):
            if (TK[i] <= celsiusToKelvin(260)) and (I[j] <= 4.3) and (P[i] <= 2000):
                lngamco2[j, i] = 2*lambdaCO2Na[i]*mNaCl[j] +  xiCO2NaCl[i]*mNaCl[j]**2
                mco2[j, i] = P[i] * yCO2[i]/np.exp(muCO2[i] - np.log(fCO2[i]/P[i]) + \
                                                   2*lambdaCO2Na[i]*mNaCl[j] +  xiCO2NaCl[i]*mNaCl[j]**2)
            else:
                lngamco2[j, i] = 0
                mco2[j, i] = 0

    log10_co2_gamma =  np.log10(np.exp(lngamco2))  #  ((lngamco2)/np.log(10))  #

    return log10_co2_gamma, mco2

def co2fugacity(TK, P):
    """
    This function computes the fugacity and density of CO2 by Duan and Sun 2003
    Also Calculate the Saturation Index SI and partial pressure of CO2(g) at any given T, P
    using the Duan equation of state. A Poynting correction factor is also applied.
    Parameters
    ----------
       TK          Temperature [K]
       P           pressure [bar]
     Returns
    ----------
       fCO2       co2 fugacity
       denCO2     co2 density [g/cm3]
       SI         co2 saturation index [bar]
       pCO2       co2 partial pressure [bar]
     Usage
    ----------
       [fCO2, denCO2, SI, pCO2] = co2fugacity( TK, P)
    """
    if np.ndim(TK) == 0:
        length = 1
    else:
        length = len(TK)
    R = 0.08314467  #bar*L/mol/K
    PcCO2 = 73.8    ## bar
    TcCO2 = 304.15  ## K
    VcCO2 = R * TcCO2 / PcCO2  # L/mol

    Pr = P / PcCO2 # bar
    Tr = TK / TcCO2
    xmwc = 4.40098e2  # g/mol

    a1  =  0.0899288497
    a2  = -0.494783127
    a3  =  0.0477922245
    a4  =  0.0103808883
    a5  = -0.0282516861
    a6  =  0.0949887563
    a7  =  0.00052060088
    a8  = -0.000293540971
    a9  = -0.00177265112
    a10 = -0.0000251101973
    a11 =  0.0000893353441
    a12 =  0.0000788998563
    a13 = -0.0166727022
    a14 =  1.398
    a15 =  0.0296

    # Equation A1
    zfun = lambda Z : -Z + (1 + (a1 + a2 / Tr ** 2 + a3 / Tr ** 3) / Z / Tr * Pr +
                            (a4 + a5 / Tr ** 2 + a6 / Tr ** 3) / Z ** 2 / Tr ** 2 * Pr ** 2 +
                            (a7 + a8 / Tr ** 2 + a9 / Tr ** 3) / Z ** 4 / Tr ** 4 * Pr ** 4 +
                            (a10 + a11 / Tr ** 2 + a12 / Tr ** 3) / Z ** 5 / Tr ** 5 * Pr**5 +
                            a13 / Tr ** 3 / Z ** 2 / Tr ** 2 * Pr ** 2 * (a14 + a15 /
                                                                            Z ** 2 / Tr ** 2 * Pr ** 2)
                            * np.exp(-a15 / Z ** 2 / Tr ** 2 * Pr ** 2))

    Z = fsolve(zfun, [1]*length, xtol=1.0e-10)
    Vr = Z * Tr / Pr
    V = Vr * VcCO2  ## L / mol

    phi = np.exp(Z - 1 - np.log(Z) + (a1 + a2 / (Tr ** 2) + a3 / (Tr ** 3)) / Vr + \
                 (a4 + a5 / (Tr ** 2) + a6 / (Tr ** 3)) / (2 * Vr ** 2) + \
                     ((a7 + a8 / (Tr ** 2) + a9 / (Tr ** 3)) / (4 * Vr ** 4) + \
                      (a10 + a11 / (Tr ** 2) + a12 / (Tr ** 3)) / (5 * Vr ** 5) + \
                          a13 / (2 * Tr ** 3 * a15) * (a14 + 1 - (a14 + 1 + a15 / Vr ** 2) * \
                                                           np.exp(-a15 / Vr ** 2))))
    #-----fugacity
    fCO2 = phi * P  #bar

    #-----density
    denCO2 = xmwc*1e-3 / V # g/cm^3
    Poy = 1
    Patm = P / 1.01325
    poy = True
    if (poy):
        R =   8.205746E-2 ## L atm /K/mol
        Vm = np.where(V < 0, 32.0e-3, V)   ## L / mol
        Poy = np.exp(-(Patm - 1)*Vm/R/TK)
    pCO2 = phi*Patm*Poy  # atm
    SI = np.log10(pCO2)
    pCO2 = pCO2 * 1.01325  # bar

    return fCO2, denCO2, SI, pCO2

def gamma_correlation(TC, P, method = None):
    """
    This function calculates the CO2 activity correlation coefficients at
    given temperature T and pressure P
    Ref:
        (1) Segal Edward Drummond, 1981, Boiling and Mixing of Hydrothermal
            Fluids: Chemical Effects on Mineral Precipitation, page 19
        (2) Wolery, T. J., Lawrence Livermore National Laboratory, United States Dept.
            of Energy, 1992. EQ3/6: A software package for geochemical modeling of
            aqueous systems: package overview and  installation guide (version 7.0)
    Parameters
    ----------
       TC      :    Temperature [°C]
       P       :    pressure [bar]
       method  :    activity model [Duan_Sun or Drummond]
     Returns
    ----------
       cco2    :   co2 correlation coefficients
     Usage
    ----------
       [cco2] = gamma_correlation( TC, P)
    """

    if np.ndim(TC) == 0:
        TC = np.array(TC).ravel()
    if np.ndim(P) == 0:
        P = np.array(P).ravel()

    TK = celsiusToKelvin(TC)
    #   assign ionic strength from 0 to 3
    N = 100
    I = np.zeros([N, 1])
    for i in range(N):
        I[i] = 3 * i /(N - 1)


    A = np.zeros([3, 3])
    B = np.zeros([3, 1])
    cco2 = np.zeros([4, len(TK)])
    if method is not None:
        if method == 'Duan_Sun':
            ccoef = Henry_duan_sun(TK, P, I)[0]
        elif method == 'Drummond':
            ccoef = drummondgamma(TK, I)
    else:
        ccoef = drummondgamma(TK, I)

    for i in range(len(TK)):
        for ii in range(3):
            for jj in range(3):
                A[ii, jj] = np.sum(I**((ii+1) + (jj+1)))
                B[ii] = np.sum(ccoef[:, i]*I.ravel()**(ii+1))
        Coef = lu_solve(lu_factor(A), B)
        cco2[:3, i] = Coef[0], Coef[1], Coef[2]

    return cco2

def Helgeson_activity(TC, P, I, Dielec_method = None, **rhoEDB):
    """
    This function calculates the solute activity coefficient, solvent osmotic coefficient,
    and solvent  activity at given temperature and pressure using equations 298, 190 and 106 in
    Helgeson, Kirkham and Flowers, 1981, A.J.S. p.1249-1516
    Parameters:
    ----------
       TC       :   Temperature [°C]
       P        :   pressure [bar]
       I        :   ionic strength
       Dielec_method :   specify either 'FE97' or 'JN91' as the method to calculate dielectric
                       constant (optional), if not specified default - 'JN91'
       rhoEDB   :   dictionary of water properties like density (rho), dielectric factor (E) and
                       Debye–Hückel coefficients (optional)
    Returns:
    ----------
       aw       :   solvent activity
       phi      :   solvent osmotic coefficient
       mean_act :   solute activity coefficient
    Usage
    -------
       [aw, phi, mean_act] = Helgeson_activity( TC, P, I)
    """

    if np.ndim(TC) == 0:
        TC = np.array(TC).ravel()
    if np.ndim(P) == 0:
        P = np.array(P).ravel()
    if np.ndim(I) == 0 :
        I = np.array(I).ravel()
    if np.size(TC) < np.size(P):
        TC = TC*np.ones_like(P)
    if np.size(P) < np.size(TC):
        P = P*np.ones_like(TC)
    I = I.reshape(-1,1)

    if rhoEDB.__len__() != 0:
        rho = rhoEDB['rho'].ravel()
        Ah = rhoEDB['Ah'].ravel()
        Bh = rhoEDB['Bh'].ravel()
    else:
        [rho, _, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                [Ah, Bh] = dielec_FE97(TC, P)[2:4]
            elif Dielec_method == 'JN91':
                [Ah, Bh] = dielec_JN91(TC, P)[2:4]
        else:
            # default method: 'JN91'
            [Ah, Bh] = dielec_JN91(TC, P)[2:4]

    z = [-1.0, 1.0]
    mwH2O = 18.01528/1000  # kg/mol
    R = 1.9872041  # cal/mol/K

    mstar = 2*I         # total solute in solution
    mtj = I             # total concentration of ion j
    mchr= 2*I           # total solute excluding neutral species
    loggamma = [0]*2; summt = [0]*2
    rej = [1.810, 1.910]    # Rej of Cl- and Na+

    bijl = np.zeros(np.size(TC)); bi = np.zeros(np.size(TC))
    # bil and bihat correlation
    for k in range(len(TC)):
        if TC[k] < 350:
            Psat = iapws95(T = TC[k], P = 'T')[-2]
        else:
            Psat = [0]
        if P[k] in Psat:  #%% P = Psat Region
            x = np.array([ 25.,  50.,  75., 100., 125., 150., 175., 200., 225.,
                          250., 275., 300., 325.])
            y = np.array([ 2.47,  2.15,  1.79,  1.39,  0.93,  0.41, -0.18, -0.85, -1.64,
                          -2.57, -3.71, -5.21, -7.32])
            fun = splrep(x, y)
            bhat = splev(TC[k], fun)
            y = np.array([-9.77, -5.59, -2.43,  0.23,  2.44,  4.51,  6.38,  8.11,  9.73, 11.29,
                          11.71, 14.15, 15.49])
            fun = Rbf(x, y)
            bil = fun(TC[k])
        elif P[k] not in Psat and P[k] < 1000:
            isoline = P[k]
            x = np.array([ 25.,  50.,  75., 100., 125., 150., 175., 200., 225., 250., 275., 300., 325.])
            y = np.array([ 2.47,  2.15,  1.79,  1.39,  0.93,  0.41, -0.18,
                          -0.85, -1.64, -2.57, -3.71, -5.21, -7.32])
            fun = splrep(x, y)
            xpred = np.linspace(0, 500, 100)
            ypred1 = splev(xpred, fun)
            ypred1 = np.where(ypred1 < -17.12, -17.12, ypred1)
            x = np.arange(25,525,25)
            y = np.array([  2.58,   2.28,   1.95,   1.58,   1.18,   0.73,   0.25,   0.28,
                          -0.86,  -1.5 ,  -2.2 ,  -2.99,  -3.88,  -4.91,  -6.11,  -7.56,
                          -9.31, -11.43, -14.01, -17.12])
            fun = splrep(x, y)
            ypred2 = splev(xpred, fun)
            newy = (1000-isoline)/1000*ypred1 + (isoline/1000)*ypred2
            fun = splrep(xpred, newy)
            bhat = splev(TC[k], fun)
            x = np.array([ 25.,  50.,  75., 100., 125., 150., 175., 200., 225., 250., 275., 300., 325.])
            y = np.array([-9.77, -5.59, -2.43,  0.23,  2.44,  4.51,  6.38,  8.11,  9.73, 11.29,
                          11.71, 14.15, 15.49])
            fun = Rbf(x, y)
            xpred = np.linspace(0, 500, 100)
            ypred1 = fun(xpred)
            ypred1 = np.where(ypred1 > 24.03, 24.03, ypred1)
            x = np.arange(25,525,25)
            y = np.array([ -9.19, -5.07, -2.63, -0.07,  1.71,  4.13,  5.91,  7.48,  9.27,
                          10.8 , 12.24, 13.68, 15.16, 16.48, 17.8 , 19.12, 20.33, 21.55,
                          22.86, 24.03])
            fun = splrep(x, y)
            ypred2 = splev(xpred, fun)
            newy = (1000-isoline)/1000*ypred1 + (isoline/1000)*ypred2
            fun = splrep(xpred, newy)
            bil = splev(TC[k], fun)
        elif P[k] >= 1000 and P[k] <= 2000:
            isoline = P[k] - 1000
            x = np.arange(25,525,25)
            y = np.array([  2.58,   2.28,   1.95,   1.58,   1.18,   0.73,   0.25,   0.28,
                          -0.86,  -1.5 ,  -2.2 ,  -2.99,  -3.88,  -4.91,  -6.11,  -7.56,
                          -9.31, -11.43, -14.01, -17.12])
            fun = splrep(x, y)
            xpred = np.linspace(0, 500, 100)
            ypred1 = splev(xpred, fun)
            y = np.array([  2.66,  2.37,  2.06,  1.72,  1.35,  0.95,  0.51,  0.04, -0.46,
                          -1.  , -1.57, -2.19, -2.85, -3.58, -4.38, -5.26, -6.24, -7.34,
                          -8.56, -9.88])
            fun = splrep(x, y)
            ypred2 = splev(xpred, fun)
            newy = (1000-isoline)/1000*ypred1 + (isoline/1000)*ypred2
            fun = splrep(xpred, newy)
            bhat = splev(TC[k], fun)
            y = np.array([ -9.19, -5.07, -2.63, -0.07,  1.71,  4.13,  5.91,  7.48,  9.27,
                          10.8 , 12.24, 13.68, 15.16, 16.48, 17.8 , 19.12, 20.33, 21.55,
                          22.86, 24.03])
            fun = splrep(x, y)
            xpred = np.linspace(0, 500, 100)
            ypred1 = splev(xpred, fun)
            y = np.array([ -9.12, -5.52, -2.65, -0.1 ,  1.83,  4.12,  5.88,  7.66,  9.2 ,
                          10.73, 12.19, 13.66, 15.1 , 16.42, 17.79, 19.11, 20.35, 21.61,
                          22.84, 23.95])
            fun = splrep(x, y)
            ypred2 = splev(xpred, fun)
            newy = (1000-isoline)/1000*ypred1 + (isoline/1000)*ypred2
            fun = splrep(xpred, newy)
            bil = splev(TC[k], fun)
        elif P[k] == 3000: #%% P = 3000 Region
            x = np.arange(25,525,25)
            y = np.array([  2.72,  2.45,  2.15,  1.83,  1.48,  1.11,  0.72,  0.29, -0.15,
                          -0.63, -1.13, -1.67, -2.24, -2.86, -3.52, -4.24, -5.03, -5.87,
                          -6.78, -7.73])
            fun = splrep(x, y)
            bhat = splev(TC[k], fun)
            y = np.array([ -9.51, -5.6 , -2.46,  0.13,  2.14,  4.28,  6.16,  8.18,  9.59,
                          11.15, 12.62, 13.99, 15.41, 16.76, 18.1 , 19.4 , 20.66, 21.85,
                          23.09, 24.32])
            fun = splrep(x, y)
            bil = splev(TC[k], fun)
        elif P[k] == 4000: #%% P = 4000 Region
            x = np.arange(25,525,25)
            y = np.array([  2.77,  2.51,  2.22,  1.91,  1.58,  1.23,  0.86,  0.47,  0.05,
                          -0.38, -0.84, -1.33, -1.85, -2.4 , -2.99, -3.63, -4.32, -5.06,
                          -5.84, -6.63])
            fun = splrep(x, y)
            bhat = splev(TC, fun)
            y = np.array([-10.44,  -5.68,  -2.15,   0.71,   3.05,   5.13,   7.05,   9.05,
                          10.4 ,  11.92,  13.39,  14.83,  16.23,  17.52,  18.81,  20.13,
                          21.38,  22.56,  23.74,  24.94])
            fun = splrep(x, y)
            bil = splev(TC[k], fun)
        elif P[k] == 5000: #%% P = 5000 Region
            x = np.arange(25,525,25)
            y = np.array([  2.82,  2.56,  2.28,  1.99,  1.67,  1.33,  0.98,  0.6 ,  0.21,
                          -0.2 , -0.63, -1.09, -1.58, -2.1 , -2.65, -3.25, -3.89, -4.57,
                          -5.28, -6. ])
            fun = splrep(x, y)
            bhat = splev(TC[k], fun)
            y = np.array([-11.76,  -5.87,  -1.68,   1.39,   4.21,   6.26,   8.13,  10.3,
                          11.58,  13.15,  14.54,  16.01,  17.36,  18.65,  19.83,  21.23,
                          22.38,  23.64,  24.78,  25.95])
            fun = splrep(x, y)
            bil = splev(TC[k], fun)

        # bihat NaCl (b NaCl = bhat/(2.303RT)) from Table 29 (bi here) in kg/mol * 1e+3
        # b Na+Cl- from Table 30 (bil here) in kg/mol * 1e+2
        bihat = bhat*1e-3    # [kg/mol]
        bi[k] = bihat/(np.log(10)*R*celsiusToKelvin(TC[k]))  #[kg/cal]
        bijl[k] = bil*1e-2

    # activity and osmotic coefficients
    # for j in range(len(I)):
    for i in range(2):
        zabsi = np.abs(z[i])
        if i < 1: rex = 1.81
        else: rex = 1.91
        azero = 2*(rej[i] + zabsi*rex)/(zabsi + 1)
        omega = 1.66027e5*z[i]**2/rej[i]
        lambdaa = 1 + Bh*azero*I**0.5
        loggamma[i] = - Ah * z[i]**2 * I**0.5 / lambdaa - np.log10(1 + mwH2O * mstar) + \
            (omega*bi + bijl - 0.19*(np.abs(z[i]) - 1))*I

        summt[i] = mtj * ( (Ah*z[i]**2/((azero*Bh)**3*I)) *\
                          (lambdaa - 1/lambdaa - 2*np.log(lambdaa)) + \
                              (-np.log10(1 + mwH2O * mstar)/(mwH2O*mstar)) - \
                                  0.5*(omega*bi*I + (bijl - 0.19 *(np.abs(z[i]) - 1)) * \
                                       mchr*0.5) )

    mean_act = 10**((loggamma[0]+loggamma[1])/2)
    phi = -np.log(10)*(summt[0]+summt[1])/mstar
    phi = np.where(np.isnan(phi), 0, phi)

    aw = np.exp(-phi*mstar*mwH2O)
    return aw, phi, mean_act

def aw_correlation(TC, P, Dielec_method = None, **rhoEDB):
    """
    Calculates the water activity correlation coefficients at given temperature and pressure
    Parameters:
    ----------
       TC       :       Temperature [°C]
       P        :       pressure [bar]
       Dielec_method :  specify either 'FE97' or 'JN91' as the method to calculate dielectric
                           constant (optional), if not specified default - 'JN91'
       rhoEDB   :       dictionary of water properties like density (rho), dielectric factor (E) and
                           Debye–Hückel coefficients (optional)
    Returns:
    ----------
       ch20     :       water activity correlation coefficients
    Usage
    -------
       [ch20] = aw_correlation( TC, P)
    """

    if rhoEDB.__len__() != 0:
        rho = rhoEDB['rho'].ravel()
        E = rhoEDB['E'].ravel()
        Ah = rhoEDB['Ah'].ravel()
        Bh = rhoEDB['Bh'].ravel()
    else:
        [rho, _, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                [E, _, Ah, Bh] = dielec_FE97(TC, P)[:4]
            elif Dielec_method == 'JN91':
                [E, _, Ah, Bh] = dielec_JN91(TC, P)[:4]
        else:
            # default method: 'JN91'
            [E, _, Ah, Bh] = dielec_JN91(TC, P)[:4]
            Dielec_method = 'JN91'
        rhoEDB = {'rho': rho, 'E': E,  'Ah': Ah, 'Bh': Bh}

    if np.ndim(TC) == 0 | np.ndim(P) == 0:
        TC = np.array(TC).ravel()
        P = np.array(P).ravel()

    #   assign ionic strength from 0 to 3
    N = 100
    Is = np.linspace(0, 6, N).reshape(-1,1)
    mwH2O = 18.01528/1000  # kg/mol

    # %% Water Activity
    aw = Helgeson_activity(TC, P, Is, Dielec_method = Dielec_method, **rhoEDB)[0]
    ch20 = np.zeros([4, len(TC)])
    x0 = [1.454, 0.02236, 9.380e-3, -5.362e-4]
    for i in range(len(TC)):
        if np.sum(np.isnan(aw[:, i])) == 0:
            Is_0 = Is [1:]  # avoid zero values
            Ahi = Ah[i]
            ch20func = lambda Is_0, *x: (-2*Is_0*mwH2O * \
                                         (1 - (np.log(10)*Ahi/(x[0]**3*Is_0)) * \
                                          ((1 + x[0]*np.sqrt(Is_0)) - 2*np.log(1 + x[0]*np.sqrt(Is_0)) - \
                                           ( 1/(1 + x[0]*np.sqrt(Is_0)) ) ) + \
                    (x[1]*Is_0/2) + (2/3*x[2]*Is_0**2) + (3/4*x[3]*Is_0**3) ))

            ch20[:, i], pcov = curve_fit(ch20func, Is_0.ravel(), np.log(aw[1:, i]).ravel(),
                                         p0=x0,  maxfev = 1000000)
        else:
            ch20[:, i] = [500]*4
    return ch20

#%%----------------------------------------------------------------
def supcrtaq(TC, P, specieppt, Dielec_method = None, **rhoE):
    """
    This function evaluates the Gibbs free energy of aqueous species at T and P
    using the revised HKF equation of state. \n
    References
    ----------
        (1) Johnson JW, Oelkers EH, Helgeson HC. 1992. SUPCRT92: A software package for calculating
             the standard molal thermodynamic properties of minerals, gases, aqueous species, and
             reactions from 1 to 5000 bar and 0 to 1000°C. Computers & Geosciences 18(7): 899-947.
             doi: 10.1016/0098-3004(92)90029-Q \n
    Parameters
    ----------
       TC       : temperature [°C] \n
       P        : pressure [bar] \n
       specieppt: properties such as [dG [cal/mol], dH [cal/mol], S [cal/mol-K], V [cm3/mol]
                       a [cal/mol-K], b [*10**3 cal/mol/K**2], c [*10^-5 cal/mol/K] )
       Dielec_method : specify either 'FE97' or 'JN91' as the method to calculate dielectric
                   constant (optional), if not specified default - 'JN91'
       rhoE     : dictionary of water properties like density (rho) and dielectric factor (E)
                   (optional)
    Returns
    -------
       delG     : Gibbs energy [cal/mol]
    Usage
    -------
        The general usage of supcrtaq without the optional arguments is as follows: \n
        (1) Not on steam saturation curve: \n
            delG = supcrtaq(TC, P, specieppt) \n
            where T is temperature in celsius and P is pressure in bar
        (2) On steam saturation curve: \n
            delG = supcrtaq(TC, 'T', specieppt),  \n
            where T is temperature in celsius, followed with a quoted char 'T' \n
            delG = supcrtaq(P, 'P', specieppt),  \n
            where P is pressure in bar, followed with a quoted char 'P'.
        (3) Meanwhile, usage with any specific dielectric constant method ('FE97') for
            condition not on steam saturation curve is as follows. Default method is 'JN91' \n
            delG = supcrtaq(TC, P, specieppt, Dielec_method = 'FE97')
    """
    delGref=specieppt[2]
    # Href=specieppt[3]
    Sref=specieppt[4]
    a1 = specieppt[5]*1e-1
    a2 = specieppt[6]*1e2
    a3 = specieppt[7]
    a4 = specieppt[8]*1e4
    c1 = specieppt[9]
    c2 = specieppt[10]*1e4
    omegaref = specieppt[11]*1e5
    Z = specieppt[12] #Z is formal charge

    if rhoE.__len__() != 0:
        rho = rhoE['rho'].ravel()
        rhohat = rho/1000  #g/cm^3
        E = rhoE['E'].ravel()
    else:
        [rho, _, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                [E, rhohat] = dielec_FE97(TC, P)[:2]
            elif Dielec_method ==  'JN91':
                [E, rhohat] = dielec_JN91(TC, P)[:2]
        else:
            # default method: 'JN91'
            Dielec_method =  'JN91'
            [E, rhohat] = dielec_JN91(TC, P)[:2]

    if np.ndim(TC) == 0 | np.ndim(P) == 0:
        TC = np.array(TC).ravel()
        P = np.array(P).ravel()
        length = 1
    else:
        TC= TC.ravel()
        P = P.ravel()
        length = len(P)

    TK = celsiusToKelvin(TC)
    Tr = 298.15 #K

    g = np.zeros([length, 1]).ravel()
    theta = 228 #K
    psi = 2600 #bars
    Pr = 1 #bars
    # Yref = -5.802E-5 #K^-1
    # Eref = 78.2396
    if Dielec_method == 'FE97':
        Eref, Yref = 78.40836752, -5.836655257780387e-05
        # [Eref, _, _, _, _, _, _, _, _, dEdP_T, dEdT_P] = dielec_FE97(Tr - 273.15, Pr)  #(calculated at 298.15K and 1 bar)
        # Yref = dEdT_P/Eref**2
    elif Dielec_method ==  'JN91':
        Eref, Yref = 78.24385513, -5.795647242492297e-05
        # [Eref, _, _, _, _, _, _, _, _, dEdP_T, dEdT_P] = dielec_JN91(Tr - 273.15, Pr)  #(calculated at 298.15K and 1 bar)
        # Yref = dEdT_P/Eref**2

    n = 1.66027e5
    f1_T = TK*np.log(TK/Tr) - TK + Tr
    f2_T = (((1/(TK - theta)) - (1/(Tr - theta)))*((theta - TK)/(theta)) - \
            (TK/(theta**2))*np.log(Tr*(TK - theta)/(TK*(Tr - theta))))
    f1_P = (P - Pr)
    f2_P = np.log((psi + P)/(psi + Pr))
    f1_PT = ((P - Pr)/(TK - theta))
    f2_PT = (1/(TK - theta))*np.log((psi + P)/(psi + Pr))
    f3_PT = ((1/E) - (1/Eref) + Yref*(TK - Tr))

    if Z == 0:
        r = 0
        f4_PT = 0
    else:
        agi = [-2.037662,  5.747e-3, -6.557892e-6]
        bgi = [6.107361, -1.074377e-2, 1.268348e-5]

        for k in range(length):
            if rhohat[k]<1:
                ag = agi[0] + agi[1]*TC[k] + agi[2]*TC[k]**2
                bg = bgi[0] + bgi[1]*TC[k] + bgi[2]*TC[k]**2
                g[k] = ag*(1 - rhohat[k])**bg

                if ((TC[k]<155) | (P[k] > 1000) | (TC[k] >355)):
                    g[k] = g[k]
                else:
                    afi = [0.3666666e2, -1.504956e-10, 5.01799e-14]
                    Tg = (TC[k] - 155)/300
                    Pg = 1000 - P[k]
                    f_T = Tg**4.8 + afi[0]*Tg**16
                    f_P = afi[1]*Pg**3 + afi[2]*Pg**4

                    f_PT = f_T*f_P
                    g[k] = g[k] - f_PT
        r = Z**2*(omegaref/n + Z/3.082)**-1 + abs(Z)*g   # eqn 48
        omega = n*((Z**2/r) - (Z/(3.082 + g)))           # eqn 55
        f4_PT = (omega - omegaref)*((1/E) - 1)


    delG = delGref - Sref*(TK-Tr) - c1*f1_T - c2*f2_T + a1*f1_P + a2*f2_P + a3*f1_PT + \
        a4*f2_PT + omegaref*f3_PT + f4_PT               # eqn 59

    rhominCA = 0.35
    rhominNA = 0.05
    Pmax1 = 500
    # Pmax2 = 1000
    Tmin = 350
    Tmax = 400

    if Z != 0:
        xall = ((np.around(rhohat,3) < rhominCA) | (P < Pmax1) & (TC > Tmin) & (TC < Tmax))
    else:
        xall = (np.around(rhohat,3) < rhominNA)

    delG[xall] = np.nan

    return delG

def calclogKAnAb( XAn, TC, P, dbacessdic, Dielec_method = None, **rhoEG):
    """
    This function calculates thermodynamic properties of solid solution of Plagioclase minerals

    Parameters
    ----------
        XAn        : volume fraction of Anorthite  \n
        TC         : temperature [°C]  \n
        P          : pressure [bar]  \n
        dbacessdic : dictionary of species from direct-access database  \n
        Dielec_method   : specify either 'FE97' or 'JN91' as the method to calculate
                        dielectric constant (optional), if not specified default - 'JN91'
        rhoEG      : dictionary of water properties like  density (rho),
                       dielectric factor (E) and Gibbs Energy  (optional)
    Returns
    -------
        logKplag   : logarithmic K values   \n
        Rxn        : dictionary of reaction thermodynamic properties
    Usage
    -------
        The general usage of calclogKAnAb without the optional argument is as follows:  \n
        (1) Not on steam saturation curve:  \n
            [logK, Rxn] = calclogKAnAb(XAn, TC, P, dbacessdic),  \n
            where T is temperature in celsius and P is pressure in bar;
        (2) On steam saturation curve:  \n
            [logK, Rxn] = calclogKAnAb(XAn, TC, 'T', dbacessdic),   \n
            where T is temperature in celsius, followed with a quoted char 'T'  \n
            [logK, Rxn] = calclogKAnAb(XAn, P, 'P', dbacessdic), \n
            where P is pressure in bar, followed with a quoted char 'P'.
        (3) Meanwhile, usage with any specific dielectric constant method ('FE97') for
            condition not on steam saturation curve is as follows. Default method is 'JN91' \n
            [logK, Rxn] = calclogKAnAb(XAn, TC, P, dbacessdic, Dielec_method = 'FE97')
    """
    if type(P) == str:
        if P == 'T':
            P, _, _ = iapws95(T = TC)
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = TC   # Assign first input T to pressure in bar
            TC, _, _ = iapws95(P = P)
            TC = TC - 273.15 # convert from K to celcius

    if np.ndim(TC) == 0 :
        TC = np.array(TC).ravel()
    elif np.size(TC) == 2:
        TC = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(TC[0], TC[-1], 8)])

    if np.size(P) <= 2:
        P = np.ravel(P)
        P = P[0]*np.ones(np.size(TC))

    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                E = dielec_FE97(TC, P)[0]
            elif Dielec_method == 'JN91':
                E = dielec_JN91(TC, P)[0]
        else:
            # default method: 'JN91'
            E = dielec_JN91(TC, P)[0]
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    if Dielec_method is None:
        Dielec_method = 'JN91'

    TK=celsiusToKelvin(TC)
    Rxn = {}
    Rxn['type'] = 'plag'
    XAb = 1-XAn
    nH = (8-4*XAb)
    nAl = (2-XAb)
    nH2O = (4-2*XAb)
    nNa = XAb
    nCa = (1-XAb)
    nSi = (2+XAb)
    if XAn == 1:
        Rxn['name'] ='Anorthite'
        Rxn['formula'] ='CaAl2(SiO4)2'
    elif XAn == 0:
        Rxn['name'] ='Albite'
        Rxn['formula'] ='NaAlSi3O8'
    else:
        Rxn['name'] = 'An%d' % (XAn*100)
        Rxn['formula'] = 'Ca%1.2f' % nCa + 'Na%1.2f' % nNa + 'Al%1.2f' % nAl + 'Si%1.2f' % nSi + 'O8'

    Rxn['MW'] = nCa*MW['Ca'] + nNa* MW['Na'] + nAl*MW['Al'] + nSi*MW['Si'] + 8*MW['O']
    R = 1.9872041


    dGAl = supcrtaq(TC, P, dbacessdic['Al+++'], Dielec_method = Dielec_method, **rhoEG)
    dGSiO2aq  = supcrtaq(TC, P, dbacessdic['SiO2(aq)'], Dielec_method = Dielec_method, **rhoEG)

    dGH = supcrtaq(TC, P, dbacessdic['H+'], Dielec_method = Dielec_method, **rhoEG)
    dGNa = supcrtaq(TC, P, dbacessdic['Na+'], Dielec_method = Dielec_method, **rhoEG)
    dGCa = supcrtaq(TC, P, dbacessdic['Ca++'], Dielec_method = Dielec_method, **rhoEG)


    if (XAn < 1) & (XAn != 0):
        Smix=-R*(XAb*np.log((XAb*(4-XAb**2)*(2+XAb)**2)/27) + \
                 XAn*np.log((XAn*(1+XAn)**2*(3-XAn)**2)/16))
    else:
        Smix=0

    dGplag = XAb*dbacessdic['ss_Albite_high'][2] + XAn*dbacessdic['ss_Anorthite'][2] + R*298.15*Smix
    Splag = XAb*dbacessdic['ss_Albite_high'][4] + XAn*dbacessdic['ss_Anorthite'][4] - R*Smix
    Vplag = XAb*dbacessdic['ss_Albite_high'][5] + XAn*dbacessdic['ss_Anorthite'][5]
    Cpplag = [a + b for a, b in zip([XAb*x for x in dbacessdic['ss_Albite_high'][6:11] ],
                                    [XAn*y for y in dbacessdic['ss_Anorthite'][6:11] ])]
    Rxn['min'] = [dGplag, np.nan, Splag, Vplag] + Cpplag
    Rxn['min'].insert(0, Rxn['formula'])
    Rxn['min'].insert(1,' R&H95, Stef2001')
    plag = Rxn['min']
    dGplagTP, _ = heatcapusgscal(TC, P, plag)
    coeff = [-nH, nAl, nNa, nCa, nSi, nH2O]
    spec = ['H+', 'Al+++', 'Na+', 'Ca++', 'SiO2(aq)', 'H2O']
    Rxn['spec'] = [x for x, y in zip(spec, coeff) if y!=0]
    Rxn['coeff'] = [y for y in coeff if y!=0]
    Rxn['nSpec']=len(Rxn['coeff'])

    dGrxn = -dGplagTP - nH*dGH + nAl*dGAl + nH2O*dGH2O + nNa*dGNa+ nCa*dGCa + nSi*dGSiO2aq
    logKplag = (-dGrxn/R/(TK)/np.log(10))   #np.log10(np.exp(-dGrxn/R/(TK)))

    Rxn['V'] = Rxn['min'][5]  #,'%8.3f')
    Rxn['source'] = ' R&H95, Stef2001'
    elements  = ['%.4f' % nCa, 'Ca', '%.4f' % nNa, 'Na', '%.4f' % nAl, 'Al', '%.4f' % nSi, 'Si', '8.0000', 'O']
    filters = [y for x, y in enumerate(elements) if x%2 == 0 and float(y) == 0]
    filters = [[x, x+1] for x, y in enumerate(elements) if y in filters]
    filters = [num for elem in filters for num in elem]
    Rxn['elements'] = [y for x, y in enumerate(elements) if x not in filters]

    return logKplag, Rxn

def calclogKAnAb2( XAn, TC, P, dbacessdic, Dielec_method = None, **rhoEG):
    """
    This function calculates thermodynamic properties of solid solution of Plagioclase minerals

    Parameters
    ----------
        XAn        : volume fraction of Anorthite  \n
        TC         : temperature [°C]  \n
        P          : pressure [bar]  \n
        dbacessdic : dictionary of species from direct-access database  \n
        Dielec_method   : specify either 'FE97' or 'JN91' as the method to calculate
                        dielectric constant (optional), if not specified default - 'JN91'
        rhoEG      : dictionary of water properties like  density (rho),
                       dielectric factor (E) and Gibbs Energy  (optional)
    Returns
    -------
        logKplag   : logarithmic K values   \n
        Rxn        : dictionary of reaction thermodynamic properties
    Usage
    -------
        The general usage of calclogKAnAb without the optional argument is as follows:  \n
        (1) Not on steam saturation curve:  \n
            [logK, Rxn] = calclogKAnAb(XAn, TC, P, dbacessdic),  \n
            where T is temperature in celsius and P is pressure in bar;
        (2) On steam saturation curve:  \n
            [logK, Rxn] = calclogKAnAb(XAn, TC, 'T', dbacessdic),   \n
            where T is temperature in celsius, followed with a quoted char 'T'  \n
            [logK, Rxn] = calclogKAnAb(XAn, P, 'P', dbacessdic), \n
            where P is pressure in bar, followed with a quoted char 'P'.
        (3) Meanwhile, usage with any specific dielectric constant method ('FE97') for
            condition not on steam saturation curve is as follows. Default method is 'JN91' \n
            [logK, Rxn] = calclogKAnAb(XAn, TC, P, dbacessdic, Dielec_method = 'FE97')
    """
    if type(P) == str:
        if P == 'T':
            P, _, _ = iapws95(T = TC)
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = TC   # Assign first input T to pressure in bar
            TC, _, _ = iapws95(P = P)
            TC = TC - 273.15 # convert from K to celcius

    if np.ndim(TC) == 0 :
        TC = np.array(TC).ravel()
    elif np.size(TC) == 2:
        TC = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(TC[0], TC[-1], 8)])

    if np.size(P) <= 2:
        P = np.ravel(P)
        P = P[0]*np.ones(np.size(TC))

    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                E = dielec_FE97(TC, P)[0]
                Dielec_method = 'FE97'
            elif Dielec_method == 'JN91':
                E = dielec_JN91(TC, P)[0]
                Dielec_method = 'JN91'
        else:
            # default method: 'JN91'
            E = dielec_JN91(TC, P)[0]
            Dielec_method = 'JN91'
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    TK=celsiusToKelvin(TC)
    Rxn = {}
    Rxn['type'] = 'plag'
    XAb = 1-XAn
    nAl = (2-XAb)
    nH2O = (4-2*XAb)
    nNa = XAb
    nCa = (1-XAb)
    nSi = (2+XAb)
    if XAn == 1:
        Rxn['name'] ='Anorthite'
        Rxn['formula'] ='CaAl2(SiO4)2'
    elif XAn == 0:
        Rxn['name'] ='Albite'
        Rxn['formula'] ='NaAlSi3O8'
    else:
        Rxn['name'] = 'An%d' % (XAn*100)
        Rxn['formula'] = 'Ca%1.2f' % nCa + 'Na%1.2f' % nNa + 'Al%1.2f' % nAl + 'Si%1.2f' % nSi + 'O8'

    Rxn['MW'] = nCa*MW['Ca'] + nNa* MW['Na'] + nAl*MW['Al'] + nSi*MW['Si'] + 8*MW['O']
    R = 1.9872041


    dGAl = supcrtaq(TC, P, dbacessdic['Al(OH)4-'], Dielec_method = Dielec_method, **rhoEG)
    dGSiO2aq  = supcrtaq(TC, P, dbacessdic['SiO2(aq)'], Dielec_method = Dielec_method, **rhoEG)

    dGNa = supcrtaq(TC, P, dbacessdic['Na+'], Dielec_method = Dielec_method, **rhoEG)
    dGCa = supcrtaq(TC, P, dbacessdic['Ca++'], Dielec_method = Dielec_method, **rhoEG)


    if (XAn < 1) & (XAn != 0):
        Smix=-R*(XAb*np.log((XAb*(4-XAb**2)*(2+XAb)**2)/27) + \
                 XAn*np.log((XAn*(1+XAn)**2*(3-XAn)**2)/16))
    else:
        Smix=0

    dGplag = XAb*dbacessdic['ss_Albite_high'][2] + XAn*dbacessdic['ss_Anorthite'][2] + R*298.15*Smix
    Splag = XAb*dbacessdic['ss_Albite_high'][4] + XAn*dbacessdic['ss_Anorthite'][4] - R*Smix
    Vplag = XAb*dbacessdic['ss_Albite_high'][5] + XAn*dbacessdic['ss_Anorthite'][5]
    Cpplag = [a + b for a, b in zip([XAb*x for x in dbacessdic['ss_Albite_high'][6:11] ],
                                    [XAn*y for y in dbacessdic['ss_Anorthite'][6:11] ])]
    Rxn['min'] = [dGplag, np.nan, Splag, Vplag] + Cpplag
    Rxn['min'].insert(0, Rxn['formula'])
    Rxn['min'].insert(1,' R&H95, Stef2001')
    plag = Rxn['min']
    dGplagTP, _ = heatcapusgscal(TC, P, plag)
    coeff = [nAl, nNa, nCa, nSi, -nH2O]
    spec = ['Al(OH)4-', 'Na+', 'Ca++', 'SiO2(aq)', 'H2O']
    Rxn['spec'] = [x for x, y in zip(spec, coeff) if y!=0]
    Rxn['coeff'] = [y for y in coeff if y!=0]
    Rxn['nSpec']=len(Rxn['coeff'])

    dGrxn = -dGplagTP + nAl*dGAl - nH2O*dGH2O + nNa*dGNa+ nCa*dGCa + nSi*dGSiO2aq
    logKplag = np.log10(np.exp(-dGrxn/R/(TK)))

    Rxn['V'] = Rxn['min'][5]  #,'%8.3f')
    Rxn['source'] = ' R&H95, Stef2001'
    elements  = ['%.4f' % nCa, 'Ca', '%.4f' % nNa, 'Na', '%.4f' % nAl, 'Al', '%.4f' % nSi, 'Si', '8.0000', 'O']
    filters = [y for x, y in enumerate(elements) if x%2 == 0 and float(y) == 0]
    filters = [[x, x+1] for x, y in enumerate(elements) if y in filters]
    filters = [num for elem in filters for num in elem]
    Rxn['elements'] = [y for x, y in enumerate(elements) if x not in filters]

    return logKplag, Rxn

def calclogKAbOr2( XAb, TC, P, dbacessdic, Dielec_method = None, **rhoEG):
    """
    This function calculates thermodynamic properties of solid solution of Alkaline-Feldspar minerals

    Parameters
    ----------
        XAb        : volume fraction of Albite  \n
        TC         : temperature [°C]  \n
        P          : pressure [bar]  \n
        dbacessdic : dictionary of species from direct-access database  \n
        Dielec_method   : specify either 'FE97' or 'JN91' as the method to calculate
                        dielectric constant (optional), if not specified default - 'JN91'
        rhoEG      : dictionary of water properties like  density (rho),
                       dielectric factor (E) and Gibbs Energy  (optional)
    Returns
    -------
        logKalkfeld: logarithmic K values   \n
        Rxn        : dictionary of reaction thermodynamic properties
    Usage
    -------
        The general usage of calclogKAnAb without the optional argument is as follows:  \n
        (1) Not on steam saturation curve:  \n
            [logK, Rxn] = calclogKAbOr(XAb, TC, P, dbacessdic),  \n
            where T is temperature in celsius and P is pressure in bar;
        (2) On steam saturation curve:  \n
            [logK, Rxn] = calclogKAbOr(XAb, TC, 'T', dbacessdic),   \n
            where T is temperature in celsius, followed with a quoted char 'T'  \n
            [logK, Rxn] = calclogKAbOr(XAb, P, 'P', dbacessdic), \n
            where P is pressure in bar, followed with a quoted char 'P'.
        (3) Meanwhile, usage with any specific dielectric constant method ('FE97') for
            condition not on steam saturation curve is as follows. Default method is 'JN91' \n
            [logK, Rxn] = calclogKAbOr(XAb, TC, P, dbacessdic, Dielec_method = 'FE97')
    """
    if type(P) == str:
        if P == 'T':
            P, _, _ = iapws95(T = TC)
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = TC   # Assign first input T to pressure in bar
            TC, _, _ = iapws95(P = P)
            TC = TC - 273.15 # convert from K to celcius

    if np.ndim(TC) == 0 :
        TC = np.array(TC).ravel()
    elif np.size(TC) == 2:
        TC = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(TC[0], TC[-1], 8)])

    if np.size(P) <= 2:
        P = np.ravel(P)
        P = P[0]*np.ones(np.size(TC))

    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                E = dielec_FE97(TC, P)[0]
            elif Dielec_method == 'JN91':
                E = dielec_JN91(TC, P)[0]
        else:
            # default method: 'JN91'
            E = dielec_JN91(TC, P)[0]
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    if Dielec_method is None:
        Dielec_method = 'JN91'

    Tref = 25; Pref = 1
    # if no reference Temperature and Pressure is found, append to the bottom
    if any((TC == Tref) & (P == Pref)) == False:
        [rhoref, dGH2Oref, _, _, _, _, _] = iapws95(T = Tref, P = Pref)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                Eref = dielec_FE97(Tref, Pref)[0]
            elif Dielec_method == 'JN91':
                Eref = dielec_JN91(Tref, Pref)[0]
        else:
            # default method: 'JN91'
            Eref = dielec_JN91(Tref, Pref)[0]
        TC = np.concatenate([TC, np.ravel(Tref)])
        P = np.concatenate([P, np.ravel(Pref)])
        rho = np.concatenate([rho, np.ravel(rhoref)])
        E = np.concatenate([E, np.ravel(Eref)])
        dGH2O = np.concatenate([dGH2O, np.ravel(dGH2Oref)])
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    TK=celsiusToKelvin(TC)
    Rxn = {}
    Rxn['type'] = 'Alk feldspar'
    XOr = 1 - XAb
    nNa = XAb
    nK = (1 - XAb)
    nAl = 1
    nH2O = 2
    nSi = 3
    if XAb == 1:
        Rxn['name'] ='Albite'
        Rxn['formula'] ='NaAlSi3O8'
    elif XAb == 0:
        Rxn['name'] ='K-Feldspar'
        Rxn['formula'] ='KAlSi3O8'
    else:
        Rxn['name'] = 'Alkfeld%d' % (XAb*100)
        Rxn['formula'] = 'Na%1.2f' % nNa + 'K%1.2f' % nK + 'Al%1.2f' % nAl + 'Si%1.2f' % nSi + 'O8'

    Rxn['MW'] = nNa* MW['Na'] + nK*MW['K'] + nAl*MW['Al'] + nSi*MW['Si'] + 8*MW['O']
    R = 1.9872041
    WH = 23800/J_to_cal
    WS = 0/J_to_cal

    dGAl = supcrtaq(TC, P, dbacessdic['Al(OH)4-'], Dielec_method = Dielec_method, **rhoEG)
    dGSiO2aq  = supcrtaq(TC, P, dbacessdic['SiO2(aq)'], Dielec_method = Dielec_method, **rhoEG)

    dGNa = supcrtaq(TC, P, dbacessdic['Na+'], Dielec_method = Dielec_method, **rhoEG)
    dGK = supcrtaq(TC, P, dbacessdic['K+'], Dielec_method = Dielec_method, **rhoEG)

    if (XAb < 1) & (XAb != 0):
        Sconf = -R*(XOr*np.log(XOr) + XAb*np.log(XAb))
    else:
        Sconf = 0

    dGalk = XOr*dbacessdic['ss_K-feldspar'][2] + XAb*dbacessdic['ss_Albite_high'][2]
    Salk = XOr*dbacessdic['ss_K-feldspar'][4] + XAb*dbacessdic['ss_Albite_high'][4] + Sconf
    Valk = (XOr*dbacessdic['ss_K-feldspar'][5] + XAb*dbacessdic['ss_Albite_high'][5]) #*(P - 1)
    Cpalk = [a + b for a, b in zip([XOr*x for x in dbacessdic['ss_K-feldspar'][6:11] ],
                                   [XAb*y for y in dbacessdic['ss_Albite_high'][6:11] ])]
    WG = WH - TK*WS
    Gex = WG*XAb*XOr
    dGalk = dGalk + Gex - 298.15*Sconf
    Rxn['min'] = [dGalk, np.nan, Salk, Valk] + Cpalk
    Rxn['min'].insert(0, Rxn['formula'])
    Rxn['min'].insert(1,' R&H95, A&S99')
    alk = Rxn['min']
    dGalkTP, _ = heatcapusgscal(TC, P, alk)
    coeff = [-nH2O, nAl, nNa, nK, nSi]
    spec = ['H2O', 'Al(OH)4-', 'Na+', 'K+', 'SiO2(aq)']
    Rxn['spec'] = [x for x, y in zip(spec, coeff) if y!=0]
    Rxn['coeff'] = [y for y in coeff if y!=0]
    Rxn['nSpec'] = len(Rxn['coeff'])

    dGrxn = - dGalkTP + nAl*dGAl - nH2O*dGH2O + nNa*dGNa+ nK*dGK + nSi*dGSiO2aq
    logKalkfeld = np.log10(np.exp(-dGrxn/R/(TK)))

    Rxn['min'][2] = dGalk[(TC == Tref) & (P == Pref)][0]
    Rxn['V'] = Rxn['min'][5]  #,'%8.3f')
    Rxn['source'] = ' R&H95, A&S99'
    elements  = ['%.4f' % nNa, 'Na', '%.4f' % nK, 'K', '%.4f' % nAl, 'Al', '%.4f' % nSi, 'Si', '8.0000', 'O']
    filters = [y for x, y in enumerate(elements) if x%2 == 0 and float(y) == 0]
    filters = [[x, x+1] for x, y in enumerate(elements) if y in filters]
    filters = [num for elem in filters for num in elem]
    Rxn['elements'] = [y for x, y in enumerate(elements) if x not in filters]

    if (TC[-1] == Tref) & (P[-1] == Pref):
        logKalkfeld = logKalkfeld[:-1]

    return logKalkfeld, Rxn

def calclogKFoFa( XFo, TC, P, dbacessdic, Dielec_method = None, **rhoEG ):
    """
    This function calculates thermodynamic properties of solid solution of olivine minerals  \n
    Parameters
    ----------
        XFo        : volume fraction of Forsterite  \n
        TC         : temperature [°C]  \n
        P          : pressure [bar]  \n
        dbacessdic : dictionary of species from direct-access database  \n
        Dielec_method   : specify either 'FE97' or 'JN91' as the method to calculate
                        dielectric constant (optional), if not specified default - 'JN91'
        rhoEG      : dictionary of water properties like  density (rho),
                       dielectric factor (E) and Gibbs Energy  (optional)
    Returns
    -------
        logK_ol    : logarithmic K values   \n
        Rxn        : dictionary of reaction thermodynamic properties
    Usage
    -------
        The general usage of calclogKFoFa without the optional argument is as follows:  \n
        (1) Not on steam saturation curve:  \n
            [logK, Rxn] = calclogKFoFa(XFo, TC, P, dbacessdic),  \n
            where T is temperature in celsius and P is pressure in bar;
        (2) On steam saturation curve:  \n
            [logK, Rxn] = calclogKFoFa(XFo, TC, 'T', dbacessdic),   \n
            where T is temperature in celsius, followed with a quoted char 'T'  \n
            [logK, Rxn] = calclogKFoFa(XFo, P, 'P', dbacessdic), \n
            where P is pressure in bar, followed with a quoted char 'P'.
        (3) Meanwhile, usage with any specific dielectric constant method ('FE97') for
            condition not on steam saturation curve is as follows. Default method is 'JN91' \n
            [logK, Rxn] = calclogKFoFa(XFo, TC, P, dbacessdic, Dielec_method = 'FE97')
    """

    if type(P) == str:
        if P == 'T':
            P, _, _ = iapws95(T = TC)
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = TC   # Assign first input T to pressure in bar
            TC, _, _ = iapws95(P = P)
            TC = TC - 273.15 # convert from K to celcius

    if np.ndim(TC) == 0 :
        TC = np.array(TC).ravel()
    elif np.size(TC) == 2:
        TC = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(TC[0], TC[-1], 8)])

    if np.size(P) <= 2:
        P = np.ravel(P)
        P = P[0]*np.ones(np.size(TC))

    length = len(TC)

    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97' :
                E = dielec_FE97(TC, P)[0]
            elif Dielec_method == 'JN91':
                E = dielec_JN91(TC, P)[0]
        else:
            # default method: 'JN91'
            E = dielec_JN91(TC, P)[0]
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    if Dielec_method is None:
        Dielec_method = 'JN91'

    Tref = 25; Pref = 1
    # if no reference Temperature and Pressure is found, append to the bottom
    if any((TC == Tref) & (P == Pref)) == False:
        [rhoref, dGH2Oref, _, _, _, _, _] = iapws95(T = Tref, P = Pref)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                Eref = dielec_FE97(Tref, Pref)[0]
            elif Dielec_method == 'JN91':
                Eref = dielec_JN91(Tref, Pref)[0]
        else:
            # default method: 'JN91'
            Eref = dielec_JN91(Tref, Pref)[0]
        TC = np.concatenate([TC, np.ravel(Tref)])
        P = np.concatenate([P, np.ravel(Pref)])
        rho = np.concatenate([rho, np.ravel(rhoref)])
        E = np.concatenate([E, np.ravel(Eref)])
        dGH2O = np.concatenate([dGH2O, np.ravel(dGH2Oref)])
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    TK=celsiusToKelvin(TC)
    Rxn = {}
    Rxn['type']='ol'
    XFa = 1-XFo
    nH = 4
    nMg = 2*XFo
    nFe = 2*XFa
    nSi = 1
    nH2O = 2
    if XFo == 1:
        Rxn['name']='Forsterite'
        Rxn['formula']='Mg2SiO4'
    elif XFo == 0:
        Rxn['name']='Fayalite'
        Rxn['formula']='Fe2SiO4'
    else:
        Rxn['name'] = 'Fo%d' % (XFo*100)
        Rxn['formula']= 'Mg%1.2f' % nMg + 'Fe%1.2f' % nFe + 'Si%s' % nSi + 'O4'

    Rxn['MW'] = nMg*MW['Mg'] + nFe*MW['Fe'] + nSi*MW['Si'] + 4*MW['O']
    R = 1.9872041
    WH = 10366.2/J_to_cal
    WS = 4/J_to_cal

    dGSiO2aq  = supcrtaq(TC, P, dbacessdic['SiO2(aq)'], Dielec_method = Dielec_method, **rhoEG)
    dGH = supcrtaq(TC, P, dbacessdic['H+'], Dielec_method = Dielec_method, **rhoEG)
    dGFe = supcrtaq(TC, P, dbacessdic['Fe++'], Dielec_method = Dielec_method, **rhoEG)
    dGMg = supcrtaq(TC, P, dbacessdic['Mg++'], Dielec_method = Dielec_method, **rhoEG)

    if (XFo == 1) | (XFo == 0):
        Sconf = 0
    else:
        Sconf = -2*R*(XFo*np.log(XFo) + XFa*np.log(XFa))

    dG_ol = XFa*dbacessdic['ss_Fayalite'][2] + XFo*dbacessdic['ss_Forsterite'][2]
    S_ol = XFa*dbacessdic['ss_Fayalite'][4] + XFo*dbacessdic['ss_Forsterite'][4] + Sconf
    V_ol = XFa*dbacessdic['ss_Fayalite'][5] + XFo*dbacessdic['ss_Forsterite'][5]
    Cp_ol = [a + b for a, b in zip([XFa*x for x in dbacessdic['ss_Fayalite'][6:11] ],
                                    [XFo*y for y in dbacessdic['ss_Forsterite'][6:11] ])]
    WG = WH - TK*WS
    Gex = WG*XFo*XFa
    dG_ol = dG_ol + Gex - 298.15*Sconf
    Rxn['min'] = [dG_ol, np.nan, S_ol, V_ol] + Cp_ol
    Rxn['min'].insert(0, Rxn['formula'])
    Rxn['min'].insert(1,' R&H95, Stef2001')
    coeff = [-nH, nMg, nFe, nSi, nH2O]
    spec = ['H+', 'Mg++', 'Fe++', 'SiO2(aq)', 'H2O']
    Rxn['spec'] = [x for x, y in zip(spec, coeff) if y!=0]
    Rxn['coeff'] = [y for y in coeff if y!=0]
    Rxn['nSpec'] = len(Rxn['coeff'])

    logK_ol = np.zeros([length, 1])
    ol = Rxn['min']
    dGolTP, _ = heatcapusgscal(TC, P, ol)
    dGrxn = -dGolTP - nH*dGH + nMg*dGMg + nFe*dGFe + nH2O*dGH2O + nSi*dGSiO2aq
    logK_ol = (-dGrxn/R/(TK)/np.log(10))   #np.log10(np.exp(-dGrxn/R/(TK)))

    Rxn['min'][2] = dG_ol[(TC == Tref) & (P == Pref)][0]
    Rxn['V'] = Rxn['min'][5]  #,'%8.3f')
    Rxn['source'] = ' R&H95, Stef2001'
    elements  = ['%.4f' % nMg, 'Mg', '%.4f' % nFe, 'Fe', '%.4f' % nSi, 'Si', '4.0000', 'O']
    filters = [y for x, y in enumerate(elements) if x%2 == 0 and float(y) == 0]
    filters = [[x, x+1] for x, y in enumerate(elements) if y in filters]
    filters = [num for elem in filters for num in elem]
    Rxn['elements'] = [y for x, y in enumerate(elements) if x not in filters]

    if (TC[-1] == Tref) & (P[-1] == Pref):
        logK_ol = logK_ol[:-1]

    return logK_ol, Rxn

def calclogKEnFe( XEn, TC, P, dbacessdic, Dielec_method = None, **rhoEG ):
    """
    This function calculates thermodynamic properties of solid solution of pyroxene minerals  \n
    Parameters
    ----------
        XEn        : volume fraction of Enstatite  \n
        TC         : temperature [°C]  \n
        P          : pressure [bar]  \n
        dbacessdic : dictionary of species from direct-access database  \n
        Dielec_method   : specify either 'FE97' or 'JN91' as the method to calculate
                        dielectric constant (optional), if not specified default - 'JN91'
        rhoEG      : dictionary of water properties like  density (rho),
                       dielectric factor (E) and Gibbs Energy  (optional)
    Returns
    -------
        logK_opx   : logarithmic K values   \n
        Rxn        : dictionary of reaction thermodynamic properties
    Usage
    -------
        The general usage of calclogKEnFe without the optional argument is as follows:  \n
        (1) Not on steam saturation curve:  \n
            [logK, Rxn] = calclogKEnFe(XEn, TC, P, dbacessdic),  \n
            where T is temperature in celsius and P is pressure in bar;
        (2) On steam saturation curve:  \n
            [logK, Rxn] = calclogKEnFe(XEn, TC, 'T', dbacessdic),   \n
            where T is temperature in celsius, followed with a quoted char 'T'  \n
            [logK, Rxn] = calclogKEnFe(XEn, P, 'P', dbacessdic), \n
            where P is pressure in bar, followed with a quoted char 'P'.
        (3) Meanwhile, usage with any specific dielectric constant method ('FE97') for
            condition not on steam saturation curve is as follows. Default method is 'JN91' \n
            [logK, Rxn] = calclogKEnFe(XEn, TC, P, dbacessdic, Dielec_method = 'FE97')
    """
    if type(P) == str:
        if P == 'T':
            P, _, _ = iapws95(T = TC)
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = TC   # Assign first input T to pressure in bar
            TC, _, _ = iapws95(P = P)
            TC = TC - 273.15 # convert from K to celcius

    if np.ndim(TC) == 0 :
        TC = np.array(TC).ravel()
    elif np.size(TC) == 2:
        TC = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(TC[0], TC[-1], 8)])

    if np.size(P) <= 2:
        P = np.ravel(P)
        P = P[0]*np.ones(np.size(TC))

    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                E = dielec_FE97(TC, P)[0]
            elif Dielec_method == 'JN91':
                E = dielec_JN91(TC, P)[0]
        else:
            # default method: 'JN91'
            E = dielec_JN91(TC, P)[0]
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    if Dielec_method is None:
        Dielec_method = 'JN91'

    Tref = 25; Pref = 1
    # if no reference Temperature and Pressure is found, append to the bottom
    if any((TC == Tref) & (P == Pref)) == False:
        [rhoref, dGH2Oref, _, _, _, _, _] = iapws95(T = Tref, P = Pref)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                Eref = dielec_FE97(Tref, Pref)[0]
            elif Dielec_method == 'JN91':
                Eref = dielec_JN91(Tref, Pref)[0]
        else:
            # default method: 'FE97'
            Eref = dielec_JN91(Tref, Pref)[0]
        TC = np.concatenate([TC, np.ravel(Tref)])
        P = np.concatenate([P, np.ravel(Pref)])
        rho = np.concatenate([rho, np.ravel(rhoref)])
        E = np.concatenate([E, np.ravel(Eref)])
        dGH2O = np.concatenate([dGH2O, np.ravel(dGH2Oref)])
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    TK = celsiusToKelvin(TC)
    Rxn = {}
    Rxn['type'] = 'opx'
    XFe = 1-XEn
    nH = 2
    nMg = XEn
    nFe = XFe
    nSi = 1
    nH2O = 1
    if XEn == 1:
        Rxn['name'] = 'Enstatite'
        Rxn['formula'] = 'MgSiO3'
    elif XEn == 0:
        Rxn['name'] = 'Ferrosilite'
        Rxn['formula'] = 'FeSiO3'
    else:
        Rxn['name'] = 'En%d' % (XEn*100)
        Rxn['formula'] = 'Mg%1.2f' % nMg + 'Fe%1.2f' % nFe + 'Si%s' % nSi + 'O3'

    Rxn['MW'] = nMg*MW['Mg'] + nFe*MW['Fe'] + nSi*MW['Si'] + 3*MW['O']
    R = 1.9872041
    WH = -2600.4/J_to_cal
    WS = -1.34/J_to_cal

    dGSiO2aq  = supcrtaq(TC, P, dbacessdic['SiO2(aq)'], Dielec_method = Dielec_method, **rhoEG)
    dGH = supcrtaq(TC, P, dbacessdic['H+'], Dielec_method = Dielec_method, **rhoEG)
    dGFe = supcrtaq(TC, P, dbacessdic['Fe++'], Dielec_method = Dielec_method, **rhoEG)
    dGMg = supcrtaq(TC, P, dbacessdic['Mg++'], Dielec_method = Dielec_method, **rhoEG)

    if (XEn == 1) | (XEn == 0):
        Sconf = 0
    else:
        Sconf = -1*R*(XEn*np.log(XEn) + XFe*np.log(XFe))

    dG_opx = XEn*dbacessdic['ss_Enstatite'][2] + XFe*dbacessdic['ss_Ferrosilite'][2]
    S_opx = XEn*dbacessdic['ss_Enstatite'][4] + XFe*dbacessdic['ss_Ferrosilite'][4] + Sconf
    V_opx = XEn*dbacessdic['ss_Enstatite'][5] + XFe*dbacessdic['ss_Ferrosilite'][5]
    Cp_opx = [a + b for a, b in zip([XEn*x for x in dbacessdic['ss_Enstatite'][6:11] ],
                                    [XFe*y for y in dbacessdic['ss_Ferrosilite'][6:11] ])]
    WG = WH-TK*WS
    Gex = WG*XEn*XFe
    dG_opx = dG_opx + Gex - 298.15*Sconf
    Rxn['min'] = [dG_opx, np.nan, S_opx, V_opx] + Cp_opx
    Rxn['min'].insert(0, Rxn['formula'])
    Rxn['min'].insert(1, ' R&H95, Stef2001')
    coeff = [-nH, nMg, nFe, nSi, nH2O]
    spec = ['H+', 'Mg++', 'Fe++', 'SiO2(aq)', 'H2O']
    Rxn['spec'] = [x for x, y in zip(spec, coeff) if y != 0]
    Rxn['coeff'] = [y for y in coeff if y != 0]
    Rxn['nSpec'] = len(Rxn['coeff'])

    opx = Rxn['min']
    dGopxTP, _ = heatcapusgscal(TC, P, opx)
    dGrxn = -dGopxTP - nH*dGH + nMg*dGMg + nFe*dGFe + nH2O*dGH2O + nSi*dGSiO2aq
    logK_opx = (-dGrxn/R/(TK)/np.log(10))   # np.log10(np.exp(-dGrxn/R/(TK)))

    Rxn['min'][2]=dG_opx[(TC == Tref) & (P == Pref)][0]
    Rxn['V'] = Rxn['min'][5]  #,'%8.3f')
    Rxn['source'] = ' R&H95, Stef2001'
    elements  = ['%.4f' % nMg, 'Mg', '%.4f' % nFe, 'Fe', '%.4f' % nSi, 'Si', '3.0000', 'O']
    filters = [y for x, y in enumerate(elements) if x%2 == 0 and float(y) == 0]
    filters = [[x, x+1] for x, y in enumerate(elements) if y in filters]
    filters = [num for elem in filters for num in elem]
    Rxn['elements'] = [y for x, y in enumerate(elements) if x not in filters]

    if (TC[-1] == Tref) & (P[-1] == Pref):
        logK_opx = logK_opx[:-1]

    return logK_opx, Rxn

def calclogKDiHedEnFe( nCa, XMg, TC, P, dbacessdic, Dielec_method = None, **rhoEG ):
    """
    This function calculates thermodynamic properties of solid solution of clinopyroxene
    minerals (Di, Hed, En and Fe) \n
    Parameters
    ----------
        nCa        : number of moles of Ca in formula unit (=1 for Di, Hed)
                       and must be greater than zero  \n
        XMg        : mole fraction of Mg
                        XMg = (nMg/(nFe + nMg))  \n
        TC         : temperature [°C]  \n
        P          : pressure [bar]  \n
        dbacessdic : dictionary of species from direct-access database  \n
        Dielec_method   : specify either 'FE97' or 'JN91' as the method to calculate
                        dielectric constant (optional), if not specified default - 'JN91'
        rhoEG      : dictionary of water properties like  density (rho),
                        dielectric factor (E) and Gibbs Energy  (optional)
    Returns
    -------
        logK_cpx   : logarithmic K values   \n
        Rxn        : dictionary of reaction thermodynamic properties
    Usage
    -------
        The general usage of calclogKDiHedEnFe without the optional argument is as follows:  \n
        (1) Not on steam saturation curve:  \n
            [logK, Rxn] = calclogKDiHedEnFe(nCa, XMg, TC, P, dbacessdic),  \n
            where T is temperature in celsius and P is pressure in bar;
        (2) On steam saturation curve:  \n
            [logK, Rxn] = calclogKDiHedEnFe(nCa, XMg, TC, 'T', dbacessdic),   \n
            where T is temperature in celsius, followed with a quoted char 'T'  \n
            [logK, Rxn] = calclogKDiHedEnFe(nCa, XMg, P, 'P', dbacessdic), \n
            where P is pressure in bar, followed with a quoted char 'P'.
        (3) Meanwhile, usage with any specific dielectric constant method ('FE97') for
            condition not on steam saturation curve is as follows. Default method is 'JN91' \n
            [logK, Rxn] = calclogKDiHedEnFe(nCa, XMg, TC, P, dbacessdic, Dielec_method = 'FE97')
    """
    if type(P) == str:
        if P == 'T':
            P, _, _ = iapws95(T = TC)
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = TC   # Assign first input T to pressure in bar
            TC, _, _ = iapws95(P = P)
            TC = TC - 273.15 # convert from K to celcius

    if np.ndim(TC) == 0 :
        TC = np.array(TC).ravel()
    elif np.size(TC) == 2:
        TC = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(TC[0], TC[-1], 8)])

    if np.size(P) <= 2:
        P = np.ravel(P)
        P = P[0]*np.ones(np.size(TC))

    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                E = dielec_FE97(TC, P)[0]
                Dielec_method = 'FE97'
            elif Dielec_method == 'JN91':
                E = dielec_JN91(TC, P)[0]
                Dielec_method = 'JN91'
        else:
            # default method: 'JN91'
            E = dielec_JN91(TC, P)[0]
            Dielec_method = 'JN91'
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    Tref = 25; Pref = 1
    # if no reference Temperature and Pressure is found, append to the bottom
    if any((TC == Tref) & (P == Pref)) == False:
        [rhoref, dGH2Oref, _, _, _, _, _] = iapws95(T = Tref, P = Pref)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                Eref = dielec_FE97(Tref, Pref)[0]
            elif Dielec_method == 'JN91':
                Eref = dielec_JN91(Tref, Pref)[0]
        else:
            # default method: 'JN91'
            Eref = dielec_JN91(Tref, Pref)[0]
        TC = np.concatenate([TC, np.ravel(Tref)])
        P = np.concatenate([P, np.ravel(Pref)])
        rho = np.concatenate([rho, np.ravel(rhoref)])
        E = np.concatenate([E, np.ravel(Eref)])
        dGH2O = np.concatenate([dGH2O, np.ravel(dGH2Oref)])
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    TK = celsiusToKelvin(TC)
    R = 1.9872041

    if nCa >= 1:
        XDi = XMg
        XHed = 1-XDi
        XEn = 0
        XFs = 0
    else:
        XDi = XMg*nCa
        XHed = (1 - XMg)*nCa
        Xopx = 1 -nCa
        XEn = XMg*Xopx
        XFs = (1 - XMg)*Xopx

    #start on M1 parms
    WH_M1 = -1910/J_to_cal
    WS_M1 = -1.05/J_to_cal
    if nCa < 1:
        # for the M1 (i.e., opx) component of cpx
        Mg_M1 = XEn*2 #  %multiplying by 2 b/c thermo props multiplied by 2
        Fe_M1 = XFs*2 #  %multiplying by 2 b/c thermo props multiplied by 2

        XFe_M1 = Fe_M1/(Fe_M1 + Mg_M1)
        XMg_M1 = Mg_M1/(Fe_M1 + Mg_M1)
        if (XEn == 0) | (XFs == 0):
            Sconf_M1 = 0 # if you don't do this, you take log of 0
        else:
            Sconf_M1 = -1*R*(XFe_M1*np.log(XFe_M1) + XMg_M1*np.log(XMg_M1))
    else: #No M1 if nCa >=1
        XMg_M1 = 0; Mg_M1 = 0
        XFe_M1 = 0; Fe_M1 = 0
        Sconf_M1 = 0

    WG_M1 = WH_M1 - TK*WS_M1
    Gex_M1 = WG_M1*XFe_M1*XMg_M1
    WG_M1_25 = WH_M1 - 298.15*WS_M1
    Gex_M1_25 = WG_M1_25*XFe_M1*XMg_M1

    # now start on the M2 part
    WH_M2 = 0./J_to_cal
    WS_M2 = 0./J_to_cal
    Fe_M2 = (XHed)
    Mg_M2 = (XDi)
    Ca_M2 = (XHed + XDi)
    Tot_M2 = Fe_M2 + Mg_M2 + Ca_M2

    XFe_M2 = Fe_M2/Tot_M2
    XMg_M2 = Mg_M2/Tot_M2
    XCa_M2 = Ca_M2/Tot_M2
    if (XHed == 0) | (XDi == 0):
        Sconf_M2 = 0
    else:
        #because of the entropy term in WG, Gex and dGol are T-dependent, thus must
        #do the logK calculation on a piecewise basis.
        Sconf_M2 = -1*R*( XFe_M2*np.log(XFe_M2) + XMg_M2*np.log(XMg_M2) + \
                         XCa_M2*np.log(XCa_M2))
    WG_M2 = WH_M2 - TK*WS_M2
    Gex_M2 = WG_M2*XDi*XHed
    WG_M2_25 = WH_M2 - 298.15*WS_M2
    Gex_M2_25 = WG_M2_25*XFe_M2*XMg_M2

    Rxn = {}
    Rxn['type']='cpx'
    nH = 4
    nMg = round(Mg_M1 + Mg_M2, 2)
    nFe = round(Fe_M1 + Fe_M2, 2)
    nCa = round(Ca_M2, 2)
    nSi = 2
    nH2O = 2
    if (XEn == 0):
        if (XFs == 0):
            if XDi == 1:
                Rxn['name'] = 'Diopside'
            elif XHed == 1:
                Rxn['name'] = 'Hedenbergite'
            else:
                Rxn['name'] = 'Di%d' % round(XDi*100) + 'Hed%d' % round(XHed*100)
        elif (XHed == 0):
            Rxn['name'] = 'Di%d' % round(XDi*100) + 'Fe%d' % round(XFs*100)
        elif (XDi == 0):
            Rxn['name'] = 'Hed%d' % round(XHed*100) + 'Fe%d' % round(XFs*100)
        else:
            Rxn['name'] = 'Di%d' % round(XDi*100) + 'Hed%d' % round(XHed*100) + 'Fe%d' % round(XFs*100)
    elif (XFs == 0):
        if (XHed == 0):
            Rxn['name'] = 'Di%d' % round(XDi*100) + 'En%d' % round(XEn*100)
        elif (XDi == 0):
            Rxn['name'] = 'Hed%d' % round(XHed*100) + 'En%d' % round(XEn*100)
        else:
            Rxn['name'] = 'Di%d' % round(XDi*100) + 'Hed%d' % round(XHed*100) + 'En%d' % round(XEn*100)
    elif (XHed == 0):
        Rxn['name'] = 'Di%d' % round(XDi*100) + 'En%d' % round(XEn*100) + 'Fe%d' % round(XFs*100)
    elif (XDi == 0):
        Rxn['name'] = 'Hed%d' % round(XHed*100) + 'En%d' % round(XEn*100) + 'Fe%d' % round(XFs*100)
    else:
        Rxn['name'] = 'Di%d' % round(XDi*100) + 'Hed%d' % round(XHed*100) + 'En%d' % round(XEn*100) + 'Fe%d' % round(XFs*100)

    if (nCa == 0):
        Rxn['formula'] = 'Mg%1.2f' % nMg + 'Fe%1.2f' % nFe + 'Si%s' % nSi + 'O6'
    elif (nMg == 0):
        if (nCa == 1) and (nFe == 1):
            Rxn['formula'] = 'CaFe' + 'Si%s' % nSi + 'O6'
        else:
            Rxn['formula'] = 'Ca%1.2f' % nCa + 'Fe%1.2f' % nFe + 'Si%s' % nSi + 'O6'
    elif (nFe == 0):
        if (nCa == 1) and (nMg == 1):
            Rxn['formula'] = 'CaMg' + 'Si%s' % nSi + 'O6'
        else:
            Rxn['formula'] = 'Ca%1.2f' % nCa + 'Mg%1.2f' % nMg + 'Si%s' % nSi + 'O6'
    else:
        Rxn['formula'] = 'Ca%1.2f' % nCa + 'Mg%1.2f' % nMg + 'Fe%1.2f' % nFe + 'Si%s' % nSi + 'O6'
    Rxn['MW'] = nCa*MW['Ca'] + nMg*MW['Mg'] + nFe*MW['Fe'] + nSi*MW['Si'] + 6*MW['O']
    R = 1.9872041

    dGSiO2aq  = supcrtaq(TC, P, dbacessdic['SiO2(aq)'], Dielec_method = Dielec_method, **rhoEG)
    dGH = supcrtaq(TC, P, dbacessdic['H+'], Dielec_method = Dielec_method, **rhoEG)
    dGFe = supcrtaq(TC, P, dbacessdic['Fe++'], Dielec_method = Dielec_method, **rhoEG)
    dGMg = supcrtaq(TC, P, dbacessdic['Mg++'], Dielec_method = Dielec_method, **rhoEG)
    dGCa = supcrtaq(TC, P, dbacessdic['Ca++'], Dielec_method = Dielec_method, **rhoEG)

    # Addition of M1 and M2 sites
    Sconf = Sconf_M1 + Sconf_M2
    dG_cpx_noGex = 2*XFs*dbacessdic['ss_Ferrosilite'][2] + 2*XEn*dbacessdic['ss_Clinoenstatite'][2] + \
        XHed*dbacessdic['ss_Hedenbergite'][2] + XDi*dbacessdic['ss_Diopside'][2]
    dG_cpx_25 = dG_cpx_noGex + Gex_M1_25 + Gex_M2_25

    S_cpx = 2*XFs*dbacessdic['ss_Ferrosilite'][4] + 2*XEn*dbacessdic['ss_Clinoenstatite'][4] + \
        XHed*dbacessdic['ss_Hedenbergite'][4] + XDi*dbacessdic['ss_Diopside'][4] + Sconf
    V_cpx = 2*XFs*dbacessdic['ss_Ferrosilite'][5] + 2*XEn*dbacessdic['ss_Clinoenstatite'][5] + \
        XHed*dbacessdic['ss_Hedenbergite'][5] + XDi*dbacessdic['ss_Diopside'][5]
    Cp_cpx = [i + j + k + l for i, j, k, l in zip([2*XFs*a for a in dbacessdic['ss_Ferrosilite'][6:11] ],
                                                  [2*XEn*b for b in dbacessdic['ss_Clinoenstatite'][6:11] ],
                                                  [XHed*c for c in dbacessdic['ss_Hedenbergite'][6:11] ],
                                                  [XDi*d for d in dbacessdic['ss_Diopside'][6:11] ] ) ]
    # because of the entropy term in WG, Gex and dGol are T-dependent, thus must do the logK calculation on a piecewise basis.
    dG_cpx = dG_cpx_25 + Gex_M1 + Gex_M2 - TK*Sconf
    Rxn['min'] = [dG_cpx, np.nan, S_cpx, V_cpx] + Cp_cpx
    Rxn['min'].insert(0, Rxn['formula'])
    Rxn['min'].insert(1,' R&H95, Stef2001')
    coeff = [-nH, nCa, nMg, nFe, nSi, nH2O]
    spec = ['H+', 'Ca++', 'Mg++', 'Fe++', 'SiO2(aq)', 'H2O']
    Rxn['spec'] = [x for x, y in zip(spec, coeff) if y != 0]
    Rxn['coeff'] = [y for y in coeff if y != 0]
    Rxn['nSpec'] = len(Rxn['coeff'])

    cpx = Rxn['min']
    dGcpxTP, _ = heatcapusgscal(TC, P, cpx)
    dGrxn = - dGcpxTP - nH*dGH + nCa*dGCa + nMg*dGMg + nFe*dGFe + nH2O*dGH2O + nSi*dGSiO2aq
    logK_cpx = (-dGrxn/R/(TK)/np.log(10))   # np.log10(np.exp(-dGrxn/R/(TK)))

    Rxn['min'][2] = dG_cpx[(TC == Tref) & (P == Pref)][0]
    Rxn['V'] = Rxn['min'][5]
    Rxn['source'] = ' R&H95, Stef2001'
    elements  = ['%.4f' % nCa, 'Ca', '%.4f' % nMg, 'Mg', '%.4f' % nFe, 'Fe', '%.4f' % nSi, 'Si', '6.0000', 'O']
    filters = [y for x, y in enumerate(elements) if x%2 == 0 and float(y) == 0]
    filters = [[x, x+1] for x, y in enumerate(elements) if y in filters]
    filters = [num for elem in filters for num in elem]
    Rxn['elements'] = [y for x, y in enumerate(elements) if x not in filters]

    if (TC[-1] == Tref) & (P[-1] == Pref):
        logK_cpx = logK_cpx[:-1]

    return logK_cpx, Rxn

def calclogKAbOr( XAb, TC, P, dbacessdic, Dielec_method = None, **rhoEG):
    """
    This function calculates thermodynamic properties of solid solution of Alkaline-Feldspar minerals

    Parameters
    ----------
        XAb        : volume fraction of Albite  \n
        TC         : temperature [°C]  \n
        P          : pressure [bar]  \n
        dbacessdic : dictionary of species from direct-access database  \n
        Dielec_method   : specify either 'FE97' or 'JN91' as the method to calculate
                        dielectric constant (optional), if not specified default - 'JN91'
        rhoEG      : dictionary of water properties like  density (rho),
                       dielectric factor (E) and Gibbs Energy  (optional)
    Returns
    -------
        logKalkfeld: logarithmic K values   \n
        Rxn        : dictionary of reaction thermodynamic properties
    Usage
    -------
        The general usage of calclogKAnAb without the optional argument is as follows:  \n
        (1) Not on steam saturation curve:  \n
            [logK, Rxn] = calclogKAbOr(XAb, TC, P, dbacessdic),  \n
            where T is temperature in celsius and P is pressure in bar;
        (2) On steam saturation curve:  \n
            [logK, Rxn] = calclogKAbOr(XAb, TC, 'T', dbacessdic),   \n
            where T is temperature in celsius, followed with a quoted char 'T'  \n
            [logK, Rxn] = calclogKAbOr(XAb, P, 'P', dbacessdic), \n
            where P is pressure in bar, followed with a quoted char 'P'.
        (3) Meanwhile, usage with any specific dielectric constant method ('FE97') for
            condition not on steam saturation curve is as follows. Default method is 'JN91' \n
            [logK, Rxn] = calclogKAbOr(XAb, TC, P, dbacessdic, Dielec_method = 'FE97')
    """
    if type(P) == str:
        if P == 'T':
            P, _, _ = iapws95(T = TC)
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = TC   # Assign first input T to pressure in bar
            TC, _, _ = iapws95(P = P)
            TC = TC - 273.15 # convert from K to celcius

    if np.ndim(TC) == 0 :
        TC = np.array(TC).ravel()
    elif np.size(TC) == 2:
        TC = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(TC[0], TC[-1], 8)])

    if np.size(P) <= 2:
        P = np.ravel(P)
        P = P[0]*np.ones(np.size(TC))

    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                E = dielec_FE97(TC, P)[0]
            elif Dielec_method == 'JN91':
                E = dielec_JN91(TC, P)[0]
        else:
            # default method: 'JN91'
            E = dielec_JN91(TC, P)[0]
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    if Dielec_method is None:
        Dielec_method = 'JN91'

    Tref = 25; Pref = 1
    # if no reference Temperature and Pressure is found, append to the bottom
    if any((TC == Tref) & (P == Pref)) == False:
        [rhoref, dGH2Oref, _, _, _, _, _] = iapws95(T = Tref, P = Pref)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                Eref = dielec_FE97(Tref, Pref)[0]
            elif Dielec_method == 'JN91':
                Eref = dielec_JN91(Tref, Pref)[0]
        else:
            # default method: 'JN91'
            Eref = dielec_JN91(Tref, Pref)[0]
        TC = np.concatenate([TC, np.ravel(Tref)])
        P = np.concatenate([P, np.ravel(Pref)])
        rho = np.concatenate([rho, np.ravel(rhoref)])
        E = np.concatenate([E, np.ravel(Eref)])
        dGH2O = np.concatenate([dGH2O, np.ravel(dGH2Oref)])
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    TK=celsiusToKelvin(TC)
    Rxn = {}
    Rxn['type'] = 'Alk feldspar'
    XOr = 1 - XAb
    nNa = XAb
    nK = (1 - XAb)
    nH = 4
    nAl = 1
    nH2O = 2
    nSi = 3
    if XAb == 1:
        Rxn['name'] ='Albite'
        Rxn['formula'] ='NaAlSi3O8'
    elif XAb == 0:
        Rxn['name'] ='K-Feldspar'
        Rxn['formula'] ='KAlSi3O8'
    else:
        Rxn['name'] = 'Alkfeld%d' % (XAb*100)
        Rxn['formula'] = 'Na%1.2f' % nNa + 'K%1.2f' % nK + 'Al%1.2f' % nAl + 'Si%1.2f' % nSi + 'O8'

    Rxn['MW'] = nNa* MW['Na'] + nK*MW['K'] + nAl*MW['Al'] + nSi*MW['Si'] + 8*MW['O']
    R = 1.9872041
    WH = 23800/J_to_cal
    WS = 0/J_to_cal

    dGAl = supcrtaq(TC, P, dbacessdic['Al+++'], Dielec_method = Dielec_method, **rhoEG)
    dGSiO2aq  = supcrtaq(TC, P, dbacessdic['SiO2(aq)'], Dielec_method = Dielec_method, **rhoEG)

    dGH = supcrtaq(TC, P, dbacessdic['H+'], Dielec_method = Dielec_method, **rhoEG)
    dGNa = supcrtaq(TC, P, dbacessdic['Na+'], Dielec_method = Dielec_method, **rhoEG)
    dGK = supcrtaq(TC, P, dbacessdic['K+'], Dielec_method = Dielec_method, **rhoEG)

    if (XAb < 1) & (XAb != 0):
        Sconf = -R*(XOr*np.log(XOr) + XAb*np.log(XAb))
    else:
        Sconf = 0

    dGalk = XOr*dbacessdic['ss_K-feldspar'][2] + XAb*dbacessdic['ss_Albite_high'][2]
    Salk = XOr*dbacessdic['ss_K-feldspar'][4] + XAb*dbacessdic['ss_Albite_high'][4] + Sconf
    Valk = (XOr*dbacessdic['ss_K-feldspar'][5] + XAb*dbacessdic['ss_Albite_high'][5]) #*(P - 1)
    Cpalk = [a + b for a, b in zip([XOr*x for x in dbacessdic['ss_K-feldspar'][6:11] ],
                                   [XAb*y for y in dbacessdic['ss_Albite_high'][6:11] ])]
    WG = WH - TK*WS
    Gex = WG*XAb*XOr
    dGalk = dGalk + Gex - 298.15*Sconf
    Rxn['min'] = [dGalk, np.nan, Salk, Valk] + Cpalk
    Rxn['min'].insert(0, Rxn['formula'])
    Rxn['min'].insert(1,' R&H95, A&S99')
    alk = Rxn['min']
    dGalkTP, _ = heatcapusgscal(TC, P, alk)
    coeff = [-nH, nAl, nNa, nK, nSi, nH2O]
    spec = ['H+', 'Al+++', 'Na+', 'K+', 'SiO2(aq)', 'H2O']
    Rxn['spec'] = [x for x, y in zip(spec, coeff) if y!=0]
    Rxn['coeff'] = [y for y in coeff if y!=0]
    Rxn['nSpec'] = len(Rxn['coeff'])

    dGrxn = - dGalkTP - nH*dGH + nAl*dGAl + nH2O*dGH2O + nNa*dGNa+ nK*dGK + nSi*dGSiO2aq
    logKalkfeld = (-dGrxn/R/(TK)/np.log(10))   # np.log10(np.exp(-dGrxn/R/(TK)))

    Rxn['min'][2] = dGalk[(TC == Tref) & (P == Pref)][0]
    Rxn['V'] = Rxn['min'][5]  #,'%8.3f')
    Rxn['source'] = ' R&H95, A&S99'
    elements  = ['%.4f' % nNa, 'Na', '%.4f' % nK, 'K', '%.4f' % nAl, 'Al', '%.4f' % nSi, 'Si', '8.0000', 'O']
    filters = [y for x, y in enumerate(elements) if x%2 == 0 and float(y) == 0]
    filters = [[x, x+1] for x, y in enumerate(elements) if y in filters]
    filters = [num for elem in filters for num in elem]
    Rxn['elements'] = [y for x, y in enumerate(elements) if x not in filters]

    if (TC[-1] == Tref) & (P[-1] == Pref):
        logKalkfeld = logKalkfeld[:-1]

    return logKalkfeld, Rxn

def calclogKBiotite( XPh, TC, P, dbacessdic, Dielec_method = None, **rhoEG):
    """
    This function calculates thermodynamic properties of solid solution of Biotite minerals

    Parameters
    ----------
        XAb        : volume fraction of Albite  \n
        TC         : temperature [°C]  \n
        P          : pressure [bar]  \n
        dbacessdic : dictionary of species from direct-access database  \n
        Dielec_method   : specify either 'FE97' or 'JN91' as the method to calculate
                        dielectric constant (optional), if not specified default - 'JN91'
        rhoEG      : dictionary of water properties like  density (rho),
                       dielectric factor (E) and Gibbs Energy  (optional)
    Returns
    -------
        logKalkfeld: logarithmic K values   \n
        Rxn        : dictionary of reaction thermodynamic properties
    Usage
    -------
        The general usage of calclogKAnAb without the optional argument is as follows:  \n
        (1) Not on steam saturation curve:  \n
            [logK, Rxn] = calclogKAbOr(XAb, TC, P, dbacessdic),  \n
            where T is temperature in celsius and P is pressure in bar;
        (2) On steam saturation curve:  \n
            [logK, Rxn] = calclogKAbOr(XAb, TC, 'T', dbacessdic),   \n
            where T is temperature in celsius, followed with a quoted char 'T'  \n
            [logK, Rxn] = calclogKAbOr(XAb, P, 'P', dbacessdic), \n
            where P is pressure in bar, followed with a quoted char 'P'.
        (3) Meanwhile, usage with any specific dielectric constant method ('FE97') for
            condition not on steam saturation curve is as follows. Default method is 'JN91' \n
            [logK, Rxn] = calclogKAbOr(XAb, TC, P, dbacessdic, Dielec_method = 'FE97')
    """
    if type(P) == str:
        if P == 'T':
            P, _, _ = iapws95(T = TC)
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = TC   # Assign first input T to pressure in bar
            TC, _, _ = iapws95(P = P)
            TC = TC - 273.15 # convert from K to celcius

    if np.ndim(TC) == 0 :
        TC = np.array(TC).ravel()
    elif np.size(TC) == 2:
        TC = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(TC[0], TC[-1], 8)])

    if np.size(P) <= 2:
        P = np.ravel(P)
        P = P[0]*np.ones(np.size(TC))

    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                E = dielec_FE97(TC, P)[0]
            elif Dielec_method == 'JN91':
                E = dielec_JN91(TC, P)[0]
        else:
            # default method: 'JN91'
            E = dielec_JN91(TC, P)[0]
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    if Dielec_method is None:
        Dielec_method = 'JN91'

    Tref = 25; Pref = 1
    # if no reference Temperature and Pressure is found, append to the bottom
    if any((TC == Tref) & (P == Pref)) == False:
        [rhoref, dGH2Oref, _, _, _, _, _] = iapws95(T = Tref, P = Pref)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                Eref = dielec_FE97(Tref, Pref)[0]
            elif Dielec_method == 'JN91':
                Eref = dielec_JN91(Tref, Pref)[0]
        else:
            # default method: 'JN91'
            Eref = dielec_JN91(Tref, Pref)[0]
        TC = np.concatenate([TC, np.ravel(Tref)])
        P = np.concatenate([P, np.ravel(Pref)])
        rho = np.concatenate([rho, np.ravel(rhoref)])
        E = np.concatenate([E, np.ravel(Eref)])
        dGH2O = np.concatenate([dGH2O, np.ravel(dGH2Oref)])
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    TK=celsiusToKelvin(TC)
    Rxn = {}
    Rxn['type'] = 'Biotite'
    XAn = 1 - XPh
    nFe = 3*XAn
    nMg = 3*XPh
    nK = 1
    nH = 10
    nAl = 1
    nH2O = 6
    nSi = 3
    if XPh == 0:
        Rxn['name'] ='Annite'
        Rxn['formula'] ='KFe3AlSi3O10(OH)2'
    elif XPh == 1:
        Rxn['name'] ='Phlogopite'
        Rxn['formula'] ='KMg3AlSi3O10(OH)2'
    else:
        Rxn['name'] = 'Biotite_Mg_Fe_ratio%s' % np.round(nMg/nFe, 2)
        Rxn['formula'] = 'K%1.2f' % nK + 'Mg%1.2f' % nMg + 'Fe%1.2f' % nFe + 'Al%1.2f' % nAl + 'Si%1.2f' % nSi + 'O10(OH)2'

    Rxn['MW'] = nK*MW['K'] + nMg* MW['Mg'] + nFe* MW['Fe'] + nAl*MW['Al'] + nSi*MW['Si'] + 12*MW['O'] + 2*MW['H']
    R = 1.9872041
    WH = 9000/J_to_cal
    WS = 0/J_to_cal

    dGAl = supcrtaq(TC, P, dbacessdic['Al+++'], Dielec_method = Dielec_method, **rhoEG)
    dGSiO2aq  = supcrtaq(TC, P, dbacessdic['SiO2(aq)'], Dielec_method = Dielec_method, **rhoEG)

    dGH = supcrtaq(TC, P, dbacessdic['H+'], Dielec_method = Dielec_method, **rhoEG)
    dGMg = supcrtaq(TC, P, dbacessdic['Mg++'], Dielec_method = Dielec_method, **rhoEG)
    dGFe = supcrtaq(TC, P, dbacessdic['Fe++'], Dielec_method = Dielec_method, **rhoEG)
    dGK = supcrtaq(TC, P, dbacessdic['K+'], Dielec_method = Dielec_method, **rhoEG)

    if (XAn < 1) & (XAn != 0):
        Sconf = -3*R*(XAn*np.log(XAn) + XPh*np.log(XPh))
    else:
        Sconf = 0

    dGbiot = XAn*dbacessdic['ss_Annite'][2] + XPh*dbacessdic['ss_Phlogopite'][2]
    Sbiot = XAn*dbacessdic['ss_Annite'][4] + XPh*dbacessdic['ss_Phlogopite'][4] + Sconf
    Vbiot = (XAn*dbacessdic['ss_Annite'][5] + XPh*dbacessdic['ss_Phlogopite'][5])
    Cpbiot = [a + b for a, b in zip([XAn*x for x in dbacessdic['ss_Annite'][6:11] ],
                                   [XPh*y for y in dbacessdic['ss_Phlogopite'][6:11] ])]
    WG = WH - TK*WS
    Gex = WG*XAn*XPh
    dGbiot = dGbiot + Gex - 298.15*Sconf
    Rxn['min'] = [dGbiot, np.nan, Sbiot, Vbiot] + Cpbiot
    Rxn['min'].insert(0, Rxn['formula'])
    Rxn['min'].insert(1,' R&H95, P&H99')
    biot = Rxn['min']
    dGbiotTP, _ = heatcapusgscal(TC, P, biot)
    coeff = [-nH, nAl, nK, nMg, nFe, nSi, nH2O]
    spec = ['H+', 'Al+++', 'K+', 'Mg++', 'Fe++', 'SiO2(aq)', 'H2O']
    Rxn['spec'] = [x for x, y in zip(spec, coeff) if y!=0]
    Rxn['coeff'] = [y for y in coeff if y!=0]
    Rxn['nSpec'] = len(Rxn['coeff'])

    dGrxn = - dGbiotTP - nH*dGH + nAl*dGAl + nMg*dGMg + nFe*dGFe + nK*dGK + nSi*dGSiO2aq + nH2O*dGH2O
    logKbiot = (-dGrxn/R/(TK)/np.log(10))   # np.log10(np.exp(-dGrxn/R/(TK)))

    Rxn['min'][2] = dGbiot[(TC == Tref) & (P == Pref)][0]
    Rxn['V'] = Rxn['min'][5]
    Rxn['source'] = ' R&H95, P&H99'
    elements  = ['%.4f' % nK, 'K', '%.4f' % nMg, 'Mg', '%.4f' % nFe, 'Fe', '%.4f' % nAl, 'Al', '%.4f' % nSi, 'Si', '12.0000', 'O', '2.0000', 'H']
    filters = [y for x, y in enumerate(elements) if x%2 == 0 and float(y) == 0]
    filters = [[x, x+1] for x, y in enumerate(elements) if y in filters]
    filters = [num for elem in filters for num in elem]
    Rxn['elements'] = [y for x, y in enumerate(elements) if x not in filters]

    if (TC[-1] == Tref) & (P[-1] == Pref):
        logKbiot = logKbiot[:-1]

    return logKbiot, Rxn

def calcRxnlogK( TC, P, Prod, dbacessdic, sourcedic, specielist, Dielec_method = None,
                sourceformat = None, **rhoEG):
    """
    This function calculates logK values of any reaction \n
    Parameters
    ----------
        TC          : temperature [°C] \n
        P           : pressure [bar] \n
        Prod        : Product species of the reaction \n
        dbacessdic  : direct-acess database dictionary \n
        sourcedic   : source database reactions dictionary \n
        specielist  : source database species grouped into
                        [element, basis, redox, aqueous, minerals, gases, oxides] \n
        Dielec_method    : specify either 'FE97' or 'JN91' as the method
                        to calculate dielectric constant, default is 'JN91' \n
        sourceformat: source database format, either 'GWB' or 'EQ36', default is 'GWB'
        rhoEG       : dictionary of water properties like  density (rho),
                        dielectric factor (E) and Gibbs Energy  (optional) \n
    Returns
    -------
        logK        : logarithmic K value(s)  \n
        dGrxn       : Total reaction Gibbs energy [cal/mol] \n
        dGP         : Product specie Gibbs energy [cal/mol] \n
        dGRs        : Reactant species Gibbs energy [cal/mol] \n
    Usage
    -------
        The general usage of calcRxnlogK without the optional argument is as follows:  \n
        (1) Not on steam saturation curve:  \n
            [logK, dGrxn, dGP, dGRs] = calcRxnlogK(TC, P, Prod, dbacessdic, sourcedic, specielist),  \n
            where T is temperature in celsius and P is pressure in bar;
        (2) On steam saturation curve:  \n
            [logK, dGrxn, dGP, dGRs] = calcRxnlogK(TC, 'T', Prod, dbacessdic, sourcedic, specielist),   \n
            where T is temperature in celsius, followed with a quoted char 'T'  \n
            [logK, dGrxn, dGP, dGRs] = calclogKDiHedEnFe(P, 'P', Prod, dbacessdic, sourcedic, specielist), \n
            where P is pressure in bar, followed with a quoted char 'P'.
        (3) Meanwhile, usage with any specific dielectric constant method ('FE97') for
            condition not on steam saturation curve is as follows. Default method is 'JN91' \n
            [logK, dGrxn, dGP, dGRs] = calcRxnlogK( TC, P, Prod, dbacessdic, sourcedic, specielist, Dielec_method = 'FE97')
    """
    if type(P) == str:
        if P == 'T':
            P, _, _ = iapws95(T = TC)
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = TC   # Assign first input T to pressure in bar
            TC, _, _ = iapws95(P = P)
            TC = TC - 273.15 # convert from K to celcius

    if np.ndim(TC) == 0 :
        TC = np.array(TC).ravel()
    elif np.size(TC) == 2:
        TC = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(TC[0], TC[-1], 8)])

    if np.size(P) <= 2:
        P = np.ravel(P)
        P = P[0]*np.ones(np.size(TC))

    R = 1.9872041 # cal/mol/K
    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                E = dielec_FE97(TC, P)[0]
            elif Dielec_method == 'JN91':
                E = dielec_JN91(TC, P)[0]
        else:
            # default method: 'JN91'
            E = dielec_JN91(TC, P)[0]
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}


    if Dielec_method is None:
        Dielec_method = 'JN91'
    TK = celsiusToKelvin(TC)

    if sourceformat == 'EQ36':
        rxnspecies = [j for k,j in enumerate(sourcedic[Prod]) if k not in [2,3]]
    elif (sourceformat == None) or (sourceformat == 'GWB'):
        rxnspecies = sourcedic[Prod]

    if Prod == 'e-' or Prod == 'eh':
        dGP = 0
    elif Prod == 'H2O':
        dGP = dGH2O
    elif Prod in ['Hydroxyapatite', 'Fluorapatite', 'Ankerite', 'Acmite'] or Prod.startswith('ss_'):
        dGP, _ = heatcapusgscal(TC, P, dbacessdic[Prod])
    elif Prod in specielist[4]+specielist[5]+specielist[6]:
        dGP, _ = heatcap(TC, P, dbacessdic[Prod.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')])
    else:
        dGP  = supcrtaq(TC, P, dbacessdic[Prod.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')],
                        Dielec_method = Dielec_method, **rhoEG)

    total_reactants = int(len(rxnspecies[2:])/2)
    dGRs = 0
    for i in range(total_reactants):
        R_coeff = float(rxnspecies[2+2*i])
        R_specie = rxnspecies[4 + 2*i - 1]

        if R_specie == 'e-' or R_specie == 'eh':
            dGR = 0
        elif R_specie == 'H2O':
            dGR = dGH2O
        elif R_specie in specielist[4] + specielist[5] + specielist[6]:
            dGR, _ = heatcap(TC, P,
                             dbacessdic[R_specie.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')])
        else:
            dGR  = supcrtaq(TC, P, dbacessdic[R_specie.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')],
                            Dielec_method = Dielec_method, **rhoEG)
        dGRs = dGRs + R_coeff*dGR

    dGrxn = -dGP + dGRs
    logK = (-dGrxn/R/(TK)/np.log(10))      #np.log10(np.exp(-dGrxn/R/(TK)))

    return logK, dGrxn, dGP, dGRs

def densitylogKextrap(TC, P, spectype, *elem, **kwargs ):
    """
    This function calculates logK values extrapolation for conditions where rho < 350kg/m3  \n
    Parameters
    ----------
        TC          : temperature [°C] \n
        P           : pressure [bar] \n
        spectype    : specify the type of species for density-logK extrapolation calculation,
                        either the Product species of any reaction or solid solutions or clay like
                        'AnAb' or 'AbOr' or 'FoFa' or 'EnFe' or 'DiHedEnFe' or 'clay' or  \n
        dbacessdic  : direct-acess database dictionary \n
        rhoEGextrap : dictionary of water properties like  density (rho), dielectric factor (E) and
                        Gibbs Energy for density region 350-550kg/m3 \n
        elem        : list containing nine parameters with clay names and elements compositions with
                        the following format
                        ['Montmorillonite_Lc_MgK', 'Si', 'Al', 'FeIII', 'FeII', 'Mg', 'K', 'Na', 'Ca', 'Li'] \n
        group       : specify the structural layering of the phyllosilicate, for layers composed of
                        1 tetrahedral + 1 octahedral sheet (1:1 layer) - specify '7A',
                        2 tetrahedral + 1 octahedral sheet (2:1 layer) - specify '10A', or
                        the latter with a brucitic sheet in the interlayer (2:1:1 layer)  - specify '14A'
                        (optional), if not specified, default is '10A' for smectites, micas, et cetera \n
        X           : volume fractions of any (Anorthite, Albite, Forsterite, Enstatite) or mole fraction of Mg \n
        nCa         : number of moles of Ca in formula unit (=1 for Di, Hed), must be greater than zero  \n
        sourcedic   : source database reactions dictionary \n
        specielist  : source database species grouped into
                        [element, basis, redox, aqueous, minerals, gases, oxides] \n
        Dielec_method    : specify either 'FE97' or 'JN91' as the method
                        to calculate dielectric constant, default is 'JN91' \n
        sourceformat: source database format, either 'GWB' or 'EQ36'
    Returns
    -------
        logK        : extrapolated logarithmic K value(s)  \n
    """

    kwargs = dict({"group": None,  "X": None,  "Ca": None, 'rhoEGextrap': None,
                  "sourcedic": None,   "specielist": None, 'dbacessdic': None,
                  "Dielec_method": None,   "sourceformat": None
                  }, **kwargs)
    group = kwargs['group'];                    X = kwargs['X']
    Ca = kwargs['Ca'];                          sourceformat = kwargs['sourceformat']
    specielist = kwargs['specielist'];          sourcedic = kwargs['sourcedic']
    Dielec_method = kwargs['Dielec_method'];    rhoEGextrap = kwargs['rhoEGextrap']

    if np.ndim(TC) == 0 :
        TC = np.array(TC).ravel()

    if np.ndim(P) == 2:
        P = np.ravel(P).ravel()

    if kwargs['dbacessdic'] == None:
        dbaccess = './default_db/speq20.dat'
        dbaccess = os.path.join(os.path.dirname(os.path.abspath(__file__)), dbaccess)
        dbacessdic, _ = readAqspecdb(dbaccess)
    else:
        dbacessdic = kwargs['dbacessdic']

    if rhoEGextrap is None:
        [rho, dGH2O, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                Dielec_method = 'FE97'
                E = dielec_FE97(TC, P)[0]
            elif Dielec_method == 'JN91':
                Dielec_method = 'JN91'
                E = dielec_JN91(TC, P)[0]
        else:
            # default method: 'JN91'
            Dielec_method = 'JN91'
            E = dielec_JN91(TC, P)[0]
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}
        # Calculate the rho E G for density extrapolation method here so we have it below
        rhoEGextrap = {}
        if any(rhoEG['rho'] < 350):
            subBornptrs = rhoEG['rho'] < 350
            for i, j in enumerate(zip(TC[subBornptrs], P[subBornptrs])):
                rhoextrap = np.linspace(350, 550, 3)
                Pextrap = iapws95(T = j[0], rho = rhoextrap)[0]
                Textrap = j[0]*np.ones(np.size(Pextrap))

                dGH2O = iapws95(T = Textrap, P = Pextrap)[1]
                if Dielec_method == 'FE97':
                    E = dielec_FE97(Textrap, rhoextrap)[0]
                elif Dielec_method == 'JN91':
                    E = dielec_JN91(Textrap, Pextrap)[0]
                rhoextrap = np.around(rhoextrap, 3)
                rhoEGextrap['%d_%d' % (j[0], j[1])]= {'rho': rhoextrap,'E': E, 'dGH2O': dGH2O,
                                                      'Textrap': Textrap, 'Pextrap': Pextrap}

    #print(rhoEGextrap, TC, P)
    length = len(TC) if np.ndim(TC) != 0 else 1
    TC = np.ravel(TC) if np.ndim(TC) == 0 else TC
    P = np.ravel(P) if np.ndim(P) == 0 else P
    logK = np.nan*np.ones(np.size(TC))
    for i in range(length): #need a for loop because calculation is T-specific
        rhotarget =  iapws95(T = TC[i], P = P[i])[0]
        rhotarget = rhotarget/1000     # kg/m3 => g/cm^3
        Pextrap = rhoEGextrap['%d_%d' % (TC[i], P[i])]['Pextrap']
        Textrap = rhoEGextrap['%d_%d' % (TC[i], P[i])]['Textrap']
        rhoextrap = rhoEGextrap['%d_%d' % (TC[i], P[i])]['rho']
        rhoextrap = rhoextrap/1000     # kg/m3 => g/cm^3
        rhoextrap = np.around(rhoextrap,3)
        rhoEGextrap_only = rhoEGextrap['%d_%d' % (TC[i], P[i])]
        logrho = np.log10(rhoextrap)
        if spectype.lower() == 'anab':
            logKextrap = calclogKAnAb(X, Textrap, Pextrap, dbacessdic,
                                      Dielec_method = Dielec_method, **rhoEGextrap_only)[0]
        elif spectype.lower() == 'abor':
            logKextrap = calclogKAbOr(X, Textrap, Pextrap, dbacessdic,
                                      Dielec_method = Dielec_method, **rhoEGextrap_only)[0]
        elif spectype.lower() == 'dihedenfe':
            logKextrap = calclogKDiHedEnFe(Ca, X, Textrap, Pextrap, dbacessdic,
                                      Dielec_method = Dielec_method, **rhoEGextrap_only)[0]
        elif spectype.lower() == 'fofa':
            logKextrap = calclogKFoFa(X, Textrap, Pextrap, dbacessdic,
                                      Dielec_method = Dielec_method, **rhoEGextrap_only)[0]
        elif spectype.lower() == 'enfe':
            logKextrap = calclogKEnFe(X, Textrap, Pextrap, dbacessdic,
                                      Dielec_method = Dielec_method, **rhoEGextrap_only)[0]
        elif spectype.lower() == 'clay':
            logKextrap = calclogKclays(Textrap, Pextrap, dbacessdic, *elem, group = group,
                                      Dielec_method = Dielec_method, **rhoEGextrap_only)[0]
        else:
            logKextrap = calcRxnlogK( Textrap, Pextrap, spectype, dbacessdic, sourcedic, specielist,
                                     Dielec_method = Dielec_method, sourceformat = sourceformat,
                                     **rhoEGextrap_only)[0]
        p = np.polyfit(logrho, logKextrap, 1)
        logK[i] = np.polyval(p, np.log10(rhotarget))

    return logK

def outputfmt(fid, logK, Rxn, *T, dataset = None, logK_form = None):
    """
    This function writes logK and Rxn data to any file using GWB, EQ36, Pflotran and ToughReact format   \n
    Parameters
    ----------
        fid         : file ID \n
        logK        : logarithmic K value(s) \n
        Rxn         : dictionary of reaction thermodynamic properties \n
        dataset     : specify the dataset format, either 'GWB', 'EQ36', 'Pflotran' or 'ToughReact' \n
        logK_form   : specify the format of logK either as a set of eight values one for each
                        of the dataset’s principal temperatures, or blocks of polynomial coefficients,
                        [values, polycoeffs], default is 'a set of eight values' (optional)   \n
    Returns
    -------
        Sends data to the file with filename described in fid with any format mentioned above.
    Usage
    -------
     Example:
       outputfmt(open('logKdata.txt','w'), logK, Rxn, dataset = 'GWB') send output to text file in GWB format

    """

    if len(T) == 0:
        TK = 25*np.ones(np.size(logK))
    else:
        TK = np.asarray(T).ravel()
    logK_form = 'values' if logK_form is None else logK_form.lower()

    # %% Open the text file.
    if dataset.lower() == 'gwb':
        fid.writelines("%s                       " % Rxn['name'])
        fid.writelines( "%s= " %  list(Rxn.keys())[0])
        if Rxn['type'].find('plag') == 0:
            fid.writelines( "plagioclase\n")
        else:
            fid.writelines( "%s\n" %  Rxn['type'])
        fid.writelines( "     %s= " %  list(Rxn.keys())[2])
        fid.writelines( "%s\n" %  Rxn['formula'])
        fid.writelines( "     mole vol.=   %1.3f cc" %  Rxn['V'])
        fid.writelines( "      mole wt.=  %1.4f g\n" %  Rxn['MW'])
        fid.writelines( "     %s species in reaction\n" %  Rxn['nSpec'])
        for i in range(len(Rxn['spec'])):
            i = i + 1
            fid.writelines( "%9.4f " %  Rxn['coeff'][i-1])
            fid.writelines( "%-9s          " %  Rxn['spec'][i-1])
            if (i % 3 == 0) | (i % 6 == 0) | (i == len(Rxn['spec'])):
                fid.writelines( "\n")

        if logK_form == 'polycoeffs':
            Tr = 298.15
            logKfunc = lambda TK, *x: x[0] + x[1]*(TK - Tr) + x[2]*(TK**2 - Tr**2) +  x[3]*((1/TK) - (1/Tr)) + \
                x[4]*((1/TK**2) - (1/Tr**2)) + x[5]*np.log(TK/Tr)
            x0 = [-31.9605, 20.6576, 3.73497e-2, -9.01862, 6.0111, 2.5]
            logKcorr = curve_fit(logKfunc, TK[logK!=500].ravel(), logK[logK!=500].ravel(), p0 = x0,  maxfev = 1000000)[0]
            fid.writelines('     a= %15.6f   ' % logKcorr[0] + 'b= %15.6f   ' % logKcorr[1] + \
                            'c= %15.5f\n' % logKcorr[2])
            fid.writelines('     d= %15.2f   ' % logKcorr[3] + 'e= %15.2f   ' % logKcorr[4] + \
                            'f= %15.3f \n' % logKcorr[5])
            fid.writelines('     TminK= %-15.2f ' % np.min(TK) + 'TmaxK= %-7.2f\n' % np.max(TK))
        else:
            for i in range(len(logK)):
                i = i + 1
                if (i == 1) | (i == 5) | (i == 9):
                    fid.writelines("       %9.4f" %  logK[i-1])
                else:
                    fid.writelines("  %9.4f" %  logK[i-1])
                if (i % 4 == 0) | (i == 8) | (i == len(logK)):
                    fid.writelines( "\n")

        fid.writelines( "*    gflag = 1 [reported delG0f used]\n" )
        if Rxn['type'].find('plag') == 0:
            fid.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
        else:
            fid.writelines( "*    extrapolation algorithm: supcrt92/water95\n" )
        if Rxn['type'].find('serp') == 0:
            fid.writelines( "*    reference-state data source = Blanc et al 2015\n" )
        elif 'source' in Rxn:
            fid.writelines( "*    reference-state data source = %s\n" % Rxn['source'])
        else:
            fid.writelines( "*    reference-state data source = supcrt92?\n" )

        fid.write( "*         delG0f =   %8.3f  kcal/mol\n" % (Rxn['min'][2]/1000) )
        if Rxn['type'] == 'Smectites':
            fid.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (Rxn['min'][3]/1000) )
        else:
            fid.writelines( "*         delH0f =   NaN  kcal/mol\n")
        fid.writelines( "*         S0PrTr =   %8.3f  cal/mol\n" % Rxn['min'][4])
        fid.writelines( "\n")
    elif dataset.lower() == 'eq36':
        fid.writelines('%-25s %s \n' % (Rxn['name'], Rxn['formula']))
        fid.writelines('     sp.type =  solid\n')
        fid.writelines('*    EQ3/6   =  com, alt, sup\n')
        fid.writelines('     revised =  01-Jan-2020\n')
        fid.writelines('*    mol.wt. =%8.3f g/mol\n' % Rxn['MW'])
        fid.writelines('     V0PrTr  = %8.3f cm**3/mol [source: %s ]\n' % (Rxn['V'], Rxn['source']))
        fid.writelines('****\n')
        fid.writelines( "     %s element(s):\n" % int(len(Rxn['elements'])/2))
        for i in range(len(Rxn['elements'])):
            i = i + 1
            if (i == 1) | (i == 7) | (i == 13):
                fid.writelines( "    %9.4f " %  float(Rxn['elements'][i - 1]))
            elif i % 2 != 0:
                fid.writelines( "%9.4f " %  float(Rxn['elements'][i - 1]))
            else:
                fid.writelines( "%-9s     " %  (Rxn['elements'][i - 1]))
            if (i % 6 == 0) | (i == len(Rxn['elements'])):
                fid.writelines( "\n")
        fid.writelines('****\n')
        fid.writelines( "     %s species in reaction:\n" % (Rxn['nSpec'] + 1))
        fid.writelines( "  %9.4f " %  (-1))
        fid.writelines( " %-21s     " %  Rxn['name'])
        for i in range(len(Rxn['spec'])):
            i = i + 1
            fid.writelines( "  %9.4f " %  Rxn['coeff'][i-1])
            fid.writelines( " %-21s     " %  Rxn['spec'][i-1])
            if (i % 2 != 0) | (i == len(Rxn['spec'])):
                fid.writelines( "\n")
        fid.writelines('*\n')
        fid.writelines('**** logK grid [T, P @ Miscellaneous parameters]\n')
        for i in range(len(logK)):
            i = i + 1
            if (i == 1) | (i == 5):
                fid.writelines( "      %9.4f" %  logK[i-1])
            else:
                fid.writelines( "  %9.4f" %  logK[i-1])
            if (i % 4 == 0) | (i == len(logK)):
                fid.writelines( "\n")
        fid.writelines( "*    gflag = 1 [reported delG0f used]\n" )
        if Rxn['type'].find('plag') == 0:
            fid.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
        else:
            fid.writelines( "*    extrapolation algorithm: supcrt92/water95\n" )
        if Rxn['type'].find('serp') == 0:
            fid.writelines( "*    ref-state data  [source:   Blanc et al 2015 ]\n" )
        elif 'source' in Rxn:
            fid.writelines( "*    ref-state data  [source:   %s ]\n" % Rxn['source'])
        else:
            fid.writelines( "*    ref-state data  [source:   supcrt92? ]\n" )
        fid.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (Rxn['min'][2]/1000) )
        if Rxn['type'] == 'Smectites':
            fid.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (Rxn['min'][3]/1000) )
        else:
            fid.writelines( "*         delH0f =   NaN  kcal/mol\n")
        fid.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % Rxn['min'][4])
        fid.writelines( "*    Cp coefficients [source:   %s  ]\n" % Rxn['source'])
        fid.writelines( "*         T**0   =   %11.8e  \n" % (Rxn['min'][6]) )
        fid.writelines( "*         T**1   =   %11.8e  \n" % (Rxn['min'][7]))
        if Rxn['min'][8] < 1:
            fid.writelines( "*         T**-2  =  %12.8e  \n" % (Rxn['min'][8]))
        else:
            fid.writelines( "*         T**-2  =   %11.8e  \n" % (Rxn['min'][8]))
        if len(Rxn['min']) > 10:
            fid.writelines( "*         T**-0.5 =   %11.8e  \n" % (Rxn['min'][9]))
            fid.writelines( "*         T**2   =   %11.8e  \n" % (Rxn['min'][10]))
        fid.writelines( "+" + "-"*68 + "\n")
    elif dataset.lower() == 'pflotran':
        list_logk = ' '.join(str("%9.4f" % e) for e in list(logK))
        Rxns_lst = ' '.join([ "%8.4f" % Rxn['coeff'][i]+' ' + Rxn['spec'][i]
                             for i in range(len(Rxn['spec']))])
        info = "'%s'" % Rxn['name'] + ' ' + "%7.3f" % Rxn['V'] + ' ' + str(Rxn['nSpec']) + ' ' + \
            Rxns_lst + ' ' + list_logk + ' ' + "%8.4f" % Rxn['MW']
        fid.writelines('%s\n' % info)
    elif dataset.lower() == 'toughreact':
        logKfunc = lambda TK, *x: x[0]*np.log(TK) + x[1] + x[2]*TK + x[3]*TK**(-1) + x[4]*TK**(-2)
        x0 = [-3.19605e1, 2.06576e2, 3.73497e-2, -9.01862e3, 6.0111e5]
        logKcorr = curve_fit(logKfunc, TK[logK!=500].ravel(), logK[logK!=500].ravel(), p0 = x0,  maxfev = 1000000)[0]
        list_logk = '  '.join(str("%9.4f" % e) for e in list(logK))
        list_logKcorr = ' '.join(str("%.5e" % e) for e in list(logKcorr))
        Rxns_spec = [j.replace('++++', '+4') if j.endswith('++++',0)
                      else j.replace('+++', '+3') if j.endswith('+++',0)
                      else j.replace('++', '+2') if j.endswith('++',0)
                      else j.replace('----', '-4') if j.endswith('----',0)
                      else j.replace('---', '-3') if j.endswith('---',0)
                      else j.replace('--', '-2') if j.endswith('--',0) else j for j in Rxn['spec']]
        Rxn_lst = '  '.join([ "%8.4f" % Rxn['coeff'][i]+' ' + Rxns_spec[i] for i in range(len(Rxn['spec']))])

        info = "%-32s" % Rxn['name'] + "%8.3f" % Rxn['MW'] + " %7.3f" % Rxn['V'] +\
            ' ' + str(Rxn['nSpec']) + ' ' + Rxn_lst
        info = "'%s'" % info[:len(info.split()[0])] + info[len(info.split()[0]) + 2:]
        fid.writelines('%s\n' % info)
        info = '%-35s' % Rxn['name'] + ' ' + list_logk
        info = "'%s'" % info[:len(info.split()[0])] + info[len(info.split()[0]) + 2:]
        fid.writelines('%s\n' % info)
        info = '%-35s' % Rxn['name'] + '  ' + list_logKcorr.replace('e','E')
        info = "'%s'" % info[:len(info.split()[0])] + info[len(info.split()[0]) + 2:]
        fid.writelines('%s\n' % info)

    return

def calclogKclays(TC, P, dbacessdic, *elem, group = None, cation_order = None,
                  Dielec_method = None, Mintype = None, **rhoEG):
    """
    This function calculates logK values and reaction parameters of clay reactions using below references: \n
    References
    ----------
        (1) Blanc, P., Vieillard, P., Gailhanou, H., Gaboreau, S., Gaucher, É., Fialips, C. I.,
            Madé, B & Giffaut, E. (2015). A generalized model for predicting the thermodynamic properties
            of clay minerals. American journal of science, 315(8), 734-780. \n
        (2) Blanc, P., Gherardi, F., Vieillard, P., Marty, N. C. M., Gailhanou, H., Gaboreau, S.,
            Letat, B., Geloni, C., Gaucher, E.C. and Madé, B. (2021). Thermodynamics for clay minerals:
            calculation tools and application to the case of illite/smectite interstratified minerals.
            Applied Geochemistry, 104986.  \n
        (3) Vinograd, V.L., 1995. Substitution of [4]Al in layer silicates: Calculation of the Al-Si
            configurational entropy according to 29Si NMR Spectra. Physics and Chemistry of Minerals
            22, 87-98.
    Parameters
    ----------
        TC              : temperature [°C]  \n
        P               : pressure [bar]  \n
        dbacessdic      : dictionary of species from direct-access database  \n
        elem            : list containing nine parameters with clay names and elements compositions with
                            the following format
                            ['Montmorillonite_Lc_MgK', 'Si', 'Al', 'FeIII', 'FeII', 'Mg', 'K', 'Na', 'Ca', 'Li'] \n
        group           : specify the structural layering of the phyllosilicate, for layers composed of
                            1 tetrahedral + 1 octahedral sheet (1:1 layer) - specify '7A',
                            2 tetrahedral + 1 octahedral sheet (2:1 layer) - specify '10A', or
                            the latter with a brucitic sheet in the interlayer (2:1:1 layer)  - specify '14A'
                            (optional), if not specified, default is '10A' for smectites, micas, et cetera \n
        cation_order    : specify ordering of Si and Al ions either 'Eastonite', 'Ordered',
                            'Random', or 'HDC'  (optional), if not specified, default is based on guidelines
                            by Vinograd (1995) \n
        Dielec_method   : specify either 'FE97' or 'JN91' as the method to calculate
                            dielectric constant (optional), if not specified, default - 'JN91'
        rhoEG           : dictionary of water properties like  density (rho),
                            dielectric factor (E) and Gibbs Energy  (optional)
    Returns
    -------
        logK_clay       : logarithmic K values   \n
        Rxn             : dictionary of reaction thermodynamic properties
    Usage
    -------
        The general usage of calclogKclays without the optional argument is as follows:  \n
        (1) Not on steam saturation curve:  \n
            [logK, Rxn] = calclogKclays(TC, P, dbacessdic, *elem),  \n
            where T is temperature in celsius and P is pressure in bar;
        (2) On steam saturation curve:  \n
            [logK, Rxn] = calclogKclays(TC, 'T', dbacessdic, *elem),   \n
            where T is temperature in celsius, followed with a quoted char 'T'  \n
            [logK, Rxn] = calclogKclays(P, 'P', dbacessdic, *elem), \n
            where P is pressure in bar, followed with a quoted char 'P'.
        (3) Meanwhile, usage with any specific dielectric constant method ('FE97') for
            condition not on steam saturation curve is as follows. Default method is 'JN91' \n
            [logK, Rxn] = calclogKclays(TC, P, dbacessdic, *elem, group = '10A',
                                        cation_order = 'HDC', Dielec_method = 'FE97')
    """

    if type(P) == str:
        if P == 'T':
            P, _, _ = iapws95(T = TC)
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = TC   # Assign first input T to pressure in bar
            TC, _, _ = iapws95(P = P)
            TC = TC - 273.15 # convert from K to celcius

    if np.ndim(TC) == 0 :
        TC = np.array(TC).ravel()
    elif np.size(TC) == 2:
        TC = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(TC[0], TC[-1], 8)])

    if np.size(P) <= 2:
        P = np.ravel(P)
        P = P[0]*np.ones(np.size(TC))

    Dielec_method = 'JN91' if Dielec_method is None else Dielec_method
    group = '10A' if group is None else group
    Mintype = 'Smectites' if Mintype is None else Mintype

    if rhoEG.__len__() != 0:
        rho = rhoEG['rho'].ravel()
        E = rhoEG['E'].ravel()
        dGH2O = rhoEG['dGH2O'].ravel()
    else:
        [rho, dGH2O, _, _, _, P, TC] = iapws95(T = TC, P = P)
        if Dielec_method is not None:
            if Dielec_method == 'FE97':
                E = dielec_FE97(TC, P)[0]
            elif Dielec_method == 'JN91':
                E = dielec_JN91(TC, P)[0]
        else:
            E = dielec_JN91(TC, P)[0] # default method: 'JN91'
        rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    name = '' if len(elem) == 0 else elem[0]
    T_Si = 0 if len(elem) == 0 else float(elem[1]);
    if len(elem) == 0:
        T_Al = 0
    else:
        if group == '7A':
            T_Al = 2 - float(elem[1]) if float(elem[2]) != 0 else 0
        else:
            T_Al = 4 - float(elem[1]) if float(elem[2]) != 0 else 0
    if len(elem) == 0:
        O_Al = 0
    elif (float(elem[2]) > T_Al):
        O_Al = float(elem[2]) - T_Al
    else:
        O_Al = 0
    if len(elem) == 0:
        O_FeIII = 0
        T_FeIII = 0
    elif float(elem[3]) >= 2:
        O_FeIII = float(elem[3])*0.5
        T_FeIII = float(elem[3]) - O_FeIII
    else:
        O_FeIII = float(elem[3])
        T_FeIII = float(elem[3]) - O_FeIII
    O_FeII = 0 if len(elem) == 0 else float(elem[4])

    if len(elem) == 0:
        I_Mg = 0; I_Li = 0
        O_Mg = 0; O_Li = 0
    else:
        if ('Montmorillonite_Lc' in name) or ('Saponite' in name) or ('Nontronite' in name) or ('Beidellite' in name):
            Tot_Oxy = 0.17
        elif 'Montmorillonite_Hc' in name:
            Tot_Oxy = 0.3
        elif ('Illite' in name) or ('Vermiculite' in name):
            Tot_Oxy = 0.43
        else:
            Tot_Oxy = 0.0
        Calc_Oxy = (float(elem[6]) + float(elem[7]))*0.5 + float(elem[8])
        if Calc_Oxy == 0:
            I_Mg = Tot_Oxy if float(elem[5]) != 0 else 0
            I_Li = Tot_Oxy if float(elem[9]) != 0 else 0
        else:
            I_Mg = 0; I_Li = 0
        O_Mg = float(elem[5]) - I_Mg
        O_Li = float(elem[9]) - I_Li
    I_K = 0 if len(elem) == 0 else float(elem[6])
    I_Na = 0 if len(elem) == 0 else float(elem[7])
    I_Ca = 0 if len(elem) == 0 else float(elem[8])
    TK = celsiusToKelvin(TC)

    Interlayer = {'Tot' : {'Cs+' : 0, 'Rb+' : 0, 'K+' : I_K, 'Na+' : I_Na, 'Li+' : I_Li,
                           'H3O+' : 0, 'NH4+' : 0, 'Mn2+' : 0.0, 'Fe2+' : 0, 'Co2+' : 0,
                           'Cu2+' : 0, 'Cd2+' : 0, 'Zn2+' : 0, 'Ba2+' : 0, 'Sr2+' : 0,
                           'Ca2+' : I_Ca, 'Mg2+' : I_Mg}}
    Octahedral = {'Tot' : {'Li+' : O_Li, 'Mg2+': O_Mg, 'Fe2+' : O_FeII, 'Mn2+' : 0,
                           'Ni2+' : 0, 'Co2+' : 0, 'Zn2+' : 0, 'Cd2+' : 0,
                           'VO2+' : 0, 'Al3+' : O_Al, 'Fe3+' : O_FeIII, 'V3+' : 0,
                           'Cr3+' : 0, 'Mn3+' : 0, 'Ti4+' : 0.}}
    Tetrahedral = {'Tot' : {'Si4+' : T_Si, 'Al3+': T_Al, 'Fe3+' : T_FeIII}}
    # initialize site occupancy
    for k  in ['M1','M2']:
        Octahedral[k] = {j : 0 for j in Octahedral['Tot']}
    for k in ['T1','T2']:
        Tetrahedral[k] = {j : 0 for j in Tetrahedral['Tot']}
    Brucitic = {}
    for k in ['M3','M4']:
        Brucitic[k] = {j : 0 for j in Octahedral['Tot']}
    # Mixing constants:
    K = { 'Inter' : -0.856571, 'M2' : -0.579046, 'M3' : -0.107956}
    # Entropy of the elements (CODATA)
    S_elem = {'Si' : 18.81, 'O2' : 205.152, 'H2' : 130.68, 'Al' : 28.3, 'Mg' : 32.67, 'K' : 64.68,
              'Na' : 51.3, 'Ca' : 41.59, 'Fe' : 27.32, 'Li' : 29.12, 'Mn' : 32.01, 'Ni' : 29.87,
              'Co' : 30.04, 'Cs' : 85.23, 'Rb' : 76.78, 'Ba' : 62.48, 'Sr' : 55.69, 'Cu' : 33.15,
              'Cd' : 51.8, 'Zn' : 41.72, 'N2' : 191.6, 'V' : 28.94, 'Cr' : 23.62, 'Ti' : 30.72,
              'B' : 5.9, 'Be' : 9.5}
    # Entropy Configuration for Magnetic spin
    R = 8.31451 #J K​−1 mol−1
    S_spin = {'Sc3+' : 0.00, 'Ti3+' : 1/2, 'Ti4+' : 0.00, 'V3+' : 1, 'Cr3+' : 3/2, 'Mn2+' : 5/2,
              'Mn3+' : 2, 'Fe2+' : 2, 'Fe3+' : 5/2, 'Ni2+' : 1, 'Co2+' : 3/2} # spin quantum number
    S_spin = {i: R*np.log(2*x + 1) for i,x in S_spin.items()} # R*ln(2S+1)    (J/K/mole)
    # Slat, V, Cp, a, b, c for oxides in specific sites
    Tetrahedral['Slat'] = {'SiO2' : 35.94, 'Al2O3': 29.42, 'Fe2O3' : 66.62}
    Tetrahedral['V'] = {'SiO2' : 25.7, 'Al2O3': 46.72, 'Fe2O3' : 27.29}
    Tetrahedral['Cp'] = {'SiO2' : 47.61, 'Al2O3': 80.91, 'Fe2O3' : 101.33}
    Tetrahedral['a'] = {'SiO2' : 14.99, 'Al2O3': -93.35, 'Fe2O3' : 0}
    Tetrahedral['b'] = {'SiO2' : 44, 'Al2O3': 177.61, 'Fe2O3' : 0}
    Tetrahedral['c'] = {'SiO2' : 17.33, 'Al2O3': 107.83, 'Fe2O3' : 0}

    Octahedral['Slat'] = {'Li2O' : 40.8, 'LiOH' : 45.04, 'MgO' : 27.89, 'Mg(OH)2' : 61.12,
                          'FeO' : 55.74, 'Fe(OH)2' : 61.9, 'Al2O3' : 55.02, 'Al(OH)3' : 79.37,
                          'Fe2O3' : 0, 'Fe(OH)3' : 178.75, 'MnO' : 57.15, 'Mn(OH)2' : 81.74,
                          'Cr2O3' : 56.3, 'Cr(OH)3' : 85.95, 'NiO' : 33.03, 'Ni(OH)2' : 42.59,
                          'CoO' : 48.6, 'Co(OH)2' : 91.10, 'ZnO' : 39.79, 'Zn(OH)2' : 79.65,
                          'CdO' : 52.23, 'TiO2' : 44.36}
    Octahedral['V'] = {'Li2O' : 4.26, 'LiOH' : 9.51, 'MgO' : 4.57, 'Mg(OH)2' : 25.91, 'FeO' : 10.59,
                       'Fe(OH)2' : 21.61, 'Al2O3' : 4.91, 'Al(OH)3' : 34.96, 'Fe2O3' : 14.23,
                       'Fe(OH)3' : 24.98, 'MnO' : 10.72, 'Mn(OH)2' : 21.29, 'Cr2O3' : 29.55,
                       'Cr(OH)3' : 27.96, 'NiO' : 7.01, 'Ni(OH)2' : 15, 'CoO' : 9.95,
                       'Co(OH)2' : 19.65, 'ZnO' : 8.65, 'Zn(OH)2' : 23.5, 'CdO' : 13.11, 'TiO2' : 15.24}
    Octahedral['Cp'] = {'Li2O' : 47.48, 'LiOH' : 45.38, 'MgO' : 30.84, 'Mg(OH)2' : 72.19, 'FeO' : 42.35,
                        'Fe(OH)2' : 80.39, 'Al2O3' : 74.89, 'Al(OH)3' : 89.63, 'Fe2O3' : 104.41,
                        'Fe(OH)3' : 103.01, 'MnO' : 42.52, 'Mn(OH)2' : 83.87, 'Cr2O3' : 120.76,
                        'Cr(OH)3' : 111.185, 'NiO' : 45.7, 'Ni(OH)2' : 116.19, 'CoO' : 42.35,
                        'Co(OH)2' : 83.7, 'ZnO' : 36.72, 'Zn(OH)2' : 65.76, 'CdO' : 41.95, 'TiO2' : 51.75}
    Octahedral['a'] = {'Li2O' : 0, 'LiOH' : 0, 'MgO' : 131.03, 'Mg(OH)2' : 71.47, 'FeO' : 141.77,
                       'Fe(OH)2' : 59.74, 'Al2O3' : 299.02, 'Al(OH)3' : 111.51, 'Fe2O3' : 221.92,
                       'Fe(OH)3' : 127.36, 'MnO' : 0, 'Mn(OH)2' : 0, 'Cr2O3' : 0, 'Cr(OH)3' : 0,
                       'NiO' : 0, 'Ni(OH)2' : 0, 'CoO' : 0, 'Co(OH)2' : 0, 'ZnO' : 0, 'Zn(OH)2' : 0,
                       'CdO' : 0, 'TiO2' : 0}
    Octahedral['b'] =  {'Li2O' : 0, 'LiOH' : 0, 'MgO' : -66.740, 'Mg(OH)2' : 66.540, 'FeO' : -64.550,
                         'Fe(OH)2' : 89.87, 'Al2O3' : -100.59, 'Al(OH)3' : 77.25, 'Fe2O3' : -515.68,
                         'Fe(OH)3' : 671.46, 'MnO' : 0, 'Mn(OH)2' : 0, 'Cr2O3' : 0, 'Cr(OH)3' : 0,
                         'NiO' : 0, 'Ni(OH)2' : 0, 'CoO' : 0, 'Co(OH)2' : 0, 'ZnO' : 0, 'Zn(OH)2' : 0,
                         'CdO' : 0, 'TiO2' : 0}
    Octahedral['c'] =  { 'Li2O' : 0, 'LiOH' : 0, 'MgO' : -71.38, 'Mg(OH)2' : -16.99, 'FeO' : -71.27,
                       'Fe(OH)2' : -5.46, 'Al2O3' : -172.58, 'Al(OH)3' : -39.92, 'Fe2O3' : 32.22,
                       'Fe(OH)3' : -199.61, 'MnO' : 0, 'Mn(OH)2' : 0, 'Cr2O3' : 0, 'Cr(OH)3' : 0,
                       'NiO' : 0, 'Ni(OH)2' : 0, 'CoO' : 0, 'Co(OH)2' : 0, 'ZnO' : 0, 'Zn(OH)2' : 0,
                       'CdO' : 0, 'TiO2' : 0}

    Interlayer['Slat'] = {'Cs2O' : 226.01, 'Rb2O' : 204.83,	'K2O' : 157.70,	'Na2O' : 186.15, 'Li2O' : 131.43,
                          'BaO' : 171.12, 'SrO' : 157.1, 'CaO' : 133.08, 'MgO' : 136.99, 'CuO' : 150.06,
                          'CoO' : 162.43, 'NiO' : 148.05, 'ZnO' : 152.54, 'H2O' : 169.65, '(NH4)2O' : 364.5,
                          'CdO' : 160.85}
    Interlayer['V'] = {'Cs2O' : 62.25, 'Rb2O' : 46.40, 'K2O' : 27.26, 'Na2O' : 22.97, 'Li2O' : 4.26,
                       'BaO' : 25.92, 'SrO' : 20.19, 'CaO' : 32.47,	'MgO' : 8.97, 'CuO' : 5.19,	'CoO' : 9.95,
                       'NiO' : 7.01, 'ZnO' : 8.65, 'H2O' : 14.85, '(NH4)2O' : 38.16, 'CdO' : 13.11}
    Interlayer['Cp'] = {'Cs2O' : 77.12,	'Rb2O' : 73.99,	'K2O' : 74.50,	'Na2O' : 70.20,	'Li2O' : 47.48,
                        'BaO' : 47.38,	'SrO' : 44.9,	'CaO' : 42.43,	'MgO' : 35.59,	'CuO' : 37.67,
                        'CoO' : 42.35,	'NiO' : 45.7,	'ZnO' : 36.72,	'H2O' : 74.23,	'(NH4)2O' : 247.63,
                        'CdO' : 41.95}
    Interlayer['a'] = {'Cs2O' : 0,	'Rb2O' : 0,	'K2O' : 148.56,	'Na2O': 43.27,	'Li2O' : 0,	'BaO' : 0,
                       'SrO' : 0,	'CaO' : 63.88,	'MgO' : 0,	'CuO' : 0,	'CoO' : 0,	'NiO' : 0,
                       'ZnO' : 0,	'H2O' : 0,	'(NH4)2O' : 0,	'CdO' : 0}
    Interlayer['b'] = {'Cs2O' : 0,	'Rb2O' : 0,	'K2O' : 24.73,	'Na2O': 395.84,	'Li2O' : 0,	'BaO' : 0,
                       'SrO' : 0,	'CaO' : 315.36,	'MgO' : 0,	'CuO' : 0,	'CoO' : 0,	'NiO' : 0,
                       'ZnO' : 0,	'H2O' : 0,	'(NH4)2O' : 0,	'CdO' : 0}
    Interlayer['c'] = {'Cs2O' : 0,	'Rb2O' : 0,	'K2O' : -72.39,	'Na2O': -80.98,	'Li2O' : 0,	'BaO' : 0,
                       'SrO' : 0,	'CaO' : -102.65,	'MgO' : 0,	'CuO' : 0,	'CoO' : 0,	'NiO' : 0,
                       'ZnO' : 0,	'H2O' : 0,	'(NH4)2O' : 0,	'CdO' : 0}
    # Enthalpy of formation of Oxides
    dH = {'Li2O' : -597.90, 'Na2O' : -414.80, 'K2O' : -363.17, 'Rb2O' : -339,
          'Cs2O' : -345.73, '(NH4)2O' : -234.30,  '(H3O)2O' : -857.49, 'BeO' : -609.40,
          'MgO' : -601.60, 'CaO' : -634.92, 'SrO' : -591.3, 'BaO' : -548.1, 'FeO' : -272.04,
          'MnO' : -385.2, 'CuO' : -156.1, 'CoO' : -237.9, 'NiO' : -239.3, 'CdO' : -258.4,
          'ZnO' : -350.5, 'Fe2O3' : -826.23, 'Al2O3' : -1675.70, 'B2O3' : -1273.5,
          'V2O3' : -1218.8, 'Cr2O3' : -1053.1, 'Mn2O3' : -959, 'SiO2' : -910.70,
          'TiO2' : -944, '(VO2)2O' : -1550.59, 'H2O' : -285.83}
    # Enthalpy of formation for elements in specific sites:
    Interlayer['dH'] = {'Cs+' : 544.22, 'Rb+' : 522.89, 'K+' : 453.0, 'Na+' : 260.0,
                        'Li+' : 14.2, 'H3O+' : -312.730, 'NH4+' : 171.84857190,
                        'Mn2+' : -189.180, 'Fe2+' : -211.8386339, 'Co2+' : -208.9244064,
                        'Cu2+' : -256.2185226, 'Cd2+' : -212.38346050, 'Zn2+' : -222.8680592,
                        'Ba2+' : 70.57, 'Sr2+' : 15.350, 'Ca2+' : -71.23, 'Mg2+' : -147.250,
                        'H2O' : -249.58}
    Octahedral['dH'] = {'Li+' : -110.00, 'Mg2+': -191.72, 'Fe2+' : -230.790, 'Mn2+' : -218.940,
                        'Ni2+' : -237.50, 'Co2+' : -232.590, 'Zn2+' : -242.780,
                        'Cd2+' : -235.07550080,  'VO2+' : -296.25, 'Al3+' : -251.750,
                        'Fe3+' : -290.79, 'V3+' : -280.45, 'Cr3+' : -243.610,
                        'Mn3+' : -281.08, 'Ti4+' : -291.05}
    Brucitic['dH'] = {'Li+' : 66.34, 'Mg2+': -88.98, 'Fe2+' : -216.99, 'Mn2+' : -159.60,
                      'Ni2+' : -197.67, 'Co2+' : -187.60, 'Zn2+' : -208.49,
                      'Cd2+' : -192.6891279,  'VO2+' : -318.16, 'Al3+' : -229.05,
                      'Fe3+' : -288.99, 'V3+' : -285.76,  'Cr3+' : -210.19, 'Mn3+' : -287.05,
                      'Ti4+' : -307.49, 'H2O' : -311.98}
    Tetrahedral['dH'] = {'Si4+' : -285.33, 'Al3+': -260.450, 'Fe3+' : -310.0}

    charge = {'Mn2+' : 2, 'Fe2+' : 2, 'Co2+' : 2, 'Cu2+' : 2, 'Cd2+' : 2, 'V3+' : 3,
              'Zn2+' : 2, 'Ba2+' : 2, 'Sr2+' : 2, 'Ca2+' : 2, 'Mg2+' : 2, 'Fe3+' : 3,
              'Na+' : 1, 'Li+' : 1, 'Cs+' : 1, 'Rb+' : 1, 'K+' : 1, 'Cr3+' : 3,
              'Mn3+' : 3, 'Ti4+' : 4, 'VO2+' : 1, 'Al3+': 3, 'Si4+' : 4,
              'H3O+' : 1, 'NH4+' : 1, 'Ni2+' : 2}

    nCs = Interlayer['Tot']['Cs+']
    nRb = Interlayer['Tot']['Rb+']
    nK = Interlayer['Tot']['K+']
    nNa = Interlayer['Tot']['Na+']
    nCa = Interlayer['Tot']['Ca2+']
    nH3O = Interlayer['Tot']['H3O+']
    nNi = Octahedral['Tot']['Ni2+']
    nTi = Octahedral['Tot']['Ti4+']; nCr = Octahedral['Tot']['Cr3+']
    nV = Octahedral['Tot']['V3+']; nVO = Octahedral['Tot']['VO2+']
    nLi = Interlayer['Tot']['Li+'] + Octahedral['Tot']['Li+']
    nMg = Interlayer['Tot']['Mg2+'] + Octahedral['Tot']['Mg2+']
    nFe = Interlayer['Tot']['Fe2+'] + Octahedral['Tot']['Fe2+']
    nZn = Interlayer['Tot']['Zn2+'] + Octahedral['Tot']['Zn2+']
    nMn = Interlayer['Tot']['Mn2+'] + Octahedral['Tot']['Mn2+']
    nCo = Interlayer['Tot']['Co2+'] + Octahedral['Tot']['Co2+']
    nCd = Interlayer['Tot']['Cd2+'] + Octahedral['Tot']['Cd2+']
    nAl = Tetrahedral['Tot']['Al3+'] + Octahedral['Tot']['Al3+']
    nFe3 = Tetrahedral['Tot']['Fe3+'] + Octahedral['Tot']['Fe3+']
    nSi = Tetrahedral['Tot']['Si4+']

    nFeII = Octahedral['Tot']['Fe2+']
    if group == '10A':
        nAlIV = 4 - nSi
        if nAl > (4 - nSi):
            nAlVI = nAl - nAlIV
        else:
            nAlVI = 0
        nFeIII = nFe3 - np.sum([Tetrahedral[j]['Fe3+']
                               for j in Tetrahedral if j in ['T1', 'T2']])
    elif group == '7A':
        nAlIV = 2 - nSi - np.sum([Tetrahedral[j]['Fe3+']
                                  for j in Tetrahedral if j in ['T1', 'T2']])
        if nAl > (2 - nSi):
            nAlVI = nAl - nAlIV
        else:
            nAlVI = 0
        nFeIII = nFe3 - np.sum([Tetrahedral[j]['Fe3+']
                               for j in Tetrahedral if j in ['T1', 'T2']])
    else:
        nAlIV = 4 - nSi
        if nAl > (4 - nSi):
            nAlVI = nAl - nAlIV
        else:
            nAlVI = 0
        nFeIII = nFe3 - np.sum([Tetrahedral[j]['Fe3+']
                               for j in Tetrahedral if j in ['T1', 'T2']])
    # followed the guidelines after ref (3)
    if cation_order is None:
        if group == '10A' and nAlIV < 0.4:
            cation_order = 'Random'
        elif nAlIV > 1.3:
            cation_order = 'Ordered'
        else:
            cation_order = 'HDC'
    else:
        cation_order = cation_order

    NbO = np.sum( [Octahedral['Tot'][j] for j in Octahedral['Tot']])
    NbO_tri = np.sum([ Octahedral['Tot'][j]
                      for j in list(Octahedral['Tot'].keys())[:9]])
    nbDI = 2/NbO if NbO >= 2 else 1
    if group in ['7A', '10A']:
        if NbO < 2.33:
            TRI = 0
        else:
            TRI = 1
    else:
        TRI = (6 - NbO)/6
    Test_east = 1 if cation_order == 'Eastonite' else 0
    Interlayer['Tot_Remainder'] = 1 - np.sum([Interlayer['Tot'][k]
                                              for k in Interlayer['Tot']])
    # Tetrahedral T1 and T2 site configuration
    for k in list(Tetrahedral['Tot'].keys()):
        if cation_order not in ['Ordered', 'Eastonite']:
            if group == '7A':
                Tetrahedral['T1'][k] = Tetrahedral['Tot'][k]*0.5
                Tetrahedral['T2'][k] = Tetrahedral['Tot'][k] - Tetrahedral['T1'][k]
            else:
                Tetrahedral['T1'][k] = Tetrahedral['Tot'][k]*0.5
                Tetrahedral['T2'][k] = Tetrahedral['Tot'][k] - Tetrahedral['T1'][k]
        else:
            if group == '7A':
                if k == 'Si4+':
                    if Tetrahedral['Tot'][k] < 1:
                        Tetrahedral['T1'][k] = Tetrahedral['Tot'][k]
                    else:
                        Tetrahedral['T1'][k] = 1
                    Tetrahedral['T2'][k] = Tetrahedral['Tot'][k] - Tetrahedral['T1'][k]
                else:
                    if Tetrahedral['Tot'][k] < 1:
                        Tetrahedral['T2'][k] = Tetrahedral['Tot'][k]
                    else:
                        Tetrahedral['T2'][k] = 1
                    Tetrahedral['T1'][k] = Tetrahedral['Tot'][k] - Tetrahedral['T2'][k]
            else:
                if k == 'Si4+':
                    if Tetrahedral['Tot'][k] < 2:
                        Tetrahedral['T1'][k] = Tetrahedral['Tot'][k]
                    else:
                        Tetrahedral['T1'][k] = 2
                    Tetrahedral['T2'][k] = Tetrahedral['Tot'][k] - Tetrahedral['T1'][k]
                else:
                    if Tetrahedral['Tot'][k] < 2:
                        Tetrahedral['T2'][k] = Tetrahedral['Tot'][k]
                    else:
                        Tetrahedral['T2'][k] = 2
                    Tetrahedral['T1'][k] = Tetrahedral['Tot'][k] - Tetrahedral['T2'][k]

    if Mintype == 'Smectites':
        # Brucitic M4 site configuration
        for k in np.sort(list(Octahedral['Tot'].keys())):
            if group in ['7A', '10A']:
                Brucitic['M4'][k] = 0
            else:
                if Octahedral['Tot']['Al3+'] < 1:
                    if k != 'Al3+':
                        Brucitic['M4'][k] = Octahedral['Tot'][k]*\
                            (1 - Octahedral['Tot']['Al3+'])/(NbO - Brucitic['M4']['Al3+'])
                    else:
                        Brucitic['M4'][k] = Octahedral['Tot']['Al3+']
                else:
                    if k != 'Al3+':
                        Brucitic['M4'][k] = 0
                    else:
                        Brucitic['M4'][k] = 1

        NbO_tri1 = NbO_tri - np.sum([Brucitic['M4'][j] for j in Brucitic['M4'] if j != 'Al3+'])
        # Brucitic M3 site configuration for first 9 ions
        for k in list(Octahedral['Tot'].keys())[:9]:
            if group in ['7A', '10A']:
                Brucitic['M3'][k] = 0
            else:
                if NbO_tri1 >= 4:
                    Brucitic['M3'][k] = Octahedral['Tot'][k]*2/NbO_tri
                else:
                    Brucitic['M3'][k] = Octahedral['Tot'][k]*NbO_tri1/2/NbO_tri

        # Octahedral M2 site configuration for first 8 ions
        for k in list(Octahedral['Tot'].keys())[:8]:
            if group in ['7A', '10A']:
                if TRI == 0:
                    Octahedral['M2'][k] = Octahedral['Tot'][k]*nbDI
                else:
                    if Test_east == 1:
                        if Octahedral['Tot']['Al3+'] > 1:
                            Octahedral['M1']['Al3+'] = 1
                        else:
                            Octahedral['M1']['Al3+'] = Octahedral['Tot']['Al3+']
                        Octahedral['M2'][k] = Octahedral['Tot'][k]*2/(2 + (1 - Octahedral['M1']['Al3+']))
                    else:
                        Octahedral['M2'][k] = Octahedral['Tot'][k]*2/NbO
            else:
                if NbO_tri1 >= 4:
                    Octahedral['M2'][k] = Octahedral['Tot'][k]*2/NbO_tri
                else:
                    Octahedral['M2'][k] = Octahedral['Tot'][k]*NbO_tri1/2/NbO_tri

        NbOdi_M2M3 = 4 - np.sum([Brucitic['M3'][j] for j in list(Octahedral['Tot'].keys())[:9]
                                 if j != 'Cd2+'] +  [Octahedral['M2'][j]
                                                     for j in list(Octahedral['Tot'].keys())[:7]])

        # Brucitic M3 configuration for last ions
        for k in list(Octahedral['Tot'].keys())[9:]:
            if group in ['7A', '10A']:
                Brucitic['M3'][k] = 0
            else:
                if NbOdi_M2M3 >= 0:
                    Brucitic['M3'][k] = (Octahedral['Tot'][k] - Brucitic['M4'][k])* \
                        NbOdi_M2M3/(NbO - 1)
                else:
                    Brucitic['M3'][k] = 0

        # Octahedral M2 configuration for last ions
        for k in list(Octahedral['Tot'].keys())[8:]:
            if group in ['7A', '10A']:
                if TRI == 0:
                    Octahedral['M2'][k] = Octahedral['Tot'][k]*nbDI
                else:
                    if Test_east == 1:
                        if k != 'Al3+':
                            Octahedral['M2'][k] = Octahedral['Tot'][k]*2/(2 + (1 - Octahedral['M1']['Al3+']))
                        else:
                            if Octahedral['Tot'][k] > 1:
                                Octahedral['M1'][k] = Octahedral['Tot'][k] - Octahedral['M1']['Al3+']
                            else:
                                Octahedral['M1'][k] = 0
                    else:
                        Octahedral['M2'][k] = Octahedral['Tot'][k]*2/NbO
            else:
                if NbOdi_M2M3 >= 0:
                    Octahedral['M2'][k] = (Octahedral['Tot'][k] - Brucitic['M4'][k])* \
                        NbOdi_M2M3/(NbO - 1)
                else:
                    Octahedral['M2'][k] = 0

        # Octahedral M1 site configuration
        for k in list(Octahedral['Tot'].keys()):
            if k != 'Al3+':
                if group in ['7A', '10A']:
                    Octahedral['M1'][k] = Octahedral['Tot'][k] - Octahedral['M2'][k]
                else:
                    Octahedral['M1'][k] = Octahedral['Tot'][k] - Octahedral['M2'][k] - \
                        Brucitic['M3'][k] - Brucitic['M4'][k]
            else:
                if group in ['7A', '10A']:
                    if TRI == 0:
                        Octahedral['M1'][k] = Octahedral['Tot'][k] - Octahedral['M2'][k]
                    else:
                        if Test_east == 1:
                            if Octahedral['Tot'][k] > 1:
                                Octahedral['M1'][k] = 1
                            else:
                                Octahedral['M1'][k] = Octahedral['Tot'][k]
                        else:
                            Octahedral['M1'][k] = Octahedral['Tot'][k] - Octahedral['M2'][k]
                else:
                    Octahedral['M1'][k] = Octahedral['Tot'][k] - Octahedral['M2'][k] - \
                        Brucitic['M3'][k] - Brucitic['M4'][k]

    elif Mintype == 'Chlorites':
        for k in list(Octahedral['Tot'].keys()):
            if group == '14A':
                if k == 'Al3+':
                    Octahedral['M1'][k] = (Octahedral['Tot'][k] + Tetrahedral['Tot'][k])/5
                    Octahedral['M2'][k] = (Octahedral['Tot'][k] + Tetrahedral['Tot'][k])*2/5
                    Brucitic['M3'][k] = (Octahedral['Tot'][k] + Tetrahedral['Tot'][k])*2/5
                    Brucitic['M4'][k] = 1
                elif k == 'Fe3+':
                    Octahedral['M1'][k] = (Octahedral['Tot'][k] + Tetrahedral['Tot'][k])/5
                    Octahedral['M2'][k] = (Octahedral['Tot'][k] + Tetrahedral['Tot'][k])*2/5
                    Brucitic['M3'][k] = (Octahedral['Tot'][k] + Tetrahedral['Tot'][k])*2/5
                    Brucitic['M4'][k] = 0
                else:
                    Octahedral['M1'][k] = Octahedral['Tot'][k]/5
                    Octahedral['M2'][k] = Octahedral['Tot'][k]*2/5
                    Brucitic['M3'][k] = Octahedral['Tot'][k]*2/5
                    Brucitic['M4'][k] = 0
            else:
                if k in ['Al3+', 'Fe3+']:
                    Octahedral['M1'][k] = (Octahedral['Tot'][k] + Tetrahedral['Tot'][k])/3
                    Octahedral['M2'][k] = (Octahedral['Tot'][k] + Tetrahedral['Tot'][k])*2/3
                    Brucitic['M3'][k] = 0
                    Brucitic['M4'][k] = 0
                else:
                    Octahedral['M1'][k] = Octahedral['Tot'][k]/3
                    Octahedral['M2'][k] = Octahedral['Tot'][k]*2/3
                    Brucitic['M3'][k] = 0
                    Brucitic['M4'][k] = 0

    Octahedral['M1_Remainder'] = 1 - np.sum([Octahedral['M1'][k] for k in Octahedral['M1']])


    Calc = {'Total_sites' : {}, 'XlnX' : {}, 'Nb_Oxy' : {}, 'DO_moy' : {}}
    Calc['DO_moy']['H_i'] =  Interlayer['dH']['H2O']
    Calc['DO_moy']['H_b'] =  Brucitic['dH']['H2O']
    Calc['DO_moy']['H_ext'] =  -287.20
    if group == '7A':
        Oxy = 9
        OH = 4
        Calc['Nb_Oxy']['H_i'] = 0.5
        Calc['Nb_Oxy']['H_b'] = 0
        Calc['Nb_Oxy']['H_ext'] = 1.5
    elif group == '10A':
        Oxy = 12
        OH = 2
        Calc['Nb_Oxy']['H_i'] = 1
        Calc['Nb_Oxy']['H_b'] = 0
        Calc['Nb_Oxy']['H_ext'] = 0
    else:
        Oxy = 18
        OH = 8
        Calc['Nb_Oxy']['H_i'] = 1
        Calc['Nb_Oxy']['H_b'] = 3
        Calc['Nb_Oxy']['H_ext'] = 0

    Calc['Nb_Oxy']['Inter'] = np.sum([0.5*Interlayer['Tot'][j]*charge[j]
                                      for j in Interlayer['Tot'] if j != 'NH4+'])
    for j, k in zip(['T1', 'T2', 'M1', 'M2', 'M3', 'M4'],
                    [Tetrahedral['T1'], Tetrahedral['T2'], Octahedral['M1'],
                     Octahedral['M2'], Brucitic['M3'], Brucitic['M4']]):
        i = '%s' % j
        Calc['Total_sites'][i] = np.sum([k[j] for j in k])
        Calc['Nb_Oxy'][i] = np.sum([0.5*k[j]*charge[j] for j in k ])

    for i, k  in zip(['Inter', 'M1', 'M2', 'M3', 'M4', 'T1', 'T2'],
                  [Interlayer, Octahedral, Octahedral, Brucitic, Brucitic,
                   Tetrahedral, Tetrahedral]):
        if i == 'Inter':
            a = 'Tot'
            summ = np.sum([k[a][j] if k[a][j] != 0 else 1e-11 for j in k[a]]) + k['Tot_Remainder']
            X = [k[a][j]/summ if k[a][j] != 0 else 1e-11/summ for j in k[a]]
            X.append(k['Tot_Remainder']/summ)

        else:
            a = '%s' % i
            summ = np.sum([k[a][j] if k[a][j] != 0 else 1e-11 for j in k[a]])
            X = [k[a][j]/summ if k[a][j] != 0 else 1e-11/summ for j in k[a]]

        lnX = [np.log(j) for j in X]
        if (group != '14A')&(i in ['M3', 'M4']):
            Calc['XlnX'][i] = 0
        elif (Octahedral['M1_Remainder'] == 1)&(i == 'M1'):
            Calc['XlnX'][i] = 0
        else:
            Calc['XlnX'][i] = np.sum([lnX[j]*X[j] for j in range(len(X))])


    for i, k  in zip(['Inter', 'M1', 'M2', 'M3', 'M4', 'T1', 'T2'],
                  [Interlayer, Octahedral, Octahedral, Brucitic, Brucitic,
                   Tetrahedral, Tetrahedral]):
        if i == 'Inter':
            a = 'Tot'
        else:
            a = '%s' % i
        k['dH_%s' % i] = {j : 0 for j in ['moy'] + list(k[a]) }
        if Calc['Nb_Oxy'][i] == 0:
            k['dH_%s' % i]['moy'] = 0
        else:
            k['dH_%s' % i]['moy'] = np.sum([0.5*k[a][j]*charge[j]*k['dH'][j]
                                            for j in k[a]])/Calc['Nb_Oxy'][i]
        c = []
        for b in list(k['dH_%s' % i].keys())[1:]:
            c = c + [b]
            k['dH_%s' % i][b] = np.sum([0.5*k[a][j]*charge[j]*0.5*k[a][b]*charge[b]*\
                                        np.abs(k['dH'][j] - k['dH'][b]) for j in k[a] if j not in c])

    for j, k in zip(['Inter', 'T1', 'T2', 'M1', 'M2', 'M3', 'M4'],
                    [Interlayer, Tetrahedral, Tetrahedral, Octahedral,
                     Octahedral, Brucitic, Brucitic]):
        i = '%s' % j
        if j in ['M1', 'M4', 'T1', 'T2']:
            Calc['DO_moy'][i] = k['dH_%s' % j]['moy']
        else:
            if Calc['Nb_Oxy'][i] == 0:
                Calc['DO_moy'][i] = 0
            else:
                Calc['DO_moy'][i] = k['dH_%s' % j]['moy'] + K[j]*\
                    np.sum(list(k['dH_%s' % j].values())[1:])/Calc['Nb_Oxy'][i]**2

    dHoxphtl = -(np.sum([Calc['Nb_Oxy'][k]*Calc['Nb_Oxy'][j]*(Calc['DO_moy'][k]-Calc['DO_moy'][j])
                        for j in ['T1', 'T2'] for k in ['Inter', 'M1', 'M2']] + \
                       [Calc['Nb_Oxy'][k]*Calc['Nb_Oxy'][j]*(Calc['DO_moy'][k]-Calc['DO_moy'][j])
                        for j in ['H_i'] for k in ['M1', 'M2']] + \
                           [Calc['Nb_Oxy'][k]*Calc['Nb_Oxy'][j]*(Calc['DO_moy'][k]-Calc['DO_moy'][j])
                            for j in ['M1'] for k in ['M2']] + \
                               [Calc['Nb_Oxy'][k]*Calc['Nb_Oxy'][j]*(Calc['DO_moy'][k]-Calc['DO_moy'][j])
                                for j in ['T1'] for k in ['T2']] + \
                                   [Calc['Nb_Oxy'][k]*Calc['Nb_Oxy'][j]*(Calc['DO_moy'][k]-Calc['DO_moy'][j])
                                    for j in ['H_b'] for k in ['M3', 'M4']] +\
                                       [Calc['Nb_Oxy'][k]*Calc['Nb_Oxy'][j]*(Calc['DO_moy'][k]-Calc['DO_moy'][j])
                                        for j in ['M4'] for k in ['M3']] +\
                                           [Calc['Nb_Oxy'][k]*Calc['Nb_Oxy'][j]*(Calc['DO_moy'][k]-Calc['DO_moy'][j])
                                            for j in ['H_b'] for k in ['T1', 'T2']]) +\
                 np.sum([Calc['Nb_Oxy'][k]*Calc['Nb_Oxy'][j]*(Calc['DO_moy'][k]-Calc['DO_moy'][j])
                         for j in ['H_ext'] for k in ['M1', 'M2']] +\
                        [Calc['Nb_Oxy'][k]*Calc['Nb_Oxy'][j]*(Calc['DO_moy'][k]-Calc['DO_moy'][j])
                         for j in ['H_ext'] for k in ['T1', 'T2']]))/Oxy

    dHox = dHoxphtl - Calc['Nb_Oxy']['H_i']*(dH['H2O'] - Interlayer['dH']['H2O']) -\
        Calc['Nb_Oxy']['H_b']*(dH['H2O'] - Brucitic['dH']['H2O']) - \
            Calc['Nb_Oxy']['H_ext']*(dH['H2O'] - Calc['DO_moy']['H_ext'])

    dHf = dHox + np.sum([Calc['Nb_Oxy'][j] for j in list(Calc['Nb_Oxy'].keys())[:3]])*dH['H2O']
    for i, k in zip(['Tot', 'T1', 'T2', 'M1', 'M2', 'M3', 'M4'],
                    [Interlayer, Tetrahedral, Tetrahedral, Octahedral,
                     Octahedral, Brucitic, Brucitic]):
        dHf = dHf + np.sum([0.5*k[i][j]*dH['%s2O' % (j.rstrip('+'))]
                            if (charge[j] == 1 and j not in ['NH4+', 'H3O+', 'VO2+'])
                            else k[i][j]*dH['%sO' % (j.rstrip('0123456789.+ '))] if charge[j] == 2
                            else 0.5*k[i][j]*dH['(%s)2O' % j.rstrip('+')] if j in ['NH4+', 'H3O+', 'VO2+']
                            else k[i][j]*dH['%sO2' % j.rstrip('0123456789.+ ')] if j in ['Si4+', 'Ti4+']
                            else 0.5*k[i][j]*dH['%s2O%d' % (j.rstrip('0123456789.+ '), charge[j])]
                            for j in k[i] ])

    if group in ['7A', '10A']:
        R1 = (2*nMg + 2*nFeII + 3*nFeIII + 3*nAlVI)/OH - 1
    else:
        R1 = 2*nMg/(8 - 3*nAlVI) + 2*nFeII/(8 - 3*nAlVI) + 3*nFeIII/(8 - 3*nAlVI) - 1
        R2 = 2*nMg/(8 - 3 - 3*(nAlVI - 1)) + 2*nFeII/(8 - 3 - 3*(nAlVI - 1)) + \
            3*nFeIII/(8 - 3 - 3*(nAlVI - 1)) - 1
        R3 = 2*nMg/(8 - 3 - 2/3) + 2*nFeII/(8 - 3 - 2/3) + 3*nFeIII/(8 - 3 - 2/3) + \
            3*(nAlVI - 2)/(8 - 3 - 2/3) - 1
        Etape = {'M4':{}, 'M1':{}, 'M2_M3':{}, 'Mixed':{}}
        Etape['M4'] = {'Na2O' : 0.5*nNa, 'K2O' : 0.5*nK, 'CaO' : nCa, 'Fe(OH)3' : nFeIII/(R1 + 1),
                       'Mg(OH)2' : nMg/(R1 + 1), 'Fe(OH)2' : nFeII/(R1 + 1),
                       'Al(OH)3' : [nAlVI if nAlVI <= 1 else 0][0], 'LiOH' : nLi/(R1 + 1),
                       'Mn(OH)2' : nMn/(R1 + 1), 'Cr(OH)3' : nCr/(R1 + 1), 'Ni(OH)2' : nNi/(R1 + 1),
                       'Co(OH)2' : nCo/(R1 + 1), 'Zn(OH)2' : nZn/(R1 + 1),
                       'Al2O3_VI' : 0.5*(nAlVI - [nAlVI if nAlVI <= 1 else 0][0]),
                       'FeO_VI' : (nFeII - nFeII/(R1 + 1)), 'MgO_VI' : (nMg - nMg/(R1 + 1)),
                       'Fe2O3_VI' : 0.5*(nFeIII - nFeIII/(R1 + 1)), 'Li2O' : 0.5*(nLi - nLi/(R1 + 1)),
                       'MnO' : (nMn - nMn/(R1 + 1)), 'Cr2O3' : 0.5*(nCr - nCr/(R1 + 1)),
                       'NiO' : (nNi - nNi/(R1 + 1)), 'CoO' : (nCo - nCo/(R1 + 1)), 'ZnO' : (nZn - nZn/(R1 + 1)),
                       'TiO2' : 0, 'Al2O3_IV' : 0.5*nAlIV, 'SiO2_IV' : nSi}
        Etape['M1'] = {'Na2O' : 0.5*nNa, 'K2O' : 0.5*nK, 'CaO' : nCa, 'Fe(OH)3' : nFeIII/(R2 + 1),
                       'Mg(OH)2' : nMg/(R2 + 1), 'Fe(OH)2' : nFeII/(R2 + 1),
                       'Al(OH)3' : [nAlVI if nAlVI <= 2 else 0][0], 'LiOH' : nLi/(R2 + 1),
                       'Mn(OH)2' : nMn/(R2 + 1), 'Cr(OH)3' : nCr/(R2 + 1), 'Ni(OH)2' : nNi/(R2 + 1),
                       'Co(OH)2' : nCo/(R2 + 1), 'Zn(OH)2' : nZn/(R2 + 1),
                       'Al2O3_VI' : 0.5*(nAlVI - [nAlVI if nAlVI <= 2 else 0][0]),
                       'FeO_VI' : (nFeII - nFeII/(R2 + 1)), 'MgO_VI' : (nMg - nMg/(R2 + 1)),
                       'Fe2O3_VI' : 0.5*(nFeIII - nFeIII/(R2 + 1)), 'Li2O' : 0.5*(nLi - nLi/(R2 + 1)),
                       'MnO' : (nMn - nMn/(R2 + 1)), 'Cr2O3' : 0.5*(nCr - nCr/(R2 + 1)),
                       'NiO' : (nNi - nNi/(R2 + 1)), 'CoO' : (nCo - nCo/(R2 + 1)), 'ZnO' : (nZn - nZn/(R2 + 1)),
                       'TiO2' : 0, 'Al2O3_IV' : 0.5*nAlIV, 'SiO2_IV' : nSi}
        Etape['M2_M3'] = {'Na2O' : 0.5*nNa, 'K2O' : 0.5*nK, 'CaO' : nCa, 'Fe(OH)3' : nFeIII/(R3 + 1),
                       'Mg(OH)2' : nMg/(R3 + 1), 'Fe(OH)2' : nFeII/(R3 + 1),
                       'Al(OH)3' : ((8-3-2/3)-(3*nFeIII/(R3 + 1)+2*nMg/(R3 + 1)+2*nFeII/(R3 + 1)))/3+1+2/3/3,
                       'LiOH' : nLi/(R3 + 1), 'Mn(OH)2' : nMn/(R3 + 1), 'Cr(OH)3' : nCr/(R3 + 1),
                       'Ni(OH)2' : nNi/(R3 + 1), 'Co(OH)2' : nCo/(R3 + 1), 'Zn(OH)2' : nZn/(R3 + 1),
                       'Al2O3_VI' : 0.5*(nAlVI - (((8-3-2/3)-(3*nFeIII/(R3 + 1) +\
                                                              2*nMg/(R3 + 1)+2*nFeII/(R3 + 1)))/3+1+2/3/3)),
                       'FeO_VI' : (nFeII - nFeII/(R3 + 1)), 'MgO_VI' : (nMg - nMg/(R3 + 1)),
                       'Fe2O3_VI' : 0.5*(nFeIII - nFeIII/(R3 + 1)), 'Li2O' : 0.5*(nLi - nLi/(R3 + 1)),
                       'MnO' : (nMn - nMn/(R3 + 1)), 'Cr2O3' : 0.5*(nCr - nCr/(R3 + 1)),
                       'NiO' : (nNi - nNi/(R3 + 1)), 'CoO' : (nCo - nCo/(R3 + 1)), 'ZnO' : (nZn - nZn/(R3 + 1)),
                       'TiO2' : 0, 'Al2O3_IV' : 0.5*nAlIV, 'SiO2_IV' : nSi}
        for k in list(Etape['M1'].keys()):
            if k in ['Na2O', 'K2O', 'CaO', 'Al2O3_IV', 'SiO2_IV']:
                Etape['Mixed'][k] = np.sum([Etape[j][k] for j in Etape if j != 'Mixed'])/3
            else:
                if nAlVI > 1+1/3:
                    Etape['Mixed'][k] = Etape['M2_M3'][k]
                else:
                    if nAlVI > 1:
                        Etape['Mixed'][k] = Etape['M1'][k]
                    else:
                        Etape['Mixed'][k] = Etape['M4'][k]


    Tetrahedral['moles'] = {}; Octahedral['moles'] = {}; Interlayer['moles'] = {}
    Tetrahedral['moles']['Fe2O3'] = 0.5*np.sum([Tetrahedral[j]['Fe3+']
                                                for j in Tetrahedral if j in ['T1', 'T2']])
    for j, k in zip(['CdO', 'TiO2'],['Cd2+', 'Ti4+']):
        Octahedral['moles'][j] = Octahedral['M1'][k] + Octahedral['M2'][k] + Brucitic['M3'][k] +\
             Brucitic['M4'][k]

    if group in ['7A', '10A']:
        Tetrahedral['moles']['Al2O3'] = 0.5*np.sum([Tetrahedral[j]['Al3+']
                                                    for j in Tetrahedral if j in ['T1', 'T2']])
        Tetrahedral['moles']['SiO2'] = np.sum([Tetrahedral[j]['Si4+']
                                               for j in Tetrahedral if j in ['T1', 'T2']])
        for j, k in zip(['LiOH', 'Li2O', 'Mg(OH)2', 'MgO', 'Fe(OH)2', 'FeO',
                         'Fe(OH)3', 'Fe2O3', 'Mn(OH)2', 'MnO', 'Cr(OH)3', 'Cr2O3',
                         'Ni(OH)2', 'NiO', 'Co(OH)2', 'CoO', 'Zn(OH)2', 'ZnO', 'Al(OH)3', 'Al2O3'],
                        ['Li+', 'Li+', 'Mg2+', 'Mg2+', 'Fe2+', 'Fe2+', 'Fe3+', 'Fe3+',
                         'Mn2+', 'Mn2+', 'Cr3+', 'Cr3+', 'Ni2+', 'Ni2+',
                         'Co2+', 'Co2+', 'Zn2+', 'Zn2+', 'Al3+', 'Al3+']):
            if 'OH' not in j:
                if charge[k] == 1:
                    hydroxide = '%sOH' % (k.rstrip('0123456789.+ '))
                else:
                    hydroxide = '%s(OH)%d' % (k.rstrip('0123456789.+ '), charge[k])
                if charge[k] == 1 | charge[k] == 3:
                    Octahedral['moles'][j] = (Octahedral['M1'][k] + Octahedral['M2'][k] + \
                        Brucitic['M3'][k] + Brucitic['M4'][k] - Octahedral['moles'][hydroxide])/2
                else:
                    Octahedral['moles'][j] = Octahedral['M1'][k] + Octahedral['M2'][k] + \
                        Brucitic['M3'][k] + Brucitic['M4'][k] - Octahedral['moles'][hydroxide]
            elif j == 'Al(OH)3':
                Octahedral['moles'][j] = (OH - np.sum([Octahedral['moles'][a]*2 if a.endswith('2')
                                                     else Octahedral['moles'][a]*3
                                                     for a in Octahedral['moles'].keys()
                                                     if 'OH' in a and 'Li' not in a and 'Al' not in a]))/3
            else:
                Octahedral['moles'][j] = (Octahedral['M1'][k] + Octahedral['M2'][k] + \
                    Brucitic['M3'][k] + Brucitic['M4'][k])/(R1 + 1)
    else:
        Tetrahedral['moles']['Al2O3'] = 0.5*(4 - nSi)
        Tetrahedral['moles']['SiO2'] = nSi
        for j in ['LiOH', 'Li2O', 'Mg(OH)2', 'MgO', 'Fe(OH)2', 'FeO', 'Fe(OH)3', 'Fe2O3',
                  'Mn(OH)2', 'MnO', 'Cr(OH)3', 'Cr2O3', 'Ni(OH)2', 'NiO', 'Co(OH)2', 'CoO',
                  'Zn(OH)2', 'ZnO', 'Al(OH)3', 'Al2O3']:
            if j in ['Al2O3', 'FeO', 'MgO', 'Fe2O3']:
                Octahedral['moles'][j] = Etape['Mixed']['%s_VI' % j]
            else:
                Octahedral['moles'][j] = Etape['Mixed'][j]
    for k in Interlayer['Tot'].keys():
        if charge[k] == 1 and k not in ['H3O+', 'NH4+']:
            j = '%s2O' % (k.rstrip('0123456789.+ '))
            div = 0.5
        elif k == 'H3O+':
            j = 'H2O'
            div = 0.5
        elif k == 'NH4+':
            j = '(NH4)2O'
            div = 0.5
        else:
            j = '%sO' % (k.rstrip('0123456789.+ '))
            div = 1
        if j in Interlayer['Slat'].keys():
            Interlayer['moles'][j] = div*Interlayer['Tot'][k]
    Interlayer['moles']['NiO'] = 0
    Slat = np.sum([Octahedral['moles'][j]*Octahedral['Slat'][j]
                   for j in Octahedral['Slat'].keys()] + \
                  [Tetrahedral['moles'][j]*Tetrahedral['Slat'][j]
                   for j in Tetrahedral['Slat'].keys()] + \
                      [Interlayer['moles'][j]*Interlayer['Slat'][j]
                       for j in Interlayer['Slat'].keys()])
    V = np.sum([Octahedral['moles'][j]*Octahedral['V'][j]
                for j in Octahedral['V'].keys()] + \
               [Tetrahedral['moles'][j]*Tetrahedral['V'][j]
                for j in Tetrahedral['V'].keys()] + \
                   [Interlayer['moles'][j]*Interlayer['V'][j]
                    for j in Interlayer['V'].keys()]) # cm3/mol
    Cp = np.where(np.sum([Octahedral['moles'][j] for j in Octahedral['moles'].keys()
                          if j in ['Mn(OH)2', 'Cr(OH)3', 'Co(OH)2']]) == 0,
                  np.sum([Octahedral['moles'][j]*Octahedral['Cp'][j]
                          for j in Octahedral['Cp'].keys()] + \
                          [Tetrahedral['moles'][j]*Tetrahedral['Cp'][j]
                          for j in Tetrahedral['Cp'].keys()] + \
                              [Interlayer['moles'][j]*Interlayer['Cp'][j]
                              for j in Interlayer['Cp'].keys()]),
                          0)
    a = np.where(np.sum([Octahedral['moles'][j] for j in Octahedral['moles'].keys()
                          if j in ['Li2O', 'MnO', 'Cr2O3', 'NiO', 'CoO', 'ZnO']] + \
                        [Interlayer['moles'][j] for j in Interlayer['moles'].keys()
                         if j in ['Cs2O', 'Rb2O', 'Li2O', 'ZnO', 'BaO', 'SrO', 'CoO',
                                  'MgO', 'CuO', 'H2O']]) == 0,
                  np.sum([Octahedral['moles'][j]*Octahedral['a'][j]
                          for j in Octahedral['a'].keys()] + \
                         [Tetrahedral['moles'][j]*Tetrahedral['a'][j]
                          for j in Tetrahedral['a'].keys()] + \
                             [Interlayer['moles'][j]*Interlayer['a'][j]
                              for j in Interlayer['a'].keys()]),
                          0)
    b = np.where(np.sum([Octahedral['moles'][j] for j in Octahedral['moles'].keys()
                          if j in ['Li2O', 'MnO', 'Cr2O3', 'NiO', 'CoO', 'ZnO']] + \
                        [Interlayer['moles'][j] for j in Interlayer['moles'].keys()
                         if j in ['Cs2O', 'Rb2O', 'Li2O', 'ZnO', 'BaO', 'SrO', 'CoO',
                                  'MgO', 'CuO', 'H2O']]) == 0,
                  np.sum([Octahedral['moles'][j]*Octahedral['b'][j]
                          for j in Octahedral['b'].keys()] + \
                         [Tetrahedral['moles'][j]*Tetrahedral['b'][j]
                          for j in Tetrahedral['b'].keys()] + \
                             [Interlayer['moles'][j]*Interlayer['b'][j]
                              for j in Interlayer['b'].keys()]),
                          0)
    c = np.where(np.sum([Octahedral['moles'][j] for j in Octahedral['moles'].keys()
                          if j in ['Li2O', 'MnO', 'Cr2O3', 'NiO', 'CoO', 'ZnO']] + \
                        [Interlayer['moles'][j] for j in Interlayer['moles'].keys()
                         if j in ['Cs2O', 'Rb2O', 'Li2O', 'ZnO', 'BaO', 'SrO', 'CoO',
                                  'MgO', 'CuO', 'H2O']]) == 0,
                  np.sum([Octahedral['moles'][j]*Octahedral['c'][j]
                          for j in Octahedral['c'].keys()] + \
                         [Tetrahedral['moles'][j]*Tetrahedral['c'][j]
                          for j in Tetrahedral['c'].keys()] + \
                             [Interlayer['moles'][j]*Interlayer['c'][j]
                              for j in Interlayer['c'].keys()]),
                          0)
    S_spin_mag = np.sum([S_spin[j]*Octahedral['M2'][j]
                         for j in set(S_spin.keys())&set(Octahedral['M2'].keys())] +\
                        [S_spin[j]*Octahedral['M1'][j]
                         for j in set(S_spin.keys())&set(Octahedral['M1'].keys())] +\
                            [S_spin[j]*Brucitic['M3'][j]
                             for j in set(S_spin.keys())&set(Brucitic['M3'].keys())] +\
                                [S_spin[j]*Brucitic['M4'][j]
                                 for j in set(S_spin.keys())&set(Brucitic['M4'].keys())] +\
                                    [S_spin[j]*Tetrahedral['T1'][j]
                                     for j in set(S_spin.keys())&set(Tetrahedral['T1'].keys())] +\
                                        [S_spin[j]*Tetrahedral['T2'][j]
                                         for j in set(S_spin.keys())&set(Tetrahedral['T2'].keys())])
    Al_Tet_ratio = np.sum([Tetrahedral[j]['Al3+']
                           for j in ['T1', 'T2'] ])/np.sum([Tetrahedral[j][k]
                                                            for j in ['T1', 'T2']
                                                            for k in Tetrahedral['T1'].keys()])
    R = 8.31451 #J K​−1 mol−1
    if Al_Tet_ratio < 0.2:
        Sconf_site = 1270*Al_Tet_ratio**3 + (-903.7)*Al_Tet_ratio**2 + 166.2*Al_Tet_ratio
        Sconf_site = -Sconf_site/R/2
    elif Al_Tet_ratio < 0.31:
        Sconf_site = 4610*Al_Tet_ratio**3 + (-4271.3)*Al_Tet_ratio**2 + 1280*Al_Tet_ratio + (-114.79)
        Sconf_site = -Sconf_site/R/2
    else:
        Sconf_site = Calc['XlnX']['T2']
    if cation_order == 'Ordered':
        Sconf_T = 0
    elif cation_order == 'Random':
        Sconf_T = Calc['XlnX']['T1'] + Calc['XlnX']['T2']
    else:
        Sconf_T = Sconf_site
    if group == '10A':
        Scf = -R*(Calc['XlnX']['Inter'] + 2*Calc['XlnX']['M2'] + Calc['XlnX']['M1'] + 2*Sconf_T)
    elif group == '14A':
        Scf = -R*(2*Calc['XlnX']['M2'] + 2*Calc['XlnX']['M3'] + Calc['XlnX']['M1'] + \
                  Calc['XlnX']['M4'] + 2*Sconf_T)
    else:
        Scf = -R*(2*Calc['XlnX']['M2'] + Calc['XlnX']['M1'] + Sconf_T)
    S_allelem = 0
    for x in list(S_elem.keys()):
        ele =  x.rstrip('0123456789.+ ')
        if x == 'O2':
            ele_lst = [Octahedral[k][j] for k in ['M1', 'M2']
                       for j in Octahedral[k].keys() if ele in j] + \
                [Brucitic[k][j] for k in ['M3', 'M4']
                 for j in Brucitic[k].keys() if ele in j] + \
                    [Tetrahedral[k][j] for k in ['T1', 'T2']
                     for j in Tetrahedral[k].keys() if ele in j] + \
                        [Interlayer['Tot'][j]*0.5 if j == 'H3O+'
                         else Interlayer['Tot'][j] for j in Interlayer['Tot'].keys() if ele in j]
            ele_lst.append(Oxy/2)
        elif x == 'H2':
            ele_lst = [Octahedral[k][j] for k in ['M1', 'M2']
                       for j in Octahedral[k].keys() if ele in j] + \
                [Brucitic[k][j] for k in ['M3', 'M4']
                 for j in Brucitic[k].keys() if ele in j] + \
                    [Tetrahedral[k][j] for k in ['T1', 'T2']
                     for j in Tetrahedral[k].keys() if ele in j] + \
                        [Interlayer['Tot'][j]*1.5 if j == 'H3O+'
                         else Interlayer['Tot'][j]*2 if j == 'NH4+'
                         else Interlayer['Tot'][j] for j in Interlayer['Tot'].keys() if ele in j]
            ele_lst = ele_lst + [Calc['Nb_Oxy']['H_i'], Calc['Nb_Oxy']['H_b'], Calc['Nb_Oxy']['H_ext']]
        elif x == 'N2':
            ele_lst = [Octahedral[k][j] for k in ['M1', 'M2']
                       for j in Octahedral[k].keys() if ele in j and j not in ['Ni2+', 'Na+']] + \
                [Brucitic[k][j] for k in ['M3', 'M4']
                 for j in Brucitic[k].keys() if ele in j and j not in ['Ni2+', 'Na+']] + \
                    [Tetrahedral[k][j] for k in ['T1', 'T2']
                     for j in Tetrahedral[k].keys() if ele in j and j not in ['Ni2+', 'Na+']] + \
                        [Interlayer['Tot'][j]*0.5 if j == 'NH4+' else Interlayer['Tot'][j]
                         for j in Interlayer['Tot'].keys()
                         if ele in j and j not in ['Ni2+', 'Na+']]
        else:
            ele_lst = [Octahedral[k][j] for k in ['M1', 'M2']
                       for j in Octahedral[k].keys() if ele in j] + \
                [Brucitic[k][j] for k in ['M3', 'M4']
                 for j in Brucitic[k].keys() if ele in j] + \
                    [Tetrahedral[k][j] for k in ['T1', 'T2']
                     for j in Tetrahedral[k].keys() if ele in j] + \
                        [Interlayer['Tot'][j] for j in Interlayer['Tot'].keys() if ele in j]
        S_allelem = S_allelem + np.sum(ele_lst)*S_elem[x]

    S = Slat + Scf + S_spin_mag if group == '10A' else Slat + S_spin_mag
    dG = (dHf*1e3 - 298.15*(S - S_allelem))*1e-3

    Rxn = {}
    Rxn['type'] = Mintype
    Rxn['name'] = name
    Rxn['formula'] = ''
    Rxn['MW'] = 0
    if nCs != 0:
        Rxn['formula'] = Rxn['formula'] + 'Cs%1.2f' % nCs
        Rxn['MW'] = Rxn['MW'] + nCs*MW['Cs']
    if nRb != 0:
        Rxn['formula'] = Rxn['formula'] + 'Rb%1.2f' % nRb
        Rxn['MW'] = Rxn['MW'] + nRb*MW['Rb']
    if nK != 0:
        Rxn['formula'] = Rxn['formula'] + 'K%1.2f' % nK
        Rxn['MW'] = Rxn['MW'] + nK*MW['K']
    if nNa != 0:
        Rxn['formula'] = Rxn['formula'] + 'Na%1.2f' % nNa
        Rxn['MW'] = Rxn['MW'] + nNa*MW['Na']
    if nLi != 0:
        Rxn['formula'] = Rxn['formula'] + 'Li%1.2f' % nLi
        Rxn['MW'] = Rxn['MW'] + nLi*MW['Li']
    if nFe != 0:
        Rxn['formula'] = Rxn['formula'] + 'Fe%1.2f' % nFe
        Rxn['MW'] = Rxn['MW'] + nFe*MW['Fe']
    if nMn != 0:
        Rxn['formula'] = Rxn['formula'] + 'Mn%1.2f' % nMn
        Rxn['MW'] = Rxn['MW'] + nMn*MW['Mn']
    if nNi != 0:
        Rxn['formula'] = Rxn['formula'] + 'Ni%1.2f' % nNi
        Rxn['MW'] = Rxn['MW'] + nNi*MW['Ni']
    if nCo != 0:
        Rxn['formula'] = Rxn['formula'] + 'Co%1.2f' % nCo
        Rxn['MW'] = Rxn['MW'] + nCo*MW['Co']
    if nCd != 0:
        Rxn['formula'] = Rxn['formula'] + 'Cd%1.2f' % nCd
        Rxn['MW'] = Rxn['MW'] + nCd*MW['Cd']
    if nZn != 0:
        Rxn['formula'] = Rxn['formula'] + 'Zn%1.2f' % nZn
        Rxn['MW'] = Rxn['MW'] + nZn*MW['Zn']
    if nCa != 0:
        Rxn['formula'] = Rxn['formula'] + 'Ca%1.2f' % nCa
        Rxn['MW'] = Rxn['MW'] + nCa*MW['Ca']
    if nMg != 0:
        Rxn['formula'] = Rxn['formula'] + 'Mg%1.2f' % nMg
        Rxn['MW'] = Rxn['MW'] + nMg*MW['Mg']
    if nAl != 0:
        Rxn['formula'] = Rxn['formula'] + 'Al%1.2f' % nAl
        Rxn['MW'] = Rxn['MW'] + nAl*MW['Al']
    if nFe3 != 0:
        Rxn['formula'] = Rxn['formula'] + 'FeIII%1.2f' % nFe3
        Rxn['MW'] = Rxn['MW'] + (nFe3)*MW['Fe']
    if nV != 0:
        Rxn['formula'] = Rxn['formula'] + 'V%1.2f' % nV
        Rxn['MW'] = Rxn['MW'] + nV*MW['V']
    if nCr != 0:
        Rxn['formula'] = Rxn['formula'] + 'Cr%1.2f' % nCr
        Rxn['MW'] = Rxn['MW'] + nCr*MW['Cr']
    if nTi != 0:
        Rxn['formula'] = Rxn['formula'] + 'Ti%1.2f' % nTi
        Rxn['MW'] = Rxn['MW'] + nTi*MW['Ti']
    if nVO != 0:
        Rxn['formula'] = Rxn['formula'] + 'VO%1.2f' % nVO
        Rxn['MW'] = Rxn['MW'] + nVO*(MW['V'] + 2*MW['O'])

    nH = np.sum([Interlayer['Tot'][j]*charge[j]
                 for j in list(Interlayer['Tot'].keys())[:7] + \
                     list(Interlayer['Tot'].keys())[-5:]]) + \
        np.sum([Octahedral['Tot'][j]*charge[j]
                for j in list(Octahedral['Tot'].keys())[:-1] if j != 'Cd2+']) + \
            np.sum([Tetrahedral['Tot'][j]*charge[j] for j in list(Tetrahedral['Tot'].keys())[1:]])
    nH = round(nH, 3)
    nH2O = (Oxy - nH3O - 2*nSi - 2*nTi)
    Rxn['formula'] = Rxn['formula'] + 'Si%s' % nSi + 'O%d(OH)%d' % (Oxy - OH, OH)
    Rxn['MW'] = Rxn['MW'] + nSi*MW['Si'] + Oxy*MW['O'] + OH*MW['H']
    Rxn['min']=[dG*1000/J_to_cal, dHf*1000/J_to_cal, S/J_to_cal,
                V, a/J_to_cal, b/J_to_cal, c/J_to_cal]
    Rxn['min'].insert(0, Rxn['formula'])
    Rxn['min'].insert(1, 'B2015, B2021')

    coeff = [-nH, nCs, nRb, nK, nNa, nLi, nFe, nMn, nNi, nCo, nCd, nZn, nCa, nMg,
             nAl, nFe3, nV, nCr, nTi, nVO, nSi, nH2O]
    spec = ['H+', 'Cs+', 'Rb+', 'K+', 'Na+', 'Li+', 'Fe++', 'Mn++', 'Ni++', 'Co++', 'Cd++',
            'Zn++', 'Ca++', 'Mg++', 'Al+++', 'Fe+++', 'V+++', 'Cr+++',
            'Ti(OH)4', 'VO++', 'SiO2(aq)', 'H2O']
    Rxn['spec'] = [x for x, y in zip(spec, coeff) if y!=0]
    Rxn['coeff'] = [y for y in coeff if y!=0]
    Rxn['nSpec'] = len(Rxn['coeff'])
    Rxn['V'] = V
    Rxn['dG'] = dG
    Rxn['dHf'] = dHf
    Rxn['Cp'] = float(Cp)
    Rxn['source'] = 'B2015, B2021'

    elements  = ['%.4f' % nCa, 'Ca', '%.4f' % nK, 'K', '%.4f' % nNa, 'Na',
                 '%.4f' % (nFe + nFe3), 'Fe', '%.4f' % nMg, 'Mg', '%.4f' % nAl, 'Al',
                 '%.4f' % nSi, 'Si', '%.4f' % OH, 'H', '%.4f' % Oxy, 'O']
    filters = [y for x, y in enumerate(elements) if x%2 == 0 and float(y) == 0]
    filters = [[x, x+1] for x, y in enumerate(elements) if y in filters]
    filters = [num for elem in filters for num in elem]
    Rxn['elements'] = [y for x, y in enumerate(elements) if x not in filters]

    clay = Rxn['min']
    dGTP, _ = heatcap(TC, P, clay)
    R = 1.9872041 # cal/mol/K
    dGRs = 0
    for i in range(Rxn['nSpec']):
        R_coeff = float(Rxn['coeff'][i])
        R_specie = Rxn['spec'][i]

        if R_specie == 'H2O':
            dGR = dGH2O
        else:
            dGR  = supcrtaq(TC, P, dbacessdic[R_specie], Dielec_method = Dielec_method, **rhoEG)
        dGRs = dGRs + R_coeff*dGR

    dGrxn = -dGTP + dGRs
    logK_clay = (-dGrxn/R/(TK)/np.log(10))

    return logK_clay, Rxn

def write_GWBdb(T, P, **kwargs ):
    """
    This function writes the new GWB database into a new folder called "output"   \n
    Parameters
    ----------
        T               :    temperature [°C]   \n
        P               :    pressure [bar]   \n
        nCa_cpx         :    number of moles of Ca in solid solution of clinopyroxene (optional)
                                if it is ommitted solid solution of clinopyroxene will not
                                be included   \n
        logK_form       :    specify the format of logK either as a set of eight values one for each
                                of the dataset’s principal temperatures, or blocks of polynomial coefficients, [values, polycoeffs]
                                default is 'a set of eight values'   \n
        solid_solution  :    specify the inclusion of solid-solution [Yes or No],
                                default is 'No'   \n
        clay_thermo     :    specify the inclusion of clay thermodynamic properties [Yes or No],
                                default is 'No'   \n
        dbaccess        :    direct-access database filename and location  (optional)  \n
        sourcedb        :    source database filename and location  (optional)  \n
        objdb           :    new database filename and location    (optional) \n
        co2actmodel     :    co2 activity model equation [Duan_Sun or Drummond]  (optional),
                                if not specified, default is 'Drummond'   \n
        Dielec_method   :    specify either 'IAPWS95' or 'SUPCRT92' as the method to calculate
                                dielectric constant, default is 'SUPCRT92'   (optional) \n
    Returns
    -------
        Outputs the new database to an ASCII file with filename described in 'objdb'.   \n
    Usage
    -------
     Example:
         (1) General format with default dielectric constant and CO2 activity model and exclusions
             of solid solutions   \n
             write_GWBdb(T, P, dbaccess = 'location', sourcedb = 'location', objdb = 'location')   \n
         (2) Inclusion of solid solutions and clay thermo and exclusion of solid solution of clinopyroxene  \n
             write_GWBdb(T, P, solid_solution = 'Yes', clay_thermo = 'Yes', dbaccess = 'location,
                         sourcedb = 'location', objdb = 'location')   \n
         (3) Inclusion of all solid solutions and clay thermo with \emph{'Duan_Sun'} CO2 activity model and 'IAPWS95'
             dielectric constant calculation \n
             write_GWBdb(T, P, nCa, solid_solution = 'Yes', clay_thermo = 'Yes', dbaccess = 'location',
                         sourcedb = 'location', objdb = 'location'co2actmodel = 'Duan_Sun',
                         Dielec_method = 'IAPWS95')   \n
    """

    kwargs = dict({"nCa_cpx": None,  "logK_form": None,  "solid_solution": None,
                  "clay_thermo": None,   "dbaccess": None,
                  "sourcedb": None,   "objdb": None,   "sourceformat": None,
                  "Dielec_method": None,   "co2actmodel": None
                  }, **kwargs)
    nCa_cpx = kwargs['nCa_cpx'];                    logK_form = kwargs['logK_form']
    solid_solution = kwargs['solid_solution'];      clay_thermo = kwargs['clay_thermo']
    dbaccess = kwargs['dbaccess'];                  sourcedb = kwargs['sourcedb']
    objdb = kwargs['objdb'];                        Dielec_method = kwargs['Dielec_method']
    co2actmodel = kwargs['co2actmodel'];            sourceformat = kwargs['sourceformat']

    if dbaccess == None:
        dbaccess = './default_db/speq21.dat'
    dbaccess = os.path.join(os.path.dirname(os.path.abspath(__file__)), dbaccess)

    if sourcedb == None:
        if (sourceformat == None) or sourceformat == 'GWB':
            sourcedb = './default_db/thermo.com.dat'
        elif sourceformat == 'EQ36':
            sourcedb = './default_db/data0.dat'

    sourcedb = os.path.join(os.path.dirname(os.path.abspath(__file__)), sourcedb)

    dbacessdic, dbname = readAqspecdb(dbaccess)

    if (sourceformat == None) or sourceformat == 'GWB':
        sourceformat = 'GWB'
        sourcedic, specielist, chargedic, MWdic, Mineraltype, fugacity_info, act_param = readSourceGWBdb(sourcedb)
        activity_model = act_param['activity_model']
    elif sourceformat == 'EQ36':
        sourceformat = 'EQ36'
        sourcedic, specielist, chargedic, MWdic, block_info, Elemlist, act_param = readSourceEQ36db(sourcedb)
        activity_model = act_param['activity_model']

    if sourceformat == 'EQ36':
        dataset = 'tdat'
        dataset_format =  'apr20'
    else:
        dataset = sourcedb.split('.')[-1]
        dataset_format =  act_param['dataset_format']

    logK_form = 'values' if (logK_form is None) | (dataset_format == 'oct94') else logK_form.lower()

    if np.ndim(T) == 0:
        T = np.ravel(T)
    elif np.size(T) == 2:
        T = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(T[0], T[-1], 8)])

    if np.size(P) <= 2:
        P = np.ravel(P)
        P = P[0]*np.ones(np.size(T))

    [rho, dGH2O, dHH2O, SH2O, _, P, T] = iapws95(T = T, P = P)
    TK = celsiusToKelvin(T)

    if dataset_format ==  'apr20' and logK_form.lower() == 'polycoeffs':
        Tr = 298.15
        logKfunc = lambda TK, *x: x[0] + x[1]*(TK - Tr) + x[2]*(TK**2 - Tr**2) +  x[3]*((1/TK) - (1/Tr)) + \
            x[4]*((1/TK**2) - (1/Tr**2)) + x[5]*np.log(TK/Tr)
        x0 = [-31.9605, 20.6576, 3.73497e-2, -9.01862, 6.0111, 2.5]

    if os.path.exists(os.path.join(os.getcwd(), 'output')) == False:
        os.makedirs(os.path.join(os.getcwd(), 'output'))

    periodic_table = open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                       'PeriodicTableJSON.json'), encoding='utf8')
    data = json.load(periodic_table)
    Element = {data['elements'][x]['symbol'] : pd.DataFrame([data['elements'][x]['name'],
                                                             data['elements'][x]['atomic_mass']],
                                                            index = ['name', 'mass']).T
               for x in range(len(data['elements']))}
    periodic_table.close()

    fid = open(sourcedb, 'r')

    missing_species = []
    elemspeclist = [ symbol for x in specielist[0] for symbol, item in Element.items()
                    if item.name[0][:5] == x[:5] ] if sourceformat == 'GWB' else specielist[0]

    form_del = [1] if sourceformat == 'GWB' else [1, 3, 4]
    all_species_source = [[i]+k for i, k in sourcedic.items() if i not in (['eh', 'e-', 'H2O']) ]
    all_species_source = [[k for j, k in enumerate(all_species_source[i])
                           if (j not in form_del and k not in elemspeclist and str(k).strip('0123456789.- ') != '') ]
                          if  (i <= len(specielist[0]))
                          else [k for j, k in enumerate(all_species_source[i])
                                if (j not in form_del and str(k).strip('0123456789.- ') != '') ]
                          for i in range(len(all_species_source)) ]
    for num in range(len(all_species_source)): #
        if num < len(all_species_source):
            lst = [v for v in all_species_source[num] if v not in (['eh', 'e-', 'H2O']) ]

            bool_miss = [x.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
                         not in dbacessdic.keys() for x in lst  ]
            if any(bool_miss):
                sublist = [i for (i, v) in zip(lst, bool_miss) if v ]
                if lst[0] not in sublist:
                    missing_species.append([lst[0]] + sublist)
                else:
                    missing_species.append(sublist)

    missingfile = open(os.path.join(os.path.abspath("."), 'output', 'spxNotFound.txt'), 'w+')
    #missing_species = [i for i in missing_species if len(i)<1]
    for line in missing_species:
        if len(line) > 0:
            missingfile.writelines(line[0])
            missingfile.writelines('\n')
            for i in range(len(line)):
                missingfile.writelines('   %s' % line[i])
                missingfile.writelines('\n')
    missingfile.close()
    missing_species = [item for sublist in missing_species for item in sublist]
    missing_species = [i for n, i in enumerate(missing_species) if i not in missing_species[:n]]
    elem_avail = list(np.unique([k for i, j in enumerate([[i] + k for i, k in sourcedic.items()
                                                          if i not in missing_species])
                                 for l, k in enumerate(j) if i < len(specielist[1])
                                 and str(k).strip('0123456789.- ') != ''
                                 and not str(k).endswith(("+", "-", '(aq)', '(g)'))
                                 and k not in ['O2', 'H2O'] and len(k) < 3]))

    if objdb == None:
        objdb = 'thermo.%sbars' % int(P[0])
    logKnan_alert = False
    # timestr = '.' + time.strftime("%d%b%y_%H%M")

    fout = open(os.path.join(os.path.abspath("."),'output',  objdb + '.' + dataset), 'w+') # + timestr

    if sourceformat != 'EQ36':
        s = fid.readline()
        fout.writelines(s)
        s = fid.readline()
        fout.writelines(s[:16] + dataset_format + '\n')

        if dataset_format ==  'apr20':
            s = fid.readline()
            fout.writelines(s)

        s = fid.readline()
        fout.writelines(s)
        s = fid.readline()
        if s.strip(' \n*') != '':
            fout.writelines(s[:27] + dbname + '\n')
        s = fid.readline()
        fout.writelines(s[:15] + ': pyGeochemCalc, ' + time.ctime() + '\n')
        s = fid.readline()
        fout.writelines(s)
        s = fid.readline()
        fout.writelines(s)
    else:
        fout.writelines('dataset of thermodynamic data for gwb programs \n' + \
                        'dataset format: ' + dataset_format + '\n' + \
                            'activity model: %s \n' % activity_model + \
                                'fugacity model: tsonopoulos \n' + \
                                '*  THERMODYNAMIC DATABASE: ' + dbname + '\n' +\
                                    '*  generated by: pyGeochemCalc, ' + time.ctime() + '\n' +\
                                        '*  Output package:  gwb \n' + \
                                            '*  Data set:        com \n')

    if dataset_format == 'oct94':
        fout.writelines('*  Note: coefficients for calculating the activity coefficients \n' + \
                        '*    for CO2 are based on Ref:\n' + \
                            '*    S.E.Drummond,1981. Boiling and Mixing of Hydrothermal\n' + \
                                '*    Fluids: Chemical Effects on Mineral Precipitation.\n')
    elif dataset_format ==  'apr20' and logK_form.lower() == 'polycoeffs':
        fout.writelines('*   \n' + \
                        '* This thermo data file uses the polynomial expression of the logK values: \n' + \
                            '*  \n' + \
                                '* log10 K(TK) = a + b*(TK-Tr) + c*(TK^2-Tr^2) + d*(1/TK-1/Tr) + e*(1/TK^2-1/Tr^2) + f*ln(TK/Tr) \n' + \
                                        '* TK is the Temperature in Kelvin, Tr is the relative temperature (T = 298.15 K). \n')


    #skip lines till temperature rows
    for i in range(1000) :
        s = fid.readline()
        if s.strip('\n').strip('* ') in ['temperatures', 'temperatures (degC)', 'Temperature grid (degC)']:
            break
    fout.writelines( "\n")
    if s.strip('\n').strip('* ') == 'temperatures':
        if sourceformat != 'EQ36':
            fout.writelines(s[:-1] + '(degC)\n')
        else:
            fout.writelines('* ' + s[:-1] + '(degC)\n')
    elif s.strip('\n').strip('* ') == 'Temperature grid (degC)':
        fout.writelines('* temperatures (degC)\n')
    else:
        fout.writelines(s)

    for i in range(len(T)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "       %9.4f" %  T[i-1])
        else:
            fout.writelines( "   %9.4f" %  T[i-1])
        if (i % 4 == 0) | (i == len(T)):
            fout.writelines( "\n")

    #skip lines till pressure rows
    for i in range(1000) :
        s = fid.readline()
        if s.strip('\n').strip('* ') in ['pressures', 'pressures (bar)', 'Pressure grid (bars)']:
            break
    if s.strip('\n').strip('* ') == 'pressures':
        if sourceformat != 'EQ36':
            fout.writelines(s[:-1] + '(bar)\n')
        else:
            fout.writelines('* ' + s[:-1] + '(bar)\n')
    elif s.strip('\n').strip('* ') == 'Pressure grid (bars)':
        fout.writelines('* pressures (bar)\n')
    else:
        fout.writelines(s)
    for i in range(len(P)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "       %9.4f" %  P[i-1])
        else:
            fout.writelines( "   %9.4f" %  P[i-1])
        if (i % 4 == 0) | (i == len(P)):
            fout.writelines( "\n")

    #%% Calculation for debye huckel and bdot and water properties
    if Dielec_method is not None:
        if Dielec_method == 'FE97':
            [E, _, Adh, Bdh, bdot ] = dielec_FE97(T, P)[:5]
        elif Dielec_method == 'JN91':
            [E, _, Adh, Bdh, bdot ] = dielec_JN91(T, P)[:5]
    else:
        # default method: 'JN91'
        Dielec_method = 'JN91'
        [E, _, Adh, Bdh, bdot] = dielec_JN91(T, P)[:5]

    rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}
    rhoEDB = {'rho': rho, 'E': E,  'Ah': Adh, 'Bh': Bdh}

    # Calculate the rho E G for density extrapolation method here so we have it below
    rhoEGextrap = {}
    if any(rhoEG['rho'] < 350):
        subBornptrs = rhoEG['rho'] < 350
        for i, j in enumerate(zip(T[subBornptrs], P[subBornptrs])):
            rhoextrap = np.linspace(350, 550, 3)
            Pextrap = iapws95(T = j[0], rho = rhoextrap)[0]
            Textrap = j[0]*np.ones(np.size(Pextrap))

            dGH2O = iapws95(T = Textrap, P = Pextrap)[1]
            if Dielec_method is not None:
                if Dielec_method == 'FE97':
                    E = dielec_FE97(Textrap, rhoextrap)[0]
                elif Dielec_method == 'JN91':
                    E = dielec_JN91(Textrap, Pextrap)[0]
            else:
                # default method: 'JN91'
                E = dielec_JN91(Textrap, Pextrap)[0]
            rhoextrap = np.around(rhoextrap, 3)
            rhoEGextrap['%d_%d' % (j[0], j[1])]= {'rho': rhoextrap,'E': E, 'dGH2O': dGH2O,
                                                   'Textrap': Textrap, 'Pextrap': Pextrap}


    #skip lines till adh rows
    #  'cco2' in s.strip('\n') 'log k for eh reaction' or 'Eh reaction: logKr' in s.strip('\n')
    for i in range(100) :
        s = fid.readline()
        if any(re.findall(r'|'.join(('(adh)', 'Debye-Huckel A_gamma')), s.strip('\n'), re.IGNORECASE)):
            break
    if sourceformat != 'EQ36':
        fout.writelines(s)
    else:
        fout.writelines('* debye huckel a (adh)\n')
    for i in range(len(Adh)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "       %9.4f" %  Adh[i-1])
        else:
            fout.writelines( "   %9.4f" %  Adh[i-1])
        if (i % 4 == 0) | (i == len(Adh)):
            fout.writelines( "\n")
    #skip lines till bdh rows
    for i in range(100):
        s = fid.readline()
        if any(re.findall(r'|'.join(('(bdh)', 'Debye-Huckel B_gamma')), s.strip('\n'), re.IGNORECASE)):
            break
    if sourceformat != 'EQ36':
        fout.writelines(s)
    else:
        fout.writelines('* debye huckel b (bdh)\n')
    for i in range(len(Bdh)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "       %9.4f" %  Bdh[i-1])
        else:
            fout.writelines( "   %9.4f" %  Bdh[i-1])
        if (i % 4 == 0) | (i == len(Bdh)):
            fout.writelines( "\n")
    #skip lines till bdot rows
    if activity_model == 'debye-huckel':
        for i in range(100) :
            s = fid.readline()
            if any(re.findall(r'|'.join(('bdot', 'B-dot')), s.strip('\n'), re.IGNORECASE)):
                break
    if sourceformat != 'EQ36':
        fout.writelines(s)
    else:
        if activity_model == 'debye-huckel':
            fout.writelines('* bdot\n')

    if activity_model == 'debye-huckel':
        for i in range(len(bdot)):
            i = i + 1
            if (i == 1) | (i == 5):
                fout.writelines( "       %9.4f" %  bdot[i-1])
            else:
                fout.writelines( "   %9.4f" %  bdot[i-1])
            if (i % 4 == 0) | (i == len(bdot)):
                fout.writelines( "\n")

        #%% Calculation for co2 fitting coefs
        cco2 = gamma_correlation(T, P, co2actmodel)
        cco2 = cco2.T #.ravel()
        for j in range(4):
            fout.writelines('* c co2 %s\n' % (j+1))
            for i in range(len(cco2)):
                i = i + 1
                if (i == 1) | (i == 5):
                    fout.writelines( "       %9.4f" %  cco2[i-1, j])
                else:
                    fout.writelines( "   %9.4f" %  cco2[i-1, j])
                if (i % 4 == 0) | (i == len(cco2)):
                    fout.writelines( "\n")

        #skip lines till c h2o rows
        if sourceformat != 'EQ36':
            for i in range(100) :
                s = fid.readline()
                # print(s)
                if s[:7] == '* c h2o':
                    break

        #%% Calculation for h2o fitting coefs
        ch2o = aw_correlation(T, P, Dielec_method = Dielec_method, **rhoEDB)
        ch2o = ch2o.T #.ravel()
        for j in range(4):
            fout.writelines('* c h2o %s\n' % (j+1))
            for i in range(len(cco2)):
                i = i + 1
                if (i == 1) | (i == 5):
                    fout.writelines( "       %9.4f" %  ch2o[i-1, j])
                else:
                    fout.writelines( "   %9.4f" %  ch2o[i-1, j])
                if (i % 4 == 0) | (i == len(ch2o)):
                    fout.writelines( "\n")

    if dataset_format == 'oct94':
    # #Copy and replace c h2o values
        for i in range(100) :
            s = fid.readline()
            if s[:7] == '* log k':
                break
            # fout.writelines(s)
        #%% Calculations for "log k for eh" rows
        fout.writelines(s)
        logK = calcRxnlogK( T, P, 'eh', dbacessdic, sourcedic, specielist,
                           Dielec_method = Dielec_method, **rhoEG)[0]
        # Utilized for density extrapolation of logK
        if any(rhoEG['rho'] < 350):
            logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'eh', sourcedic = sourcedic,
                                                  dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                  specielist = specielist, Dielec_method = Dielec_method)
        logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
        for i in range(len(logK)):
            i = i + 1
            if (i == 1) | (i == 5):
                fout.writelines( "       %9.4f" %  logK[i-1])
            else:
                fout.writelines( "   %9.4f" %  logK[i-1])
            if (i % 4 == 0) | (i == len(logK)):
                fout.writelines( "\n")

        #%% Calculations for "log k for o2"
        #skip lines till "log k for o2" rows
        for i in range(50) :
            s = fid.readline()
            # print(s)
            if s[:14] == '* log k for o2':
                break
        fout.writelines(s)
        logK = calcRxnlogK( T, P, 'O2(g)', dbacessdic, sourcedic, specielist,
                           Dielec_method = Dielec_method, **rhoEG)[0]
        # Utilized for density extrapolation of logK
        if any(rhoEG['rho'] < 350):
            logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'O2(g)', sourcedic = sourcedic,
                                                  dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                  specielist = specielist, Dielec_method = Dielec_method)
        logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
        for i in range(len(logK)):
            i = i + 1
            if (i == 1) | (i == 5):
                fout.writelines( "       %9.4f" %  logK[i-1])
            else:
                fout.writelines( "   %9.4f" %  logK[i-1])
            if (i % 4 == 0) | (i == len(logK)):
                fout.writelines( "\n")

        #%% Calculations for "log k for h2"
        #skip lines till "log k for h2" rows
        for i in range(50) :
            s = fid.readline()
            # print(s)
            if s[:14] == '* log k for h2':
                break
        fout.writelines(s)
        logK = calcRxnlogK( T, P, 'H2(g)', dbacessdic, sourcedic, specielist,
                           Dielec_method = Dielec_method, **rhoEG)[0]
        # Utilized for density extrapolation of logK
        if any(rhoEG['rho'] < 350):
            logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'H2(g)', sourcedic = sourcedic,
                                                  dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                  specielist = specielist, Dielec_method = Dielec_method)
        logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
        for i in range(len(logK)):
            i = i + 1
            if (i == 1) | (i == 5):
                fout.writelines( "       %9.4f" %  logK[i-1])
            else:
                fout.writelines( "   %9.4f" %  logK[i-1])
            if (i % 4 == 0) | (i == len(logK)):
                fout.writelines( "\n")

        #%% Calculations for "log k for n2"
        #skip lines till "log k for n2" rows
        for i in range(50) :
            s = fid.readline()
            # print(s)
            if s[:14] == '* log k for n2':
                break
        fout.writelines(s)
        logK = calcRxnlogK( T, P, 'N2(g)', dbacessdic, sourcedic, specielist,
                           Dielec_method = Dielec_method, **rhoEG)[0]
        # Utilized for density extrapolation of logK
        if any(rhoEG['rho'] < 350):
            logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'N2(g)', sourcedic = sourcedic,
                                                  dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                  specielist = specielist, Dielec_method = Dielec_method)
        logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
        for i in range(len(logK)):
            i = i + 1
            if (i == 1) | (i == 5):
                fout.writelines( "       %9.4f" %  logK[i-1])
            else:
                fout.writelines( "   %9.4f" %  logK[i-1])
            if (i % 4 == 0) | (i == len(logK)):
                fout.writelines( "\n")
        fout.writelines( "\n")
    else:
        fout.writelines('\n')

    if sourceformat == 'EQ36':
        fout.writelines( "\n")

        f_ionsize = open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'ion_size.txt'), 'r')
        Rd = f_ionsize.readlines()
        Rd = Rd[1:]
        ion_sizedic = {Rd[x].split()[0] : Rd[x].split()[1] for x in range(len(Rd))}
        f_ionsize.close()
        # Needed for rebalancing aqueous and mineral reactions in terms of O2(aq) instead of O2(g)
        dic = {'O2(g)' : ['', 1, '1.0000', 'O2(aq)']}
        logK_rebal = calcRxnlogK( T, P, 'O2(g)', dbacessdic, dic, specielist,
                                 Dielec_method = Dielec_method, **rhoEG)[0]
        # Utilized for density extrapolation of logK
        if any(rhoEG['rho'] < 350):
            logK_rebal[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'O2(g)',
                                                        sourcedic = dic, dbacessdic = dbacessdic,
                                                        rhoEGextrap = rhoEGextrap, specielist = specielist,
                                                        Dielec_method = Dielec_method)
        logK_rebal = np.where(np.isnan(logK_rebal), 500, logK_rebal) # set abitrary 500 to nan values

    #%% Elements
    #skip lines till "elements" rows
    for i in range(5000) :
        s = fid.readline()
        if s.rstrip('\n').lstrip('0123456789.- ') == 'elements':
            break
    fout.writelines( "   %s elements" % len(elem_avail))
    fout.writelines( "\n\n")

    #copy and paste lines till "basis" rows
    for i in range(1000) :
        s = fid.readline()
        s_mod = s.split()[0] if s.strip('\n') != '' else s
        if s.rstrip('\n').lstrip('0123456789.- ') == 'basis species':
            break
        if sourceformat != 'EQ36':
            if s_mod[:5] in [Element[j].name[0][:5] for j in elem_avail]:
                fout.writelines(s)
        else:
            if s_mod in elem_avail and not s.startswith('+---'):
                fout.writelines('%-15s (%-2s)          mole wt.=  %8.4f g\n'
                                % (Element[s_mod].name[0], s_mod, float(s.split()[1])))
            else:
                continue
    fout.writelines( "\n-end-\n\n")

    #only used for counter
    counter = 0
    for j in specielist[1]:
        if (j not in missing_species):
            counter +=1
    fout.writelines( "   %s basis species\n\n" % counter)

    #%% Basis reactions
    #skip lines till "redox couples" rows
    for i in range(2000):
        s = fid.readline()
        if s.rstrip('\n').lstrip('0123456789.- ') in ['redox couples', 'auxiliary basis species']:
            break

    for j in specielist[1]:
        if (j not in missing_species):
            k = j.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
            if sourceformat != 'EQ36':
                fout.writelines('%s\n' % j)
            else:
                fout.writelines('%s\n' % j.replace('O2(g)', 'O2(aq)'))
            if sourceformat != 'EQ36':
                fout.writelines('%s\n' % chargedic[j])
            else:
                if j == 'O2(g)':
                    ionsize = [float(re.sub('[^0123456789\.]', '', x)) for x in block_info['O2(g)_b'] if x.strip('*    ').startswith('DHazero')]
                else:
                    ionsize = [float(re.sub('[^0123456789\.]', '', x)) for x in block_info[j] if x.strip('*    ').startswith('DHazero')]
                if any(ionsize) and any([MWdic[j]]):
                    fout.writelines('     charge=  %d      ion size=  %.1f A      mole wt.=   %8.4f g\n'
                                    % (float(chargedic[j].split()[-1]), ionsize[0], MWdic[j]))
                else:
                    formula = j if sourcedic[j][0] == '' else sourcedic[j][0]
                    formula = formula.rstrip('(aq)(g)')
                    ionsize = 3 if j.endswith('(aq)') else float(ion_sizedic[j]) if j in ion_sizedic.keys() else 500
                    fout.writelines('     charge=  %d      ion size=  %.1f A      mole wt.=   %8.4f g\n'
                                    % (float(chargedic[j].split()[-1]), ionsize,
                                       calc_elem_count_molewt(formula, Elementdic = Element)[-1]) )

            if sourceformat != 'EQ36':
                fout.writelines( "     %s elements in species\n" % sourcedic[j][1])
                Rxn = sourcedic[j][2:]
            else:
                fout.writelines( "     %s elements in species\n" % int(len(Elemlist[j])/2) )
                Rxn = Elemlist[j]

            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 7):
                    fout.writelines( "    %9.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                else:
                    if sourceformat != 'EQ36':
                        fout.writelines( "%-9s     " %  (Rxn[i - 1]))
                    else:
                        fout.writelines( "%-9s     " %  (Rxn[i - 1]).replace('O2(g)', 'O2(aq)'))
                if (i % 6 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
            fout.writelines( "*    extrapolation algorithm:      [92joh/oel]\n" )
            if j != 'H2O':
                ref = dbacessdic[k][1].split('  ')[0]
                ref = ref.split(':')[1] if ('ref' in ref) or ('REF' in ref) else ref
                dG, dH, S = dbacessdic[k][2], dbacessdic[k][3], dbacessdic[k][4]
            else:
                dG, dH, S, ref = dGH2O[0], dHH2O[0], SH2O[0], 'iapws95/' + Dielec_method
            fout.writelines( "*    reference-state data source = %s\n" % ref)
            fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dG/1000) )
            fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dH/1000))
            fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % S)
            fout.writelines( "\n")
        else:
            continue
    fout.writelines( "-end-\n\n")

    #only used for counter
    counter = 0
    for j in specielist[2]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species]) == len(rxnlst)):
            counter +=1
    fout.writelines( "   %s redox couples\n\n" % counter)

    #%% Redox reactions
    #skip lines till "aqueous species" rows
    for i in range(2000):
        s = fid.readline()
        # print(s)
        if s.rstrip('\n').lstrip('0123456789.- ') == 'aqueous species':
            break

    if sourceformat != 'EQ36':
        speclst = specielist[2]
    else:
        speclst = [ x for x in specielist[2] if x != 'O2(aq)']
    for j in speclst:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([i for i in rxnlst if i not in missing_species]) == len(rxnlst)):
            k = j.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
            if dataset_format == 'oct94':
                fout.writelines('%s\n' % j)
                if sourcedic[j][0] != '':
                    fout.writelines('*    formula= %s\n' % sourcedic[j][0])
                else:
                    fout.writelines('*    formula= %s\n' % dbacessdic[k][0])
            elif dataset_format == 'apr20':
                if sourcedic[j][0] != '':
                    fout.writelines('%-30s %s %s\n' % (j, 'formula=', sourcedic[j][0]))
                else:
                    fout.writelines('%-30s %s %s\n' % (j, 'formula=', dbacessdic[k][0]))
            if sourceformat != 'EQ36':
                fout.writelines('%s\n' % chargedic[j])
            else:
                ionsize = [float(re.sub('[^0123456789\.]', '', x)) for x in block_info[j] if x.strip('*    ').startswith('DHazero')]
                if any(ionsize) and any([MWdic[j]]):
                    fout.writelines('     charge=  %d      ion size=  %.1f A      mole wt.=   %8.4f g\n'
                                    % (float(chargedic[j].split()[-1]), ionsize[0], MWdic[j]))
                else:
                    formula = j if sourcedic[j][0] == '' else sourcedic[j][0]
                    formula = formula.rstrip('(aq)')
                    ionsize = 3 if j.endswith('(aq)') else float(ion_sizedic[j]) if j in ion_sizedic.keys() else 500
                    fout.writelines('     charge=  %d      ion size=  %.1f A      mole wt.=   %8.4f g\n'
                                    % (float(chargedic[j].split()[-1]), ionsize,
                                       calc_elem_count_molewt(formula, Elementdic = Element)[-1]) )
            if sourceformat != 'EQ36':
                fout.writelines( "     %s species in reaction\n" % sourcedic[j][1])
            else:
                fout.writelines( "     %s species in reaction\n" % (sourcedic[j][1] - 1) )
            Rxn = sourcedic[j][2:] if sourceformat != 'EQ36' else sourcedic[j][4:]

            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 7):
                    fout.writelines( "    %9.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( "%-9s     " %  (Rxn[i - 1]))
                if (i % 6 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                               Dielec_method = Dielec_method, sourceformat = sourceformat, **rhoEG)[0]

            if any(np.isnan(logK)):
                logKnan_alert = True
            else:
                logKnan_alert = False
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j,
                                                      sourcedic = sourcedic, dbacessdic = dbacessdic,
                                                      rhoEGextrap = rhoEGextrap, specielist = specielist,
                                                      Dielec_method = Dielec_method, sourceformat = sourceformat)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values

            if (dataset_format ==  'apr20') & (logK_form == 'polycoeffs'):
                TK = celsiusToKelvin(T)
                logKcorr = curve_fit(logKfunc, TK[logK!=500].ravel(), logK[logK!=500].ravel(), p0 = x0,  maxfev = 1000000)[0]
                fout.writelines('     a= %15.6f   ' % logKcorr[0] + 'b= %15.6f   ' % logKcorr[1] + \
                                'c= %15.5f\n' % logKcorr[2])
                fout.writelines('     d= %15.2f   ' % logKcorr[3] + 'e= %15.2f   ' % logKcorr[4] + \
                                'f= %15.2f \n' % logKcorr[5])
                fout.writelines('     TminK= %-15.2f ' % np.min(TK) + 'TmaxK= %-7.2f\n' % np.max(TK))
            else:
                for i in range(len(logK)):
                    i = i + 1
                    if (i == 1) | (i == 5):
                        fout.writelines( "     %9.4f" %  logK[i-1])
                    else:
                        fout.writelines( "  %9.4f" %  logK[i-1])
                    if (i % 4 == 0) | (i == len(logK)):
                        fout.writelines( "\n")
            fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
            fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
            if j != 'H2O':
                ref = dbacessdic[k][1].split('  ')[0]
                ref = ref.split(':')[1] if ('ref' in ref) or ('REF' in ref) else ref
                dG, dH, S = dbacessdic[k][2], dbacessdic[k][3], dbacessdic[k][4]
            else:
                dG, dH, S, ref = dGH2O[0], dHH2O[0], SH2O[0], 'iapws95/' + Dielec_method
            fout.writelines( "*    reference-state data source = %s\n" % ref)
            fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dG/1000) )
            fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dH/1000))
            fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % S)
            fout.writelines( "\n")
        else:
            continue
    fout.writelines( "-end-\n\n")
    if logKnan_alert == True:
        warnings.warn('Some temperature and pressure points are out of aqueous species HKF eqns regions of applicability, hence, density extrapolation has been applied')

    #only used for counter
    counter = 0
    for j in specielist[3]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            counter +=1
    fout.writelines( "   %s aqueous species\n\n" % counter)

    #%% Aqueous reactions
    #skip lines till "minerals" rows
    for i in range(20000):
        s = fid.readline()
        if s.rstrip('\n').lstrip('0123456789.- ') in ['minerals', 'solids']:
            break

    for j in specielist[3]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([i for i in rxnlst if i not in missing_species]) == len(rxnlst)):
            k = j.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
            if dataset_format == 'oct94':
                fout.writelines('%s\n' % j)
                if sourcedic[j][0] != '':
                    fout.writelines('*    formula= %s\n' % sourcedic[j][0])
                else:
                    fout.writelines('*    formula= %s\n' % dbacessdic[k][0])
            elif dataset_format == 'apr20':
                if sourcedic[j][0] != '':
                    fout.writelines('%-30s %s %s\n' % (j, 'formula=', sourcedic[j][0]))
                else:
                    fout.writelines('%-30s %s %s\n' % (j, 'formula=', dbacessdic[k][0]))

            if sourceformat != 'EQ36':
                fout.writelines('%s\n' % chargedic[j])
            else:
                ionsize = [float(re.sub('[^0123456789\.]', '', x)) for x in block_info[j] if x.strip('*    ').startswith('DHazero')]
                if any(ionsize) and any([MWdic[j]]):
                    fout.writelines('     charge=  %d      ion size=  %.1f A      mole wt.=   %8.4f g\n'
                                    % (float(chargedic[j].split()[-1]), ionsize[0], MWdic[j]))
                else:
                    formula = j if sourcedic[j][0] == '' else sourcedic[j][0]
                    formula = formula.rstrip('(aq)')
                    ionsize = 3 if j.endswith('(aq)') else float(ion_sizedic[j]) if j in ion_sizedic.keys() else 500
                    fout.writelines('     charge=  %d      ion size=  %.1f A      mole wt.=   %8.4f g\n'
                                    % (float(chargedic[j].split()[-1]), ionsize,
                                       calc_elem_count_molewt(formula, Elementdic = Element)[-1] ) )

            if sourceformat != 'EQ36':
                fout.writelines( "     %s species in reaction\n" % sourcedic[j][1])
            else:
                fout.writelines( "     %s species in reaction\n" % (sourcedic[j][1] - 1) )
            Rxn = sourcedic[j][2:]  if sourceformat != 'EQ36' else sourcedic[j][4:]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 7) | (i == 13):
                    fout.writelines( "    %9.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                else:
                    if sourceformat != 'EQ36':
                        fout.writelines( "%-9s     " %  (Rxn[i - 1]))
                    else:
                        fout.writelines( "%-9s     " %  (Rxn[i - 1]).replace('O2(g)', 'O2(aq)'))
                if (i % 6 == 0) | (i % 12 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                               Dielec_method = Dielec_method, sourceformat = sourceformat, **rhoEG)[0]
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j,
                                                      sourcedic = sourcedic, dbacessdic = dbacessdic,
                                                      rhoEGextrap = rhoEGextrap, specielist = specielist,
                                                      Dielec_method = Dielec_method, sourceformat = sourceformat)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            if sourceformat == 'EQ36' and 'O2(g)' in Rxn:
                coeff_O2 = float(Rxn[Rxn.index('O2(g)') - 1])
                logK = np.where(logK != 500, logK + coeff_O2*logK_rebal, logK)

            if (dataset_format ==  'apr20') & (logK_form == 'polycoeffs'):
                TK = celsiusToKelvin(T)
                logKcorr = curve_fit(logKfunc, TK[logK!=500].ravel(), logK[logK!=500].ravel(), p0 = x0,  maxfev = 1000000)[0]
                fout.writelines('     a= %15.6f   ' % logKcorr[0] + 'b= %15.6f   ' % logKcorr[1] + \
                                'c= %15.5f\n' % logKcorr[2])
                fout.writelines('     d= %15.2f   ' % logKcorr[3] + 'e= %15.2f   ' % logKcorr[4] + \
                                'f= %15.2f \n' % logKcorr[5])
                fout.writelines('     TminK= %-15.2f ' % np.min(TK) + 'TmaxK= %-7.2f\n' % np.max(TK))
            else:
                for i in range(len(logK)):
                    i = i + 1
                    if (i == 1) | (i == 5):
                        fout.writelines( "     %9.4f" %  logK[i-1])
                    else:
                        fout.writelines( "  %9.4f" %  logK[i-1])
                    if (i % 4 == 0) | (i == len(logK)):
                        fout.writelines( "\n")
            fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
            fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
            if j != 'H2O':
                ref = dbacessdic[k][1].split('  ')[0]
                ref = ref.split(':')[1] if ('ref' in ref) or ('REF' in ref) else ref
                dG, dH, S = dbacessdic[k][2], dbacessdic[k][3], dbacessdic[k][4]
            else:
                dG, dH, S, ref = dGH2O[0], dHH2O[0], SH2O[0], 'iapws95/' + Dielec_method
            fout.writelines( "*    reference-state data source = %s\n" % ref)
            fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dG/1000) )
            fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dH/1000))
            fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % S)
            fout.writelines( "\n")
        else:
            continue

    fout.writelines( "-end-\n\n")

    #%% free electron for tdat dataset format
    if dataset_format == 'apr20':
        if sourceformat != 'EQ36':
            speclst = specielist[4]
        else:
            speclst = ['e-']
        fout.writelines( "   %s free electron\n\n" % len(speclst))
        for j in speclst:
            fout.writelines('%s\n' % j)
            if sourceformat != 'EQ36':
                fout.writelines('%s\n' % chargedic[j])
                fout.writelines( "     %s species in reaction\n" % sourcedic[j][1])
            else:
                fout.writelines('     charge=  %d      ion size=  %.1f A      mole wt.=   %8.4f g\n'
                                % (-1, 0, 0) )
                fout.writelines( "     %s species in reaction\n" % (sourcedic[j][1] - 1) )

            Rxn = sourcedic[j][2:]  if sourceformat != 'EQ36' else sourcedic[j][4:]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 7) | (i == 13):
                    fout.writelines( "    %9.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( "%-9s     " %  (Rxn[i - 1]))
                if (i % 6 == 0) | (i % 12 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                               Dielec_method = Dielec_method, sourceformat = sourceformat, **rhoEG)[0]
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j, sourcedic = sourcedic,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      specielist = specielist, Dielec_method = Dielec_method,
                                                      sourceformat = sourceformat)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values

            if (dataset_format ==  'apr20') & (logK_form == 'polycoeffs'):
                TK = celsiusToKelvin(T)
                logKcorr = curve_fit(logKfunc, TK[logK!=500].ravel(), logK[logK!=500].ravel(), p0 = x0,  maxfev = 1000000)[0]
                fout.writelines('     a= %15.6f   ' % logKcorr[0] + 'b= %15.6f   ' % logKcorr[1] + \
                                'c= %15.5f\n' % logKcorr[2])
                fout.writelines('     d= %15.2f   ' % logKcorr[3] + 'e= %15.2f   ' % logKcorr[4] + \
                                'f= %15.2f \n' % logKcorr[5])
                fout.writelines('     TminK= %-15.2f ' % np.min(TK) + 'TmaxK= %-7.2f\n' % np.max(TK))
            else:
                for i in range(len(logK)):
                    i = i + 1
                    if (i == 1) | (i == 5):
                        fout.writelines( "     %9.4f" %  logK[i-1])
                    else:
                        fout.writelines( "  %9.4f" %  logK[i-1])
                    if (i % 4 == 0) | (i == len(logK)):
                        fout.writelines( "\n")

        fout.writelines( "-end-\n\n")

    #only used for counter
    if clay_thermo is not None:
        if clay_thermo.lower() == 'yes':
            fclay = open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'clay_elements.dat'), 'r')
            Rd = fclay.readlines()
            Rd = [j.replace('-','_').strip('\n') for j in Rd]
            counter = len(Rd)
        else:
            counter = 0
    else: # default no clay thermo calculation
        clay_thermo = 'no'
        counter = 0

    solidsolution_no = 11
    if solid_solution is not None:
        if solid_solution.lower() == 'yes':
            if nCa_cpx is None:
                nCa = 0
                counter = counter + 3*solidsolution_no # no cpx
            else:
                nCa = nCa_cpx
                if nCa > 0:
                    counter = counter + 4*solidsolution_no
                else:
                    counter = counter + 3*solidsolution_no  # no cpx
        else:
            counter = counter + 0
    else: # default no solid solution
        counter = counter + 0
        solid_solution = 'no'

    if sourceformat != 'EQ36':
        speclst = specielist[5]
    else:
        speclst = specielist[4] + specielist[5]
    for j in speclst:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species]) == len(rxnlst)):
            if (solid_solution.lower() == 'no') & (clay_thermo.lower() == 'no'):
                counter += 1
            elif  (solid_solution.lower() == 'yes') & (clay_thermo.lower() == 'no'):
                if j not in ['Anorthite', 'Albite', 'Forsterite', 'Fayalite', 'Enstatite', 'Ferrosilite']:
                    counter += 1
                else:
                    continue
            elif  (solid_solution.lower() == 'no') & (clay_thermo.lower() == 'yes'):
                if j not in [ Rd[h].split(',')[0] for h in range(len(Rd))]:
                    counter += 1
                else:
                    continue
            else:
                if j not in ['Anorthite', 'Albite', 'Forsterite', 'Fayalite', 'Enstatite', 'Ferrosilite'] + [ Rd[h].split(',')[0] for h in range(len(Rd))]:
                    counter += 1
                else:
                    continue

    if (solid_solution.lower() == 'yes') and (nCa == 1):
        fout.writelines( "   %s minerals\n\n" % (counter - 2))
    else:
        fout.writelines( "   %s minerals\n\n" % (counter))

    # print('-> Processing mineral output')
    #%% Mineral reactions
    #skip lines till "gases" rows
    for i in range(20000):
        s = fid.readline()
        if s.rstrip('\n').lstrip('0123456789.- ') == 'gases':
            break
    if solid_solution.lower() == 'yes':
        mineralcount = 0
        for XAn in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKAnAb(XAn, T, P, dbacessdic, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'AnAb', X = XAn,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, TK, dataset = 'GWB', logK_form = logK_form)
            mineralcount += 1
        for XFo in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKFoFa(XFo, T, P, dbacessdic, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'FoFa', X = XFo,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, TK, dataset = 'GWB', logK_form = logK_form)
            mineralcount += 1
        for XEn in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKEnFe(XEn, T, P, dbacessdic, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'EnFe', X = XEn,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, TK, dataset = 'GWB', logK_form = logK_form)
            mineralcount += 1
        if nCa > 0:
            for XMg in np.round(np.linspace(1, 0, solidsolution_no), 1):
                logK, Rxn = calclogKDiHedEnFe(nCa, XMg, T, P, dbacessdic, **rhoEG)
                # Utilized for density extrapolation of logK
                if any(rhoEG['rho'] < 350):
                    logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'DiHedEnFe',
                                                          dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                          X = XMg, Ca = nCa,
                                                          Dielec_method = Dielec_method)
                logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
                outputfmt(fout, logK, Rxn, TK, dataset = 'GWB', logK_form = logK_form)
                mineralcount += 1

    # clay minerals
    if clay_thermo.lower() == 'yes':
        for i in range(len(Rd)):
            if Rd[i].split(',')[0] in ['Berthierine_FeII', 'Berthierine_FeIII', 'Lizardite',
                                       'Cronstedtite', 'Mg-Cronstedtite', 'Greenalite', 'Hisingerite']:
                layering = '7A'
            elif Rd[i].split(',')[0] in ['Clinochlore', 'Chamosite', 'Amesite']:
                layering = '14A'
            else:
                layering = '10A'
            logK, Rxn = calclogKclays(T, P, dbacessdic, *Rd[i].split(','), group = layering, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'Clay',
                                                      *Rd[i].split(','), dbacessdic = dbacessdic,
                                                      rhoEGextrap = rhoEGextrap, group = layering,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, TK, dataset = 'GWB', logK_form = logK_form)

    for j in speclst:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            if solid_solution.lower() == 'yes' and j in ['Anorthite', 'Albite', 'Forsterite', 'Fayalite', 'Enstatite', 'Ferrosilite']:
                continue
            elif solid_solution.lower() == 'yes' and (nCa == 1) and j in ['Diopside', 'Hedenbergite']:
                continue
            elif clay_thermo.lower() == 'yes' and j in [ Rd[h].split(',')[0] for h in range(len(Rd))]:
                continue
            else:
                k = j.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
                fout.writelines("%s                       " % j)
                if sourceformat != 'EQ36':
                    fout.writelines( "type= %s\n" %  Mineraltype[j])
                else:
                    fout.writelines( "\n" )
                if sourcedic[j][0] != '':
                    fout.writelines('     formula= %s\n' % sourcedic[j][0])
                else:
                    fout.writelines('     formula= %s\n' % dbacessdic[k][0])
                fout.writelines("     mole vol.=   %1.3f cc" %  dbacessdic[k][5])
                if MWdic[j] != []:
                    fout.writelines("      mole wt.=  %1.4f g\n" %  MWdic[j])
                else:
                    formula = k if sourcedic[j][0] == '' else sourcedic[j][0]
                    formula = formula.rstrip('(aq)(am)')#.rstrip('+2').rstrip('+3').rstrip('+4')
                    fout.writelines("      mole wt.=  %1.4f g\n" %  calc_elem_count_molewt(formula, Elementdic = Element)[-1] )
                if sourceformat != 'EQ36':
                    fout.writelines( "     %s species in reaction\n" % sourcedic[j][1])
                else:
                    fout.writelines( "     %s species in reaction\n" % (sourcedic[j][1] - 1) )

                Rxn = sourcedic[j][2:] if sourceformat != 'EQ36' else sourcedic[j][4:]
                for i in range(len(Rxn)):
                    i = i + 1
                    if (i == 1) | (i == 7) | (i == 13):
                        fout.writelines("%9.4f " %  float(Rxn[i - 1]))
                    elif i % 2 != 0:
                        fout.writelines("%9.4f " %  float(Rxn[i - 1]))
                    else:
                        if sourceformat != 'EQ36':
                            fout.writelines( "%-9s     " %  (Rxn[i - 1]))
                        else:
                            fout.writelines( "%-9s     " %  (Rxn[i - 1]).replace('O2(g)', 'O2(aq)'))
                    if (i % 6 == 0) | (i % 12 == 0) | (i == len(Rxn)):
                        fout.writelines( "\n")
                logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                                   Dielec_method = Dielec_method, sourceformat = sourceformat, **rhoEG)[0]

                # Utilized for density extrapolation of logK
                if any(rhoEG['rho'] < 350):
                    logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j,
                                                          sourcedic = sourcedic, dbacessdic = dbacessdic,
                                                          rhoEGextrap = rhoEGextrap, specielist = specielist,
                                                          Dielec_method = Dielec_method, sourceformat = sourceformat)

                logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
                if sourceformat == 'EQ36' and 'O2(g)' in Rxn:
                    coeff_O2 = float(Rxn[Rxn.index('O2(g)') - 1])
                    logK = np.where(logK != 500, logK + coeff_O2*logK_rebal, logK)

                if (dataset_format ==  'apr20') & (logK_form == 'polycoeffs'):
                    TK = celsiusToKelvin(T)
                    logKcorr = curve_fit(logKfunc, TK[logK!=500].ravel(), logK[logK!=500].ravel(), p0 = x0,  maxfev = 1000000)[0]
                    fout.writelines('     a= %15.6f   ' % logKcorr[0] + 'b= %15.6f   ' % logKcorr[1] + \
                                    'c= %15.5f\n' % logKcorr[2])
                    fout.writelines('     d= %15.2f   ' % logKcorr[3] + 'e= %15.2f   ' % logKcorr[4] + \
                                    'f= %15.2f \n' % logKcorr[5])
                    fout.writelines('     TminK= %-15.2f ' % np.min(TK) + 'TmaxK= %-7.2f\n' % np.max(TK))
                else:
                    for i in range(len(logK)):
                        i = i + 1
                        if (i == 1) | (i == 5) | (i == 9):
                            fout.writelines("       %9.4f" %  logK[i-1])
                        else:
                            fout.writelines("  %9.4f" %  logK[i-1])
                        if (i % 4 == 0) | (i == len(logK)):
                            fout.writelines( "\n")

                fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
                if dbacessdic[k][0] == 'nan':
                    fout.writelines( "*    extrapolation algorithm: supcrt92/water95\n" )
                else:
                    fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
                ref = dbacessdic[k][1].split('  ')[0]
                ref = ref.split(':')[1] if ('ref' in ref) or ('REF' in ref) else ref
                fout.writelines( "*    reference-state data source = %s\n" % ref )
                fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dbacessdic[k][2]/1000) )
                fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dbacessdic[k][3]/1000))
                fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % dbacessdic[k][4])
                fout.writelines( "\n")
        else:
            continue

    fout.writelines( "-end-\n\n")

    #only used for counter
    counter = 0
    for j in specielist[6]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            counter +=1
    fout.writelines( "   %s gases\n\n" % counter )

    #%% Gas reactions
    #skip lines till "oxides" rows
    for i in range(20000):
        s = fid.readline()
        # print(s)
        if s.rstrip('\n').lstrip('0123456789.- ') in ['oxides', 'solid solutions']:
            break

    for j in specielist[6]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            k = j.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
            fout.writelines("%s\n" % j)
            if MWdic[j] != []:
                fout.writelines( "     mole wt.=   %1.4f g\n" %  MWdic[j])
            else:
                formula = j.rstrip('(g)')
                fout.writelines("      mole wt.=  %1.4f g\n" %  calc_elem_count_molewt(formula, Elementdic = Element)[-1] )

            if sourceformat == 'EQ36':
                fugacity_info = {'fugacity_chi': {'CH4(g)': '     chi=       -537.779     1.54946 -.000927827     1.20861  -.00370814  3.33804e-6\n',
                                                  'CO2(g)': '     chi=       -1430.87       3.598  -.00227376     3.47644   -.0104247  8.46271e-6\n',
                                                  'H2(g)': '     chi=       -12.5908     .259789  -7.2473e-5   .00471947 -2.69962e-5  2.15622e-8\n',
                                                  'H2O(g)': '     chi=       -6191.41     14.8528  -.00914267    -66.3326      .18277  -.00013274\n'},
                                 'fugacity_Pcrit': {'Ar(g)': '     Pcrit=     48.7 bar      Tcrit=     150.8 K      omega=        .001\n',
                                                    'CH4(g)': '     Pcrit=     46.0 bar      Tcrit=     190.4 K      omega=        .011\n',
                                                    'CO2(g)': '     Pcrit=     73.8 bar      Tcrit=     304.1 K      omega=        .239\n',
                                                    'H2(g)': '     Pcrit=     13.0 bar      Tcrit=      33.2 K      omega=       -.218\n',
                                                    'H2O(g)': '     Pcrit=    221.2 bar      Tcrit=     647.3 K      omega=        .344    a=-.0109    b=   0.0\n',
                                                    'H2S(g)': '     Pcrit=     89.4 bar      Tcrit=     373.2 K      omega=        .097\n',
                                                    'He(g)': '     Pcrit=     2.27 bar      Tcrit=      5.19 K      omega=       -.365\n',
                                                    'N2(g)': '     Pcrit=     33.9 bar      Tcrit=     126.2 K      omega=        .039\n',
                                                    'NH3(g)': '     Pcrit=    113.5 bar      Tcrit=     405.5 K      omega=        .250\n',
                                                    'O2(g)': '     Pcrit=     50.4 bar      Tcrit=     154.6 K      omega=        .025\n',
                                                    'SO2(g)': '     Pcrit=     78.8 bar      Tcrit=     430.8 K      omega=        .256\n'}}
            if dataset_format == 'apr20':
                if j in fugacity_info['fugacity_chi'].keys():
                    fout.writelines("%s" % fugacity_info['fugacity_chi'][j])
                if j in fugacity_info['fugacity_Pcrit'].keys():
                    fout.writelines("%s" % fugacity_info['fugacity_Pcrit'][j])

            if sourceformat != 'EQ36':
                fout.writelines( "     %s species in reaction\n" % sourcedic[j][1])
            else:
                fout.writelines( "     %s species in reaction\n" % (sourcedic[j][1] - 1) )
            Rxn = sourcedic[j][2:] if sourceformat != 'EQ36' else sourcedic[j][4:]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 7) | (i == 13):
                    fout.writelines( "    %9.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                else:
                    if sourceformat != 'EQ36':
                        fout.writelines( "%-9s     " %  (Rxn[i - 1]))
                    else:
                        fout.writelines( "%-9s     " %  (Rxn[i - 1]).replace('O2(g)', 'O2(aq)'))
                if (i % 6 == 0) | (i % 12 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                               Dielec_method = Dielec_method, sourceformat = sourceformat, **rhoEG)[0]
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j,
                                                      sourcedic = sourcedic, dbacessdic = dbacessdic,
                                                      rhoEGextrap = rhoEGextrap, specielist = specielist,
                                                      Dielec_method = Dielec_method, sourceformat = sourceformat)

            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            if sourceformat == 'EQ36' and 'O2(g)' in Rxn:
                coeff_O2 = float(Rxn[Rxn.index('O2(g)') - 1])
                logK = np.where(logK != 500, logK + coeff_O2*logK_rebal, logK)
            if (dataset_format ==  'apr20') & (logK_form == 'polycoeffs'):
                TK = celsiusToKelvin(T)
                logKcorr = curve_fit(logKfunc, TK[logK!=500].ravel(), logK[logK!=500].ravel(), p0 = x0,  maxfev = 1000000)[0]
                fout.writelines('     a= %15.6f   ' % logKcorr[0] + 'b= %15.6f   ' % logKcorr[1] + \
                                'c= %15.5f\n' % logKcorr[2])
                fout.writelines('     d= %15.2f   ' % logKcorr[3] + 'e= %15.2f   ' % logKcorr[4] + \
                                'f= %15.2f \n' % logKcorr[5])
                fout.writelines('     TminK= %-15.2f ' % np.min(TK) + 'TmaxK= %-7.2f\n' % np.max(TK))
            else:
                for i in range(len(logK)):
                    i = i + 1
                    fout.writelines( "      %9.4f" %  logK[i-1])
                    if (i % 4 == 0) | (i == len(logK)):
                        fout.writelines( "\n")

            ref = dbacessdic[k][1].split('  ')[0]
            ref = ref.split(':')[1] if ('ref' in ref) or ('REF' in ref) else ref
            fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
            fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
            fout.writelines( "*    reference-state data source = %s\n" % ref )
            fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dbacessdic[k][2]/1000) )
            fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dbacessdic[k][3]/1000))
            fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % dbacessdic[k][4])
            fout.writelines( "\n")
        else:
            continue

    fout.writelines( "-end-\n\n")

    if sourceformat != 'EQ36':

        #only used for counter
        counter = 0
        for j in specielist[7]:
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
            rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
            if (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
                counter +=1
        fout.writelines( "   %s oxides\n\n" % counter )

        #%% Oxides reactions
        #skip lines till "references" rows
        for i in range(20000):
            s = fid.readline()
            # print(s)
            if s.strip(' \n*').lstrip('0123456789.- ').startswith(("references", 'virial coefficients', 'Virial coefficients', 'SIT epsilon coefficients', 'Pitzer parameters')):
                break

        for j in specielist[7]:
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
            rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
            if (len([k for k in rxnlst if k not in missing_species]) == len(rxnlst)):
                fout.writelines("%s\n" % j)
                fout.writelines( "      mole wt.=   %1.4f g\n" %  MWdic[j])
                fout.writelines( "     %s species in reaction\n" % sourcedic[j][1])
                Rxn = sourcedic[j][2:]
                for i in range(len(Rxn)):
                    i = i + 1
                    if (i == 1) | (i == 7) | (i == 13):
                        fout.writelines( "    %9.4f " %  float(Rxn[i - 1]))
                    elif i % 2 != 0:
                        fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                    else:
                        fout.writelines( "%-9s     " %  (Rxn[i - 1]))
                    if (i % 6 == 0) | (i % 12 == 0) | (i == len(Rxn)):
                        fout.writelines( "\n")
                fout.writelines( "\n")
            else:
                continue

    else:
        fout.writelines( "   0 oxides\n\n" )

    #%% Pitzer parameters
    if activity_model == 'debye-huckel':
        fout.writelines( "-end-\n\n")
    elif activity_model == 'h-m-w':
        fout.writelines( "-end-\n*\n")
        if sourceformat == 'GWB':
            fout.writelines(s)
            for i in range(5000):
                s = fid.readline()
                fout.writelines(s)
                if not s.strip(' \n').startswith("*"):
                    break

            for i in range(5000):
                s = fid.readline()
                if any(s.lstrip().rstrip('\n').startswith(x) for x in act_param['act_list']):
                    rowlst = []; rowlst.append(s)
                    for i in range(50):
                        s = fid.readline()
                        if s.lstrip().rstrip('\n').startswith(""):
                            break
                        rowlst.append(s)
                    if all([x not in missing_species for x in rowlst[0].rstrip('\n').split()]):
                        fout.writelines(rowlst)
                        fout.writelines( "\n")
                else:
                    fout.writelines(s)
        elif sourceformat == 'EQ36':
            fout.writelines("* Pitzer parameters are represented by the 25C-centric four-term temperature \n" + \
                            "* function given by: \n" +  "* \n" + \
                            "* x(T) = a1 + a2*(1/T - 1/298.15) + a3*ln(T/298.15) + a4*(T - 298.15) \n" + "*  \n" + \
                            "* where T is temperature in Kelvin and a1 through a4 denote the temperature  \n" + \
                            "* function fitting coefficients for the temperature-dependent Pitzer  \n" + \
                            "* parameters. The conversion of non-standard or expanded forms of Pitzer  \n" + \
                            "* interaction parameters recently adopted by several workers for highly  \n" + \
                            "* soluble salts to the standard form currently embedded in EQ3/6 Version 8.0  \n" + \
                            "* was conducted using the approach described in 02Rard/Wij.  This conversion  \n" + \
                            "* imposes usage limits on these parameters within a valid range of temperature  \n" + \
                            "* and ionic strength. \n" + "*  \n" + \
                            "* In GWB (Version 9.0.1 or lower) Pitzer parameters are represented by the  \n" + \
                            "* 25C-centric five-term temperature function given by: \n" + "*  \n" + \
                            "* val = val25 + c1*(Tk-Tr) + c2*(1/Tk-1/Tr) + c3*ln(Tk/Tr) + c4(Tk^2-Tr^2) \n" + "*  \n" + \
                            "* So the last temperature term (c4(Tk^2-Tr^2)) will be set to zero \n" + \
                            "* since no such term is available in the data0.ypf.R0. \n" + "*  \n" + \
                            "* In GWB (Version 9.0.2 or higher) Pitzer parameters are represented by the  \n" + \
                            "* 25C-centric six-term temperature function given by: \n" + \
                            "* val = val25 + c1*(Tk-Tr) + c2*(1/Tk-1/Tr) + c3*ln(Tk/Tr) + c4(Tk^2-Tr^2) + c5(1/Tk^2-1/Tr^2) \n" + \
                            "* So the last two temperature terms (c4(Tk^2-Tr^2) and c5(1/Tk^2-1/Tr^2))   \n" + \
                            "* will be set to zero (or left blank) since no such term is available.  \n\n")
            for k in act_param['alpha_beta'].keys():
                if all([x not in missing_species for x in k.rstrip('\n').split()]):
                    ks = k.rstrip('\n').split()
                    fout.writelines('%-8s\n' % ks[0]) if len(ks) == 1 else fout.writelines('%-8s  %-8s\n' % (ks[0], ks[1]))  if len(ks) == 2 else fout.writelines('%-8s  %-8s  %-8s\n' % (ks[0], ks[1], ks[2]))
                    lst = ['beta0', 'beta1', 'beta2', 'cphi', 'alpha1', 'alpha2']
                    for order in lst:
                        fout.writelines('     %-6s  = %s \n' % (order, act_param[order][k]))
                    fout.writelines('\n')
            fout.writelines('-end- end of beta set, begin with theta set of 2nd virial coefficients  \n\n')
            for k in act_param['theta'].keys():
                if all([x not in missing_species for x in k.rstrip('\n').split()]):
                    ks = k.rstrip('\n').split()
                    fout.writelines('%-8s\n' % ks[0]) if len(ks) == 1 else fout.writelines('%-8s  %-8s\n' % (ks[0], ks[1]))  if len(ks) == 2 else fout.writelines('%-8s  %-8s  %-8s\n' % (ks[0], ks[1], ks[2]))
                    fout.writelines('     %-6s  = %s \n' % ('theta', act_param['theta'][k]))
                    fout.writelines('\n')
            fout.writelines('-end-  end of theta set, begin with lambda set  \n\n')
            for k in act_param['lambda'].keys():
                if all([x not in missing_species for x in k.rstrip('\n').split()]):
                    ks = k.rstrip('\n').split()
                    fout.writelines('%-8s\n' % ks[0]) if len(ks) == 1 else fout.writelines('%-8s  %-8s\n' % (ks[0], ks[1]))  if len(ks) == 2 else fout.writelines('%-8s  %-8s  %-8s\n' % (ks[0], ks[1], ks[2]))
                    fout.writelines('     %-6s  = %s \n' % ('lambda', act_param['lambda'][k]))
                    fout.writelines('\n')
            fout.writelines('-end- end of lambda set, begin with psi set  \n\n')
            for k in act_param['psi'].keys():
                if all([x not in missing_species for x in k.rstrip('\n').split()]):
                    ks = k.rstrip('\n').split()
                    fout.writelines('%-8s\n' % ks[0]) if len(ks) == 1 else fout.writelines('%-8s  %-8s\n' % (ks[0], ks[1]))  if len(ks) == 2 else fout.writelines('%-8s  %-8s  %-8s\n' % (ks[0], ks[1], ks[2]))
                    fout.writelines('     %-6s  = %s \n' % ('psi', act_param['psi'][k]))
                    fout.writelines('\n')
            fout.writelines('-end- end of psi set \n')

    fout.writelines('* references\n')
    fout.writelines('** Please copy references to here from the corresponding\n')
    fout.writelines('** sequential-access version of the direct-access SUPCRT database.\n* stop.\n\n')

    #%% close all files
    fid.close()
    fout.close()

    if clay_thermo.lower() == 'yes':
        fclay.close()

    return print('Success, your new GWB database is ready for download')

def write_EQ36db(T, P, **kwargs ):
    """
    This function writes the new EQ3/6 database into a new folder called "output"   \n
    Parameters
    ----------
        T               :    temperature [°C]   \n
        P               :    pressure [bar]   \n
        nCa_cpx         :    number of moles of Ca in solid solution of clinopyroxene (optional)
                                if it is ommitted solid solution of clinopyroxene will not
                                be included   \n
        solid_solution  :    bool to specify the inclusion of solid-solution [True or False],
                                default is 'False'   \n
        clay_thermo     :    specify the inclusion of clay thermodynamic properties [Yes or No],
                                default is 'No'   \n
        dbaccess        :    direct-access database filename and location  (optional)  \n
        sourcedb        :    source database filename and location  (optional)  \n
        objdb           :    new database filename and location    (optional) \n
        co2actmodel     :    co2 activity model equation [Duan_Sun or Drummond]  (optional),
                                if not specified, default is 'Drummond'   \n
        Dielec_method   :    specify either 'FE97' or 'JN91' as the method to calculate
                                dielectric constant, default is 'JN91'   (optional) \n
    Returns
    -------
        Outputs the new database to an ASCII file with filename described in 'objdb'.   \n
    Usage
    -------
     Example:
         (1) General format with default dielectric constant and CO2 activity model and exclusions
             of solid solutions   \n
             write_EQ36db(T, P, dbaccess = 'location', sourcedb = 'location', objdb = 'location')   \n
         (2) Inclusion of solid solutions and clay thermo and exclusion of solid solution of clinopyroxene  \n
             write_EQ36db(T, P, solid_solution = 'Yes', clay_thermo = 'Yes', dbaccess = 'location,
                          sourcedb = 'location', objdb = 'location')   \n
         (3) Inclusion of all solid solutions and clay thermo with \emph{'Duan_Sun'} CO2 activity model and 'FE97'
             dielectric constant calculation \n
             write_EQ36db(T, P, nCa, solid_solution = 'Yes', clay_thermo = 'Yes', dbaccess = 'location',
                          sourcedb = 'location', objdb = 'location', co2actmodel = 'Duan_Sun',
                          Dielec_method = 'FE97')   \n

    """

    kwargs = dict({"nCa_cpx": None,  "solid_solution": None,
                  "clay_thermo": None,   "dbaccess": None,
                  "sourcedb": None,   "objdb": None,
                  "Dielec_method": None
                  }, **kwargs)
    solid_solution = kwargs['solid_solution'];      clay_thermo = kwargs['clay_thermo']
    dbaccess = kwargs['dbaccess'];                  sourcedb = kwargs['sourcedb']
    objdb = kwargs['objdb'];                        Dielec_method = kwargs['Dielec_method']
    nCa_cpx = kwargs['nCa_cpx']

    if dbaccess == None:
        dbaccess = './default_db/speq21.dat'
    dbaccess = os.path.join(os.path.dirname(os.path.abspath(__file__)), dbaccess)

    if sourcedb == None:
        sourcedb = './default_db/data0.dat'
    sourcedb = os.path.join(os.path.dirname(os.path.abspath(__file__)), sourcedb)

    dbacessdic, dbname = readAqspecdb(dbaccess)
    sourcedic, specielist, chargedic, MWdic, block_info, Elemlist, act_param = readSourceEQ36db(sourcedb)

    if np.ndim(T) == 0:
        T = np.ravel(T)
    elif np.size(T) == 2:
        T = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(T[0], T[-1], 8)])

    if np.size(P) <= 2:
        P = np.ravel(P)
        P = P[0]*np.ones(np.size(T))

    if os.path.exists(os.path.join(os.getcwd(), 'output')) == False:
            os.makedirs(os.path.join(os.getcwd(), 'output'))

    logKnan_alert = False
    missing_species = []
    all_species_source = [[i]+k for i, k in sourcedic.items() if i not in (['eh', 'e-', 'H2O']) ]
    all_species_source = [[k for j, k in enumerate(all_species_source[i])
                           if (j not in [1, 3, 4] and k not in specielist[0] and str(k).strip('0123456789.- ') != '') ]
                          if  (i <= len(specielist[0]))
                          else [k for j, k in enumerate(all_species_source[i])
                                if (j not in [1, 3, 4] and str(k).strip('0123456789.- ') != '') ]
                          for i in range(len(all_species_source)) ]
    for num in range(len(all_species_source)): #
        if num < len(all_species_source):
            lst = [v for v in all_species_source[num] if v not in (specielist[7] + ['eh', 'e-', 'H2O']) ]

            bool_miss = [x.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
                         not in dbacessdic.keys() for x in lst  ]
            if any(bool_miss):
                sublist = [i for (i, v) in zip(lst, bool_miss) if v ]
                if lst[0] not in sublist:
                    missing_species.append([lst[0]] + sublist)
                else:
                    missing_species.append(sublist)

    missingfile = open(os.path.join(os.path.abspath("."),'output', 'spxNotFound.txt'), 'w')
    #missing_species = [i for i in missing_species if len(i)<1]
    for line in missing_species:
        if len(line) > 0:
            missingfile.writelines(line[0])
            missingfile.writelines('\n')
            for i in range(len(line)):
                missingfile.writelines('   %s' % line[i])
                missingfile.writelines('\n')
    missingfile.close()
    missing_species = [item for sublist in missing_species for item in sublist]
    # missing_species = list(set(missing_species)) #[i for n, i in enumerate(missing_species) if i not in missing_species[:n]]
    elem_avail = list(set([k for i, j in enumerate([[i] + k for i, k in sourcedic.items()
                                                    if i not in missing_species])
                           for l, k in enumerate(j) if i < len(specielist[1]) and str(k).strip('0123456789.- ') != '' and
                           not str(k).endswith(("+", "-", '(aq)', '(g)')) and k not in ['O2', 'H2O'] and len(k) < 3]))
    all_species_source = [k for j in all_species_source for i, k in enumerate(j) ]
    # all_species_source = [k for j in all_species_source for i, k in enumerate(j)
    #                       if i!=1 and str(k).strip('0123456789.- ') != '']
    all_species_source = list(set(all_species_source))
    all_species_avail = [j for j in all_species_source if j not in missing_species]

    if objdb == None:
        objdb = 'data0.%s' % (int(P[0]))
    # timestr = '.' + time.strftime("%d%b%Y_%H%M")

    fout = open(os.path.join(os.path.abspath("."),'output', objdb + '.%s' % sourcedb.split('.')[-1]), 'w+')  # + timestr

    fid = open(sourcedb, 'r')

    s = fid.readline()
    fout.writelines(s)
    for i in range(500) :
        s = fid.readline()
        if s.startswith('Generated', 0) | s.startswith('Data', 0) | (s.rstrip('\n') == ""):
            break
    fout.writelines('CII: ' + ' pyGeochemCalc.2021' + '\n')
    fout.writelines('Generated by: ' + ' pyGeochemCalc, ' + time.ctime() + '\n')
    fout.writelines('Output package:  eq3\n' + 'Data set:        ' + dbname + '\n')

    #copy and paste lines till temperature rows
    for i in range(500) :
        s = fid.readline()
        fout.writelines(s)
        if s.startswith('+', 0):
            break
    s = fid.readline()
    fout.writelines(s)
    s = fid.readline()
    fout.writelines(s)
    s = fid.readline()
    fout.writelines(s)
    fout.writelines('      %9.4f %9.4f\n' % (T[0], T[-1]))

    [rho, dGH2O, dHH2O, SH2O, _, P, T] = iapws95(T = T, P = P)
    for i in range(50) :
        s = fid.readline()
        if s.strip('\n') in ['temperatures', 'Temperature grid (degC)']:
            break
    fout.writelines(s)
    for i in range(len(T)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "      %9.4f" %  T[i-1])
        else:
            fout.writelines( " %9.4f" %  T[i-1])
        if (i % 4 == 0) | (i == len(T)):
            fout.writelines( "\n")

    for i in range(50) :
        s = fid.readline()
        if s.strip('\n') in ['pressures', 'Pressure grid (bars)']:
            break
    fout.writelines(s)
    for i in range(len(P)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "      %9.4f" %  P[i-1])
        else:
            fout.writelines( " %9.4f" %  P[i-1])
        if (i % 4 == 0) | (i == len(P)):
            fout.writelines( "\n")

    #%% Calculation for debye huckel and bdot and water properties
    if Dielec_method is not None:
        if Dielec_method == 'FE97':
            [E, _, Adh, Bdh, bdot, _, _, _, _, _, _] = dielec_FE97(T, P)
        elif Dielec_method == 'JN91':
            [E, _, Adh, Bdh, bdot, _, _, _, _, _, _] = dielec_JN91(T, P)
    else:
        # default method: 'JN91'
        Dielec_method = 'JN91'
        [E, _, Adh, Bdh, bdot, _, _, _, _, _, _] = dielec_JN91(T, P)

    rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    # Calculate the rho E G for density extrapolation method here so we have it below
    rhoEGextrap = {}
    if any(rhoEG['rho'] < 350):
        subBornptrs = rhoEG['rho'] < 350
        for i, j in enumerate(zip(T[subBornptrs], P[subBornptrs])):
            rhoextrap = np.linspace(350, 550, 3)
            Pextrap = iapws95(T = j[0], rho = rhoextrap)[0]
            Textrap = j[0]*np.ones(np.size(Pextrap))

            dGH2O = iapws95(T = Textrap, P = Pextrap)[1]
            if Dielec_method is not None:
                if Dielec_method == 'FE97':
                    E = dielec_FE97(Textrap, rhoextrap)[0]
                elif Dielec_method == 'JN91':
                    E = dielec_JN91(Textrap, Pextrap)[0]
            else:
                # default method: 'JN91'
                E = dielec_JN91(Textrap, Pextrap)[0]
            rhoextrap = np.around(rhoextrap, 3)
            rhoEGextrap['%d_%d' % (j[0], j[1])]= {'rho': rhoextrap,'E': E, 'dGH2O': dGH2O,
                                                   'Textrap': Textrap, 'Pextrap': Pextrap}

    #skip lines till adh rows
    if act_param['activity_model'] == 'debye-huckel':
        for i in range(50) :
            s = fid.readline()
            if '(adh)' in s.strip('\n') or 'Debye-Huckel A_gamma' in s.strip('\n'):
                break
        fout.writelines(s)
        for i in range(len(Adh)):
            i = i + 1
            if (i == 1) | (i == 5):
                fout.writelines( "      %9.4f" %  Adh[i-1])
            else:
                fout.writelines( " %9.4f" %  Adh[i-1])
            if (i % 4 == 0) | (i == len(Adh)):
                fout.writelines( "\n")
        #skip lines till bdh rows
        for i in range(50) :
            s = fid.readline()
            if '(bdh)' in s.strip('\n') or 'Debye-Huckel B_gamma' in s.strip('\n'):
                break
        fout.writelines(s)
        for i in range(len(Bdh)):
            i = i + 1
            if (i == 1) | (i == 5):
                fout.writelines( "      %9.4f" %  Bdh[i-1])
            else:
                fout.writelines( " %9.4f" %  Bdh[i-1])
            if (i % 4 == 0) | (i == len(Bdh)):
                fout.writelines( "\n")
        #skip lines till bdot rows
        for i in range(50) :
            s = fid.readline()
            if 'bdot' in s.strip('\n') or 'B-dot' in s.strip('\n'):
                break
        fout.writelines(s)
        for i in range(len(bdot)):
            i = i + 1
            if (i == 1) | (i == 5):
                fout.writelines( "      %9.4f" %  bdot[i-1])
            else:
                fout.writelines( " %9.4f" %  bdot[i-1])
            if (i % 4 == 0) | (i == len(bdot)):
                fout.writelines( "\n")

        #skip lines till cco2 rows
        for i in range(50) :
            s = fid.readline()
            if 'cco2' in s.strip('\n'):
                break
        fout.writelines(s)
        s = fid.readline()
        fout.writelines(s)
        s = fid.readline()
        fout.writelines(s)
        s = fid.readline()
        fout.writelines(s)
    elif act_param['activity_model'] == 'h-m-w':
        for i in range(50) :
            s = fid.readline()
            if any(re.findall(r'|'.join(('aphi', 'debye huckel')), s.strip('\n'), re.IGNORECASE)):
                break
        fout.writelines(s)
        Aphi = Adh*np.log(10)/3
        for i in range(len(Aphi)):
            i = i + 1
            if (i == 1) | (i == 5):
                fout.writelines( "      %9.4f" %  Aphi[i-1])
            else:
                fout.writelines( " %9.4f" %  Aphi[i-1])
            if (i % 4 == 0) | (i == len(Aphi)):
                fout.writelines( "\n")

    for i in range(50) :
        s = fid.readline()
        if any(re.findall(r'|'.join(('log k for eh reaction', 'Eh reaction: logKr')), s.strip('\n'), re.IGNORECASE)):
            break
    fout.writelines(s)
    #%% Calculations for "log k for eh" rows
    logK = calcRxnlogK( T, P, 'eh', dbacessdic, sourcedic, specielist,
                       Dielec_method = Dielec_method, sourceformat = 'EQ36', **rhoEG)[0]
    # Utilized for density extrapolation of logK
    if any(rhoEG['rho'] < 350):
        logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'eh',
                                              dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap, sourcedic = sourcedic,
                                              specielist = specielist, Dielec_method = Dielec_method,
                                              sourceformat = 'EQ36')
    logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
    for i in range(len(logK)):
        i = i + 1
        if (i == 1) | (i == 5):
            fout.writelines( "      %9.4f" %  logK[i-1])
        else:
            fout.writelines( " %9.4f" %  logK[i-1])
        if (i % 4 == 0) | (i == len(logK)):
            fout.writelines( "\n")

    for i in range(50) :
        s = fid.readline()
        if s.startswith('+', 0):
            break
    if act_param['activity_model'] == 'debye-huckel':
        fout.writelines(s)
        s = fid.readline()
        fout.writelines(s)
        s = fid.readline()
        fout.writelines(s)
        s = fid.readline()
        fout.writelines(s)
        # copy and paste bdot parameters
        for i in range(3000) :
            s = fid.readline()
            s = s.replace(' acid','_acid').replace(' high','_high').replace(' low','_low') if 'acid' in s else s
            s_mod = s.split()[0]
            if s.startswith('+--', 0):
                break
            if s_mod in all_species_avail:
                fout.writelines(s)
            else:
                missing_species.append(s_mod)
        fout.writelines(s)
        s = fid.readline()
        fout.writelines(s)
        s = fid.readline()
        fout.writelines(s)
    elif act_param['activity_model'] == 'h-m-w':
        fout.writelines(s)
        s = fid.readline()
        fout.writelines(s)
        s = fid.readline()
        fout.writelines(s)
        for i in range(2000):
            s = fid.readline()
            if (s.rstrip('\n') == "elements"):
                break
            if any(s.lstrip().rstrip('\n').startswith(x) for x in act_param['act_list']):
                rowlst = []; rowlst.append(s)
                for i in range(50):
                    s = fid.readline()
                    if s.lstrip().rstrip('\n').startswith("+" + "-"*30):
                        break
                    rowlst.append(s)
                if all([x in all_species_avail for x in rowlst[0].rstrip('\n').split()]):
                    fout.writelines(rowlst)
                    fout.writelines( "+" + "-"*64 + "\n")
            else:
                #break
                fout.writelines(s)
        fout.writelines(s)
        s = fid.readline()
        fout.writelines(s)


    # copy and paste elements
    counter = 0
    for i in range(2000) :
        s = fid.readline()
        s_mod = s.split()[0]
        if s_mod in elem_avail:
            fout.writelines(s)
            counter = counter + 1
        if s.startswith('+', 0):
            break
    fout.writelines(s)
    s = fid.readline()
    fout.writelines(s)
    s = fid.readline()
    fout.writelines(s)

    #%% Basis reactions
    counter = 0
    for j in specielist[1]:
        k = j.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
        if j not in missing_species:
            fout.writelines('%s\n' % j)

            if j == 'O2(g)':
                fout.writelines(block_info['O2(g)_b'])
            else:
                fout.writelines(block_info[j])

            fout.writelines('****\n')
            fout.writelines( "     %s element(s):\n" % int(len(Elemlist[j])/2))
            Rxn = Elemlist[j]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 7):
                    fout.writelines( "    %9.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( "%-9s     " %  (Rxn[i - 1]))
                if (i % 6 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            fout.writelines('****\n')
            fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
            fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
            if j != 'H2O':
                ref = dbacessdic[k][1].split('  ')[0]
                ref = ref.split(':')[1] if ('ref' in ref) or ('REF' in ref) else ref
                dG, dH, S = dbacessdic[k][2], dbacessdic[k][3], dbacessdic[k][4]
            else:
                dG, dH, S, ref = dGH2O[0], dHH2O[0], SH2O[0], 'iapws95/' + Dielec_method
            fout.writelines( "*    ref-state data  [source:   %s  ]\n" % ref)
            fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dG/1000) )
            fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dH/1000))
            fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % S)
            fout.writelines( "+" + "-"*68 + "\n")
            counter = counter + 1
        else:
            continue
    fout.writelines( "auxiliary basis species\n")
    fout.writelines( "+" + "-"*68 + "\n")

    #%% Auxiliary Basis reactions
    for j in specielist[2]:
        k = j.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1, 2, 3]]
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([i for i in rxnlst if i not in missing_species]) == len(rxnlst)):
            if sourcedic[j][0] != '':
                fout.writelines('%-25s %s \n' % (j, sourcedic[j][0]))
            else:
                fout.writelines('%-25s %s \n' % (j, dbacessdic[k][0]))
            fout.writelines(block_info[j])

            fout.writelines('****\n')
            fout.writelines( "     %s element(s):\n" % int(len(Elemlist[j])/2))
            Rxn = Elemlist[j]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 7):
                    fout.writelines( "    %9.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( "%-9s     " %  (Rxn[i - 1]))
                if (i % 6 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            fout.writelines('****\n')
            fout.writelines( "     %s species in aqueous dissociation reaction:\n" % sourcedic[j][1])
            Rxn = sourcedic[j][2:]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 5) | (i == 9):
                    fout.writelines( "  %9.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( " %-21s     " %  (Rxn[i - 1]))
                if (i % 4 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            fout.writelines('*\n')
            fout.writelines('**** logK grid [T, P @ Miscellaneous parameters]\n')
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                                Dielec_method = Dielec_method, sourceformat = 'EQ36', **rhoEG)[0]
            if any(np.isnan(logK)):
                logKnan_alert = True
            else:
                logKnan_alert = False
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap, sourcedic = sourcedic,
                                                      specielist = specielist, Dielec_method = Dielec_method,
                                                      sourceformat = 'EQ36')
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            for i in range(len(logK)):
                i = i + 1
                if (i == 1) | (i == 5):
                    fout.writelines( "      %9.4f" %  logK[i-1])
                else:
                    fout.writelines( "  %9.4f" %  logK[i-1])
                if (i % 4 == 0) | (i == len(logK)):
                    fout.writelines( "\n")
            fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
            fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
            if j != 'H2O':
                ref = dbacessdic[k][1].split('  ')[0]
                ref = ref.split(':')[1] if ('ref' in ref) or ('REF' in ref) else ref
                dG, dH, S = dbacessdic[k][2], dbacessdic[k][3], dbacessdic[k][4]
            else:
                dG, dH, S, ref = dGH2O[0], dHH2O[0], SH2O[0], 'iapws95/' + Dielec_method
            fout.writelines( "*    ref-state data  [source:   %s  ]\n" % ref)
            fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dG/1000) )
            fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dH/1000))
            fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % S)
            fout.writelines( "+" + "-"*68 + "\n")
        else:
            continue
    fout.writelines( "aqueous species\n")
    fout.writelines( "+" + "-"*68 + "\n")
    if logKnan_alert == True:
        warnings.warn('Some temperature and pressure points are out of aqueous species HKF eqns regions of applicability, hence, density extrapolation has been applied')

    #%% Aqueous reactions
    for j in specielist[3]:
        k = j.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1, 2, 3]]
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ]
        if (j not in missing_species) and (len([i for i in rxnlst if i not in missing_species]) == len(rxnlst)):
            if sourcedic[j][0] != '':
                fout.writelines('%-25s %s \n' % (j, sourcedic[j][0]))
            else:
                fout.writelines('%-25s %s \n' % (j, dbacessdic[k][0]))
            fout.writelines(block_info[j])
            fout.writelines('****\n')
            fout.writelines( "     %s element(s):\n" % int(len(Elemlist[j])/2))
            Elem = Elemlist[j]
            for i in range(len(Elem)):
                i = i + 1
                if (i == 1) | (i == 7):
                    fout.writelines( "    %9.4f " %  float(Elem[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Elem[i - 1]))
                else:
                    fout.writelines( "%-9s     " %  (Elem[i - 1]))
                if (i % 6 == 0) | (i == len(Elem)):
                    fout.writelines( "\n")
            fout.writelines('****\n')
            fout.writelines( "     %s species in aqueous dissociation reaction:\n" % sourcedic[j][1])
            Rxn = sourcedic[j][2:]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 5) | (i == 9):
                    fout.writelines( "  %9.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( " %-21s     " %  (Rxn[i - 1]))
                if (i % 4 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            fout.writelines('*\n')
            fout.writelines('**** logK grid [T, P @ Miscellaneous parameters]\n')
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                                Dielec_method = Dielec_method, sourceformat = 'EQ36', **rhoEG)[0]
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap, sourcedic = sourcedic,
                                                      specielist = specielist, Dielec_method = Dielec_method,
                                                      sourceformat = 'EQ36')
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            for i in range(len(logK)):
                i = i + 1
                if (i == 1) | (i == 5):
                    fout.writelines( "      %9.4f" %  logK[i-1])
                else:
                    fout.writelines( "  %9.4f" %  logK[i-1])
                if (i % 4 == 0) | (i == len(logK)):
                    fout.writelines( "\n")
            fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
            fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
            if j != 'H2O':
                ref = dbacessdic[k][1].split('  ')[0]
                ref = ref.split(':')[1] if ('ref' in ref) or ('REF' in ref) else ref
                dG, dH, S = dbacessdic[k][2], dbacessdic[k][3], dbacessdic[k][4]
            else:
                dG, dH, S, ref = dGH2O[0], dHH2O[0], SH2O[0], 'iapws95/' + Dielec_method
            fout.writelines( "*    ref-state data  [source:   %s  ]\n" % ref)
            fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dG/1000) )
            fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dH/1000))
            fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % S)
            fout.writelines( "+" + "-"*68 + "\n")
        else:
            continue
    fout.writelines( "solids\n")
    fout.writelines( "+" + "-"*68 + "\n")

    #%% Mineral reactions
    solid_solution = 'no' if solid_solution is None else solid_solution
    clay_thermo = 'no' if clay_thermo is None else clay_thermo
    if solid_solution.lower() == 'yes':
        if nCa_cpx is None:
            nCa = 0
        else:
            nCa = nCa_cpx
        solidsolution_no = 11
        for XAn in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKAnAb(XAn, T, P, dbacessdic, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'AnAb', X = XAn,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, dataset = 'EQ36')

        for XFo in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKFoFa(XFo, T, P, dbacessdic, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'FoFa', X = XFo,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, dataset = 'EQ36')

        for XEn in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKEnFe(XEn, T, P, dbacessdic, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'EnFe', X = XEn,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, dataset = 'EQ36')

        if nCa > 0:
            for XMg in np.round(np.linspace(1, 0, solidsolution_no), 1):
                logK, Rxn = calclogKDiHedEnFe(nCa, XMg, T, P, dbacessdic, **rhoEG)
                # Utilized for density extrapolation of logK
                if any(rhoEG['rho'] < 350):
                    logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'DiHedEnFe',
                                                          dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                          X = XMg, Ca = nCa,
                                                          Dielec_method = Dielec_method)
                logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
                outputfmt(fout, logK, Rxn, dataset = 'EQ36')


    # clay minerals
    if clay_thermo.lower() == 'yes':
        fclay = open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'clay_elements.dat'), 'r')
        Rd = fclay.readlines()
        Rd = [j.replace('-','_').strip('\n') for j in Rd]
        for i in range(len(Rd)):
            if Rd[i].split(',')[0] in ['Berthierine_FeII', 'Berthierine_FeIII', 'Lizardite',
                                        'Cronstedtite', 'Mg-Cronstedtite', 'Greenalite', 'Hisingerite']:
                layering = '7A'
            elif Rd[i].split(',')[0] in ['Clinochlore', 'Chamosite', 'Amesite']:
                layering = '14A'
            else:
                layering = '10A'
            logK, Rxn = calclogKclays(T, P, dbacessdic, *Rd[i].split(','), group = layering, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'Clay', *Rd[i].split(','),
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      group = layering, Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, dataset = 'EQ36')

    # other minerals in the source database
    for j in specielist[4]:
        k = j.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1, 2, 3]]
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ]
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species]) == len(rxnlst)):
            if solid_solution.lower() == 'yes' and j in ['Anorthite', 'Albite', 'Forsterite',
                                                          'Fayalite', 'Enstatite', 'Ferrosilite']:
                continue
            elif solid_solution.lower() == 'yes' and (nCa == 1) and j in ['Diopside', 'Hedenbergite']:
                continue
            elif clay_thermo.lower() == 'yes' and j in [ Rd[h].split(',')[0] for h in range(len(Rd))]:
                continue
            else:
                if sourcedic[j][0] != '':
                    fout.writelines('%-25s %s \n' % (j, sourcedic[j][0]))
                else:
                    fout.writelines('%-25s %s \n' % (j, dbacessdic[k][0]))
                fout.writelines(block_info[j])

                fout.writelines('****\n')
                fout.writelines( "     %s element(s):\n" % int(len(Elemlist[j])/2))
                Elem = Elemlist[j]
                for i in range(len(Elem)):
                    i = i + 1
                    if (i == 1) | (i == 7) | (i == 13):
                        fout.writelines( "    %9.4f " %  float(Elem[i - 1]))
                    elif i % 2 != 0:
                        fout.writelines( "%9.4f " %  float(Elem[i - 1]))
                    else:
                        fout.writelines( "%-9s     " %  (Elem[i - 1]))
                    if (i % 6 == 0) | (i == len(Elem)):
                        fout.writelines( "\n")
                fout.writelines('****\n')
                fout.writelines( "     %s species in reaction:\n" % sourcedic[j][1])
                Rxn = sourcedic[j][2:]
                for i in range(len(Rxn)):
                    i = i + 1
                    if (i == 1) | (i == 5) | (i == 9) | (i == 13) | (i == 17) | (i == 21):
                        fout.writelines( "  %9.4f " %  float(Rxn[i - 1]))
                    elif i % 2 != 0:
                        fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                    else:
                        fout.writelines( " %-21s     " %  (Rxn[i - 1]))
                    if (i % 4 == 0) | (i == len(Rxn)):
                        fout.writelines( "\n")
                fout.writelines('*\n')
                fout.writelines('**** logK grid [T, P @ Miscellaneous parameters]\n')
                logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                                    Dielec_method = Dielec_method, sourceformat = 'EQ36', **rhoEG)[0]
                # Utilized for density extrapolation of logK
                if any(rhoEG['rho'] < 350):
                    logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j,
                                                          dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap, sourcedic = sourcedic,
                                                          specielist = specielist, Dielec_method = Dielec_method,
                                                          sourceformat = 'EQ36')
                logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
                for i in range(len(logK)):
                    i = i + 1
                    if (i == 1) | (i == 5):
                        fout.writelines( "      %9.4f" %  logK[i-1])
                    else:
                        fout.writelines( "  %9.4f" %  logK[i-1])
                    if (i % 4 == 0) | (i == len(logK)):
                        fout.writelines( "\n")
                fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
                fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
                if j != 'H2O':
                    dG, dH, S = dbacessdic[k][2], dbacessdic[k][3], dbacessdic[k][4]
                else:
                    dG, dH, S, ref = dGH2O[0], dHH2O[0], SH2O[0], 'iapws95/' + Dielec_method
                fout.writelines( "*    ref-state data  [source:   %s  ]\n" % ref)
                fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dG/1000) )
                fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dH/1000))
                fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % S)
                fout.writelines( "*    Cp coefficients [source:   %s  ]\n" % ref)
                fout.writelines( "*         T**0   =   %11.8e  \n" % (dbacessdic[k][6]) )
                fout.writelines( "*         T**1   =   %11.8e  \n" % (dbacessdic[k][7]*10**-3))
                if dbacessdic[j][8] < 1:
                    fout.writelines( "*         T**-2  =  %12.8e  \n" % (dbacessdic[k][8]*10**5))
                else:
                    fout.writelines( "*         T**-2  =   %11.8e  \n" % (dbacessdic[k][8]*10**5))
                fout.writelines( "*         Tlimit =   %7.2fC  \n" % (dbacessdic[k][9]))


                fout.writelines( "+" + "-"*68 + "\n")
        else:
            continue
    fout.writelines( "liquids\n")
    fout.writelines( "+" + "-"*68 + "\n")


    #%% Liquids reactions
    for j in specielist[5]:
        k = j.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1, 2, 3]]
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ]
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            if sourcedic[j][0] != '':
                fout.writelines('%-25s %s \n' % (j, sourcedic[j][0]))
            else:
                fout.writelines('%-25s %s \n' % (j, dbacessdic[k][0]))
            fout.writelines(block_info[j])

            fout.writelines('****\n')
            fout.writelines( "     %s element(s):\n" % int(len(Elemlist[j])/2))
            Elem = Elemlist[j]
            for i in range(len(Elem)):
                i = i + 1
                if (i == 1) | (i == 7) | (i == 13):
                    fout.writelines( "    %9.4f " %  float(Elem[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Elem[i - 1]))
                else:
                    fout.writelines( "%-9s     " %  (Elem[i - 1]))
                if (i % 6 == 0) | (i == len(Elem)):
                    fout.writelines( "\n")
            fout.writelines('****\n')
            fout.writelines( "     %s species in reaction:\n" % sourcedic[j][1])
            Rxn = sourcedic[j][2:]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 5) | (i == 9) | (i == 13):
                    fout.writelines( "  %9.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( " %-21s     " %  (Rxn[i - 1]))
                if (i % 4 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            fout.writelines('*\n')
            fout.writelines('**** logK grid [T, P @ Miscellaneous parameters]\n')
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                                Dielec_method = Dielec_method, sourceformat = 'EQ36', **rhoEG)[0]
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap, sourcedic = sourcedic,
                                                      specielist = specielist, Dielec_method = Dielec_method,
                                                      sourceformat = 'EQ36')
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            for i in range(len(logK)):
                i = i + 1
                if (i == 1) | (i == 5):
                    fout.writelines( "      %9.4f" %  logK[i-1])
                else:
                    fout.writelines( "  %9.4f" %  logK[i-1])
                if (i % 4 == 0) | (i == len(logK)):
                    fout.writelines( "\n")
            fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
            fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
            if j == 'Quicksilver':
                fout.writelines( "*    alternate name = Quicksilver\n" )
            if j != 'H2O':
                dG, dH, S = dbacessdic[k][2], dbacessdic[k][3], dbacessdic[k][4]
            else:
                dG, dH, S, ref = dGH2O[0], dHH2O[0], SH2O[0], 'iapws95/' + Dielec_method
            fout.writelines( "*    ref-state data  [source:   %s  ]\n" % ref)
            fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dG/1000) )
            fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dH/1000))
            fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % S)
            fout.writelines( "*    Cp coefficients [source:   %s  ]\n" % ref)
            fout.writelines( "*         T**0   =   %11.8e  \n" % (dbacessdic[k][6]) )
            fout.writelines( "*         T**1   =   %11.8e  \n" % (dbacessdic[k][7]*10**-3))
            if dbacessdic[k][8] < 1:
                fout.writelines( "*         T**-2  =  %12.8e  \n" % (dbacessdic[k][8]*10**5))
            else:
                fout.writelines( "*         T**-2  =   %11.8e  \n" % (dbacessdic[k][8]*10**5))
            fout.writelines( "*         Tlimit =   %7.2fC  \n" % (dbacessdic[k][9]))
            fout.writelines( "+" + "-"*68 + "\n")
        else:
            continue
    fout.writelines( "gases\n")
    fout.writelines( "+" + "-"*68 + "\n")

    #%% Gases reactions
    for j in specielist[6]:
        k = j.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1, 2, 3]]
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ]
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            if sourcedic[j][0] != '':
                fout.writelines('%-25s %s \n' % (j, sourcedic[j][0]))
            else:
                fout.writelines('%-25s %s \n' % (j, dbacessdic[k][0]))
            fout.writelines(block_info[j])

            fout.writelines('****\n')
            fout.writelines( "     %s element(s):\n" % int(len(Elemlist[j])/2))
            Elem = Elemlist[j]
            for i in range(len(Elem)):
                i = i + 1
                if (i == 1) | (i == 7) | (i == 13):
                    fout.writelines( "    %9.4f " %  float(Elem[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Elem[i - 1]))
                else:
                    fout.writelines( "%-9s     " %  (Elem[i - 1]))
                if (i % 6 == 0) | (i == len(Elem)):
                    fout.writelines( "\n")
            fout.writelines('****\n')
            fout.writelines( "     %s species in reaction:\n" % sourcedic[j][1])
            Rxn = sourcedic[j][2:]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 5) | (i == 9) | (i == 13):
                    fout.writelines( "  %9.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( " %-21s     " %  (Rxn[i - 1]))
                if (i % 4 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            fout.writelines('*\n')
            fout.writelines('**** logK grid [T, P @ Miscellaneous parameters]\n')
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                                Dielec_method = Dielec_method, sourceformat = 'EQ36', **rhoEG)[0]
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap, sourcedic = sourcedic,
                                                      specielist = specielist, Dielec_method = Dielec_method,
                                                      sourceformat = 'EQ36')
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            for i in range(len(logK)):
                i = i + 1
                if (i == 1) | (i == 5):
                    fout.writelines( "      %9.4f" %  logK[i-1])
                else:
                    fout.writelines( "  %9.4f" %  logK[i-1])
                if (i % 4 == 0) | (i == len(logK)):
                    fout.writelines( "\n")
            fout.writelines( "*    gflag = 1 [reported delG0f used]\n" )
            fout.writelines( "*    extrapolation algorithm: supcrt92 [92joh/oel]\n" )
            if j != 'H2O':
                dG, dH, S = dbacessdic[k][2], dbacessdic[k][3], dbacessdic[k][4]
            else:
                dG, dH, S, ref = dGH2O[0], dHH2O[0], SH2O[0], 'iapws95/' + Dielec_method
            fout.writelines( "*    ref-state data  [source:   %s  ]\n" % ref)
            fout.writelines( "*         delG0f =   %8.3f  kcal/mol\n" % (dG/1000) )
            fout.writelines( "*         delH0f =   %8.3f  kcal/mol\n" % (dH/1000))
            fout.writelines( "*         S0PrTr =   %8.3f  cal/(mol*K)\n" % S)
            fout.writelines( "*    Cp coefficients [source:   %s  ]\n" % ref)
            fout.writelines( "*         T**0   =   %11.8e  \n" % (dbacessdic[k][6]) )
            fout.writelines( "*         T**1   =   %11.8e  \n" % (dbacessdic[k][7]*10**-3))
            if dbacessdic[j][8] < 1:
                fout.writelines( "*         T**-2  =  %12.8e  \n" % (dbacessdic[k][8]*10**5))
            else:
                fout.writelines( "*         T**-2  =   %11.8e  \n" % (dbacessdic[k][8]*10**5))
            fout.writelines( "*         Tlimit =   %7.2fC  \n" % (dbacessdic[k][9]))
            fout.writelines( "+" + "-"*68 + "\n")
        else:
            continue
    fout.writelines( "solid solutions\n")
    fout.writelines( "+" + "-"*68 + "\n")

    #%% Solid solution reactions
    for j in specielist[7]:
        rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]]
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ]
        if (len([k for k in rxnlst if k in missing_species]) == len(rxnlst)):
            if sourcedic[j][0] != '':
                fout.writelines('%-25s %s \n' % (j, sourcedic[j][0]))
            else:
                fout.writelines('%-25s %s \n' % (j, dbacessdic[j][0]))
            fout.writelines(block_info[j][0])

            fout.writelines( "  %s components\n" % sourcedic[j][1])
            Rxn = sourcedic[j][2:]
            for i in range(len(Rxn)):
                i = i + 1
                if (i == 1) | (i == 5) | (i == 9) | (i == 13):
                    fout.writelines( "  %9.4f " %  float(Rxn[i - 1]))
                elif i % 2 != 0:
                    fout.writelines( "%9.4f " %  float(Rxn[i - 1]))
                else:
                    fout.writelines( " %-21s     " %  (Rxn[i - 1]))
                if (i % 4 == 0) | (i == len(Rxn)):
                    fout.writelines( "\n")
            fout.writelines(block_info[j][1])
            fout.writelines( "+" + "-"*68 + "\n")
        else:
            continue

    fout.writelines( "references\n")
    fout.writelines( "+" + "-"*68 + "\n")
    fout.writelines('** Please copy references to here from the corresponding\n')
    fout.writelines('** sequential-access version of the direct-access SUPCRT database.\n stop.\n\n')

    #%% close all files
    fid.close()
    fout.close()
    if clay_thermo.lower() == 'yes':
        fclay.close()

    return print('Success, your new EQ3/6 database is ready for download')

def write_pflotrandb(T, P, **kwargs ):
    """
    This function writes the new pflotran database into a new folder called "output"   \n
    Parameters
    ----------
        T               :    temperature [°C]   \n
        P               :    pressure [bar]   \n
        nCa_cpx         :    number of moles of Ca in solid solution of clinopyroxene (optional)
                                if it is ommitted solid solution of clinopyroxene will not
                                be included   \n
        solid_solution  :    bool to specify the inclusion of solid-solution [Yes or No],
                                default is 'No'   \n
        clay_thermo     :    specify the inclusion of clay thermodynamic properties [Yes or No],
                                default is 'No'   \n
        dbaccess        :    direct-access database filename and location  (optional)  \n
        sourcedb        :    source database filename and location  (optional)  \n
        objdb           :    new database filename and location    (optional) \n
        Dielec_method   :    specify either 'FE97' or 'JN91' as the method to calculate
                                dielectric constant, default is 'JN91' (optional) \n
        sourceformat    :    source database format, either 'GWB' or 'EQ36', default is 'GWB'
    Returns
    -------
        Outputs the new database to an ASCII file with filename described in 'objdb'.   \n
    Usage
    -------
      Example:
          (1) General format with default dielectric constant and CO2 activity model and exclusions
              of solid solutions   \n
              write_pflotrandb(T, P, dbaccess = 'location', sourcedb = 'location',
                              objdb = 'location', sourceformat = 'EQ36')   \n
          (2) Inclusion of solid solutions and clay thermo and exclusion of solid solution of clinopyroxene  \n
              write_pflotrandb(T, P, solid_solution = 'Yes', clay_thermo = 'Yes', dbaccess = 'location,
                              sourcedb = 'location', objdb = 'location', sourceformat = 'EQ36')   \n
          (3) Inclusion of all solid solutions and clay thermo with \emph{'Duan_Sun'} CO2 activity model and 'FE97'
              dielectric constant calculation \n
              write_pflotrandb(T, P, nCa, solid_solution = 'Yes', clay_thermo = 'Yes', dbaccess = 'location',
                               sourcedb = 'location', objdb = 'location', co2actmodel = 'Duan_Sun',
                               Dielec_method = 'FE97', sourceformat = 'EQ36')   \n
    """
    kwargs = dict({"nCa_cpx": None,  "solid_solution": None,
                  "clay_thermo": None,   "dbaccess": None,
                  "sourcedb": None,   "objdb": None,
                  "Dielec_method": None,   "sourceformat": None
                  }, **kwargs)
    if kwargs['dbaccess'] == None:
        dbaccess = './default_db/speq21.dat'
    else:
        dbaccess = kwargs['dbaccess']
    dbaccess = os.path.join(os.path.dirname(os.path.abspath(__file__)), dbaccess)

    if kwargs['sourcedb'] == None:
        if (kwargs['sourceformat'] == None) or kwargs['sourceformat'] == 'GWB':
            sourcedb = './default_db/thermo.com.dat'
        elif kwargs['sourceformat'] == 'EQ36':
            sourcedb = './default_db/data0.dat'
    else:
        sourcedb = kwargs['sourcedb']
    sourcedb = os.path.join(os.path.dirname(os.path.abspath(__file__)), sourcedb)

    dbacessdic, dbname = readAqspecdb(dbaccess)

    if (kwargs['sourceformat'] == None) or kwargs['sourceformat'] == 'GWB':
        sourceformat = 'GWB'
        sourcedic, specielist, chargedic, MWdic, Mineraltype, fugacity_info, _ = readSourceGWBdb(sourcedb)
    elif kwargs['sourceformat'] == 'EQ36':
        sourceformat = 'EQ36'
        sourcedic, specielist, chargedic, MWdic, block_info, Elemlist, _ = readSourceEQ36db(sourcedb)

    if os.path.exists(os.path.join(os.getcwd(), 'output')) == False:
            os.makedirs(os.path.join(os.getcwd(), 'output'))

    periodic_table = open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                       'PeriodicTableJSON.json'), encoding='utf8')
    data = json.load(periodic_table)
    Element = {data['elements'][x]['symbol'] : pd.DataFrame([data['elements'][x]['name'],
                                                             data['elements'][x]['atomic_mass']],
                                                            index = ['name', 'mass']).T
               for x in range(len(data['elements']))}
    periodic_table.close()

    missing_species = []
    elemspeclist = [ symbol for x in specielist[0] for symbol, item in Element.items()
                    if item.name[0][:5] == x[:5] ] if sourceformat == 'GWB' else specielist[0]

    form_del = [1] if sourceformat == 'GWB' else [1, 3, 4]
    all_species_source = [[i]+k for i, k in sourcedic.items() if i not in (['eh', 'e-', 'H2O']) ]
    all_species_source = [[k for j, k in enumerate(all_species_source[i])
                           if (j not in form_del and k not in elemspeclist and str(k).strip('0123456789.- ') != '') ]
                          if  (i <= len(specielist[0]))
                          else [k for j, k in enumerate(all_species_source[i])
                                if (j not in form_del and str(k).strip('0123456789.- ') != '') ]
                          for i in range(len(all_species_source)) ]
    for num in range(len(all_species_source)): #
        if num < len(all_species_source):
            lst = [v for v in all_species_source[num] if v not in (['eh', 'e-', 'H2O']) ]

            bool_miss = [x.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
                         not in dbacessdic.keys() for x in lst  ]
            if any(bool_miss):
                sublist = [i for (i, v) in zip(lst, bool_miss) if v ]
                if lst[0] not in sublist:
                    missing_species.append([lst[0]] + sublist)
                else:
                    missing_species.append(sublist)

    missingfile = open(os.path.join(os.path.abspath("."),'output',  'spxNotFound.txt'), 'w')
    for line in missing_species:
        if len(line) > 0:
            missingfile.writelines(line[0])
            missingfile.writelines('\n')
            for i in range(len(line)):
                missingfile.writelines('   %s' % line[i])
                missingfile.writelines('\n')
    missingfile.close()
    missing_species = [item for sublist in missing_species for item in sublist]
    missing_species = [i for n, i in enumerate(missing_species) if i not in missing_species[:n]]
    logKnan_alert = False
    if kwargs['objdb'] == None:
        objdb = 'thermo_pflotran%sbars' % int(P[0])
    else:
        objdb = kwargs['objdb']
    # timestr = '.' + time.strftime("%d%b%y_%H%M")

    fout = open(os.path.join(os.path.abspath("."),'output',  objdb + '.dat'), 'w+') # + timestr

    if np.ndim(T) == 0 | np.ndim(P) == 0:
        T = np.ravel(T)
        P = np.ravel(P)
    [rho, dGH2O, dHH2O, SH2O, _, P, T] = iapws95(T = T, P = P)
    #%% Calculation for debye huckel and bdot and water properties
    if kwargs['Dielec_method'] is not None:
        if kwargs['Dielec_method'] == 'FE97':
            Dielec_method = 'FE97'
            E = dielec_FE97(T, P)[0]
        elif kwargs['Dielec_method'] == 'JN91':
            Dielec_method = 'JN91'
            E = dielec_JN91(T, P)[0]
    else:
        # default method: 'JN91'
        Dielec_method = 'JN91'
        E = dielec_JN91(T, P)[0]

    rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    # Calculate the rho E G for density extrapolation method here so we have it below
    rhoEGextrap = {}
    if any(rhoEG['rho'] < 350):
        subBornptrs = rhoEG['rho'] < 350
        for i, j in enumerate(zip(T[subBornptrs], P[subBornptrs])):
            rhoextrap = np.linspace(350, 550, 3)
            Pextrap = iapws95(T = j[0], rho = rhoextrap)[0]
            Textrap = j[0]*np.ones(np.size(Pextrap))

            dGH2O = iapws95(T = Textrap, P = Pextrap)[1]
            if Dielec_method is not None:
                if Dielec_method == 'FE97':
                    E = dielec_FE97(Textrap, rhoextrap)[0]
                elif Dielec_method == 'JN91':
                    E = dielec_JN91(Textrap, Pextrap)[0]
            else:
                # default method: 'JN91'
                E = dielec_JN91(Textrap, Pextrap)[0]
            rhoextrap = np.around(rhoextrap, 3)
            rhoEGextrap['%d_%d' % (j[0], j[1])]= {'rho': rhoextrap,'E': E, 'dGH2O': dGH2O,
                                                   'Textrap': Textrap, 'Pextrap': Pextrap}


    fout.writelines("'temperatures(degC) points' %s" % len(T))
    for i in range(len(T)):
        fout.writelines( " %6.1f" %  T[i])
    fout.writelines( "\n")
    fout.write('!:database is isobaric, at %d bars\n' % P[0])
    fout.write('!:basis_species  a0  valence  formula weight [g]\n')

    if sourceformat == 'EQ36':
        f_ionsize = open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'ion_size.txt'), 'r')
        Rd = f_ionsize.readlines()
        Rd = Rd[1:]
        ion_sizedic = {Rd[x].split()[0] : Rd[x].split()[1] for x in range(len(Rd))}
        f_ionsize.close()


    #%% Basis reactions
    for j in specielist[1]:
        if j not in missing_species:
            if sourceformat == 'GWB':
                charge = chargedic[j].lstrip().split()[1]
                ionsize = chargedic[j].lstrip().split()[4]
                MW = MWdic[j]
            elif sourceformat == 'EQ36':
                charge = chargedic[j].split()[-1]
                if j == 'O2(g)':
                    ionsize = [float(re.sub('[^0123456789\.]', '', x)) for x in block_info['O2(g)_b'] if x.strip('*    ').startswith('DHazero')]
                else:
                    ionsize = [float(re.sub('[^0123456789\.]', '', x)) for x in block_info[j] if x.strip('*    ').startswith('DHazero')]
                if any(ionsize) and any([MWdic[j]]):
                    ionsize, MW = ionsize[0], MWdic[j]
                else:
                    formula = j if sourcedic[j][0] == '' else sourcedic[j][0]
                    formula = formula.rstrip('(aq)(g)')
                    ionsize = 3 if j.endswith('(aq)') else float(ion_sizedic[j]) if j in ion_sizedic.keys() else 500 #
                    MW =  calc_elem_count_molewt(formula, Elementdic = Element)[-1]
            info = "'%s'" % j + ' ' + str(ionsize) + ' ' + charge +' ' + str(MW)
            fout.writelines('%s\n' % info)
        else:
            continue
    fout.write("'null' 0 0 0\n")

    fout.write("!:species_name  num (n_i A_i, i=1,num)  log K (1:8)  a0  valence  formula weight [g]\n")
    #%% Redox and Aqueous reactions
    for j in specielist[2] + specielist[3]:
        if sourceformat == 'GWB':
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]]
            charge = chargedic[j].lstrip().split()[1]
            ionsize = chargedic[j].lstrip().split()[4]
            MW, source_rxns = MWdic[j], sourcedic[j][2:]
        elif sourceformat == 'EQ36':
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1, 2, 3]]
            charge = chargedic[j].split()[-1]
            source_rxns = sourcedic[j][4:]
            ionsize = [float(re.sub('[^0123456789\.]', '', x)) for x in block_info[j] if x.strip('*    ').startswith('DHazero')]
            if any(ionsize) and any([MWdic[j]]):
                ionsize, MW = ionsize[0], MWdic[j]
            else:
                formula = j if sourcedic[j][0] == '' else sourcedic[j][0]
                formula = formula.rstrip('(aq)')
                ionsize = 3 if j.endswith('(aq)') else float(ion_sizedic[j]) if j in ion_sizedic.keys() else 500
                MW =  calc_elem_count_molewt(formula, Elementdic = Element)[-1]

        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ]
        if (j not in missing_species) and (len([i for i in rxnlst if i not in missing_species])==len(rxnlst)):
            name, species = j, sourcedic[j][1]
            Rxn = [row if i%2==0 else "'%s'" %row for i,row in enumerate(source_rxns)]
            Rxn = ' '.join(Rxn)
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                               Dielec_method = Dielec_method, sourceformat = sourceformat, **rhoEG)[0]
            if any(np.isnan(logK)):
                logKnan_alert = True
            else:
                logKnan_alert = False
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      sourcedic = sourcedic, specielist = specielist,
                                                      Dielec_method = Dielec_method,
                                                      sourceformat = sourceformat)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            list_logk = ' '.join(str("%9.4f" % e) for e in list(logK))
            info = "'%s'" % name + ' ' + str(species) + ' ' + Rxn + ' ' + list_logk + ' ' + str(ionsize) +\
                ' ' + charge +' ' + str(MW)
            fout.writelines('%s\n' % info)

        else:
            continue
    fout.writelines("'null' 1 0. '0' 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.\n")
    if logKnan_alert == True:
        warnings.warn('Some temperature and pressure points are out of aqueous species HKF eqns regions of applicability, hence, density extrapolation has been applied')

    fout.write("!:gas_name molar_vol  num (n_i A_i, i=1,num) log K (1:8)  formula weight [g]\n")
    #%% Gas reactions
    for j in specielist[6]:
        if sourceformat == 'GWB':
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]]
            source_rxns = sourcedic[j][2:]
        elif sourceformat == 'EQ36':
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1, 2, 3]]
            source_rxns = sourcedic[j][4:]
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species]) == len(rxnlst)):
            name, species = j, sourcedic[j][1]
            if MWdic[j] != [] or MWdic[j] != '':
                MW = MWdic[j]
            else:
                formula = j.rstrip('(g)')
                MW =  calc_elem_count_molewt(formula, Elementdic = Element)[-1]
            Rxn = [row if i%2 == 0 else "'%s'" %row for i,row in enumerate(source_rxns)]
            Rxn = ' '.join(Rxn)
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                                Dielec_method = Dielec_method, sourceformat = sourceformat, **rhoEG)[0]
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      sourcedic = sourcedic, specielist = specielist,
                                                      Dielec_method = Dielec_method,
                                                      sourceformat = sourceformat)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            list_logk = ' '.join(str("%9.4f" % e) for e in list(logK))
            info = "'%s'" % name + ' ' + '0.000' + ' ' + str(species) + ' ' + Rxn +\
                ' ' + list_logk + ' ' + str(MW)
            fout.writelines('%s\n' % info)
        else:
            continue

    fout.write("'null' 0. 1 1. '0' 0. 0. 0. 0. 0. 0. 0. 0. 0.\n")

    fout.write("!:mineral_name molar_vol  num (n_i A_i, i=1,num) log K (1:8)  formula weight [g]\n")
    #%% Mineral reactions
    solid_solution = 'no' if kwargs['solid_solution'] is None else kwargs['solid_solution']
    clay_thermo = 'no' if kwargs['clay_thermo'] is None else kwargs['clay_thermo']
    if kwargs['nCa_cpx'] is None:
        nCa = 0
    else:
        nCa = kwargs['nCa_cpx']
    if solid_solution.lower() == 'yes':
        solidsolution_no = 11
        for XAn in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKAnAb(XAn, T, P, dbacessdic, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'AnAb',
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap, X = XAn,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, dataset = 'Pflotran')

        for XFo in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKFoFa(XFo, T, P, dbacessdic, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'FoFa',
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap, X = XFo,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, dataset = 'Pflotran')

        for XEn in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKEnFe(XEn, T, P, dbacessdic, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'EnFe',
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap, X = XEn,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, dataset = 'Pflotran')

        if nCa > 0:
            for XMg in np.round(np.linspace(1, 0, solidsolution_no), 1):
                logK, Rxn = calclogKDiHedEnFe(nCa, XMg, T, P, dbacessdic, **rhoEG)
                # Utilized for density extrapolation of logK
                if any(rhoEG['rho'] < 350):
                    logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'DiHedEnFe',
                                                          dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap, X = XMg, Ca = nCa,
                                                          Dielec_method = Dielec_method)
                logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
                outputfmt(fout, logK, Rxn, dataset = 'Pflotran')

    # clay minerals
    if clay_thermo.lower() == 'yes':
        fclay = open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'clay_elements.dat'), 'r')
        Rd = fclay.readlines()
        Rd = [j.replace('-','_').strip('\n') for j in Rd]
        for i in range(len(Rd)):
            if Rd[i].split(',')[0] in ['Berthierine_FeII', 'Berthierine_FeIII', 'Lizardite',
                                        'Cronstedtite', 'Mg-Cronstedtite', 'Greenalite', 'Hisingerite']:
                layering = '7A'
            elif Rd[i].split(',')[0] in ['Clinochlore', 'Chamosite', 'Amesite']:
                layering = '14A'
            else:
                layering = '10A'

            logK, Rxn = calclogKclays(T, P, dbacessdic, *Rd[i].split(','), group = layering, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'Clay',
                                                      *Rd[i].split(','), dbacessdic = dbacessdic,
                                                      rhoEGextrap = rhoEGextrap, group = layering,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, dataset = 'Pflotran')

    if sourceformat == 'GWB':
        speclst = specielist[5]
    elif sourceformat == 'EQ36':
        speclst = specielist[4]+specielist[5]
    for j in speclst:
        if sourceformat == 'GWB':
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]]
            source_rxns = sourcedic[j][2:]
        elif sourceformat == 'EQ36':
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1, 2, 3]]
            source_rxns = sourcedic[j][4:]
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            if solid_solution.lower() == 'yes' and j in ['Anorthite', 'Albite', 'Forsterite',
                                                'Fayalite', 'Enstatite', 'Ferrosilite']:
                continue
            elif solid_solution.lower() == 'yes' and (nCa == 1) and j in ['Diopside', 'Hedenbergite']:
                continue
            elif clay_thermo.lower() == 'yes' and j in [ Rd[h].split(',')[0] for h in range(len(Rd))]:
                continue
            else:
                k = j.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
                MW, MV, species = MWdic[j], dbacessdic[k][5], sourcedic[j][1]
                Rxn = [row if i%2 == 0 else "'%s'" %row for i, row in enumerate(source_rxns)]
                Rxn = ' '.join(Rxn)

                logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                                    Dielec_method = Dielec_method, sourceformat = sourceformat, **rhoEG)[0]
                # Utilized for density extrapolation of logK
                if any(rhoEG['rho'] < 350):
                    logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j,
                                                          dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                          sourcedic = sourcedic, specielist = specielist,
                                                          Dielec_method = Dielec_method,
                                                          sourceformat = sourceformat)
                logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
                list_logk = ' '.join(str("%9.4f" % e) for e in list(logK))
                info = "'%s'" % j + ' ' + "%7.3f" % (MV) + ' ' + str(species) + ' ' + \
                    Rxn + ' ' + list_logk + ' ' + "%8.4f" % (MW)
                fout.writelines('%s\n' % info)

        else:
            continue

    fout.write("'null' 1 0. '0' 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.\n")

    fout.write("!:oxide_name molar_vol  num (n_i A_i, i=1,num) log K (1:8)  formula weight [g]\n")
    #%% Oxides reactions
    if sourceformat == 'GWB':
        for j in specielist[7]:
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]] # remove formula and specie number
            rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ] # remove all coefficients
            if (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
                MW, species = MWdic[j], sourcedic[j][1]
                Rxn = [row if i%2 == 0 else "'%s'" %row for i,row in enumerate(sourcedic[j][2:])]
                Rxn = ' '.join(Rxn)
                list_logk = ' '.join(['500.00']*len(T))
                info = "'%s'" % j + ' ' + '0.000' + ' ' + str(species) + ' ' + Rxn +\
                    ' ' + list_logk + ' ' + str(MW)
                fout.writelines('%s\n' % info)
            else:
                continue

        fout.write("'null' 0. 1 1. '0' 0. 0. 0. 0. 0. 0. 0. 0. 0.\n")

    #%% close all files
    fout.close()
    if clay_thermo.lower() == 'yes':
        fclay.close()
    return print('Success, your new Pflotran database is ready for download')

def write_ToughReactdb(T, P, **kwargs ):
    """
    This function writes the new ToughReact database into a new folder called "output"   \n
    Parameters
    ----------
        T               :    temperature [°C]   \n
        P               :    pressure [bar]   \n
        nCa_cpx         :    number of moles of Ca in solid solution of clinopyroxene (optional)
                                if it is ommitted solid solution of clinopyroxene will not
                                be included   \n
        solid_solution  :    bool to specify the inclusion of solid-solution [Yes or No],
                                default is 'No'   \n
        clay_thermo     :    specify the inclusion of clay thermodynamic properties [Yes or No],
                                default is 'No'   \n
        dbaccess        :    direct-access database filename and location  (optional)  \n
        sourcedb        :    source database filename and location  (optional)  \n
        objdb           :    new database filename and location    (optional) \n
        Dielec_method   :    specify either 'FE97' or 'JN91' as the method to calculate
                                dielectric constant, default is 'JN91'   (optional) \n
        sourceformat    :    source database format, either 'GWB' or 'EQ36', default is 'GWB'
    Returns
    -------
        Outputs the new database to an ASCII file with filename described in 'objdb'.   \n
    Usage
    -------
      Example:
          (1) General format with default dielectric constant and CO2 activity model and exclusions
              of solid solutions   \n
              write_ToughReactdb(T, P, dbaccess = 'location', sourcedb = 'location',
                                objdb = 'location', sourceformat = 'GWB')   \n
          (2) Inclusion of solid solutions and clay thermo and exclusion of solid solution of clinopyroxene  \n
              write_ToughReactdb(T, P, solid_solution = 'Yes', clay_thermo = 'Yes', dbaccess = 'location',
                                sourcedb = 'location', objdb = 'location', sourceformat = 'GWB')   \n
          (3) Inclusion of all solid solutions and clay thermo with \emph{'Duan_Sun'} CO2 activity model and 'FE97'
              dielectric constant calculation \n
              write_ToughReactdb(T, P, nCa, solid_solution = 'Yes', clay_thermo = 'Yes', dbaccess = 'location',
                                sourcedb = 'location', objdb = 'location', co2actmodel = 'Duan_Sun',
                                Dielec_method = 'FE97', sourceformat = 'GWB')   \n

    """

    kwargs = dict({"nCa_cpx": None,  "solid_solution": None,
                  "clay_thermo": None,   "dbaccess": None,
                  "sourcedb": None,   "objdb": None,
                  "Dielec_method": None,   "sourceformat": None
                  }, **kwargs)

    if kwargs['dbaccess'] == None:
        dbaccess = './default_db/speq21.dat'
    else:
        dbaccess = kwargs['dbaccess']
    dbaccess = os.path.join(os.path.dirname(os.path.abspath(__file__)), dbaccess)

    if kwargs['sourcedb'] == None:
        if (kwargs['sourceformat'] == None) or kwargs['sourceformat'] == 'GWB':
            sourcedb = './default_db/thermo.com.dat'
        elif kwargs['sourceformat'] == 'EQ36':
            sourcedb = './default_db/data0.dat'
    else:
        sourcedb = kwargs['sourcedb']
    sourcedb = os.path.join(os.path.dirname(os.path.abspath(__file__)), sourcedb)

    dbacessdic, dbname = readAqspecdb(dbaccess)

    if np.ndim(T) == 0:
        T = np.ravel(T)
    elif np.size(T) == 2:
        T = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(T[0], T[-1], 8)])

    if np.size(P) <= 2:
        P = np.ravel(P)
        P = P[0]*np.ones(np.size(T))

    if (kwargs['sourceformat'] == None) or kwargs['sourceformat'] == 'GWB':
        sourceformat = 'GWB'
        sourcedic, specielist, chargedic, MWdic, Mineraltype, fugacity_info, _ = readSourceGWBdb(sourcedb)
    elif kwargs['sourceformat'] == 'EQ36':
        sourceformat = 'EQ36'
        sourcedic, specielist, chargedic, MWdic, block_info, Elemlist, _ = readSourceEQ36db(sourcedb)

    rej = {'H+' : 3.08, 'Li+' : 1.64, 'Na+': 1.910, 'K+' : 2.27, 'Rb+' : 2.41, 'Cs+' : 2.61,
            'NH4+' : 2.31, 'Ag+' : 2.20, 'Au+' : 2.31, 'Cu+' : 1.90, 'Mg++' : 2.54, 'Sr++' : 3.0,
            'Ca++' : 2.87, 'Ba++' : 3.22, 'Pb++' : 3.08, 'Zn++' : 2.62, 'Cu++': 2.60, 'Cd++' : 2.85,
            'Hg++' : 2.98, 'Fe++' : 2.62, 'Mn++' : 2.68, 'Fe+++' : 3.46, 'Al+++' : 3.33, 'Au+++' : 3.72,
            'La+++' : 3.96, 'Gd+++' : 3.79, 'In+++' : 3.63, 'Ga+++' : 3.44, 'Tl+++' : 3.77, 'F-' : 1.33,
            'Cl-' : 1.810, 'Br-' : 1.96, 'I-' : 2.20, 'OH-' : 1.40, 'HS-' : 1.84, 'NO3-' : 2.81,
            'HCO3-' : 2.10, 'HSO4-' : 2.37, 'ClO4-' : 3.59, 'ReO4--' : 4.230, 'SO4--' : 3.15,
            'CO3--' : 2.810}

    if os.path.exists(os.path.join(os.getcwd(), 'output')) == False:
            os.makedirs(os.path.join(os.getcwd(), 'output'))

    periodic_table = open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                       'PeriodicTableJSON.json'), encoding='utf8')
    data = json.load(periodic_table)
    Element = {data['elements'][x]['symbol'] : pd.DataFrame([data['elements'][x]['name'],
                                                             data['elements'][x]['atomic_mass']],
                                                            index = ['name', 'mass']).T
               for x in range(len(data['elements']))}
    periodic_table.close()

    missing_species = []
    elemspeclist = [ symbol for x in specielist[0] for symbol, item in Element.items()
                    if item.name[0][:5] == x[:5] ] if sourceformat == 'GWB' else specielist[0]

    form_del = [1] if sourceformat == 'GWB' else [1, 3, 4]
    all_species_source = [[i]+k for i, k in sourcedic.items() if i not in (['eh', 'e-', 'H2O']) ]
    all_species_source = [[k for j, k in enumerate(all_species_source[i])
                           if (j not in form_del and k not in elemspeclist and str(k).strip('0123456789.- ') != '') ]
                          if  (i <= len(specielist[0]))
                          else [k for j, k in enumerate(all_species_source[i])
                                if (j not in form_del and str(k).strip('0123456789.- ') != '') ]
                          for i in range(len(all_species_source)) ]
    for num in range(len(all_species_source)): #
        if num < len(all_species_source):
            lst = [v for v in all_species_source[num] if v not in (['eh', 'e-', 'H2O']) ]

            bool_miss = [x.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
                         not in dbacessdic.keys() for x in lst  ]
            if any(bool_miss):
                sublist = [i for (i, v) in zip(lst, bool_miss) if v ]
                if lst[0] not in sublist:
                    missing_species.append([lst[0]] + sublist)
                else:
                    missing_species.append(sublist)

    missingfile = open(os.path.join(os.path.abspath("."),'output', 'spxNotFound.txt'), 'w')
    for line in missing_species:
        if len(line) > 0:
            missingfile.writelines(line[0])
            missingfile.writelines('\n')
            for i in range(len(line)):
                missingfile.writelines('   %s' % line[i])
                missingfile.writelines('\n')
    missingfile.close()
    missing_species = [item for sublist in missing_species for item in sublist]
    missing_species = [i for n, i in enumerate(missing_species) if i not in missing_species[:n]]

    if kwargs['objdb'] == None:
        objdb = './thermo_ToughReact%sbars' % int(P[0])
    else:
        objdb = kwargs['objdb']
    # timestr = '.' + time.strftime("%d%b%Y_%H%M")

    fout = open(os.path.join(os.path.abspath("."),'output', objdb + '.dat'), 'w+') # + timestr

    [rho, dGH2O, dHH2O, SH2O, _, P, T] = iapws95(T = T, P = P)
    #%% Calculation for debye huckel and bdot and water properties
    if kwargs['Dielec_method'] is not None:
        if kwargs['Dielec_method'] == 'FE97':
            Dielec_method = 'FE97'
            E = dielec_FE97(T, P)[0]
        elif kwargs['Dielec_method'] == 'JN91':
            Dielec_method = 'JN91'
            E = dielec_JN91(T, P)[0]
    else:
        # default method: 'JN91'
        Dielec_method = 'JN91'
        E = dielec_JN91(T, P)[0]

    rhoEG = {'rho': rho, 'E': E,  'dGH2O': dGH2O}

    # Calculate the rho E G for density extrapolation method here so we have it below
    rhoEGextrap = {}
    if any(rhoEG['rho'] < 350):
        subBornptrs = rhoEG['rho'] < 350
        for i, j in enumerate(zip(T[subBornptrs], P[subBornptrs])):
            rhoextrap = np.linspace(350, 550, 3)
            Pextrap = iapws95(T = j[0], rho = rhoextrap)[0]
            Textrap = j[0]*np.ones(np.size(Pextrap))

            dGH2O = iapws95(T = Textrap, P = Pextrap)[1]
            if Dielec_method is not None:
                if Dielec_method == 'FE97':
                    E = dielec_FE97(Textrap, rhoextrap)[0]
                elif Dielec_method == 'JN91':
                    E = dielec_JN91(Textrap, Pextrap)[0]
            else:
                # default method: 'JN91'
                E = dielec_JN91(Textrap, Pextrap)[0]
            rhoextrap = np.around(rhoextrap, 3)
            rhoEGextrap['%d_%d' % (j[0], j[1])]= {'rho': rhoextrap,'E': E, 'dGH2O': dGH2O,
                                                   'Textrap': Textrap, 'Pextrap': Pextrap}

    fout.writelines('The file format of this thermodynamic database is suitable for TOUGHREACT\n')
    fout.writelines('Generated by pyGeochemCalc.2021, '  + time.ctime() + '\n')
    fout.writelines('\n!end-of-header     Do not remove this record!\n')

    fout.writelines("'temperature points'  %10s" % len(T))
    for i in range(len(T)):
        fout.writelines( " %6.1f" %  T[i])
    fout.writelines( "\n")

    logKfunc = lambda TK, *x: x[0]*np.log(TK) + x[1] + x[2]*TK + x[3]*TK**(-1) + x[4]*TK**(-2)
    TK = celsiusToKelvin(T)
    x0 = [-3.19605e1, 2.06576e2, 3.73497e-2, -9.01862e3, 6.0111e5]

    #%% Basis reactions
    for j in specielist[1]:
        if j not in missing_species:
            if any([MWdic[j]]):
                MW = MWdic[j]
            else:
                formula = j if sourcedic[j][0] == '' else sourcedic[j][0]
                formula = formula.rstrip('(aq)(g)')
                MW =  calc_elem_count_molewt(formula, Elementdic = Element)[-1]
            name = j.replace('++++', '+4') if j.endswith('++++',0) else j.replace('+++', '+3') if j.endswith('+++',0) else j.replace('++', '+2') if j.endswith('++',0) else j.replace('----', '-4') if j.endswith('----',0) else j.replace('---', '-3') if j.endswith('---',0) else j.replace('--', '-2') if j.endswith('--',0) else j
            if sourceformat == 'GWB':
                charge = float(chargedic[j].lstrip().split()[1])
            elif sourceformat == 'EQ36':
                charge = float(chargedic[j].split()[-1])
            # ToughReact implementation
            if j in rej.keys():
                ionrad = rej[j]
            elif charge == -1 :
                ionrad = rej['Cl-']
            elif charge == -2 :
                ionrad = round(np.mean([rej['SO4--'], rej['CO3--']]))
            elif charge <= -3 :
                ionrad = charge*4.2/3
            elif charge == 1 :
                ionrad = rej['NH4+']
            elif charge == 2 :
                ionrad = np.mean([rej[j] for j in rej.keys()
                                  if j.endswith('++',0) and not j.endswith('+++',0)])
            elif charge == 3 :
                ionrad = np.mean([ rej[j] for j in rej.keys() if j.endswith('+++',0)])
            elif charge == 4 :
                ionrad = 4.5
            elif charge > 3 :
                ionrad = charge*4.5/4
            else:
                ionrad = 0

            info = "%-31s" % name + '%5.2f' % ionrad + ' %5.2f' % charge + '   %8.3f' % MW
            info = "'%s'" % info[:len(info.split()[0])] + info[len(info.split()[0]) + 2:]
            fout.writelines('%s\n' % info)
        else:
            continue
    fout.write("'null'   0.  0.\n")

    #%% Redox and Aqueous reactions
    for j in specielist[2] + specielist[3]:
        if sourceformat == 'GWB':
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]]
            charge = float(chargedic[j].lstrip().split()[1])
            source_rxns = sourcedic[j][2:]
        elif sourceformat == 'EQ36':
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1, 2, 3]]
            charge = float(chargedic[j].split()[-1])
            source_rxns = sourcedic[j][4:]
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ]
        if (j not in missing_species) and (len([i for i in rxnlst if i not in missing_species])==len(rxnlst)):
            name = j.replace('++++', '+4') if j.endswith('++++',0) else j.replace('+++', '+3') if j.endswith('+++',0) else j.replace('++', '+2') if j.endswith('++',0) else j.replace('----', '-4') if j.endswith('----',0) else j.replace('---', '-3') if j.endswith('---',0) else j.replace('--', '-2') if j.endswith('--',0) else j
            species = sourcedic[j][1]
            if any([MWdic[j]]):
                MW = MWdic[j]
            else:
                formula = j if sourcedic[j][0] == '' else sourcedic[j][0]
                formula = formula.rstrip('(aq)(g)')
                MW =  calc_elem_count_molewt(formula, Elementdic = Element)[-1]
            # ToughReact implementation
            if j in rej.keys():
                ionrad = rej[j]
            elif charge == -1 :
                ionrad = rej['Cl-']
            elif charge == -2 :
                ionrad = round(np.mean([rej['SO4--'], rej['CO3--']]))
            elif charge <= -3 :
                ionrad = charge*4.2/3
            elif charge == 1 :
                ionrad = rej['NH4+']
            elif charge == 2 :
                ionrad = np.mean([rej[j] for j in rej.keys()
                                  if j.endswith('++',0) and not j.endswith('+++',0)])
            elif charge == 3 :
                ionrad = np.mean([ rej[j] for j in rej.keys() if j.endswith('+++',0)])
            elif charge == 4 :
                ionrad = 4.5
            elif charge > 3 :
                ionrad = charge*4.5/4
            else:
                ionrad = 0
            Rxn = [row if i%2==0 else "'%s'" % (row.replace('++++', '+4') if row.endswith('++++',0)
                                                else row.replace('+++', '+3') if row.endswith('+++',0)
                                                else row.replace('++', '+2') if row.endswith('++',0)
                                                else row.replace('----', '-4') if row.endswith('----',0)
                                                else row.replace('---', '-3') if row.endswith('---',0)
                                                else row.replace('--', '-2') if row.endswith('--',0)
                                                else row) for i,row in enumerate(source_rxns)]
            Rxn = '  '.join(Rxn)
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                                Dielec_method = Dielec_method, sourceformat = sourceformat, **rhoEG)[0]
            if any(np.isnan(logK)):
                logKnan_alert = True
            else:
                logKnan_alert = False
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j, sourcedic = sourcedic,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      specielist = specielist, Dielec_method = Dielec_method,
                                                      sourceformat = sourceformat)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            logKcorr = curve_fit(logKfunc, TK[logK != 500].ravel(), logK[logK != 500].ravel(), p0 = x0,  maxfev = 1000000)[0]
            list_logk = '  '.join(str("%9.4f" % e) for e in list(logK))
            list_logKcorr = ' '.join(str("%.5e" % e) for e in list(logKcorr))

            info = "%-32s" % name + '  %7.3f' % MW + '  %3.1f' % ionrad + ' %5.2f' % charge +\
                '  ' + str(species) + ' ' + Rxn
            info = "'%s'" % info[:len(info.split()[0])] + info[len(info.split()[0]) + 2:]
            fout.writelines('%s\n' % info)

            info = '%-35s' % name + ' ' + list_logk
            info = "'%s'" % info[:len(info.split()[0])] + info[len(info.split()[0]) + 2:]
            fout.writelines('%s\n' % info)

            info = '%-35s' % name + '  ' + list_logKcorr.replace('e','E')
            info = "'%s'" % info[:len(info.split()[0])] + info[len(info.split()[0]) + 2:]
            fout.writelines('%s\n' % info)

        else:
            continue
    fout.writelines("'null'   0. 0. 0. 0\n")
    if logKnan_alert == True:
        warnings.warn('Some temperature and pressure points are out of aqueous species HKF eqns regions of applicability, hence, density extrapolation has been applied')

    #%% Mineral reactions
    solid_solution = 'no' if kwargs['solid_solution'] is None else kwargs['solid_solution']
    clay_thermo = 'no' if kwargs['clay_thermo'] is None else kwargs['clay_thermo']

    if kwargs['nCa_cpx'] == 0:
        nCa = 0
    else:
        nCa = kwargs['nCa_cpx']
    if kwargs['solid_solution'].lower() == 'yes':
        solidsolution_no = 11
        for XAn in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKAnAb(XAn, T, P, dbacessdic, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'AnAb', X = XAn,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, TK, dataset = 'ToughReact')

        for XFo in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKFoFa(XFo, T, P, dbacessdic, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'FoFa', X = XFo,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, TK, dataset = 'ToughReact')

        for XEn in np.round(np.linspace(1, 0, solidsolution_no), 1):
            logK, Rxn = calclogKEnFe(XEn, T, P, dbacessdic, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'EnFe', X = XEn,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, TK, dataset = 'ToughReact')

        if nCa > 0:
            for XMg in np.round(np.linspace(1, 0, solidsolution_no), 1):
                logK, Rxn = calclogKDiHedEnFe(nCa, XMg, T, P, dbacessdic, **rhoEG)
                # Utilized for density extrapolation of logK
                if any(rhoEG['rho'] < 350):
                    logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'DiHedEnFe',
                                                          dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                          X = XMg, Ca = nCa,
                                                          Dielec_method = Dielec_method)
                logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
                outputfmt(fout, logK, Rxn, TK, dataset = 'ToughReact')

    # clay minerals
    if clay_thermo.lower() == 'yes':
        fclay = open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'clay_elements.dat'), 'r')
        Rd = fclay.readlines()
        Rd = [j.replace('-','_').strip('\n') for j in Rd]
        for i in range(len(Rd)):
            if Rd[i].split(',')[0] in ['Berthierine_FeII', 'Berthierine_FeIII', 'Lizardite',
                                        'Cronstedtite', 'Mg-Cronstedtite', 'Greenalite', 'Hisingerite']:
                layering = '7A'
            elif Rd[i].split(',')[0] in ['Clinochlore', 'Chamosite', 'Amesite']:
                layering = '14A'
            else:
                layering = '10A'
            logK, Rxn = calclogKclays(T, P, dbacessdic, *Rd[i].split(','), group = layering, **rhoEG)
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], 'Clay',
                                                      *Rd[i].split(','), dbacessdic = dbacessdic,
                                                      rhoEGextrap = rhoEGextrap, group = layering,
                                                      Dielec_method = Dielec_method)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            outputfmt(fout, logK, Rxn, TK, dataset = 'ToughReact')

    if sourceformat == 'GWB':
        speclst = specielist[5]
    elif sourceformat == 'EQ36':
        speclst = specielist[4]+specielist[5]
    for j in speclst:
        if sourceformat == 'GWB':
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]]
            source_rxns = sourcedic[j][2:]
        elif sourceformat == 'EQ36':
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1, 2, 3]]
            source_rxns = sourcedic[j][4:]
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ]
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            if solid_solution.lower() == 'yes' and j in ['Anorthite', 'Albite', 'Forsterite',
                                                'Fayalite', 'Enstatite', 'Ferrosilite']:
                continue
            elif solid_solution.lower() == 'yes' and (nCa == 1) and j in ['Diopside', 'Hedenbergite']:
                continue
            elif clay_thermo.lower() == 'yes' and j in [ Rd[h].split(',')[0] for h in range(len(Rd))]:
                continue
            else:
                name = j
                k = j.replace('(CH3COO)', '(Ac)').replace('CH3COO', '(Ac)')
                MV, species = dbacessdic[k][5], sourcedic[j][1]
                if any([MWdic[j]]):
                    MW = MWdic[j]
                else:
                    formula = j if sourcedic[j][0] == '' else sourcedic[j][0]
                    formula = formula.rstrip('(aq)(g)(am)')
                    MW =  calc_elem_count_molewt(formula, Elementdic = Element)[-1]
                Rxn = [row if i%2 == 0 else "'%s'" % (row.replace('++++', '+4') if row.endswith('++++',0)
                                                else row.replace('+++', '+3') if row.endswith('+++',0)
                                                else row.replace('++', '+2') if row.endswith('++',0)
                                                else row.replace('----', '-4') if row.endswith('----',0)
                                                else row.replace('---', '-3') if row.endswith('---',0)
                                                else row.replace('--', '-2') if row.endswith('--',0)
                                                else row) for i, row in enumerate(source_rxns)]
                Rxn = ' '.join(Rxn)
                logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                                    Dielec_method = Dielec_method, sourceformat = sourceformat, **rhoEG)[0]
                # Utilized for density extrapolation of logK
                if any(rhoEG['rho'] < 350):
                    logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j, sourcedic = sourcedic,
                                                          dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                          specielist = specielist, Dielec_method = Dielec_method,
                                                          sourceformat = sourceformat)
                logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
                logKcorr = curve_fit(logKfunc, TK[logK!=500].ravel(), logK[logK!=500].ravel(), p0 = x0,  maxfev = 1000000)[0]
                list_logk = '  '.join(str("%9.4f" % e) for e in list(logK))
                list_logKcorr = ' '.join(str("%.5e" % e) for e in list(logKcorr))

                info = "%-32s" % name + "%8.3f" % MW + " %7.2f" % MV +\
                    ' ' + str(species) + ' ' + Rxn
                info = "'%s'" % info[:len(info.split()[0])] + info[len(info.split()[0]) + 2:]
                fout.writelines('%s\n' % info)

                info = '%-35s' % name + ' ' + list_logk
                info = "'%s'" % info[:len(info.split()[0])] + info[len(info.split()[0]) + 2:]
                fout.writelines('%s\n' % info)

                info = '%-35s' % name + '  ' + list_logKcorr.replace('e','E')
                info = "'%s'" % info[:len(info.split()[0])] + info[len(info.split()[0]) + 2:]
                fout.writelines('%s\n' % info)
        else:
            continue
    fout.write("'null'   0.  0. 0\n")

    #%% Gas reactions
    for j in specielist[6]:
        if sourceformat == 'GWB':
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1]]
            source_rxns = sourcedic[j][2:]
        elif sourceformat == 'EQ36':
            rxnlst = [b for a, b in enumerate(sourcedic[j]) if a not in [0, 1, 2, 3]]
            source_rxns = sourcedic[j][4:]
        rxnlst = [v for x, v in enumerate(rxnlst) if x % 2 != 0 ]
        if (j not in missing_species) and (len([k for k in rxnlst if k not in missing_species])==len(rxnlst)):
            name, species = j, sourcedic[j][1]
            if any([MWdic[j]]):
                MW = MWdic[j]
            else:
                formula = j if sourcedic[j][0] == '' else sourcedic[j][0]
                formula = formula.rstrip('(aq)(g)')
                MW =  calc_elem_count_molewt(formula, Elementdic = Element)[-1]
            Rxn = [row if i%2 == 0 else "'%s'" %(row.replace('++++', '+4') if row.endswith('++++',0)
                                                else row.replace('+++', '+3') if row.endswith('+++',0)
                                                else row.replace('++', '+2') if row.endswith('++',0)
                                                else row.replace('----', '-4') if row.endswith('----',0)
                                                else row.replace('---', '-3') if row.endswith('---',0)
                                                else row.replace('--', '-2') if row.endswith('--',0)
                                                else row) for i,row in enumerate(source_rxns)]
            Rxn = ' '.join(Rxn)
            logK = calcRxnlogK( T, P, j, dbacessdic, sourcedic, specielist,
                                Dielec_method = Dielec_method, sourceformat = sourceformat, **rhoEG)[0]
            # Utilized for density extrapolation of logK
            if any(rhoEG['rho'] < 350):
                logK[subBornptrs] = densitylogKextrap(T[subBornptrs], P[subBornptrs], j, sourcedic = sourcedic,
                                                      dbacessdic = dbacessdic, rhoEGextrap = rhoEGextrap,
                                                      specielist = specielist, Dielec_method = Dielec_method,
                                                      sourceformat = sourceformat)
            logK = np.where(np.isnan(logK), 500, logK) # set abitrary 500 to nan values
            logKcorr = curve_fit(logKfunc, TK[logK!=500].ravel(), logK[logK!=500].ravel(), p0 = x0,  maxfev = 1000000)[0]
            list_logk = '  '.join(str("%9.4f" % e) for e in list(logK))
            list_logKcorr = ' '.join(str("%.5e" % e) for e in list(logKcorr))

            info = "%-32s" % name + "%8.3f" % MW + ' ' + '0.100E-09' +\
                ' ' + str(species) + ' ' + Rxn
            info = "'%s'" % info[:len(info.split()[0])] + info[len(info.split()[0]) + 2:]
            fout.writelines('%s\n' % info)

            info = '%-35s' % name + ' ' + list_logk
            info = "'%s'" % info[:len(info.split()[0])] + info[len(info.split()[0]) + 2:]
            fout.writelines('%s\n' % info)

            info = '%-35s' % name + '  ' + list_logKcorr.replace('e','E')
            info = "'%s'" % info[:len(info.split()[0])] + info[len(info.split()[0]) + 2:]
            fout.writelines('%s\n' % info)
        else:
            continue

    fout.write("'null'   0.  0. 0\n")
    fout.write("'null'  0.  0             ! surface complex\n")
    fout.write("'null'  0.  0\n")

    #%% close all files
    fout.close()
    if clay_thermo.lower() == 'yes':
        fclay.close()

    return print('Success, your new ToughReact database is ready for download')

def write_database(T, P, **kwargs ):
    """
    This function writes the new database for either GWB, EQ3/6, ToughReact, Pflotran into
    a new folder called "output"   \n
    Parameters
    ----------
        T               :    temperature [°C]   \n
        P               :    pressure [bar]   \n
        nCa_cpx         :    number of moles of Ca in solid solution of clinopyroxene (optional)
                                if it is ommitted solid solution of clinopyroxene will not
                                be included, 0 < nCa >=1   \n
        solid_solution  :    bool to specify the inclusion of solid-solution [Yes or No],
                                default is 'No'   \n
        clay_thermo     :    specify the inclusion of clay thermodynamic properties [Yes or No],
                                default is 'No'   \n
        logK_form       :    specify the format of logK either as a set of eight values one for each
                                of the dataset’s principal temperatures, or blocks of polynomial coefficients, [values, polycoeffs]
                                default is 'a set of eight values'   \n
        dbaccess        :    direct-access database filename and location  (optional)  \n
        sourcedb        :    source database filename and location  (optional)  \n
        objdb           :    new database filename and location    (optional) \n
        co2actmodel     :    co2 activity model equation [Duan_Sun or Drummond]  (optional),
                                if not specified, default is 'Drummond'   \n
        Dielec_method   :    specify either 'FE97' or 'JN91' as the method to calculate
                                dielectric constant, default is 'JN91'   (optional) \n
        dataset         :    specify the dataset format, either 'GWB', 'EQ36', 'Pflotran' or 'ToughReact',
                                default is old GWB database ['GWB'] (optional) \n
        sourceformat    :    source database format, either 'GWB' or 'EQ36', default is 'GWB'
    Returns
    -------
        Outputs the new database to an ASCII file with filename described in 'objdb' if specified.   \n
    Usage
    -------
      Example:
          (1) General format with default dielectric constant and CO2 activity model and exclusions
              of solid solutions for GWB   \n
              write_database(T, P, nCa, dataset = 'GWB')   \n
          (2) Inclusion of solid solutions and exclusion of solid solution of clinopyroxene and clay thermo  \n
              write_database(T, P, solid_solution = 'Yes', clay_thermo = 'Yes', dataset = 'GWB')   \n
          (3) Inclusion of all solid solutions and clay thermo with \emph{'Duan_Sun'} CO2 activity model and
              'FE97' dielectric constant calculation \n
              write_database(T, P, nCa, solid_solution = 'Yes', clay_thermo = 'Yes', co2actmodel = 'Duan_Sun',
                            Dielec_method = 'FE97', dataset = 'GWB')   \n

    """

    kwargs = dict({"nCa_cpx": None,  "solid_solution": None,
                  "clay_thermo": None, "logK_form": None,
                  "dbaccess": None, "sourcedb": None,   "objdb": None,
                  "co2actmodel": None,   "Dielec_method": None,
                  "dataset": None,   "sourceformat": None
                  }, **kwargs)

    if kwargs["dbaccess"] == None:
        dbaccess = './default_db/speq21.dat'
    else:
        dbaccess = kwargs["dbaccess"]

    if type(P) == str:
        if P == 'T':
            if np.ndim(T) == 0:
                T = np.ravel(T)
            elif np.size(T) == 2:
                if T[-1] > 400:
                    # for critical region of water (350 - 400C)
                    T = np.array([0.01 if (x == 0)&(T[0] == 0)
                                  else round(0 + x*(T[-1] - T[0])/(8 - 1) - 55) if 350 <= (0 + x*(T[-1] - T[0])/(8 - 1)) <= 400
                                  else round(0 + x*(T[-1] - T[0])/(8 - 1)) if 350 <= roundup_tenth(0 + x*(T[-1] - T[0])/(8 - 1)) <= 400
                                  else roundup_tenth(0 + x*(T[-1] - T[0])/(8 - 1))
                                  for x in range(8)])
                else:
                    T = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(T[0], T[-1], 8)])
            P, _, _ = iapws95(T = T)
            P[np.isnan(P) | (P < 1)] = 1.0133
        elif P == 'P':
            P = T   # Assign first input T to pressure in bar
            T, _, _ = iapws95(P = P)
            T = T - 273.15 # convert from K to celcius

    if np.size(T) < np.size(P):
        T = T*np.ones_like(P)
    if np.size(P) < np.size(T):
        P = P*np.ones_like(T)

    if np.ndim(T) == 0:
        T = np.ravel(T)
    elif np.size(T) == 2:
        if T[-1] > 400:
            # for critical region of water (350 - 400C)
            T = np.array([0.01 if (x == 0)&(T[0] == 0)
                          else round(0 + x*(T[-1] - T[0])/(8 - 1) - 55) if 350 <= (0 + x*(T[-1] - T[0])/(8 - 1)) <= 400
                          else round(0 + x*(T[-1] - T[0])/(8 - 1)) if 350 <= roundup_tenth(0 + x*(T[-1] - T[0])/(8 - 1)) <= 400
                          else roundup_tenth(0 + x*(T[-1] - T[0])/(8 - 1))
                          for x in range(8)])
        else:
            T = np.array([roundup_tenth(j) if j != 0 else 0.01 for j in np.linspace(T[0], T[-1], 8)])

    if np.size(P) <= 2:
        P = P[0]*np.ones(np.size(T))

    nCa = 0 if kwargs["nCa_cpx"] is None else kwargs["nCa_cpx"]

    solid_solution = 'No' if kwargs["solid_solution"] == None else kwargs["solid_solution"]
    clay_thermo = 'No' if kwargs["clay_thermo"] == None else kwargs["clay_thermo"]

    if kwargs["dataset"].lower() == 'gwb':
        write_GWBdb(T, P, nCa_cpx = nCa, logK_form = kwargs["logK_form"], solid_solution = solid_solution,
                    clay_thermo = clay_thermo, dbaccess = dbaccess, sourcedb = kwargs["sourcedb"],
                    objdb = kwargs["objdb"], sourceformat = kwargs["sourceformat"],
                    co2actmodel = kwargs["co2actmodel"],  Dielec_method = kwargs["Dielec_method"] )
    elif kwargs["dataset"].lower() == 'eq36':
        write_EQ36db(T, P, nCa_cpx = nCa, solid_solution = solid_solution, clay_thermo = clay_thermo,
                      dbaccess = dbaccess, sourcedb = kwargs["sourcedb"], objdb = kwargs["objdb"],
                      co2actmodel = kwargs["co2actmodel"], Dielec_method = kwargs["Dielec_method"] )
    elif kwargs["dataset"].lower() == 'pflotran':
        write_pflotrandb(T, P, nCa_cpx = nCa, solid_solution = solid_solution, clay_thermo = clay_thermo,
                          dbaccess = dbaccess, sourcedb = kwargs["sourcedb"], objdb = kwargs["objdb"],
                          Dielec_method = kwargs["Dielec_method"], sourceformat = kwargs["sourceformat"] )
    elif kwargs["dataset"].lower() == 'toughreact':
        write_ToughReactdb(T, P, nCa_cpx = nCa, solid_solution = solid_solution, clay_thermo = clay_thermo,
                            dbaccess = dbaccess, sourcedb = kwargs["sourcedb"], objdb = kwargs["objdb"],
                            Dielec_method = kwargs["Dielec_method"], sourceformat = kwargs["sourceformat"] )

    return

def main_function_name(module):
    """
     print main_function name
    """
    functname = []
    for i in dir(module):
        if not i.startswith(('Delta', 'Psi', 'phir', 'phi0', '__')):
            if i not in ['IAPWS95_COEFFS', 'var_name', 'J_to_cal', 'splev', 'splrep',
                         'MW', 'curve_fit', 'eps', 'feval', 'warnings', 'inspect',
                         'lu_factor', 'lu_solve', 'math', 'Rbf', 'newton',
                         'np', 'os', 'textwrap', 'theta', 'time', 'read_specific_lines',
                         'fsolve', 'main_function_name', 'warnings',
                         're', 'timer', 'root_scalar', 'functools']:
                functname.append(i)
    return functname

# pipreqs --encoding=utf8 --debug . # creates requirements.txt